

SQL2033N  An error occurred while accessing TSM during the processing of
      a database utility. TSM reason code: "<reason-code>".

Explanation: 

Some common TSM reason codes are:

106      

         The specified file is being used by another process. You tried
         to read from or write to a file that is currently being used by
         another process.


137      

         TSM authentication failure.


168      

         Password file is needed, but user is not root. This message is
         often generated when the DSMI_DIR environment variable points
         to a directory that contains a 32-bit version of the dsmtca
         program, yet the DB2 instance is 64-bit, or vice-versa.


400      

         An invalid option was specified on the OPTIONS parameter passed
         to TSM.


406      

         TSM Cannot find or read its options file.

The utility stops processing.

User response: 

Responses for common TSM problems are:

106      

         Ensure that you specified the correct file or directory name,
         correct the permissions, or specify a new location.


137      

         If the TSM parameter PASSWORDACCESS is set to GENERATE, ensure
         that the system administrator has used the dsmapipw utility to
         set the password. If PASSWORDACCESS is set to PROMPT, ensure
         that the TSM_NODENAME and TSM_PASSWORD database configuration
         parameters have been set correctly.


168      

         Ensure that DSMI_DIR points to a directory containing the
         correct version of dsmtca, restart the instance, and execute
         the command again.


400      

         Ensure that the OPTIONS parameter specified on the BACKUP or
         RESTORE command is correct.


406      

         Ensure that the DSMI_CONFIG environment variable points to a
         valid TSM options file. Ensure that the instance owner has read
         access to the dsm.opt file. Ensure that the DSMI_CONFIG
         environment variable is set in the db2profile.
