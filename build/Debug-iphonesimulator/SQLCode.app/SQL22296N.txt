

SQL22296N  Unable to send mail using the SMTP protocol due to invalid
      recipient addresses.

Explanation: 

The SMTP server failed to successfully negotiate the send mail protocol
for all the specified recipients.

User response: 

Verify that the recipient addresses are specified correctly.

This error might also be returned if the SMTP server is unable to reach
the recipient addresses. This can be independently verified by using any
other mail client to send mail to the recipients using the same SMTP
server. If this is successful, invoke the Independent Trace Facility at
the operating system command prompt. Contact IBM Support if the problem
persists.
