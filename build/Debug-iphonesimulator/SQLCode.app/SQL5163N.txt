

SQL5163N  A required configuration parameter "<parameter>" is missing
      from the db2dsdriver.cfg configuration file.

Explanation: 

The db2dsdriver.cfg configuration file contains database information,
and is used by the following drivers and clients:

*  IBM Data Server Driver for ODBC and CLI
*  IBM Data Server Driver Package
*  For DB2 Version 9.7: for CLI and open source applications, the IBM
   Data Server Client and IBM Data Server Runtime Client

The information in the db2dsdriver.cfg file is similar to the
information that is in the system database directory on an IBM Data
Server Client or IBM Data Server Runtime Client.

The client driver configuration file must contain all required
parameters.

User response: 

1. Add the required configuration parameter to the db2dsdriver.cfg
   configuration file.
2. Stop the application process and start it again for the new
   db2dsdriver.cfg file settings to take effect.


   Related information:
   IBM data server driver configuration file
