

SQL5147N  "<Parameter_1>" cannot be set to MANUAL if "<Parameter_2>" is
      AUTOMATIC.

Explanation: 

If "<Parameter_2>" is AUTOMATIC then "<Parameter_1>" needs to be
AUTOMATIC as well.

User response: 

To set "<Parameter_1>" to another value, "<Parameter_2>" must be set to
a value other than AUTOMATIC first.
