

SQL0197N  A qualified column name is not allowed in the ORDER BY clause.

Explanation: 

The ORDER BY clause of a fullselect that includes a set operator (UNION,
EXCEPT, INTERSECT) cannot have qualified column names.

User response: 

Ensure that all column names in the ORDER BY clause are unqualified.

 sqlcode: -197

 sqlstate: 42877
