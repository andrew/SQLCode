

SQL1072C  The request failed because the database manager resources are
      in an inconsistent state. The database manager might have been
      incorrectly terminated, or another application might be using
      system resources in a way that conflicts with the use of system
      resources by the database manager.

Explanation: 

This message can be returned in multiple scenarios:

*  The database manager was incorrectly terminated. (On UNIX-based
   systems, for example, this situation might occur if the processes
   were terminated with the "kill" command, rather than the STOP
   DATABASE MANAGER command).
*  Another application or user might have removed the database manager
   resources. (On UNIX-based systems, for example, a user with
   sufficient privilege might have accidentally removed an Interprocess
   Communication (IPC) resource owned by the database manager, using the
   "ipcrm" command).
*  The use of system resources by another application conflicts with the
   use of system resources by the database manager. (On UNIX-based
   systems, for example, another application might be using the same
   keys that the database manager uses for creating IPC resources).
*  Another instance of the database manager might be using the same
   resources. This situation could occur on UNIX-based systems if the
   two instances are on different file systems and the sqllib
   directories happen to use conflicting IPC keys.

User response: 

Review the db2diag diagnostic log files for more detailed diagnostic
information.

Perform one or more of the following troubleshooting and resource
cleanup steps, and then resubmit the request that failed:

*  Remove all the database manager processes running under the instance
   ID. 

   For example, on UNIX-based systems, perform the following steps:

    
   1. List all the database manager processes running under the instance
      ID by issuing the db2_ps command.
   2. Remove those processes by issuing the following command: 
      kill -9 <process-ID>

*  Ensure that no other application is running under the instance ID,
   and then remove all resources owned by the instance ID. 

   For example, on UNIX-based systems, perform the following steps:

    
   1. List all IPC resources owned by the instance ID by issuing the
      following command: 
      ipcs | grep <instance-ID>

   2. Remove those resources by issuing the following command: 
      ipcrm -[q|m|s] <instance-ID>

*  If you find that two instances can be started individually but cannot
   be active at the same time, generate new IPC keys for one of those
   instances by performing the following steps as the instance owner: 

   For single database partition instances:
            
            1. Delete the sqllib/.ftok file.
            2. Run the sqllib/bin/db2ftok command to generate a new
               .ftok file.


   For multiple database partition instances:
            
            1. Create another directory at the same level as sqllib.
            2. Move everything from under sqllib to the new directory.
            3. Delete sqllib.
            4. Rename the new directory to sqllib.


   Related information:
   DB2 diagnostic (db2diag) log files
