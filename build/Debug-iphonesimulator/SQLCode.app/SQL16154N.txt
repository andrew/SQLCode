

SQL16154N  XML schema contains an element "<element-name>" that has more
      than one attribute defined with the ID property.

Explanation: 

While parsing an XML schema (or DTD), two or more attributes with the ID
property were declared for the element named "<element-name>".

Parsing or validation did not complete.

User response: 

Correct the XML schema or DTD and try the operation again.

sqlcode: -16154

sqlstate: 2200M
