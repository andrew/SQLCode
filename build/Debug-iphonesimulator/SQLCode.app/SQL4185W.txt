

SQL4185W  There is a data type or length mismatch between column
      "<column>" and the INSERT or UPDATE item.

Explanation: 

One of the following conditions has not been met: 
*  If the data type of the column name is character string, then the
   corresponding item in the INSERT or UPDATE statement should be
   character string of length equal to or less than the length of the
   column name.
*  If the data type of the column name is exact numeric, then the
   corresponding item in the INSERT or UPDATE statement should be exact
   numeric.
*  If the data type of the column name is approximate numeric, then the
   corresponding item in the INSERT or UPDATE statement should be
   approximate numeric or exact numeric.

Processing continues.

User response: 

Correct the SQL statement.
