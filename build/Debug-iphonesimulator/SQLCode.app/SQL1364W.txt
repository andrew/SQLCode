

SQL1364W  One or more of the parameters were set to AUTOMATIC in a case
      where the parameter does not support AUTOMATIC.

Explanation: 

One or more of the configuration parameters were set to AUTOMATIC in a
case where the parameter does not support AUTOMATIC.

User response: 

If the parameter changes were submitted as a group, resubmit the changes
individually to see which parameter changes were successful.

If only one parameter was submitted then this message indicates that the
value AUTOMATIC is not supported for this parameter.

To find out which configuration parameters support the AUTOMATIC value,
refer to the Administration Guide.
