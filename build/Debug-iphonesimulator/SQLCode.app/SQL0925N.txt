

SQL0925N  SQL COMMIT invalid for application execution environment.

Explanation: 

COMMIT is disallowed in the following cases: 
*  In a Distributed Transaction Processing environment such as CICS, a
   static SQL COMMIT statement was attempted, but a commit statement
   specific to the environment is required. For example, in a CICS
   environment this would be the CICS SYNCPOINT command.
*  A DB2 application precompiled or set to use CONNECT 2 in a non-TP
   Monitor environment has issued a dynamic SQL COMMIT statement,
   whereas only static SQL COMMITs are allowed.
*  When issued from a stored procedure, SQL COMMIT is also not allowed
   if the calling program is executing in a distributed unit of work or
   Distributed Transaction Processing environment.

User response: 

Correct the problem by doing one of the following: 
*  Remove the statement issuing the COMMIT and replace it with a
   statement which does the valid equivalent for the environment
*  In the case of a connect type 2 in a non-TP Monitor environment, use
   only static COMMIT.
*  In the case of a stored procedure, remove the COMMIT

 sqlcode: -925

 sqlstate: 2D521
