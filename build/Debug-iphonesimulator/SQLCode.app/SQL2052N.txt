

SQL2052N  The backup operation failed because the backup utility was
      unable to collect the required information and metadata for one or
      more DB2 members.

Explanation: 

In a DB2 pureScale environment, when you perform a backup operation from
a DB2 member, the backup utility must collect recovery metadata for all
other DB2 members in the instance. This message is returned when the
backup utility cannot collect the metadata information for one or more
members. The backup utility might be unable to carry out the
serialization that is necessary to process the metadata.

User response: 

1. Check the db2diag log files for any communication or I/O errors that
   are associated with the backup agent or the logger EDU. Correct the
   errors and run the backup operation again.
2. If the backup operation continues to fail, perform the following
   steps: 
   a. Resolve the reason for which the backup utility was unable to
      collect metadata information.
   b. Run the backup operation again.

sqlcode: -2052

sqlstate: 5U055


   Related information:
   Backup and restore operations in a DB2 pureScale environment
