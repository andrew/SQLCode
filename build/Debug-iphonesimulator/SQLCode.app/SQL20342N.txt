

SQL20342N  "<auth-ID>" does not have one or more required privileges
      "<privilege-list>" on object "<object-name>" of type
      "<object-type>" necessary for ownership of the object.

Explanation: 

The TRANSFER statement attempted to transfer ownership of the object to
authorization ID "<auth-ID>" which does not have the necessary
privileges to be the owner of the object. The privileges
"<privilege-list>" on the object "<object-name>" are the privileges that
are missing.

The statement cannot be processed.

User response: 

Grant the authorization ID "<auth-ID>" all the privileges necessary, as
indicated by "<privilege-list>", on object "<object-name>", for the
authorization ID to be the owner of the object being transferred.

sqlcode: -20342

sqlstate: 42514
