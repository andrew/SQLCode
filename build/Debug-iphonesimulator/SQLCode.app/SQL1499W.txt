

SQL1499W  Database upgrade was successful; however, additional user
      action may be required. See the administration notification log
      for more details.

Explanation: 

Database upgrade was successful; however, additional user action may be
required because one or more of the following conditions were detected:

*  Database upgrade altered NOT FENCED routines to FENCED and NOT
   THREADSAFE or altered user defined wrappers to FENCED on UNIX and
   Linux operating systems.
*  Database upgrade could not successfully collect statistics on the
   system catalog tables.
*  Database upgrade detected identifiers called NULL.
*  Database upgrade marked indexes for rebuild on one or more tables.
*  Database upgrade detected asterisks in workload connection
   attributes.
*  Database upgrade detected databases enabled for XML Extender.
*  Database upgrade detected databases enabled for DB2 WebSphere MQ
   functions.

User response: 

Refer to the administration notification log for additional information
on the conditions detected to help you determine the action that might
be required.
