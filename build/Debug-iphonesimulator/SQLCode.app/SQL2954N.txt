

SQL2954N  The ingest operation failed because more than the allowable
      number of field definitions were specified. Allowable number of
      field definitions: "<maximum-number>".

Explanation: 

You can stream data from files and pipes into DB2 database tables by
using the ingest utility. You can specify a list of field definitions
for the ingested data, but there is an upper limit to the number of
field definitions that you can specify. The maximum allowable number of
field definitions is given in the runtime token "<maximum-number>".

This message is returned when an attempt is made to specify more field
definitions than the ingest utility can support.

User response: 

Perform the ingest operation again, specifying no more than the given
maximum allowable number of field definitions.


   Related information:
   db2Ingest API- Ingest data from an input file or pipe into a DB2
   table.
   INGEST command
   Ingest utility restrictions and limitations
   Comparison between the ingest, import, and load utilities
