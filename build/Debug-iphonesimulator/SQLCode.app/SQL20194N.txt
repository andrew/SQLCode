

SQL20194N  Buffer pool "<bufferpool-name>" does not exist on database
      partition "<dbpartitionnum>".

Explanation: 

The ALTER BUFFERPOOL statement is specifying a buffer pool,
"<bufferpool-name>", that does not exist on the database partition
"<dbpartitionnum>".

User response: 

Using the ALTER DATABASE PARTITION GROUP statement, add the database
partition "<dbpartitionnum>" to a database partition group that has the
buffer pool "<bufferpool-name>" already defined for it. If the buffer
pool is not associated with any specific database partition group, then
add the database partition to any database partition group or create a
new database partition group for this database partition. Issue the
ALTER BUFFERPOOL statement again.

sqlcode: -20194

sqlstate: 53040
