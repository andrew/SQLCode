

SQL1370N  An attempt to quiesce instance or database "<name1>" failed
      because instance or database "<name2>" is already quiesced by user
      "<username>". Quiesce type: "<type>".

Explanation: 

An attempt was made to quiesce an instance or database which would have
caused an overlapping quiesce, such as quiescing an instance while a
database is already quiesced by another user.

Quiesce type "<type>" refers to the instance or database already
quiesced and is a '1' for an instance and a '2' for a database.

User response: 

Contact the user who currently has the instance or database quiesced to
determine when DB2 will no longer be quiesced, and retry the request at
that time.
