

SQL2807N  Node "<node>" already exists for instance "<instance>".

Explanation: 

DB2NCRT failed because the node already exists.

User response: 

 Ensure the node number is correct and reissue the command.
