

SQL2550N  Database backed up on node "<node1>" cannot be restored to
      node "<node2>".

Explanation: 

The backup image used for the restore is a backup of a database from a
different node. You can only restore a backup to the same node.

User response: 

Ensure that you have the correct backup image for the node then issue
the request again.
