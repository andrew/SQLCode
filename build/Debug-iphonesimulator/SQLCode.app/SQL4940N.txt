

SQL4940N  The "<clause>" clause is not permitted or is required.

Explanation: 

The indicated clause is either not allowed in the context where it
appears in the SQL statement or it is required in the statement.

A subquery, an INSERT statement, or a CREATE VIEW statement cannot have
INTO, ORDER BY, or FOR UPDATE clauses. An embedded SELECT statement
cannot have ORDER BY or FOR UPDATE clauses. An embedded SELECT statement
cannot contain a set operator except in a subquery. SELECT statements
used in cursor declarations cannot have an INTO clause.

An embedded SELECT statement must have an INTO clause.

The function cannot be completed.

User response: 

Remove or add the clause to correct the statement.
