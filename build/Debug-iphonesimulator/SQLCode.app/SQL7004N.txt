

SQL7004N  The syntax of the request is invalid.

Explanation: 

REXX was unable to parse the command string submitted.

The command cannot be processed.

User response: 

Use proper command syntax.
