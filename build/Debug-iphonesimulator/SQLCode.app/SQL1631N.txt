

SQL1631N  Event monitor "<event-monitor-name>" of type
      "<event-monitor-type>" is already active. Event monitor was not
      activated.

Explanation: 

Only one event monitor of type ACTIVITIES, STATISTICS or THRESHOLD
VIOLATIONS may be active at any one time. Event monitor activation
failed because an event monitor of the same type is already active.

User response: 

Deactivate event monitor "<event-monitor-name>" before attempting to
activate this event monitor.

 sqlcode: -1631

 sqlstate: 5U024
