

SQL3144W  The length of fixed length column "<column-number>" exceeds
      the 240-byte limitation. Data from the column may be truncated.

Explanation: 

The Lotus 1-2-3** and Symphony** programs have a limit of 240 bytes for
label records. Whenever a character field longer than 240 bytes is
written to a worksheet format (WSF) file, the data will be truncated to
240 bytes.

All data entries for the column are truncated with no additional
messages written to the message log.

Continue processing.

User response: 

Verify output. If significant data from the column is lost because of
truncation, investigate selecting the data from the column in several
fields by substringing, or redesign the database.
