

SQL1074N  The address of the password parameter is not valid.

Explanation: 

The application program used an address that is not valid for this
parameter. Either the address points to an unallocated buffer or the
character string in the buffer does not have a null terminator.

The command cannot be processed.

User response: 

Ensure that a valid address is used in the application program and the
input string is null terminated.
