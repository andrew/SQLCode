

SQL16241N  XML document has "<tag>" content in a choice model group
      which is limited to 'element', 'group', 'choice', 'sequence', and
      'any'.

Explanation: 

While parsing an XML document the parser encountered invalid content. A
choice model group include the content "<tag>" that is not one of
'element', 'group', 'choice', 'sequence', or 'any'.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16241

sqlstate: 2200M
