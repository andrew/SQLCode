

SQL0511N  The FOR UPDATE clause is not allowed because the table
      specified by the cursor cannot be modified.

Explanation: 

The result table of the SELECT or VALUES statement cannot be updated.

On the database manager, the result table is read-only if the cursor is
based on a VALUES statement or the SELECT statement contains any of the
following:

*  The DISTINCT keyword
*  A column function in the SELECT list
*  A GROUP BY or HAVING clause
*  A FROM clause that identifies one of the following: 
   *  More than one table or view
   *  A read-only view
   *  An OUTER clause with a typed table or typed view
   *  A data change statement

*  A set operator (other than UNION ALL).

Note that these conditions do not apply to subqueries of the SELECT
statement.

The statement cannot be processed.

User response: 

Do not perform updates on the result table as specified.

Federated system users: isolate the problem to the data source failing
the request.

If a data source is failing the request, examine the restrictions for
that data source to determine the cause of the problem and its solution.

If the restriction exists on a data source, see the SQL reference manual
for that data source to determine why the object is not updatable.

sqlcode: -511

sqlstate: 42829


   Related information:
   Troubleshooting data source connection errors
