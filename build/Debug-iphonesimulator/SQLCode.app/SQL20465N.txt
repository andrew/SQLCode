

SQL20465N  The binary XML value is incomplete or contains unrecognized
      data at location "<position>" starting with the hex data "<text>".
      Reason code = "<reason-code>".

Explanation: 

An error occurred when processing a binary XML value. The first eight
bytes of XML data in hexadecimal at "<position>" bytes is "<text>". If
the unrecognized data is located within the last eight bytes of the
binary XML data, only the data starting at location "<position>" up to
the end of the binary XML data is displayed. The reason code indicates
the specific problem:

1. The XDBX data specified is incomplete.
2. An XML sequence was specified for an insert operation and this is not
   supported.
3. The XDBX data specified for an insert operation contains an
   unsupported tag.
4. The referenced string ID is not previously defined.
5. The length specified is not correct.

The statement cannot be processed.

User response: 

Fix the problem in your XML data or change your application to use
textual XML format for data transfer.

sqlcode: -20465

sqlstate: 22541
