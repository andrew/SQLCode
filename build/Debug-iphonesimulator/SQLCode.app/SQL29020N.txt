

SQL29020N  An internal error has occurred. Error = "<error>".

Explanation: 

An error has occurred during internal processing.

User response: 

Contact IBM Support and provide the db2diag log file and the qpdiag.log
file.
