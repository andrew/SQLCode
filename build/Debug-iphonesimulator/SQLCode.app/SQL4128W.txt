

SQL4128W  The COLUMN REFERENCE for "<column-name>" should be a grouping
      column or should be specified within a column function.

Explanation: 

Each COLUMN REFERENCE contained in a subquery in the SEARCH CONDITION of
a HAVING clause must reference a grouping column or be specified within
a column function.

Processing continues.

User response: 

Correct the SQL statement.
