

SQL3799W  Load recovery for table "<schema.tablename>" at time
      "<timestamp>" on node "<node-number>" is pending due to warning
      "<sqlcode>" with additional information "<additional-info>".

Explanation: 

A warning condition is encountered during load recovery. The utility
waits for a response to continue.

(Note : if you are using a partitioned database server, the node number
indicates which node the error occurred on. Otherwise, it is not
pertinent and should be ignored.)

User response: 

Examine the error code in the message for more information. Take
corrective action and return to the utility with the correct caller
action parameter to indicate if processing should continue or terminate.
