

SQL16231N  XML document contains a reference to namespace "<uri>"
      without an <import> declaration.

Explanation: 

While parsing an XML document, the parser encountered a reference to
namespace "<uri>" without an import declaration for that namespace.

Parsing or validation did not complete.

User response: 

Include and import for the namespace in the XML document and try the
operation again.

sqlcode: -16231

sqlstate: 2200M
