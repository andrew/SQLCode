

SQL3033N  A keyword such as INSERT, REPLACE, CREATE, INSERT_UPDATE, or
      REPLACE_CREATE is missing from the target specification or it is
      misspelled.

Explanation: 

For IMPORT, the Action String (for example, "REPLACE into ...")
parameter does not contain the keyword INSERT, REPLACE, CREATE,
INSERT_UPDATE, or REPLACE_CREATE. For LOAD, the Action String parameter
does not contain the keyword INSERT, REPLACE, or RESTART. The keyword
must be followed by at least one blank.

The command cannot be processed.

User response: 

Resubmit the command with a valid Action String parameter.
