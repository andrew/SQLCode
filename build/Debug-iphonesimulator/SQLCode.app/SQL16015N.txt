

SQL16015N  An element constructor contains an attribute node named
      ""<node-name>"" that follows an XQuery node that is not an
      attribute node. Error QName=err:"<error-name>".

Explanation: 

The sequence used for constructing element content contains an attribute
node named "<node-name>" that follows an XQuery node that is not an
attribute node. The particular context where this occurs is based on the
error QName. 

err:XQTY0024
         The content sequence of an element constructor contains an
         attribute node named "<node-name>" that follows an XQuery node
         that is not an attribute node. Attribute nodes can only occur
         at the beginning of the content sequence.

err:XUTY0004
         The content of an insertion sequence based on the source
         expression of an insert expression contains an attribute node
         named "<node-name>" that follows an XQuery node that is not an
         attribute node. Attribute nodes can only occur at the beginning
         of the insertion sequence.

The XQuery expression cannot be processed.

User response: 

Modify the content sequence to ensure that attribute nodes follow other
attribute nodes.

 sqlcode: -16015

 sqlstate: 10507
