

SQL0577N  User defined routine "<routine-name>" (specific name
      "<specific-name>") attempted to modify data but was not defined as
      MODIFIES SQL DATA.

Explanation: 

The program used to implement the body of a routine is not allowed to
modify SQL data.

User response: 

Remove any SQL statements that modify data then recompile the program.
Investigate the level of SQL allowed as specified when defining the
routine.

 sqlcode: -577

 sqlstate: 38002

 sqlstate: 42985
