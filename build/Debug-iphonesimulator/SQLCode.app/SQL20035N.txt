

SQL20035N  Invalid left operand of a dereference operator. Path
      expression starts with "<expression-string>".

Explanation: 

The left operand of the dereference operator in a path expression is not
valid. Possible causes are: 
*  The left operand includes a column function that uses a column
   function as an argument.
*  The left operand expression includes a column function and a
   reference to a column that is not in the GROUP BY clause.

User response: 

Correct the left operand of the dereference operator for the path
expression that starts with "<expression-string>".

 sqlcode: -20035

 sqlstate: 428DV
