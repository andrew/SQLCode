

SQL1398N  The routine "<routine-name>" is not supported in a partitioned
      database environment.

Explanation: 

The routine "<routine-name>" is not supported in a partitioned database
environment. The return data type of the RID function does not uniquely
identify a row across database partitions. The RID function is supported
in a non-partitioned database environment for compatibility with DB2 for
z/OS.

User response: 

Only use the routine in a non-partitioned database environment. Instead
of using the RID function, use the RID_BIT function.

sqlcode: -1398

sqlstate: 56038
