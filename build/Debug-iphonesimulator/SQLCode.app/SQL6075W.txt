

SQL6075W  The Start Database Manager operation successfully added the
      database partition server. The database partition server is not
      active until all database partition servers are stopped and
      started again.

Explanation: 

The db2nodes.cfg file is not updated to include the new database
partition server until all database partition servers are simultaneously
stopped by the STOP DATABASE MANAGER (db2stop) command. Until the file
is updated, the existing database partition servers cannot communicate
with the new one.

User response: 

Issue db2stop to stop all the database partition servers. When all
database partitions are successfully stopped, issue db2start to start
all the database partition servers, including the new one.
