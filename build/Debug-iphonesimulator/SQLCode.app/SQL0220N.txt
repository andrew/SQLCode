

SQL0220N  The Explain table "<name>", column "<name2>" does not have the
      proper definition or is missing.

Explanation: 

The Explain facility has been invoked but the Explain table "<name>" did
not have the expected definition. The definition could be incorrect due
to: 
*  Incorrect number of columns defined (if "<name2>" is numeric)
*  Incorrect data type assigned to columns (if "<name2>" is a column
   name).
*  Incorrect CCSID for the table.

User response: 

Correct the definitions of the specified Explain table. The SQL Data
Definition Language statements needed to create the Explain tables are
available in the file called EXPLAIN.DDL in the misc directory under
sqllib.

 sqlcode: -220

 sqlstate: 55002
