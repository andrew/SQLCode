

SQL6109N  Utility expected partitioning column "<column-name-1>", but
      found partitioning column "<column-name-2>".

Explanation: 

In the db2split configuration file: 
*  One of the partitioning columns defined for the table was not
   specified.
*  The order of the partitioning columns was incorrect
*  A column was specified that is not a partitioning column for the
   table.

User response: 

Do the following: 
1. Ensure that the db2split configuration file is correct.
2. Split the data.
3. Issue the Load operation with the newly partitioned data.
