

SQL1227N  The catalog statistic "<value>" for column "<column>" is out
      of range for its target column, has an invalid format, or is
      inconsistent in relation to some other statistic. Reason Code =
      "<code>".

Explanation: 

The value or format of a statistic provided for an updatable catalog is
either invalid, out of range, or inconsistent. The most common checks
for value, range and format are (corresponding to "<code>"):

1        

         Numeric statistics must be -1 or >= 0.


2        

         Numeric statistics representing percentages (eg. CLUSTERRATIO)
         must be between 0 and 100.


3        

         This message is returned with reason code 3 when an attempt is
         made to update HIGH2KEY or LOW2KEY (or both) in a way that
         violates the rules for updating column statistics manually.


4        

         PAGE_FETCH_PAIRS related rules:

          
         *  Individual values in the PAGE_FETCH_PAIRS statistic must be
            separated by a series of blank delimiters.
         *  There must be exactly 11 pairs in a single PAGE_FETCH_PAIR
            statistic.
         *  There must always be a valid PAGE_FETCH_PAIRS value if the
            CLUSTERFACTOR is > 0.
         *  Individual values in PAGE_FETCH_PAIRS statistics must not be
            longer than 19 digits and must be less than the maximum
            integer value (MAXINT = 9223372036854775807).
         *  Buffer size entries of PAGE_FETCH_PAIRS must be ascending in
            value. Also, any buffer size value in a PAGE_FETCH_PAIRS
            entry cannot be greater than MIN(NPAGES, 1048576) for 32-bit
            platforms and MIN(NPAGES, 2147483647) for 64-bit platforms
            where NPAGES is the number of pages in the corresponding
            table.
         *  "fetches" entries of AVGPARTITION_PAGE_FETCH_PAIRS must be
            descending in value, with no individual fetches entry being
            less than NPAGES. Also, any "fetch" size value in a
            AVGPARTITION_PAGE_FETCH_PAIRS entry must not be greater than
            CARD (cardinality) statistic of the corresponding table.
         *  If buffer size value is the same in two consecutive pairs,
            then page fetch value must also be the same in both the
            pairs.


5        

         CLUSTERRATIO and CLUSTERFACTOR related rules:

          
         *  Valid values for CLUSTERRATIO are -1 or between 0 and 100
         *  Valid values for CLUSTERFACTOR are -1 or between 0 and 1
         *  Either CLUSTERRATIO or CLUSTERFACTOR must be -1 at all
            times.
         *  If CLUSTERFACTOR is a positive value, it must be accompanied
            by a valid PAGE_FETCH_PAIR statistic.


6        

         The cardinality of a column (COLCARD statistic in SYSCOLUMNS)
         or column group (COLGROUPCARD in SYSCOLGROUPS) cannot be
         greater than the cardinality of its corresponding table (CARD
         statistic in SYSTABLES).


7        

         No statistics are supported for user-defined structured types.
         For columns with the following data types, statistics support
         is limited to AVGCOLLEN and NUMNULLS: LONG VARCHAR, LONG
         VARGRAPHIC, BLOB, CLOB, and DBCLOB.


8        

         A statistic is inconsistent with other related statistics for
         this entity or is invalid in this context.


9        

         For tables that are not partitioned, the following table
         partitioning statistics in SYSSTAT.INDEXES cannot be updated:
         AVGPARTITION_CLUSTERRATIO, AVGPARTITION_CLUSTERFACTOR,
         AVGPARTITION_PAGE_FETCH_PAIRS, DATAPARTITION_CLUSTERFACTOR


10       

         AVGPARTITION_PAGE_FETCH_PAIRS related rules:

          
         *  Individual values in the AVGPARTITION_PAGE_FETCH_PAIRS
            statistic must be separated by a series of blank delimiters.
         *  There must be exactly 11 pairs in a single
            AVGPARTITION_PAGE_FETCH_PAIR statistic.
         *  There must always be a valid AVGPARTITION_PAGE_FETCH_PAIRS
            value if the AVGPARTITION_CLUSTERFACTOR is > 0.
         *  Individual values in AVGPARTITION_PAGE_FETCH_PAIRS
            statistics must not be longer than 19 digits and must be
            less than the maximum integer value (MAXINT =
            9223372036854775807).
         *  Buffer size entries of AVGPARTITION_PAGE_FETCH_PAIRS must be
            ascending in value. Also, any buffer size value in a
            AVGPARTITION_PAGE_FETCH_PAIRS entry cannot be greater than
            MIN(NPAGES, 1048576) for 32-bit platforms and MIN(NPAGES,
            2147483647) for 64-bit platforms where NPAGES is the number
            of pages in the corresponding table.
         *  "fetches" entries of AVGPARTITION_PAGE_FETCH_PAIRS must be
            descending in value, with no individual fetches entry being
            less than NPAGES. Also, any "fetch" size value in a
            AVGPARTITION_PAGE_FETCH_PAIRS entry must not be greater than
            CARD (cardinality) statistic of the corresponding table.
         *  If buffer size value is the same in two consecutive pairs,
            then page fetch value must also be the same in both the
            pairs.


11       

         AVGPARTITION_CLUSTERRATIO and AVGPARTITION_CLUSTERFACTOR
         related rules:

          
         *  Valid values for AVGPARTITION_CLUSTERRATIO are -1 or between
            0 and 100.
         *  Valid values for AVGPARTITION_CLUSTERFACTOR are -1 or
            between 0 and 1.
         *  Either AVGPARTITION_CLUSTERRATIO or
            AVGPARTITION_CLUSTERFACTOR must be -1 at all times.
         *  If AVGPARTITION_CLUSTERFACTOR is a positive value, it must
            be accompanied by a valid AVGPARTITION_PAGE_FETCH_PAIR
            statistic.


12       

         DATAPARTITION_CLUSTERFACTOR related rules:

          
         *  Valid values for DATAPARTITION_CLUSTERFACTOR are -1 or
            between 0 and 1.


13       

         AVGCOMPRESSEDROWSIZE related rules:

          
         *  Valid values for AVGCOMPRESSEDROWSIZE are -1, or between 0
            and AVGROWSIZE.


14       

         AVGROWCOMPRESSIONRATIO related rules:

          
         *  Valid values for AVGROWCOMPRESSIONRATIO are -1, or greater
            than 1.


15       

         PCTROWSCOMPRESSED related rules:

          
         *  Valid values for PCTROWSCOMPRESSED are -1, or between 0 and
            100, inclusive.

User response: 

Make sure the new catalog statistic satisfies the indicated range,
length and format checks.

Make sure that any updates to statistics are consistent in their
inter-relationships (eg. cardinality).

For tables that are not partitioned, do not attempt to update statistics
columns that are specific to partitioned tables.

sqlcode: -1227

sqlstate: 23521


   Related information:
   Rules for updating column statistics manually
   Rules for updating distribution statistics manually
   Catalog statistics tables
