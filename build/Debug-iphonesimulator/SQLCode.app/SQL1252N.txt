

SQL1252N  More than one procedure was identified at the data source for
      the source procedure "<source-procedure-name>" specified in the
      CREATE PROCEDURE (Sourced) statement. Reason code =
      "<reason-code>".

Explanation: 

More than one procedure was identified at the data source for the source
procedure "<procedure-name>" specified in the CREATE PROCEDURE (Sourced)
statement. Possible reason codes are: 

1        There are multiple source procedures at the data source with
         identical source procedure name and source schema name.

2        There are multiple source procedures at the data source with
         identical source procedure name, source schema name and NUMBER
         OF PARAMETERS.

3        There are multiple source procedures at the data source with
         identical source procedure name.

User response: 

Actions based on the reason codes are: 

1        Specify the NUMBER OF PARAMETERS in the CREATE PROCEDURE
         (Sourced) statement to help uniquely identify the procedure at
         the data source.

2        Specify the UNIQUE ID in the CREATE PROCEDURE (Sourced)
         statement to help uniquely identify the procedure at the data
         source.

3        Specify the source-schema-name in the CREATE PROCEDURE
         (Sourced) statement to help uniquely identify the procedure at
         the data source.

 sqlcode: -1252

 sqlstate: 42725
