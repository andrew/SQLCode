

SQL1090C  The release number of the precompiled application program or
      utility is not valid.

Explanation: 

The release number of the precompiled application program or utility is
not compatible with the release number of the installed version of the
database manager.

The error will also occur if the application program is using down-level
database manager libraries or DLLs while accessing the installed version
of the database manager configuration file.

The command cannot be processed.

User response: 

Verify that there is no older version of the database manager libraries
or DLLs that will be picked up for your application processing.

If the problem persists, repeat the precompile process with the current
database manager. Use only application programs precompiled with a
compatible release level of the database manager.
