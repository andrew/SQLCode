

SQL1444N  The application context cannot be destroyed because it is in
      use.

Explanation: 

The user attempted to destroy an application context while it is still
in use. Either there is a thread attached to the context, or the context
has a CONNECT or ATTACH associated with it. A CONNECT RESET or DETACH
must be done (if a CONNECT or ATTACH has been done), and all threads
must detach from the context before it can be destroyed.

User response: 

Ensure that all calls to attach to a context have a corresponding
detach, all CONNECTS have a corresponding CONNECT RESET, and all
ATTACHES have a corresponding DETACH.
