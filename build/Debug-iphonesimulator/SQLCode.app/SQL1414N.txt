

SQL1414N  Table designator "<table-designator>" is not valid for the
      expression.

Explanation: 

The table designator is not defined to be a table designator in this SQL
statement or the table designator cannot be referenced where it is
specified in the SQL statement. The statement cannot be executed.

User response: 

Correct the syntax and resubmit the statement. Refer to the SQL
Reference for rules for table designator of ROW CHANGE TIMESTAMP
expression and ROW CHANGE TOKEN expression.

sqlcode: -1414

 sqlstate: 42703
