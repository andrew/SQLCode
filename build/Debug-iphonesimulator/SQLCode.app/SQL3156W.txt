

SQL3156W  The null field in the C record for column "<name>" is not
      valid. Data from the column will not be loaded.

Explanation: 

The null field in the C record for the indicated column is not valid.

Data from the indicated column is not loaded.

User response: 

Change the null field in the C record and resubmit the command.
