

SQL20328N  The document with target namespace "<namespace>" and schema
      location "<location>" already has been added for the XML schema
      identified by "<schema-name>".

Explanation: 

This error can occur while invoking the XSR_ADDSCHEMADOC stored
procedure. Within an XML schema, there cannot be two documents with the
same targetnamespace and schemalocation.

The statement cannot be processed.

User response: 

Change either the namespace or schemalocation of the document that is
being added.

sqlcode: -20328

sqlstate: 42749
