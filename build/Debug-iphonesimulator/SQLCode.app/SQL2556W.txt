

SQL2556W  The database was restored but did not include all of the logs
      required for rollforward recovery to a consistent point in time.

Explanation: 

A RESTORE DATABASE command was issued with the optional LOGTARGET path
specified but the backup image did not contain all of the logs required
for a successful rollforward recovery.

User response: 

To successfully complete rollforward recovery, supply all of the
required log extents from a log archive or other location.


   Related information:
   RESTORE DATABASE command
