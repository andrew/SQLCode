

SQL20535N  The data change operation "<operation>" is not supported for
      the target object "<object-name>" because of an implicit or
      explicit period specification involving "<period-name>". Reason
      code: "<reason-code>".

Explanation: 

The data change operation is not supported because the target of the
operation references a temporal table and a period specification was
specified. The period specification was either implicitly specified by
using a special register, or explicitly specified in the fullselect
specified as the target. More information is provided by the reason code
indicated:

1        

         The CURRENT TEMPORAL SYSTEM_TIME special register contains a
         non-null value and the target of the data change operation is a
         system-period temporal table (directly or indirectly). The data
         in a system-period temporal table cannot be changed when a
         period specification is in effect. The target of the data
         change statement is one of the following:

          
         *  a system-period temporal table
         *  a view that is defined with an outer fullselect that
            references a system-period temporal table in the FROM clause
            (directly or indirectly) and that does not have an INSTEAD
            OF trigger defined for the data change operation
         *  a fullselect that references a system-period temporal table
            in the FROM clause (directly or indirectly)


2        

         The CURRENT TEMPORAL SYSTEM_TIME special register contains a
         non-null value and the target of the data change statement is a
         view defined with the WITH CHECK OPTION. The data change
         statement cannot be processed because the view definition
         includes a WHERE clause containing one of the following syntax
         elements:

          
         *  a subquery that references a system-period temporal table
            (directly or indirectly)
         *  an invocation of an SQL routine that has a package
            associated with it
         *  an invocation of an external routine with data access
            indication other than NO SQL


3        

         The target of the data change statement is specified as a
         fullselect that references a view in the FROM clause which is
         followed by a period specification for SYSTEM_TIME. The
         referenced view is defined with the WITH CHECK OPTION. The data
         change statement cannot be processed because the view
         definition includes a WHERE clause containing one of the
         following syntax elements:

          
         *  a subquery that references a system-period temporal table
            (directly or indirectly)
         *  an invocation of an SQL routine that has a package
            associated with it
         *  an invocation of an external routine with data access
            indication other than NO SQL


4        

         The CURRENT TEMPORAL BUSINESS_TIME special register contains a
         non-null value and the target of the data change statement is a
         view defined with the WITH CHECK option. The data change
         statement cannot be processed because the view definition
         includes a WHERE clause containing one of the following syntax
         elements:

          
         *  a subquery that references an application-period temporal
            table (directly or indirectly)
         *  an invocation of an SQL routine that has a package
            associated with it
         *  an invocation of an external routine with data access
            indication other than NO SQL


5        

         The target of the data change statement is specified as a
         fullselect that references a view in the FROM clause which is
         followed by a period specification for BUSINESS_TIME. The
         referenced view is defined with the WITH CHECK OPTION. The data
         change statement cannot be processed because the view
         definition includes a WHERE clause containing one of the
         following syntax elements:

          
         *  a subquery that references an application-period temporal
            table (directly or indirectly)
         *  an invocation of an SQL routine that has a package
            associated with it
         *  an invocation of an external routine with data access
            indication other than NO SQL

The statement cannot be processed.

User response: 

Take an appropriate action for the reason-code:

1        

         Set the CURRENT TEMPORAL SYSTEM_TIME special register to the
         null value and try the data change operation again. If the
         statement is included in an application package that should not
         be sensitive to the setting of the CURRENT TEMPORAL SYSTEM_TIME
         special register, then bind the package using SYSTIMESENSITIVE
         NO.


2        

         Set the CURRENT TEMPORAL SYSTEM_TIME special register to the
         null value and try the data change operation again. If the
         statement is included in an application package that should not
         be sensitive to the setting of the CURRENT TEMPORAL SYSTEM_TIME
         special register, then bind the package using SYSTIMESENSITIVE
         NO. Another possible alternative could be to replace the
         reference to the view with another view that is defined without
         the WITH CHECK OPTION if the associated checking of data
         changes is not required.


3        

         Remove the period specification in the target fullselect of the
         data change operation. Another possible alternative could be to
         replace the reference to the view with another view that is
         defined without the WITH CHECK OPTION if the associated
         checking of data changes is not required.


4        

         Set the CURRENT TEMPORAL BUSINESS_TIME special register to the
         null value and try the data change operation again. If the
         statement is included in an application package that should not
         be sensitive to the setting of the CURRENT TEMPORAL
         BUSINESS_TIME special register, then bind the package using
         BUSTIMESENSITIVE NO.


5        

         Remove the period specification in the target fullselect of the
         data change operation and use explicit predicates in the WHERE
         clause of the fullselect to specify the target rows for the
         data change operation.

sqlcode: -20535

sqlstate: 51046
