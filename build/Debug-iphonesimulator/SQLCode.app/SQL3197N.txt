

SQL3197N  An attempt was made to execute multiple copies of import or
      export.

Explanation: 

An attempt was made to execute more than one instance of the import or
export utility on a system where this is not supported.

The command cannot be processed.

User response: 

Resubmit the second operation when no other processes are attempting to
execute the same utility.
