

SQL1649W  Deactivate database is successful, however, the database
      remains available in suspended I/O write operations mode.

Explanation: 

The database cannot be shutdown while it is in suspended I/O write
operations mode. The database will be shut down when I/O write
operations are resumed.

User response: 

No action required. Issue a 'SET WRITE RESUME' command to resume I/O
write operations and deactivate the database completely.


   Related information:
   SET WRITE command
