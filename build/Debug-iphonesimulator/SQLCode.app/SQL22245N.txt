

SQL22245N  JCL generation failed. Reason (code[, token]) =
      "<reason-code>".

Explanation: 

JCL generation failed as indicated by the following reason code: 

01       A card with the reserved JCL skeleton parameter &JOB is not
         found or is misplaced in the main JCL skeleton. This card
         should be the first non-comment card in the main JCL skeleton
         after the TEMPLATE card.

02       Incorrect use of a reserved JCL skeleton parameter. The token
         in this message contains the name of the parameter that caused
         the problem.

03       There are more occurrences of the reserved JCL skeleton
         parameter &CTLSTMT in the main JCL skeleton than expected. The
         token in this message is set to the expected number of the
         &CTLSTMT parameters.

04       There are more occurrences of the reserved JCL skeleton
         parameter &STEPLIB in the main JCL skeleton than expected. The
         token in this message is set to the expected number of the
         &STEPLIB parameters.

05       The main JCL skeleton does not begin with the TEMPLATE
         statement. This statement should be the first non-comment
         statement in the main JCL skeleton.

06       The jobname or stepname in the JCL skeleton does not contain
         the JCL skeleton built-in function &SEQ required for job or
         step sequencing. The JCL skeleton built-in function &SEQ should
         be specified in the jobname or stepname of the JCL skeleton.
         The token in this message contains a fragment of the incorrect
         JCL statement.

07       The keyword JOB is not found in the first non-comment statement
         in the JCL skeleton for //JOB-statement. It might be commented
         out, mistyped, or missed delimiting spaces, especially after
         the keyword.

08       The main JCL skeleton is incomplete or has incorrect structure.
         The required standard JCL statements may be commented out,
         missed or misplaced, especially if in-stream JCL procedures are
         being used in this JCL skeleton.

09       The syntax of jobname, stepname, or ddname in the JCL skeleton
         is invalid. Possible reasons for this: incorrect length of the
         name field, or the field includes some non-alphanumerical
         characters. The token in this message contains a fragment of
         the incorrect JCL statement.

10       An incorrect use of ampersand in the JCL skeleton. The JCL
         skeleton contains one or more stand-alone ampersand characters.
         An ampersand is the first symbol of any JCL skeleton parameter
         and should not be used without an accompanying identifier. The
         token in this message contains a fragment of the incorrect JCL
         statement.

11       The reserved JCL skeleton parameter &OBJECT was not found in
         the JCL skeleton.

12       A user-defined JCL skeleton parameter should not appear in the
         JCL skeleton. The token in this message contains the name of
         the JCL skeleton parameter that caused the problem.

13       A reserved JCL skeleton parameter should not appear in the JCL
         skeleton. The token in this message contains the name of the
         reserved JCL skeleton parameter that caused the problem.

14       The generated JCL is too long and the buffer for it cannot be
         allocated. The token in this message contains requested size
         that caused the problem. To avoid this problem, decrease the
         number of database objects selected for processing.

User response: 

Correct the problem according to the given explanation. If the problem
persists, contact your Database Administrator or IBM Support.
