

SQL3524N  Option "<option>" has an invalid value of "<value>".

Explanation: 

The value provided must be an integer value. Its range for each option
is as follows: 
1. TOTALFREESPACE: the value must be in the range of 0 to 100, and is
   interpreted as a percentage of the total pages in the table that are
   to be appended to the end of the table as free space.
2. PAGEFREESPACE: the value must be in the range of 0 to 100, and is
   interpreted as a percentage of each data page that is to be left as
   free space.
3. INDEXFREESPACE: the value must be in the range of 0 to 99, and is
   interpreted as a percentage of each index page that is to be left as
   free space when loading indexes.

The utility stops processing.

User response: 

Correct the value and resubmit the command.
