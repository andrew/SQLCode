

SQL4716N  An error occurred while communicating with the external
      workload manager.

Explanation: 

The database manager failed to communicate with the external workload
manager. The following could have caused the error:

*  The external workload manager is not installed
*  The external workload manager is installed but is not active

User response: 

If the instance is running on AIX, ensure AIX WLM is installed and
active. If the instance is running on Linux, ensure Linux WLM is
installed and active.

sqlcode: -4716

 sqlstate: 5U030
