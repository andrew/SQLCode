

SQL0280W  View, trigger or materialized query table "<name>" has
      replaced an existing inoperative view, trigger or materialized
      query table.

Explanation: 

An existing inoperative view, trigger or materialized query table
"<name>" was replaced by: 
*  the new view definition as a result of a CREATE VIEW statement
*  the new trigger definition as a result of a CREATE TRIGGER statement
*  the new materialized query table definition as a result of a CREATE
   SUMMARY TABLE statement.

User response: 

None required.

 sqlcode: +280

 sqlstate: 01595
