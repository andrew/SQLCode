

SQL3059N  The version field in the H record is not valid.

Explanation: 

The version field in the H record contains a value that is not valid.

The utility stops processing. No data is loaded.

User response: 

Examine the version field in the H record.
