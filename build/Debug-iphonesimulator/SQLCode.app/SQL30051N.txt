

SQL30051N  Bind process with specified package name and consistency
      token not active.

Explanation: 

An attempt has been made to issue a precompile/bind operation when
precompile/bind is not active or an attempt was made to use an invalid
package name and/or consistency token during an active precompile/bind
operation.

The command or statement cannot be processed.

User response: 

If the application is not the database manager precompiler/binder,
verify that precompile/bind is active before issuing the bind operation
and that correct information is being passed on the bind operation.

If the application is the database manager precompiler/binder, record
the message number (SQLCODE) and all error information from the SQLCA,
if possible. Attempt to perform the operation again.

If sufficient memory resources exist and the problem continues, invoke
the Independent Trace Facility at the operating system command prompt.

Contact your technical service representative with the following
information:

Required information: 
*  Problem description
*  SQLCODE and reason code
*  SQLCA contents if possible
*  Trace file if possible.

 sqlcode: -30051

 sqlstate: 58012
