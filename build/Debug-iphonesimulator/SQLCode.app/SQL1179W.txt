

SQL1179W  The "<object-type>" called "<object-name>" may require the
      invoker to have necessary privileges on data source objects.

Explanation: 

The object identified by "<object-name>" references a federated object
(such as an OLE DB table function, federated routine, federated view, or
a nickname), where the actual data exists at a data source. When the
data source data is accessed, the user mapping and authorization
checking is based on the user that initiated the operation.

If the "<object-type>" is SUMMARY TABLE, then the operation is
refreshing the data for the materialized query table. The user that
invoked the REFRESH TABLE or SET INTEGRITY statement that causes the
refresh may be required to have the necessary privileges to access the
underlying data source object at the data source.

If the "<object-type>" is VIEW, then any user of the view may be
required to have the necessary privileges to access the underlying data
source object at the data source.

If the "<object-type>" is PROCEDURE, FUNCTION, or METHOD then the
invoker of the routine may be required to have the necessary privileges
to access the underlying data source object at the data source for any
SQL statements in the routine.

If the "<object-type>" is PACKAGE and this message results from
precompiling or binding an application, then the invoker of the
application may be required to have the necessary privileges to access
the underlying data source object at the data source for any static SQL
statements in the application.

If the "<object-type>" is PACKAGE and this message results from creating
an SQL or XQuery procedure, then the invoker of the procedure may be
required to have the necessary privileges to access the underlying data
source object at the data source for any SQL statements in the
procedure.

In any case, an authorization error may occur when the attempt is made
to access the data source object.

User response: 

Granting privileges to the object may not be sufficient to support
operations that access the data from the data source. User access may
need to be granted at the data source for the underlying data source
objects.

If the "<object-type>" is PACKAGE and this message results from
precompiling or binding an application, specify option FEDERATED YES on
the PRECOMPILE (PREP) or BIND command.

If the "<object-type>" is PACKAGE and this message results from creating
an SQL or XQuery procedure, set the precompile and bind options for SQL
and XQuery procedures to include option FEDERATED YES. You set the
precompile and bind options for SQL and XQuery procedures by setting
registry variable DB2_SQLROUTINE_PREPOPTS or calling procedure
SYSPROC.SET_ROUTINE_OPTS. For example, to set the registry variable,
issue the following command at an operating system command prompt:

db2set DB2_SQLROUTINE_PREPOPTS="FEDERATED YES"

sqlcode: +1179

sqlstate: 01639
