

SQL3278N  The node "<node>" already exists in the LDAP directory.

Explanation: 

The command did not complete successfully because another node of the
same name already exists in the LDAP directory.

User response: 

Re-submit the command using a different alias name.
