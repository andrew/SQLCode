

SQL0570W  Not all requested privileges on object "<object-name>" of type
      "<object-type>" were granted.

Explanation: 

A GRANT operation was attempted on object "<object-name>" of type
"<object-type>", but some or all of the privileges were not granted. The
authorization ID that issued the statement does not have all of the
privileges to be granted with the GRANT option or does not have
ACCESSCTRL or SECADM authority.

All valid requested privileges were granted.

User response: 

Obtain the required authority and try the operation again.

sqlcode: +570

 sqlstate: 01007
