

SQL20077N  Cannot construct structured type objects that have Datalink
      type attributes.

Explanation: 

An attempt was made to invoke the constructor of a structured type which
has a Datalink and/or a Reference type attribute. This functionality is
currently not supported. In Version 6.1 or earlier, this error may also
be issued for a structured type object with a Reference type attribute.

The statement cannot be processed.

User response: 

The error can be corrected by doing one of the following:

1. Removing the invocation of the constructor of the type from the
   program.
2. Removing any Datalink (or Reference) type attributes from the
   definition of the structured type (this may not be possible if there
   are any tables that depend on this type).

 sqlcode: -20077

 sqlstate: 428ED
