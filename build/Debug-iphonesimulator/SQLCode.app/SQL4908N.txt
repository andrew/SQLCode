

SQL4908N  The table space list specified for roll-forward recovery on
      database "<name>" is invalid on members or nodes "<node-list>".

Explanation: 

Check for one or more of the following conditions:

*  The tablespace list contains duplicate names.
*  If starting a new table space rollforward, one or more of the table
   spaces specified in the list to be rolled forward is not in
   rollforward pending state on the specified members or nodes.
*  If continuing a table space rollforward that is already in progress,
   one or more of the table spaces specified in the list to be rolled
   forward is not in rollforward in progress state or is offline on the
   specified members or nodes.

User response: 

Ensure that there are no duplicate table space names in the list.

Use the MON_GET_TABLESPACE table function on the members or nodes
specified to find out which table spaces are not ready to be rolled
forward. Use the QUERY STATUS option of the rollforward command to
determine the status of the table space rollforward. If the rollforward
status is "TBS pending", a new table space rollforward can be started.
If the rollforward status is "TBS working", a table space rollforward is
already in progress.

If starting a new table space rollforward, put the tablespaces into
rollforward pending state by restoring them.

If continuing a table space rollforward and one or more of the table
spaces involved have been restored and put into rollforward pending
state, the table space rollforward in progress must be canceled. Submit
the rollforward command again with the CANCEL option and the same table
space list. When the rollforward in progress is canceled, the table
spaces will have been put into restore pending state. Restore the table
spaces and submit the original rollforward command again.

If continuing a table space rollforward and one or more of the table
spaces involved is offline, there are three options:

*  Bring the tablespace back online and submit the original rollforward
   command again.
*  Re-submit the rollforward command but remove the offline tablespaces
   from the tablespace list. These tablespaces will be put into restore
   pending state.
*  Submit the rollforward command again with the CANCEL option and the
   same table space list. When the rollforward in progress is canceled,
   the table spaces will have been put into restore pending state.
