

SQL1606W  The Database System Monitor output buffer is full.

Explanation: 

The Database System Monitor output buffer area is not large enough to
accommodate the returned data. Likely causes are intense system activity
when the call was made, or in the case of a Database Monitor API call
within a user application, the user allocated a buffer too small to
contain the returned data.

The command completed successfully and data collected prior to the
buffer overflow is returned in the user's buffer.

User response: 

The user should reissue the command, or in the case of a Database
Monitor API call within a user application, allocate a larger buffer or
reduce the amount of information requested.
