

SQL2036N  The path for the file, named pipe, or device "<path/device>"
      is not valid.

Explanation: 

The application calling the utility has supplied a source or target path
that is not valid. The path, file, named pipe, or device specified may
not exist or is incorrectly specified.

User response: 

Reissue the utility command with a path that represents a correct path
or device.
