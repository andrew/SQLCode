

SQL3945I  Synchronization session identifier for the satellite was
      retrieved successfully.

Explanation: 

The session identifier for this satellite was found and returned
successfully.

User response: 

No action is required.
