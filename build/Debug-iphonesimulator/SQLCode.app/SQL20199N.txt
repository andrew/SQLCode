

SQL20199N  The key transform table function used by the index extension
      of index "<index-id>" of table "<table-id>" in "<tbspace-id>"
      generated duplicate rows.

Explanation: 

The key transform table function specified by the GENERATE USING clause
of the index extension used by index "<index-id>" generated duplicate
rows. For a given invocation of the key transform table function, no
duplicate rows should be produced. This error is occurred when inserting
or updating the key value for the index "<index-id>" of table
"<table-id>" in table space "<tbspace-id>".

The statement cannot be processed.

User response: 

The code for the key transform table function used by the index
extension of index "<index-id>" must be modified to avoid the creation
of duplicate rows.

To determine the index name, use the following query: 

SELECT IID, INDSCHEMA, INDNAME
  FROM SYSCAT.INDEXES AS I,
       SYSCAT.TABLES AS T
  WHERE IID = <index-id>
      AND TABLEID = <table-id>
      AND TBSPACEID = <tbspace-id>
      AND T.TBASCHEMA = I.TABSCHEMA
      AND T.TABNAME = I.TABNAME

 sqlcode: -20199

 sqlstate: 22526
