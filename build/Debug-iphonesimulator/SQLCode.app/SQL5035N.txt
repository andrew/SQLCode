

SQL5035N  The database must be upgraded to the current release.

Explanation: 

The database was created in a previous release of DB2 database product.
You must issue the UPGRADE DATABASE command to upgrade the database to
the current release.

The command cannot be processed.

User response: 

Issue the UPGRADE DATABASE command before attempting to access the
database using the current release.

If you receive this message when you issued a database restore, drop the
existing database before proceeding.

sqlcode: -5035

 sqlstate: 55001
