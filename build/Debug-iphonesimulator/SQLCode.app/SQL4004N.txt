

SQL4004N  The package name is not valid.

Explanation: 

The package name contains characters that are not valid. Either the name
is too long or no name was specified with the PACKAGE option.

No package is created.

User response: 

Resubmit the command with a valid package name or without the PACKAGE
option.
