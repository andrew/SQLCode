

SQL20393N  The maximum number of components in security policy
      "<security-policy>" has been exceeded.

Explanation: 

A security policy can have maximum of 16 components.

User response: 

Reduce the number of components specified for the security policy
"<security-policy>".

sqlcode: -20393

sqlstate: 54062
