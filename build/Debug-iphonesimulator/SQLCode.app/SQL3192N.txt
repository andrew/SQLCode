

SQL3192N  In the filetmod a user specified format "<keyword>" beginning
      with the string "<string>" is not valid.

Explanation: 

The user specified format is not valid because it may be specified more
than once, or it may contain an invalid character.

The formats must be enclosed in double-quotes.

Valid DATEFORMAT specifiers include "YYYY" and the "M", and "D"
characters.

Valid TIMEFORMAT specifiers include "AM", "PM", "TT", and the "H", "M",
and "D" characters.

Valid TIMESTAMPFORMAT specifiers include all of the specifiers for
DATEFORMAT and TIMEFORMAT, as well as "UUUUUU". However, "M" cannot be
next to both a date format specifier and a time format specifier.

A field separator is necessary if the corresponding value in the data
file can have a variable length.

The utility stops processing.

User response: 

Examine the format specifier. Correct the format, and resubmit the
command.
