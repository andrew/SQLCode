

SQL16202N  XML document contains invalid digit "<value>" for the
      associated radix.

Explanation: 

While parsing an XML document the parser encountered an invalid digit
"<value>" for the associated radix. The radix could be base 10 or base
16.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16202

sqlstate: 2200M
