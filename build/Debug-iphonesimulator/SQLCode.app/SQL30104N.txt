

SQL30104N  Error in BIND or PRECOMPILE option "<option-name>" with value
      "<value-name>".

Explanation: 

While processing the BIND or PRECOMPILE parameters, either the BIND or
PRECOMPILE option or the value is not acceptable, or the option and
value pair is not appropriate.

The statement cannot be processed.

User response: 

Examine the command options and values to determine the error and
resubmit the command.

 sqlcode: -30104

 sqlstate: 56095
