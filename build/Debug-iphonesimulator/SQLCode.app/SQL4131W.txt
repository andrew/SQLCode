

SQL4131W  The COLUMN REFERENCE for "<column>" is invalid.

Explanation: 

One of the following conditions has not been met: 
*  For a GROUPed table, the COLUMN REFERENCE must reference a GROUPing
   column or be specified within a SET FUNCTION SPECIFICATION.
*  If not a GROUPed table and VALUE EXPRESSION includes a SET FUNCTION
   SPECIFICATION, then each COLUMN REFERENCE must be specified within a
   SET FUNCTION SPECIFICATION.

Processing continues.

User response: 

Correct the SQL statement.
