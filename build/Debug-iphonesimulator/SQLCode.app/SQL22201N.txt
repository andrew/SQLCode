

SQL22201N  The DB2 Administration Server failed to authenticate the user
      "<authorization-ID>" on the host "<hostname>". Reason code
      "<reason-code>".

Explanation: 

The DB2 Administration Server was unable to authenticate the user
"<authorization-ID>" for the following reason: 
1. Invalid user ID or password.
2. Password expired.
3. User account is disabled.
4. User account is restricted.
5. The DB2 Administration Server is unable to process requests submitted
   as the root user.
6. Authorization failed.

User response: 

Depending on the reason code, try the following: 
1. Verify that a valid user ID and password were specified for the host
   "<hostname>".
2. Change the password on host "<hostname>" for user
   "<authorization-ID>". Contact your system administrator for
   assistance. Attempt the request again once the password has been
   changed.
3. Contact your system administrator to unlock the account.
4. Contact your system administrator to find out the restrictions placed
   on the account.
5. Resubmit the request as a user other than root.
6. An internal authentication error has occurred.

If you continue to receive this message after attempting the suggested
solutions, contact IBM Support.
