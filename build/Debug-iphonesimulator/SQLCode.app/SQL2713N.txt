

SQL2713N  Invalid run type (RUNTYPE) at line "<line>" of the
      configuration file.

Explanation: 

The value for run type (RUNTYPE) specified in the configuration file is
not valid.

User response: 

Valid run type (RUNTYPE) can be either PARTITION or ANALYZE (case
insensitive).
