

SQL16008N  XQuery library modules cannot be declared or imported. Error
      QName=err:XQST0016.

Explanation: 

A module declaration or module import exists in an XQuery statement, but
DB2 XQuery does not support the Module Feature.

The module cannot be declared or imported.

User response: 

Remove all module declarations or module imports from the query prolog.

 sqlcode: -16008

 sqlstate: 10502
