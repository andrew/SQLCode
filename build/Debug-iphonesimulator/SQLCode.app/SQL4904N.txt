

SQL4904N  The pointer to parameter "<n>" of function "<function>" is not
      valid.

Explanation: 

The pointer to the specified parameter in the specified function is not
valid.

The function cannot be completed.

User response: 

Correct the specified parameter and call the function again.
