

SQL4191W  The data type of column "<column-name>" is not recognized.

Explanation: 

The data type of the column is not recognized by the standard.

Processing continues.

User response: 

Correct the SQL statement.
