

SQL6081N  A communication error caused a DB2STOP FORCE command to time
      out on this node.

Explanation: 

A communication error occurred on one or more of the database nodes,
causing the DB2STOP FORCE command to time out on the current node, or,
DB2STOP FORCE terminated because a severe error occurred during FORCE on
one or more nodes. Any node where the communication error occurred will
receive the SQL6048N message.

User response: 

Do the following: 
1. Correct the communication error on the node (or nodes) that received
   the SQL6048N message.
2. Issue a DB2START command and ensure that all nodes that received the
   SQL6048N message started successfully.
3. Issue the DB2STOP FORCE command again from any node.
