

SQL1743N  The RESTORE command failed because the database in the backup
      image is encrypted but the existing database on disk is not
      encrypted.

Explanation: 

No encryption options were specified in the RESTORE operation. When no
encryption options are specified, the source and target databases in a
RESTORE operation must either be both encrypted or neither must be
encrypted.

User response: 

If this was intended specify the command with the NO ENCRYPT option and
retry the command.

If this was not intended and you are restoring into a new database,
specify the required encryption options to the restore API and retry the
command.

If this is an existing database, the encryption options can not be
changed and you must either use a different backup image, restore into a
different database, or drop the existing database and then run the
RESTORE command with the required encryption options.


   Related information:
   RESTORE DATABASE command
