

SQL1352N  Trusted Connection cannot be reused during a transaction.

Explanation: 

An attempt to reuse a connection failed because the connection was in a
transaction. The connection is now in an unconnected state.

User response: 

Before trying to reuse a connection make sure that you perform a commit
or rollback.
