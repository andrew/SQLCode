

SQL16237N  XML schema contains an occurrence range for element
      "<element-name>" that is not a valid restriction of range for the
      base element.

Explanation: 

While parsing an XML schema, the parser encountered an occurrence range
in the element "<element-name>" that is not a valid restriction of the
occurrence range for the base element.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16237

sqlstate: 2200M
