

SQL2533W  Warning! The backup file on device "<device>" contains the
      image of database "<database>" taken at timestamp "<timestamp>".
      This is not the backup image requested.

Explanation: 

The backup image read from the tape position contains a media header
that does not match the header of the image of the first file of the
backup file sequence.

User response: 

Ensure that the tape is positioned at the correct backup, then return to
the utility with the callerac parameter indicating if processing
continues.
