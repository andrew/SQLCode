

SQL2095W  Storage path "<storage-path>" is in the drop pending state
      because one or more automatic storage table spaces reside on the
      path.

Explanation: 

A request has been made to drop storage path "<storage-path>" from the
database. Because one or more automatic storage table spaces have
containers on this storage path, it cannot be removed immediately and is
in the drop pending state. A storage path cannot be removed until all
containers on it have been removed.

If more than one storage path is being dropped, this message might also
apply to the other storage paths.

User response: 

Perform one or more of the following tasks to remove containers from the
storage path:

*  Drop all automatic storage temporary table spaces. Then, recreate
   these table spaces. The newly created table spaces will not use the
   drop pending storage paths.
*  Use the REBALANCE clause of the ALTER TABLESPACE statement to move
   data and containers from storage paths that are being dropped.
*  Drop table spaces that you do not need.

sqlcode: +2095

sqlstate: 01691


   Related information:
   Scenario: Dropping a storage path and rebalancing automatic storage
   table spaces
