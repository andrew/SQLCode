

SQL3952N  The satellite ID cannot be found at the satellite control
      server.

Explanation: 

Either the satellite ID is not defined correctly on this satellite or
this satellite has not been defined at the satellite control server.

User response: 

If you use the DB2SATELLITEID registry variable, ensure that it is set
to the unique ID for the satellite. If you are using the operating
system logon ID as the satellite ID, ensure you log on using it.
Otherwise, contact the help desk or your system administrator.
