

SQL20480W  The newly defined object "<object-name>" is marked as invalid
      because it references an object "<object-name2>" which is not
      defined or is invalid, or the definer does not have privilege to
      access it.

Explanation: 

The object "<object-name>" was successfully defined, but has been marked
as invalid. Objects such as views, triggers, SQL procedures, and SQL
functions, can be defined successfully even though they reference an
object, such as "<object-name2>", that is either not defined at the
application server, or is in the invalid state, or the definer does not
have privilege to access it. Invalid objects can be automatically
revalidated implicitly the next time they are accessed or explicitly by
using the procedure SYSPROC.ADMIN_REVALIDATE_DB_OBJECTS.

User response: 

If "<object-name2>" was expected to be defined or valid, then create or
revalidate the object and then redefine "<object-name>". Ensure that all
objects referenced by "<object-name>" are valid and the definer has the
privilege to access them before the first access to the object, so it
will be revalidated successfully.

sqlcode: +20480

 sqlstate: 0168Y
