

SQL16170N  XML document contains a type "<type-name1>" with an unknown
      base type "<type-name2>".

Explanation: 

While parsing an XML document, the parser encountered a type
"<type-name1>" with an unknown base type "<type-name2>".

Parsing or validation did not complete.

User response: 

Correct the base type for "<type-name1>" in the XML document and try the
operation again.

sqlcode: -16170

sqlstate: 2200M
