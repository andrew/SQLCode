

SQL0968C  The file system is full.

Explanation: 

One of the file systems containing the database is full. This file
system may contain the database directory, the database log files, or a
table space container.

In a DB2 pureScale environment, if you are adding a member to the DB2
instance, there is not enough disk space to create the additional
members files.

The statement cannot be processed.

User response: 

Free system space by erasing unwanted files. Do not erase database
files. If additional space is required, it may be necessary to drop
tables and indexes identified as not required.

On unix-based systems, this disk full condition may be due to exceeding
the maximum file size allowed for the current userid. Use the chuser
command to update fsize. A reboot may be necessary.

This disk full condition may be caused when containers are of varying
sizes. If there is sufficient space in the file system, drop the table
space and recreate it with containers of equal size.

If the statement that could not be processed referenced LOB data types:

*  Ensure that any cursors used in the application are closed
   immediately after their use.
*  Ensure that within the application that COMMIT statements are
   periodically executed.
*  Add additional containers to the system temporary tablespace to hold
   the temporary LOB data during this statement's execution.

In a DB2 pureScale environment, free system space by erasing unwanted
files or adding capacity to the file system, then rerun the command.

sqlcode: -968

sqlstate: 57011


   Related information:
   Topology changes (add or delete members)
