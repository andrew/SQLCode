

SQL1421N  MBCS conversion error occurred when converting host variable
      or sqlvar "<number>" to or from wchar_t format. Reason code
      "<rc>".

Explanation: 

A C/C++ application with embedded SQL statements was precompiled with
the WCHARTYPE CONVERT option. At runtime, the application received an
error which occurred during conversion in either wcstombs(), for input
host variables, or mbstowcs(), for output host variables. The host
variable or sqlvar number indicates which data item experienced the
problem. Valid reason codes are: 

1        the problem occurred with input data

2        the problem occurred with output data

User response: 

If application data is already in MBCS format, re-precompile the
application with WCHARTYPE NOCONVERT and re-build. If application data
is intended to be in wchar_t format, then input data failing in
wcstombs() may be corrupt. Correct the data and re-execute the
application.

 sqlcode: -1421

 sqlstate: 22504
