

SQL2981N  An error occurred calling a system function or system command.
      Function or command: "<function-or-command-name>". Reason code:
      "<reason-code>". Additional tokens: "<additional-tokens>".

Explanation: 

An unexpected error occurred calling an operating system function or
command. The reason codes are:

1        

         The command was not found in /usr/bin or /bin. For this reason
         code, the "Additional tokens" field in the message is blank.


2        

         The function or command failed with the system error code shown
         in the "Additional tokens" field of the message. On UNIX, error
         codes are defined in system header file errno.h. For
         information on Windows error codes, consult Microsoft
         documentation.


3        

         The function or command did not return output in the expected
         format. The "Additional tokens" field of the message shows the
         last line of output.

User response: 

The user response depends on the reason code:

1        

         Verify that the specified command is installed in /usr/bin or
         /bin and is executable. If this is not the problem, contact IBM
         Technical Support.


2        

         If you cannot resolve the problem using the system error code,
         contact IBM Technical Support.


3        

         Verify that the command or function is the version that shipped
         with the operating system and not a version tailored to your
         installation. If this is not the problem, contact IBM Technical
         Support.
