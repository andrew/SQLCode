

SQL20094N  The statement failed because the column "<column-name>" is a
      generated column or is defined with the data type DB2SECURITYLABEL
      and cannot be used in the BEFORE trigger "<trigger-name>".

Explanation: 

The indicated column cannot be named in the column name list of a BEFORE
UPDATE trigger or set in a BEFORE trigger because it is one of the
following:

*  A row-begin column
*  A row-end column
*  A transaction-start-ID column
*  A generated expression column
*  A column defined with the data type DB2SECURITYLABEL

The statement cannot be processed.

User response: 

Remove the column from either the column name list or the SET assignment
statement that sets the new transition variable of a generated column
and resubmit the statement.

sqlcode: -20094

sqlstate: 42989
