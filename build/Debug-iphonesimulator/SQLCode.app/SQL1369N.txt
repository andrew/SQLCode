

SQL1369N  Invalid XML document.

Explanation: 

The current XML document is invalid.

User response: 

Validate the XML document before proceeding.
