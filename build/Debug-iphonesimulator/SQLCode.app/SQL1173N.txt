

SQL1173N  The restore operation must specify automatic storage paths.

Explanation: 

A previous attempt was made to restore the catalog partition specifying
a list of automatic storage paths in a partitioned database, and that
restore failed. Any subsequent restore operations on the catalog
partition must also specify a list of automatic storage paths. This
restriction is removed from the database once the database is returned
to normal state via a rollforward operation.

User response: 

Perform the restore operation again, specifying a list of automatic
storage paths.

sqlcode: -1173

sqlstate: 5U011
