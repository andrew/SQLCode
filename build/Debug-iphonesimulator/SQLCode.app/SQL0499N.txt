

SQL0499N  Cursor "<cursor-name>" has already been assigned to this or
      another result set from procedure "<procedure-name>".

Explanation: 

An attempt was made to assign a cursor to a result set but multiple
cursors have been allocated for procedure "<procedure-name>".

User response: 

Determine if the target result set was previously assigned to a cursor.
If multiple cursors have been allocated for procedure "<procedure-name>"
ensure that only one cursor is used to process the result sets of a
stored procedure.

 sqlcode: -499

 sqlstate: 24516
