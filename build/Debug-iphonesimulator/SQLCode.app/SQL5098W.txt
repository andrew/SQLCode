

SQL5098W  The default log file path has been changed to "<logfilepath>".

Explanation: 

The default log file path has been changed from the database directory
to the global database directory.

User response: 

Ensure that there is enough disk space in the new default log file path
for the log files. If a disk or partition was mounted for the old
default log file path, consider changing the mount point to the new
default log file path.
