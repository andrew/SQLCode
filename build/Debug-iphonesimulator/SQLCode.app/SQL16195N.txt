

SQL16195N  XML document contains an invalid redefine. "<namespace-uri>"
      has already been included or redefined.

Explanation: 

While parsing an XML document, the parser encountered an invalid
redefine. The namespace "<namespace-uri>" has already been included or
redefined.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16195

sqlstate: 2200M
