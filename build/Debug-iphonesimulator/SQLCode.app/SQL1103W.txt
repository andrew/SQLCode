

SQL1103W  The UPGRADE DATABASE command was completed successfully.

Explanation: 

The UPGRADE DATABASE command was completed successfully. You can now
access this database.

Note that this message will also be returned if the database was already
at the current level and was not upgraded.

User response: 

Refer to the DB2 Information Center for details about post-upgrade tasks
that you should perform after upgrading your database.
