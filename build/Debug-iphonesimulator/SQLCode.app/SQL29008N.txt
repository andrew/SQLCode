

SQL29008N  Error encountered during job sequence number generation.

Explanation: 

An error was encountered during job sequence number generation.

User response: 

If the problem persists contact your technical service representative.
