

SQL2070N  An invalid image was encountered on media "<media>". The image
      contained timestamp "<timestamp>".

Explanation: 

An invalid image was encountered during the processing of a database
utility. The image provided was from a backup or copy with a different
timestamp. The utility stops processing.

User response: 

Resubmit the command with correct backup or copy images.
