

SQL30083N  Attempt to change password for user id "<uid>" failed with
      security reason "<reason-code>" ("<reason-string>").

Explanation: 

The attempt to change password was rejected due to invalid or incorrect
security information. The cause of the security error is described by
the "<reason-code>" and corresponding "<reason-string>" values.

The following is a list of reason codes and corresponding reason
strings: 

0 (NOT SPECIFIED)
         The specific security error is not specified.

1 (CURRENT PASSWORD INVALID)
         The old password specified in the request is not valid.

2 (NEW PASSWORD INVALID)
         The password specified in the request is not valid under the
         password rules imposed by the system where password was to be
         changed.

3 (CURRENT PASSWORD MISSING)
         The request did not include an old password.

4 (NEW PASSWORD MISSING)
         The request did not include a new password.

5 (USERID MISSING)
         The request did not include a userid.

6 (USERID INVALID)
         The userid specified in the request is not valid.

7 (USERID REVOKED)
         The userid specified in the request has been revoked. Passwords
         can not be changed for revoked userids.

14 (INSTALLATION EXIT FAILED)
         The installation security exit failed.

15 (PROCESSING FAILURE)
         Security processing at the server failed.

17 (UNSUPPORTED FUNCTION)
         Change password function is not supported by the system, or
         change password function is not supported at this time because
         of the user account restrictions.

19 (USERID DISABLED or RESTRICTED)
         The userid has been disabled, or the userid has been restricted
         from accessing the operating environment at this time.

23 (CHGPWD_SDN in DCS entry is not configured)
         To change an MVS password on a host system connected via SNA,
         the DCS database must be cataloged with the ,,,,,,,CHGPWD_SDN
         parameter string. The ,,,,,,,CHGPWD_SDN parameter string
         identifies the symbolic destination name for Password
         Expiration Management (PEM).

24 (USERNAME AND/OR PASSWORD INVALID)
         The username specified, password specified, or both, are
         invalid.

User response: 

Ensure that the proper userid, current and new passwords are supplied.

The userid may be disabled, the userid may be restricted to accessing
specific workstations, or the userid may be restricted to certain hours
of operation.

Instructions for some specific reason codes follow: 

14       Check the file db2pem.log in the instance subdirectory
         (typically "db2") for a detailed description of the problem
         encountered.

23       Catalog the DCS database using the ,,,,,,,CHGPWD_SDN parameter,
         as specified in the DB2 Connect User's Guide.

 sqlcode: -30083

 sqlstate: 08001
