

SQL1094N  The node directory cannot be accessed because it is being
      updated.

Explanation: 

The node directory cannot be scanned or used while it is being updated.
Also, the directory cannot be accessed for update if it is already being
accessed for any reason.

The command cannot be processed.

User response: 

Resubmit the command after the update is finished.

 sqlcode: -1094

 sqlstate: 57009
