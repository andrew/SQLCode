

SQL4009N  The expression for data length is invalid.

Explanation: 

The expression for data length has syntax errors or is too complex.

The statement cannot be processed.

User response: 

Check the syntax of the size expression.
