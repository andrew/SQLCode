

SQL0554N  An authorization ID cannot grant a privilege or authority to
      itself.

Explanation: 

An authorization ID attempted to execute a statement that would grant a
privilege or authority to the authorization ID itself. If this is a
GRANT statement, the authorization ID itself appears as one of the
entries in the authorization ID list to which privileges, authorities,
security labels, or exemptions are to be granted. If this is a CREATE
TRUSTED CONTEXT or an ALTER TRUSTED CONTEXT, the authorization ID itself
appears as either the value for the SYSTEM AUTHID attribute or one of
the authorization names specified in the WITH USE FOR clause.

The statement cannot be processed.

User response: 

Remove or replace the authorization ID in the statement.

 sqlcode: -554

 sqlstate: 42502
