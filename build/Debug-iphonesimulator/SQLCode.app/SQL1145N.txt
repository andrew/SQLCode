

SQL1145N  PREPARE statement is not supported when using a gateway
      concentrator. Reason code: "<reason-code>".

Explanation: 

The statement failed for one of the following reasons, based on
"<reason-code>". 

1        When gateway concentrator feature is ON, dynamically prepared
         statements from embedded SQL are not supported. In this
         configuration, dynamically prepared statements are only
         supported if the client is a CLI application.

2        When the gateway concentrator feature is ON, dynamically
         prepared SET statements are not supported.

User response: 

Based on the reason code, perform the following actions: 

1        Change the application to use CLI for dynamic SQL statements,
         or change the application to use static SQL.

2        Use EXECUTE IMMEDIATE for SET statements.

 sqlcode: -1145

 sqlstate: 560AF
