

SQL3263N  The protocol type is not supported.

Explanation: 

The protocol type specified is not supported for the command.

User response: 

Re-submit the command using a supported protocol type.
