

SQL6554N  An error occurred when attempting to remotely execute a
      process.

Explanation: 

The utility attempted to start a child process on a different database
partition, but an error occurred.

User response: 

*  If no user ID or password was provided to the utility for remote
   access, ensure that the user ID invoking the utility is authorized to
   execute programs on the target nodes.
*  If a user ID and password were supplied to the utility, confirm that
   they were supplied correctly.
*  If running on a Windows operating system, make sure the DB2
   installation has correctly defined a Windows Service for the splitter
   operation on all nodes.
*  If you are unable to resolve this problem, contact DB2 service.
