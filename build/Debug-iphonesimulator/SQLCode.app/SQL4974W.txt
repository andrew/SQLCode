

SQL4974W  The ROLLFORWARD DATABASE QUERY STATUS command encountered
      sqlcode "<sqlcode>".

Explanation: 

The ROLLFORWARD DATABASE QUERY STATUS command encountered an error with
the sqlcode "<sqlcode>". The query might not be successful on some nodes
for various reasons. The most severe error is indicated by "<sqlcode>".
The roll-forward status is only returned for the successful nodes.

User response: 

Look up the sqlcode "<sqlcode>" in the Message Reference, or online to
determine the problems on the failed nodes. Take the required corrective
actions, then continue the forward recovery on these nodes.
