

SQL0673N  A primary or unique key index cannot be created because the
      table contains rows which are duplicates with respect to the
      values of the identified primary or unique key columns of
      constraint "<name>".

Explanation: 

The primary or unique key definition of the constraint identified by
"<name>" failed because the table being altered already contains rows
with duplicate values for the PRIMARY KEY or UNIQUE clause columns.

"<name>" is the constraint name, if specified. If a constraint name was
not specified, "<name>" is the first column name specified in the
primary key or unique constraint clause followed by three periods.

The statement cannot be processed. The specified table is not altered.

User response: 

Remove the erroneous rows from the table before attempting to define the
primary or unique key.

 sqlcode: -673

 sqlstate: 23515
