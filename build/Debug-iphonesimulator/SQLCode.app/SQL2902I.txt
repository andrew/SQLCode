

SQL2902I  The ingest utility completed at timestamp "<timestamp>".
      Number of errors: "<number>". Number of warnings: "<number>".

Explanation: 

The ingest utility completed successfully, but returned some errors or
warnings.

The number of errors includes errors from which the utility could not
recover, but does not include errors from which the utility recovered.

User response: 

If you do not want to receive this message in the future, correct the
cause of the errors or warnings.
