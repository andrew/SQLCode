

SQL16064N  The argument passed to the function fn:exactly-one is not
      valid because the sequence is empty or contains more than one
      item. Error QName=err:FORG0005.

Explanation: 

A sequence that was passed as an argument to the function fn:exactly-one
does not contain exactly one item.

User response: 

Modify the expression to ensure that the sequence passed to the function
fn:exactly-one contains exactly one item.

 sqlcode: -16064

 sqlstate: 10608
