

SQL0356N  The index was not created because a key expression was
      invalid. Key expression: "<expression-number>". Reason code:
      "<reason-code>".

Explanation: 

You can create a table index which includes expression-based keys. This
message is returned when an attempt is made to create an index that
includes expression-based keys, and one of the following conditions
exists:

*  There is something invalid about one of the expression-based key
   definitions
*  The table does not support expression-based keys

The runtime token "<expression-number>" identifies which key expression
in the statement is invalid. For example, if there are two key
expressions in the CREATE INDEX statement, and the second key expression
is invalid, the value of "<expression-number>" will be: "2". If the
expression number cannot be determined, a default value of "*" will be
returned.

The reason code indicates what was invalid about the key expression:

1        

         The key expression contained a subquery.


2        

         The key expression did not contain a reference to at least one
         column.


3        

         The key expression referenced a special register or the key
         expression referenced a function that depends on the value of
         special register.


5        

         The key expression included a user-defined function.


6        

         The same key expression appears more than once in the index
         definition.


15       

         The key expression referenced a global variable or the key
         expression referenced a function that depends on the value of a
         global variable.


16       

         The key expression referenced a sequence.


17       

         The key expression referenced an unsupported type of function:

          
         *  A non-deterministic function
         *  A function with external actions
         *  A function that has an unsupported access level: 
            *  READ SQL DATA
            *  MODIFIES SQL DATA


18       

         The key expression included a value with an unsupported data
         type.


19       

         The key expression included an aggregate function or an OLAP
         specification.


20       

         The SCOPE clause was included in the index definition and the
         key expression contained one of the following:

          
         *  Dereference operation
         *  A TYPE predicate
         *  A CAST specification


21       

         The key expression includes an XMLQUERY expression or an
         XMLEXISTS expression.


22       

         An attempt was made to create an expression-based index on a
         table that does not support expression-based indexes.


23       

         The key expression referenced an unsupported type of function
         such as the LIKE predicate.


24       

         The result data type of the expression-based key is not
         indexable.

User response: 

Correct the error in the key expression, and reissue the statement.

sqlcode: -356

sqlstate: 429BX


   Related information:
   CREATE INDEX statement
