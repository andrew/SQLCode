

SQL1217N  The REAL data type is not supported by the target database.

Explanation: 

An SQL operation is using a data type of REAL (single-precision floating
point number) as an input or output variable. The REAL data type is not
supported on the target database for this request.

The statement is not processed.

User response: 

Replace the declaration of any host variable that corresponds to the SQL
data type of REAL with a declaration that corresponds to an SQL data
type of DOUBLE in your application.

 sqlcode: -1217

 sqlstate: 56099
