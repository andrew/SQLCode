

SQL5134N  The configuration parameter tpname contains invalid
      characters.

Explanation: 

One or more characters in the tpname is not in the valid range. The
characters in the tpname must be one of the following: 
*  A - Z
*  a - z
*  0 - 9
*  $
*  #
*  @
*  . (period)

User response: 

Change the tpname and retry the command or function call.
