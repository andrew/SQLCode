

SQL16016N  The attribute name "<attribute-name>" cannot be used more
      than once in an element constructor. Error QName=err:XQDY0025.

Explanation: 

An XQuery expression uses an "<attribute-name>" more than once in an
element constructor. This is not allowed because the attribute names
that are used in an element constructor must be unique.

The XQuery expression cannot be processed.

User response: 

Specify a unique name for each attribute.

 sqlcode: -16016

 sqlstate: 10503
