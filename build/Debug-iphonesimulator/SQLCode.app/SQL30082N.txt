

SQL30082N  Security processing failed with reason "<reason-code>"
      ("<reason-string>").

Explanation: 

An error occurred during security processing. The cause of the security
error is described by the "<reason-code>" and corresponding
"<reason-string>" value.

The following is a list of reason codes and corresponding reason
strings:

0 (NOT SPECIFIED)
         

         The specific security error is not specified.


1 (PASSWORD EXPIRED)
         

         The password specified in the request has expired.


2 (PASSWORD INVALID)
         

         The password specified in the request is not valid.


3 (PASSWORD MISSING)
         

         The request did not include a password.


4 (PROTOCOL VIOLATION)
         

         The request violated security protocols.


5 (USERID MISSING)
         

         The request did not include a userid.


6 (USERID INVALID)
         

         The userid specified in the request is not valid.


7 (USERID REVOKED)
         

         The userid specified in the request has been revoked.


8 (GROUP INVALID)
         

         The group specified in the request is not valid.


9 (USERID REVOKED IN GROUP)
         

         The userid specified in the request has been revoked in the
         group.


10 (USERID NOT IN GROUP)
         

         The userid specified in the request is not in the group.


11 (USERID NOT AUTHORIZED AT REMOTE LU)
         

         The userid specified in the request is not authorized at the
         remote Logical Unit.


12 (USERID NOT AUTHORIZED FROM LOCAL LU)
         

         The userid specified in the request is not authorized at the
         remote Logical Unit when coming from the local Logical Unit.


13 (USERID NOT AUTHORIZED TO TP)
         

         The userid specified in the request is not authorized to access
         the Transaction Program.


14 (INSTALLATION EXIT FAILED)
         

         The installation exit failed.


15 (PROCESSING FAILURE)
         

         Security processing at the server failed.


16 (NEW PASSWORD INVALID)
         

         the password specified on a change password request did not
         meet the server's requirements.


17 (UNSUPPORTED FUNCTION)
         

         the security mechanism specified by the client is invalid for
         this server. Some typical examples:

          
         *  The client sent a new password value to a server that does
            not support the change password function.
         *  The client sent SERVER_ENCRYPT authentication information to
            a server that does not support password encryption.
            Authentication type catalog information must be the same at
            the server and the client.
         *  The client sent a userid (but no password) to a server that
            does not support authentication by userid only.
         *  The client has not specified an authentication type, and the
            server has not responded with a supported type. This might
            include the server returning multiple types from which the
            client is unable to choose.
         *  The CLIENT AUTHENTICATION type is not supported by "IBM Data
            Server Driver for ODBC and CLI" and "IBM Data Server Driver
            package"


18 (NAMED PIPE ACCESS DENIED)
         

         The named pipe is inaccessible due to a security violation.


19 (USERID DISABLED or RESTRICTED)
         

         The userid has been disabled, or the userid has been restricted
         from accessing the operating environment at this time.


20 (MUTUAL AUTHENTICATION FAILED)
         

         The server being contacted failed to pass a mutual
         authentication check. The server is either an imposter, or the
         ticket sent back was damaged.


21 (RESOURCE TEMPORARILY UNAVAILABLE)
         

         Security processing at the server was terminated because a
         resource was temporarily unavailable. For example, on AIX, no
         user licenses may have been available.


24 (USERNAME AND/OR PASSWORD INVALID)
         

         The username specified, password specified, or both, are
         invalid. Some specific causes are:

          
         1. If you have recently changed permissions on DB2 critical
            files such as db2ckpw or moved to a new Fixpak, the db2iupdt
            command which updates the instance might not have been run.
         2. The username being used might be in an invalid format. For
            example, on UNIX and Linux platforms, usernames must be all
            be lowercase.
         3. An error might have been made in specifying the catalog
            information. For example, the correct authentication type
            might not have been specified or, if applicable, the remote
            server might not have been cataloged on the local system.
            For more information on authentication, search the DB2
            Information Center
            (http://publib.boulder.ibm.com/infocenter/db2luw/v9) using
            terms such as "authentication".


25 (CONNECTION DISALLOWED)
         

         The security plugin has disallowed the connection.


26 (UNEXPECTED SERVER ERROR)
         

         The server security plugin encountered an unexpected error. The
         administration notification log file on the server contains
         more specific problem information. The following are examples
         of issues that can cause problems:

          
         *  The security service was not started.
         *  The userid starting the DB2 service did not have admin
            privileges.


27 (INVALID SERVER CREDENTIAL)
         

         The server security plugin encountered an invalid server
         credential.


28 (EXPIRED SERVER CREDENTIAL)
         

         The server security plugin encountered an expired server
         credential.


29 (INVALID CLIENT SECURITY TOKEN)
         

         The server security plugin encountered an invalid security
         token sent by the client.


30 (CLIENT PLUGIN MISSING API)
         

         The client security plugin is missing a required API.


31 (WRONG CLIENT PLUGIN TYPE)
         

         The client security plugin is of the wrong plugin type.


32 (UNKNOWN CLIENT GSS-API PLUGIN)
         

         The client security plugin does not have a matching GSS-API
         security plugin available for connection to the database.


33 (UNABLE TO LOAD CLIENT PLUGIN)
         

         The client security plugin cannot be loaded.


34 (INVALID CLIENT PLUGIN NAME)
         

         The client security plugin name is invalid.


35 (INCOMPATIBLE CLIENT PLUGIN API VERSION)
         

         The client security plugin reports an API version that is
         incompatible with DB2.


36 (UNEXPECTED CLIENT ERROR)
         

         The client security plugin encountered an unexpected error.


37 (INVALID SERVER PRINCIPAL NAME)
         

         The server security plugin encountered an invalid principal
         name.


38 (INVALID CLIENT CREDENTIAL)
         

         The client security plugin encountered an invalid client
         credential.


39 (EXPIRED CLIENT CREDENTIAL)
         

         The client security plugin encountered an expired client
         credential.


40 (INVALID SERVER SECURITY TOKEN)
         

         The client security plugin encountered an invalid security
         token sent by the server.


41 (SWITCH USER INVALID)
         

         The client is configured to request a trusted connection and
         switch user in the trusted connection. A trusted connection was
         not established and so the switch user request is invalid.


42 (ROOT CAPABILITY REQUIRED)
         

         Authentication using local client or server passwords is not
         currently enabled.


43 (NON-DB2 QUERY MANAGER PRODUCT DISALLOWED CONNECTION)
         

         A non-DB2 query manager product has disallowed the connection.

User response: 

Ensure that the proper userid and/or password is supplied.

The userid may be disabled, the userid may be restricted to accessing
specific workstations, or the userid may be restricted to certain hours
of operation.

17       

         Retry the command with a supported authentication type. Ensure
         that catalog information specifies the correct authentication
         type. For more information on authentication, search the DB2
         Information Center
         (http://publib.boulder.ibm.com/infocenter/db2luw/v9) using
         terms such as "authentication".


20       

         Make sure the authentication mechanism for the server is
         started, and retry.


24       

         Solutions to specific problem causes described previously in
         this message are:

          
         1. Run DB2IUPDT <InstName> to update the instance.
         2. Ensure that the username created is valid. Review the DB2
            General Naming Rules.
         3. Ensure that catalog information is correct.


25       

         Change the database name used for the connection or the TCP/IP
         address used to connect to this database.


26       

         Fix the problem identified by the plugin error message text in
         the administration notification log. For more information on
         using security plugins, examples of security plugins, and
         additional troubleshooting information search the DB2
         Information Center
         (http://publib.boulder.ibm.com/infocenter/db2luw/v9) using
         phrases such as "security plugins".

          

         If you are unable to correct the problem, invoke the
         Independant Trace Facility and retry the scenario to collect
         information for IBM support.


27       

         Verify that the server credential is provided during security
         plugin initialization and that it is in a format recognized by
         the security plugin. As the credential will be used to accept
         contexts, it must be an ACCEPT or BOTH credential.


28       

         Contact your DBA. The server's credential must be renewed
         before the command is resubmitted. If renewing alters the
         credential handle, then a db2stop and db2start will be
         necessary. For information on how to renew your server's
         credential, consult the documentation available for the
         authentication mechanism used by the security plugin.


29       

         Resubmit the statement. If the problem still exists, then
         verify that the client security plugin is generating a valid
         security token.


30       

         Check the administration notification log file for the name of
         the required missing API. Add the missing API to the security
         plugin.


31       

         Specify the right type of security plugin in the appropriate
         database manager configuration parameter. For example, do not
         specify a userid-password based security plugin for the
         SRVCON_GSSPLUGIN_LIST database manager configuration parameter.


32       

         Install the matching security plugin that the database server
         used on the client. Ensure that the indicated security plugin
         is located in the client-plugin directory.


33       

         Check the administration notification log file on the client
         for more information. Fix the problem identified by the error
         message text in the administration notification log.


34       

         Specify a valid security plugin name. The name should not
         contain any directory path information.


35       

         Ensure that the security plugin is using a supported version of
         the APIs and that it is reporting a correct version number. For
         more information about supported versions, see the DB2
         information center
         (http://publib.boulder.ibm.com/infocenter/db2luw/v9) and search
         for "Security plug-in API versioning".


36       

         Check the administration notification log file on the client
         for more information. Fix the problem identified by the error
         message text in the administration notification log.


37       

         Check the administration notification log file for the
         principal name. Make sure the principal name is in a format
         that is recognized by the security plugin.


38       

         Verify that the client credential (generated by
         db2secGenerateInitialCred or provided as an inbound delegated
         credential) is in a format recognized by the security plugin.
         As the credential will be used to initiate contexts, it must be
         an INITIATE or BOTH credential.


39       

         The user issuing the statement must obtain the appropriate
         credentials (or re-obtain their initial credentials) and then
         resubmit the statement.


40       

         Resubmit the statement. If the problem still exists, then
         verify that the server security plugin is generating a valid
         security token.


41       

         Re-establish a trusted connection with valid credentials and
         re-submit a switch user request.


42       

         To enable local client or server authentication for non-root
         installations, the system administrator must run the db2rfe
         script. Alternatively, authentication can be done using a
         security plugin.


43       

         If additional explanation is required, contact your
         administrator for the query manager product.

sqlcode: -30082

sqlstate: 08001
