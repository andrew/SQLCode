

SQL4719N  The PREVENT EXECUTION work action "<work-action_name>" is
      applied to this activity, and so the activity was not run.

Explanation: 

A PREVENT EXECUTION work action is preventing the activity from running.

User response: 

Either remove or disable the PREVENT EXECUTION work action.

sqlcode: -4719

sqlstate: 5U033
