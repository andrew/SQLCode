

SQL0634N  The delete rule of FOREIGN KEY "<name>" must not be CASCADE
      (reason code = "<reason-code>").

Explanation: 

The CASCADE delete rule specified in the FOREIGN KEY clause of the
CREATE TABLE or ALTER TABLE statement is not valid for one of the
following reason codes: 
*  (01) A self-referencing constraint exists with a delete rule of SET
   NULL, NO ACTION or RESTRICT.
*  (02) The relationship would form a cycle that would cause a table to
   be delete-connected to itself. One of the existing delete rules in
   the cycle is not CASCADE, so this relationship may be definable if
   the delete rule is not CASCADE.
*  (03) The relationship would cause another table to be
   delete-connected to the same table through multiple paths with
   different delete rules or with delete rule equal to SET NULL. 

   "<name>" is the constraint name, if specified, in the FOREIGN KEY
   clause. If a constraint name was not specified, "<name>" is the first
   column name specified in the column list of the FOREIGN KEY clause
   followed by three periods.

The statement cannot be processed.

User response: 

If possible, change the delete rule.

 sqlcode: -634

 sqlstate: 42915
