

SQL2424N  The backup could not succeed because asynchronous copy
      operations at DB2 Data Links Managers are not complete.

Explanation: 

The TSM or vendor supplied archive server might not be in operational
state.

User response: 

Ensure TSM or the vendor supplied archive server is in an operational
state and resubmit the backup command.
