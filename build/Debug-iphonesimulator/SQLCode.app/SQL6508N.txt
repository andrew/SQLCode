

SQL6508N  The program failed to create the output pipe for the ftp
      process.

Explanation: 

If input data files are remote, they will be transferred to a local
pipe. If this local pipe already exists, the process will fail.

User response: 

Please ensure your working space is clean.
