

SQL0503N  A column cannot be updated because it is not identified in the
      FOR UPDATE clause of the SELECT statement of the cursor.

Explanation: 

Using a cursor, the program attempted to update a value in a table
column that was not identified in the FOR UPDATE clause in the cursor
declaration or the prepared SELECT statement.

Any column to be updated must be identified in the FOR UPDATE clause of
the cursor declaration.

The statement cannot be processed.

User response: 

Correct the application program. If the column requires updating, add
its name to the FOR UPDATE clause of the cursor declaration.

 sqlcode: -503

 sqlstate: 42912
