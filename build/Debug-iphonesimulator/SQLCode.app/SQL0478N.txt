

SQL0478N  The statement failed because one or more dependencies exist on
      the target object. Target object type: "<object-type1>". Name of
      an object that is dependent on the target object: "<object-name>".
      Type of object that is dependent on the target object:
      "<object-type2>".

Explanation: 

When object B is said to be dependent on object A, actions taken on
object A could significantly affect object B. For example, when you drop
a table any objects that are directly or indirectly dependent on that
table could be either deleted or made inoperative. Because of these
possible outcomes for dependent objects, some types of actions fail if
there are dependencies on the object that is target of the action.

This message is returned when an attempt to drop, alter, transfer
ownership of, or revoke privileges on a target object fails because
other objects directly or indirectly depend on that target object.

Examples of scenarios in which this message can be returned:

*  If "<object-type1>" is ALIAS, the dependencies for DROP might include
   a row permission or a column mask which references this alias.
*  If "<object-type1>" is SYNONYM, the dependencies for DROP might
   include a row permission or a column mask which references this
   synonym.
*  If "<object-type1>" is VIEW, the dependencies for DROP might include
   a row permission or a column mask which references this view.
*  If "<object-type1>" is TABLE: 
   *  Dependencies for DROP might include a row permission or a column
      mask which references this table.
   *  A system-period temporal table might exist that uses this table as
      the corresponding history table.
   *  If "<object-type2>" is INDEX, there might be an expression-based
      index that depends on the columns of the table.

*  If "<object-type1>" is TABLESPACE, a system-period temporal table
   might exist for which the corresponding history table resides in this
   table space.
*  For a DB2 for z/OS server, if "<object-type1>" is DATABASE, a
   system-period temporal table might exist for which the corresponding
   history table resides in this database.

User response: 

1. Determine which objects have direct or indirect dependencies on the
   target object by using one or both of the following methods: 
   *  Consult the system catalogs.
   *  Use the GET_DEPENDENCY procedure.

2. Remove the dependencies that blocked the statement.
3. Reissue the statement.

sqlcode: -478

sqlstate: 42893


   Related information:
   Statement dependencies when changing objects
   DROP statement
   GET_DEPENDENCY procedure - List objects dependent on the given object
