

SQL6565I  Usage: db2xxld [-config config-file] [-restart] [-terminate]
      [-help]

Explanation: 


*  The '-config' option will run this program using a user specified
   configuration file; the default is autoload.cfg.
*  The '-restart' option will run this program in restart mode; the
   configuration file should not be modified since the last incomplete
   AutoLoader job.
*  The '-terminate' option will run this program in terminate mode; the
   configuration file should not be modified since the last incomplete
   AutoLoader job.
*  The '-help' option will generate this help message.

The AutoLoader configuration file is a user supplied file which contains
the LOAD command to be executed, the target database, and several
optional parameters that the user may specify. The sample configuration
file, 'AutoLoader.cfg', supplied in the samples directory contains
inline comments which describe the available options and their defaults.
When running this program with '-restart' and '-terminate' option, user
should not modify the configuration file from their last incomplete job.

User response: 

Refer to the DB2 documentation for further details about the AutoLoader
utility.
