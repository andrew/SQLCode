

SQL20512N  No alert has been registered previously with the
      DBMS_ALERT.REGISTER procedure.

Explanation: 

No alert has been registered previously with the DBMS_ALERT.REGISTER
procedure for the current session.

User response: 

Call the DBMS_ALERT.REGISTER procedure to register an alert.

sqlcode: -20512

sqlstate: 5UA04
