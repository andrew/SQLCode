

SQL3143W  The maximum length of variable length column "<column-number>"
      exceeds the limitation of 240 bytes. Data from the column may be
      truncated.

Explanation: 

The LOTUS 1-2-3** and Symphony** programs have a limit of 240 bytes for
label records. Whenever a character field longer than 240 bytes is
written to a worksheet format (WSF) file, the data will be truncated to
240 bytes.

Continue processing. Subsequent data entries for the column may be
truncated.

User response: 

Verify the output. If significant data from the column is lost because
of truncation, investigate selecting the data from the column in several
fields by substringing, or redesign the database.
