

SQL27939I  Record counts for output partitions: partition number
      "<partitionnum>". Record count: "<numofrecords>".

Explanation: 

This informational message indicates the number of records processed for
a given partition.

User response: 

No action is required.
