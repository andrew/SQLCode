

SQL1744N  The RESTORE command failed because the target database already
      exists and encryption options were specified.

Explanation: 

If you restore a backup image into an existing database, you cannot
specify encryption options as part of the RESTORE command.

User response: 

Remove all encryption options from the command and run the command
again.


   Related information:
   RESTORE DATABASE command
