

SQL1567N  An exclusive connection to a single database partition cannot
      be made in this environment

Explanation: 

In a DB2 pureScale environment, connecting to a single database
partition in exclusive mode is not supported.

User response: 

Do not specify the ON SINGLE DBPARTITIONNUM clause on the CONNECT
statement.
