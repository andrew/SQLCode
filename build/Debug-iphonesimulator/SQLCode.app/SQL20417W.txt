

SQL20417W  The SQL compilation completed without connecting to the data
      source "<data-source-name>". Connection error "<error-text>" was
      encountered.

Explanation: 

The federated server could not connect to the data source
"<data-source-name>" during SQL compilation to determine which features
the data source supports. The SQL query has been compiled using default
settings. At runtime an error may be received because the remote
server's capabilities were not correctly determined at compilation time.
The "<error-text>" contains information about what connection error was
encountered.

User response: 

Recompile the statement or bind the package again when the data source
is available, or if an error tolerant nested table expression is used in
the SQL statement, execute the package immediately after the compilation
to reduce the chance of the connection state changing between
compilation and runtime. Use the information in "<error-text>" to
resolve the error connecting to the data source, if necessary. For
information on testing the connection to a data source server, search
for "testing connection server" in the Information Center.

sqlcode: +20417

sqlstate: 01689
