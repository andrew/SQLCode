

SQL1155W  A numeric value that is out of range for the data type has
      been replaced with the maximum or minimum value, as appropriate.

Explanation: 

The value to be assigned for a column of a result set from the ADMIN_CMD
procedure either is greater than the maximum value for the data type of
the column or is less than the minimum value for the data type. If the
value was greater than the maximum value, then the maximum value for the
data type has been assigned. If the value was less than the minimum
value, then the minimum value for the data type has been assigned. For
an SMALLINT data type, the value assigned for the maximum is 32767 and
the mimimum is -32768. For an INTEGER data type, the value assigned for
the maximum is 2147483647 and the mimimum is -2147483648. For a BIGINT
data type, the value assigned for the maximum is 9223372036854775807 and
the mimimum is -9223372036854775808.

User response: 

See the db2diag log file for the actual values returned by the ADMIN_CMD
procedure.

sqlcode: +1155

sqlstate: 01608
