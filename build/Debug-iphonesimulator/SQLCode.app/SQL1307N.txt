

SQL1307N  An error occurred during invocation of the security audit
      facility. Reason Code: "<reason-code>".

Explanation: 

The invocation of the security audit API caused an error to occur.
Reasons:

1. Audit is already started.
2. Audit is already stopped.
3. Invalid checksum for audit configuration file.
4. The default or user-provided audit pathname is too long.
5. Could not update the audit configuration file. Either the filesystem
   is full or the permissions do not allow write.
6. Configuration File not found. Either the file or the directory which
   contains the file does not exist.
7. The extract file is not found.
8. Invalid format of audit records during extract. The file is
   corrupted.
9. The file does not exist.
10. Access to the file is denied due to file permissions.
11. The active audit log has already been archived and new events can
   not be logged to it.
12. Out of disk space (disk full).
13. File I/O error during Extract or Archive.
14. Extract can not be performed on the active audit log file.

User response: 

The system administrator should take specific actions for each of the
reasons.

1. No action required.
2. No action required.
3. Restore the configuration file from backup or issue 'audit reset'
   command.
4. Choose a different audit pathname which is within the filename length
   limit.
5. If the file permissions are incorrect,set the permissions to allow
   write by owner. If the filesystem is full, create free space before
   proceeding.
6. If the audit configuration file is missing, then restore it from a
   backup or issue the 'reset' command to initialize the file to
   defaults. If the directory is missing, restore from a backup or
   recreate the database manager instance.
7. Verify that file exists in the specified path. If file missing,
   restore from backup if available.
8. The audit log file is most likely corrupted. If problem persists for
   other audit log files, notify DB2 service.
9. Verify that the active log files to be archived or the archived log
   files to be extracted exist in the specified path.
10. Correct the file permissions within the file system.
11. An archived audit log file has been renamed to the active audit log
   file. The active audit log file must be renamed back to the archived
   audit log file.
12. Ensure enough disk space is available.
13. Check the db2diag log file for details.
14. Extract must be run on an archived audit log file. Archive the
   active audit log file before extracting from it.
