

SQL3009N  The Action String parameter is not valid or too long.

Explanation: 

The Action String (for example, "REPLACE into ..." for Export or "INSERT
into ..." for Import and Load) parameter in the command is not valid.
The Action String pointer may be incorrect. The Action String structure
may contain characters that are not valid. The Action String structure
may contain characters that are not valid. Either the deprecated or new
parameter may be used for Action String.

The command cannot be processed.

User response: 

Verify the Action String pointer and the structure it points to.
Resubmit the command with a valid Action String.
