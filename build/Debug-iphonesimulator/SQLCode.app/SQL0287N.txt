

SQL0287N  SYSCATSPACE cannot be used for user objects.

Explanation: 

The CREATE TABLE or GRANT USE OF TABLESPACE statement specified a table
space named SYSCATSPACE which is reserved for catalog tables.

User response: 

Specify a different table space name.

 sqlcode: -287

 sqlstate: 42838
