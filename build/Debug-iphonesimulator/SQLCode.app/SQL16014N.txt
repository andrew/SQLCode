

SQL16014N  The value of the namespace declaration attribute must be a
      literal string. Error QName=err:XQST0022.

Explanation: 

An XQuery expression contains a namespace declaration attribute that is
not a literal string. The value of a namespace declaration attribute
must be either a literal string containing a valid URI, or a zero-length
string.

The XQuery expression cannot be processed.

User response: 

Specify a literal string for the value of the namespace declaration
attribute.

 sqlcode: -16014

 sqlstate: 10502
