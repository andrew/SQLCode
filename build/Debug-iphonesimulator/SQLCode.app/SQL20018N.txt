

SQL20018N  Row function "<function-name>" must return at most one row.

Explanation: 

The function is a defined to return a single row. The result of the
processing the function is more than one row.

User response: 

Ensure that the function is defined in such a way that at most one row
is returned.

 sqlcode: -20018

 sqlstate: 21505
