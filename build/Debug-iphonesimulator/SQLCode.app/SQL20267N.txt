

SQL20267N  The function "<function-name>" (specific "<specific-name>")
      modifies SQL data and is invoked in an illegal context. Reason
      code = "<reason-code>".

Explanation: 

The function "<function-name>" with specific name "<specific-name>" is
defined with the MODIFIES SQL DATA property. Functions with this
property are only allowed as the last table reference in a
select-statement, common-table-expression, RETURN statement that is a
subselect, SELECT INTO statement, or row-fullselect in a SET statement.
As well, each argument to the table function must be correlated to a
table reference in the same FROM clause as the table function, and every
table reference must be correlated to by some argument in the table
function. An argument is correlated to a table reference when it is a
column of that table reference.

Reason-code:

1. There are table references following the table function.
2. The table function is not referenced in the outer most subselect.
3. The table function is preceded by a table reference which is not
   referenced by a function argument.
4. The table function is used in the body of a view definition.
5. The table function is used in a fullselect within an XQuery context.
6. The table function is referenced by an OUTER JOIN operator or is
   nested within other explicit joins (using parentheses in a
   joined-table clause).
7. The function is referenced in the default clause for a global
   variable or parameter of a procedure definition.

The statement cannot be processed.

User response: 

1. Rewrite the query so that the table function is the last table
   reference in the FROM clause.
2. Move the table function to be in the outermost subselect.
3. Remove the table reference not correlated to in the table function or
   include an argument in the table function that correlates to the
   table reference.
4. Remove the table function from the body of the view definition.
5. Remove the table function from the fullselect in the XQuery context.
6. Remove the OUTER JOIN operator or do not place table function in a
   nested explicit join.
7. Remove the function from the default clause for the global variable
   or parameter of a procedure definition.

For reason codes 1, 2, and 3, the query can be rewritten using a common
table expression to isolate the table function invocation. For example:

SELECT c1 FROM
   (SELECT c1 FROM t1, t2,
     TABLE(tf1(t1.c1) AS tf), t3)
      AS x, t4

Can be rewritten as:

WITH cte1 AS (SELECT c1 FROM t1,
   TABLE(tf1(t1.c1)) AS tf),
   x AS (SELECT c1 FROM t2, cte1, t3)
     SELECT c1 FROM x, t4;

sqlcode: -20267

sqlstate: 429BL
