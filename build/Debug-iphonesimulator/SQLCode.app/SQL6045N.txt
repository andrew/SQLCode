

SQL6045N  The datatype "<datatype-value>" of length "<datatype-length>"
      is not supported.

Explanation: 

The data type and data length are not supported for a partitioning key.

The utility stops processing.

User response: 

See the Administration Guide for information about data types. See the
API Reference for information about the Get Row Partitioning Information
API.
