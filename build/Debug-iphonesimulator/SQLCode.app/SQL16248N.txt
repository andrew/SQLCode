

SQL16248N  Error in annotation at or near line "<lineno>" in XML schema
      document "<uri>". Additional information for the error includes
      "<errordetails>".

Explanation: 

The annotated XML schema document "<uri>" contains an error in the
annotation at or near line number "<lineno>". Types of errors include:
invalid value, unknown elements or attributes inside an annotation,
mal-formed XML. Any available additional information on the type of
error or the erroneous value is provided in "<errordetails>".

The XML schema document can be determined by matching "<uri>" to the
SCHEMALOCATION column of catalog view SYSCAT.XSROBJECTCOMPONENTS.

The XML schema is not enabled for decomposition.

User response: 

Consult annotated XML schema documentation for list of legal annotations
and their syntax. Correct or remove the unknown annotation.

sqlcode: -16248

sqlstate: 225DE
