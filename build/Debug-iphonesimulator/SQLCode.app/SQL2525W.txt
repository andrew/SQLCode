

SQL2525W  Warning! Restoring to an existing database that is different
      from the database on the backup image, and the alias "<dbase>" of
      the existing database does not match the alias "<dbase>" of the
      backup image, but the database names are the same. The target
      database will be overwritten by the backup version. The
      Roll-forward recovery logs associated with the target database
      will be deleted.

Explanation: 

The database aliases of the target database and database image are not
the same, the database names are the same, and the database seeds are
not the same, indicating these are different databases. The target
database will be overwritten by the backup version. The Roll-forward
recovery logs associated with the target database will be deleted. The
current configuration file will be overwritten with the backup version.

User response: 

Return to the utility with the callerac parameter indicating processing
to continue or end.
