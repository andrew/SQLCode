

SQL2912N  The ingest utility failed because one or more fields are
      binary type but the length of those binary fields was not
      specified.

Explanation: 

You can stream data from files and pipes into DB2 database tables by
using the ingest utility. If any input fields are a binary data type,
the length of those binary fields must be specified with the INGEST
command or db2Ingest API call.

This message is returned when an attempt is made to ingest data that
contains binary fields and the length of those binary fields is not
specified.

User response: 

Perform the ingest operation again, specifying the length of any binary
fields.


   Related information:
   db2Ingest API- Ingest data from an input file or pipe into a DB2
   table.
   INGEST command
   Ingest utility restrictions and limitations
   Comparison between the ingest, import, and load utilities
