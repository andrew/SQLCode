

SQL1385N  The parameter "<parameter>" has been specified improperly for
      the redistribute operation. Reason code = "<reason-code>".

Explanation: 

The parameter named in the error message is invalid, incorrectly
specified, or is incompatible with one of the other redistribute options
specified.

Reason codes:

1        

         An invalid redistribution option was specified. The
         distribution option can be one of "U" (Uniform), "T" (Target
         map), "C" (Continue), or "A" (Abort).


2        

         The maximum number of partition numbers in a partition list
         must be less than or equal to the maximum number of partitions
         allowable in a cluster.


3        

         Invalid STOP AT parameter value. The STOP AT value must be ISO
         format, with a length of 26. The format must be
         "yyyy.mm.dd.hh-mm-ss-nnnnnn".


7        

         Invalid STATISTICS parameter value. The value must be either
         DB2REDIST_STAT_USE_PROFILE ('P') or DB2REDIST_STAT_NONE ('N').


8        

         The length of a table name is out of range. The maximum length
         of the name of each table is (SQL_MAX_IDENT + SQL_MAX_IDENT
         +2).


9        

         Invalid table option parameter. The table option value must be
         either DB2REDIST_TABLES_FIRST ('F') or DB2REDIST_TABLES_ONLY
         ('O').


10       

         Missing information from input parameter struct. Input struct
         db2RedistStruct can not be NULL. Also its field struct
         db2RedistIn can not be NULL.


11       

         Undefined database partition group name, or bad struct db2Char
         to database partition group name. Database partition group name
         must be provided. Struct db2Char which stores the database
         partition group name must have valid data. Check struct db2Char
         to database partition group name. When pioData is NULL, iLength
         has to be zero and vice versa.


12       

         The structure db2Char that is storing data distribution file
         name is not valid.


13       

         The structure db2Char that is storing target partition map file
         name is not valid.


14       

         The redistribute option 'T'(target map) was specified, but no
         target map file is specified.


15       

         When the redistribute option is 'T'(Target map) or 'A' (Abort),
         a data distribution file should not be specified.


16       

         When the redistribute option is 'U'(Uniform), 'C'(Continue) or
         'A' (Abort), the target map file should not bespecified.


17       

         When the redistribute option is 'T'(Target map), 'C'(Continue)
         or 'A' (Abort), the add partition list, drop partition list
         should be empty, and the add count and the drop count should be
         zero.


18       

         Redistribute options may not be specified more than once.


19       

         Wrong version number is passed into the API.


20       

         A typed table specified in the TABLE list is not the root table
         of the whole hierarchy.


21       

         Invalid DATA BUFFER parameter value. The value must be greater
         than 0, and less than the size of util_heap_sz db cfg
         parameter.

User response: 

Check the corresponding parameter based on the reason code, ensure that
a valid parameter is specified, then call the utility again.
