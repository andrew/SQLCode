

SQL3210N  Option "<option>" is not compatible with hierarchy in
      "<command-name>".

Explanation: 

"<option>" is not compatible with hierarchy in EXPORT, IMPORT, or LOAD.

User response: 

Please check the command syntax for hierarchical support.
