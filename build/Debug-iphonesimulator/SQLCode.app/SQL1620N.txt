

SQL1620N  Unable to flush event monitor. Reason code "<rc>".

Explanation: 

The event monitor could not be flushed. Possible reasons are indicated
by the following reason codes:

1. The event monitor is not active.
2. The event monitor is running at a pre-version 6 level of output, for
   which flush is not available.
3. The flush succeeded on some database partitions, but failed on at
   least one database partition.

User response: 

Actions, according to reason code, are provided as follows:

1. Ensure that the event monitor is active and, if necessary, issue a
   SET EVENT MONITOR evmonname STATE 1 statement to activate the event
   monitor.
2. If the event monitor is running at a pre-version 6 level of output,
   do not attempt to flush it.
3. If the flush failed on at least one database partition, check the
   db2diag log file for any probes from routines sqlm_bds_flush_monitor
   or sqlm_bds_flush_monitor_hdl which would indicate the partition
   experiencing the issue with the event monitor being flushed, take any
   required corrective actions (for example, make sure there is enough
   monitor heap on that partition, and for a write-to-table event
   monitor, ensure the table space has enough space on that partition),
   and then deactivate and reactivate the event monitor by issuing the
   following statements: 

   SET EVENT MONITOR evmonname STATE 0

    

   SET EVENT MONITOR evmonname STATE 1

sqlcode: -1620

sqlstate: 55034
