

SQL0159N  The statement references "<object>" which identifies a(n)
      "<object-type>" rather than a(n) "<expected-object-type>".

Explanation: 

The object "<object>" specified as part of the statement or command
refers to an object of type "<object-type>" instead of the expected type
"<expected-object-type>".

The type of the object provided with the statement or command must match
the type identified by "<expected-object-type>". For example:

*  If the statement is DROP ALIAS PBIRD.T1, then PBIRD.T1 must be an
   alias name.
*  If the "<object-type>" is TABLE, then it is possible that the type of
   table is incorrect for the statement issued.
*  A CREATE MASK or CREATE PERMISSION statement must name a base table
   that exists at the current server.

User response: 

Change the statement or command to properly match the type of object
identified by "<expected-object-type>".

sqlcode: -159

sqlstate: 42809
