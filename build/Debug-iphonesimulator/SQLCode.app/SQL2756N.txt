

SQL2756N  The configuration parameter named "<parameter-name>" could not
      be updated because another request to update the same
      configuration parameter is currently in progress.

Explanation: 

A cluster caching facility (CF) structure is a memory resource for a
database in a DB2 pureScale environment. The CF structures include Group
Buffer Pool (GBP), Shared Communication Area (SCA), and Lock (LOCK). The
corresponding configuration parameters for CF structures are CF_GBP_SZ,
CF_SCA_SZ, and CF_LOCK_SZ, respectively.

The database configuration parameter named CF_DB_MEM_SZ controls the
total CF memory limit for this database. All CF structure memory is
included within this limit.

If the value of the database configuration parameter named
"<parameter-name>" exceeds the parameter value of CF_DB_MEM_SZ, the
request cannot be completed and the operation will time-out.

This message is returned when an update to CF structure memory has not
completed and another request is made to update the memory for the same
CF structure.

User response: 

View the update request that is pending for the CF structure using the
GET DB CFG command with the SHOW DETAIL clause.

Ensure the values for memory of all CF structures are lower than the
value of CF_DB_MEM_SZ.

Wait for the update request that is pending to complete or time-out.

sqlcode: -2756

sqlstate: 5U052


   Related information:
   Configuring the cluster caching facility
