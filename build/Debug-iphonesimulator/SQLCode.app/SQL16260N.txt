

SQL16260N  XML schema annotations include no mappings to any column of
      any table or nickname.

Explanation: 

The XML schema contains no annotations that map an XML element or
attribute to any column of any table or nickname.

The XML schema is not enabled for decomposition since it provides no
information for performing decomposition.

User response: 

Add annotations to the XML schema so that at least one XML element or
attribute is mapped to a column of a table or nickname.

sqlcode: -16260

sqlstate: 225DE
