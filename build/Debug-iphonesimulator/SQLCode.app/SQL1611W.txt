

SQL1611W  No data was returned by Database System Monitor.

Explanation: 

None of the monitoring information requested by the users was available
at the time the Database System Monitor API call was issued. This can
occur when a requested database or application is inactive, or when a
monitoring group such as the Table group is turned OFF, and Table
information is requested.

User response: 

The command completed successfully, but no data is returned to the user.

The user should make sure that the databases or applications for which
monitoring is desired are active at the time the Database System Monitor
API is called, or that the desired monitoring groups are active.
