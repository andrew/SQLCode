

SQL0466W  The procedure "<procedure-name>" returns "<number-results>"
      result sets from the stored procedure.

Explanation: 

This message is returned as a result of issuing a CALL SQL statement. It
indicates that the stored procedure "<procedure-name>" has
"<number-results>" result sets associated with it.

The statement completed successfully.

User response: 

 None required.

 sqlcode: +466

 sqlstate: 0100C
