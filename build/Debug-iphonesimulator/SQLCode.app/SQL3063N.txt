

SQL3063N  The single-byte code page value "<value-1>" in the H record is
      not compatible with the single-byte code page value "<value-2>"
      for the application. The FORCEIN option was not specified.

Explanation: 

The single-byte code page value in the H record is not compatible with
the application code page value. When the FORCEIN option is not used,
the data cannot be loaded unless conversion from value 1 to value 2 is
supported.

The utility stops processing. No data is loaded.

User response: 

To load this data, resubmit the command with the FORCEIN option.
