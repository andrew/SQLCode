

SQL29014N  The value specified for DB2 registry variable
      "<registry-variable>" is invalid.

Explanation: 

The value specified for "<registry-variable>" is invalid because of the
following restrictions by "<registry-variable>": 

DQP_NTIER
         The value must be one of OFF, RUN[:timeout], or
         CHECK[:timeout].

DQP_LAST_RESULT_DEST
         The length must not exceed 32 characters.

DQP_TRACEFILE
         The length must not exceed 256 characters.

User response: 

Correct the value of the DB2 registry variable "<registry-variable>"
with the db2set command and resubmit the query.
