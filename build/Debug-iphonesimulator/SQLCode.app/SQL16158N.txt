

SQL16158N  XML schema contains an invalid relationship involving
      minLength, maxLength, or length facet value "<dt-length>" in a
      derived type compared to the base type value "<base-length>".
      Reason code = "<reason-code>".

Explanation: 

While parsing an XML document, the parser encountered an invalid
relationship between facets of a derived type and the base type. The
"<reason-code>" indicates which of the following conditions was found. 
1. The length value "<dt-length>" of the derived type is not equal to
   the length value "<base-length>" of the base type.
2. The minLength value "<dt-length>" of the derived type is less than or
   equal to the minLength value "<base-length>" of the base type.
3. The minLength value "<dt-length>" of the derived type is greater than
   the maxlength value "<base-length>" of the base type.
4. The maxLength value "<dt-length>" of the derived type is greater than
   the maxLength value "<base-length>" of the base type.
5. The maxLength value "<dt-length>" of the derived type is less than or
   equal to the minLength value "<base-length>" of the base type.
6. The length value "<dt-length>" of the derived type is less than the
   minLength value "<base-length>" of the base type.
7. The length value "<dt-length>" of the derived type is greater than
   the maxLength value of "<base-length>" the base type.
8. The minLength "<dt-length>" of the derived type is greater than the
   length value "<base-length>" of the base type.
9. The maxLength value "<dt-length>" of the derived type is less then
   the length value "<base-length>" of the base type.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16158

sqlstate: 2200M
