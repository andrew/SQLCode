

SQL2982W  The INGEST LIST command or INGEST GET STATS command did not
      return any information for the authorization ID that is currently
      connected to the database, because that authorization ID is not
      currently running any ingest operations on the current machine.
      Authorization ID: "<auth-ID>"

Explanation: 

You can monitor the progress of ingest operations being run by the
authorization ID that is connected to the database by using the INGEST
LIST command or the INGEST GET STATS command.

This message is returned when either the INGEST LIST command or the
INGEST GET STATS command was issued, but the authorization ID that is
currently connected to the database is not running any ingest operations
on the current machine. If the authorization ID recently started an
ingest operation from another session, the command might not have
completed initialization or might have already finished.

User response: 

Ensure that you are running the INGEST LIST or INGEST GET STATS command
on the same machine where you issued the INGEST command. If the
authorization ID recently started an ingest operation from another
session and the command has not yet completed, wait a few seconds for
the command to complete initialization. Then run the INGEST LIST command
or the INGEST GET STATS command again.


   Related information:
   db2Ingest API- Ingest data from an input file or pipe into a DB2
   table.
   INGEST command
   Monitoring ingest operations
