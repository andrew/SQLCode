

SQL3506W  The value specified in the null indicator in row
      "<row-number>" and column "<column-number>" is not valid. A value
      of 'N' will be assumed.

Explanation: 

For ASC files, a null indicator column can be specified for each data
column and should contain either a 'Y' or a 'N'. A 'Y' indicates that
the column is a null value and a 'N' indicates that the column contains
data. If neither of these values are in the null indicator column, it is
assumed to be a 'N' and data will be loaded into the column.

User response: 

If the data or the null indicator are incorrect, correct the input file
and resubmit the command.
