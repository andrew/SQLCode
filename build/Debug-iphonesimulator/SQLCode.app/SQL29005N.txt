

SQL29005N  User "<user-ID>" does not have an effective Query Patroller
      submitter profile.

Explanation: 

The user "<user-ID>" does not have an effective Query Patroller
submitter profile. This may occur due to one or more of the following
reasons: 
1. The user may not have a submitter profile.
2. The user and/or group submitter profile(s) belonging to the user may
   have been suspended.

User response: 

Request that the database administrator create a submitter profile or
have the submitter profile reactivated.
