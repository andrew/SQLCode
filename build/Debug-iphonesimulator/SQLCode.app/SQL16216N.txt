

SQL16216N  XML document is missing attribute "<attribute-name>" that
      must appear in "<element-type>" "<element-name>" declarations.

Explanation: 

While parsing an XML document the parser encountered an "<element-type>"
"<element-name>" declaration that is missing a required attribute
"<attribute-name>".

Parsing or validation did not complete.

User response: 

Add the missing attribute to the global or local declaration in the XML
document and try the operation again.

sqlcode: -16216

sqlstate: 2200M
