

SQL20518N  The operation is invalid because the UTL_SMTP module routine
      "<routine_name>" is called out of sequence.

Explanation: 

The SMTP protocol requires that the operations be performed in a
particular order. The routine "<routine-name>" was called but the SMTP
protocol requires that another operation be performed before the
operation this routine was attempting to perform can complete
successfully. For example, the UTL_SMTP.WRITE_DATA routine must be
called after the UTL_SMTP.OPEN_DATA routine completes successfully, and
the UTL_SMTP.RCPT routine must be called after the UTL_SMTP.MAIL routine
completes successfully.

User response: 

Refer to the SMTP protocol and ensure that the UTL_SMTP module routines
are called in the correct order.

sqlcode: -20518

sqlstate: 5UA0N
