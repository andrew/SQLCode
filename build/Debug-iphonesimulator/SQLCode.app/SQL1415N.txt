

SQL1415N  The statement was compiled for diagnostic purposes only and
      has not been executed.

Explanation: 

The statement was processed through parts of the system to collect
diagnostic information using service features. The necessary steps to
allow further processing of the statement have not been completed.

User response: 

This error is returned to prevent further processing by the system of
statements prepared using service features and is expected.
