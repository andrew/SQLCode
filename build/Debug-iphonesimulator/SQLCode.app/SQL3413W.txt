

SQL3413W  The field value in row "<row-number>" and column
      "<column-number>" is too short for the target column. A null was
      inserted.

Explanation: 

The value in the specified field is not acceptable because it is too
short for the target column. The value of the column number specifies
the byte location within the row where the field begins.

A null value is inserted.

User response: 

No action is required. If a null is not acceptable, correct the inner
field and resubmit the command, or edit the data in the table.
