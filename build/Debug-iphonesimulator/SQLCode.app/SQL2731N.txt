

SQL2731N  Error while reading from input data file "<filename>".

Explanation: 

An I/O error occurred while reading from input data file.

User response: 

Check your operating system documentation for file I/O errors.
