

SQL0285N  The indexes and/or long columns for table "<table-name>"
      cannot be assigned to separate table spaces because the primary
      table space "<tablespace-name>" is a system managed table space.

Explanation: 

If the primary table space is a system managed table space, all table
parts must be contained in that table space. A table can have parts in
separate table spaces only if the primary table space, index table space
and long table space are database managed table spaces.

User response: 

Either specify a database managed table space for the primary table
space, or do not assign the table parts to another table space.

 sqlcode: -285

 sqlstate: 42839
