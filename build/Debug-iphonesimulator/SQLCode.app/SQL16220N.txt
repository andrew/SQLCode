

SQL16220N  XML document contains an element or attribute "<name>" where
      NOTATION was used directly in the schema for that element or
      attribute.

Explanation: 

While parsing an XML document, the parser encountered an element or
attribute where the schema used NOTATION directly for that element or
attribute..

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16220

sqlstate: 2200M
