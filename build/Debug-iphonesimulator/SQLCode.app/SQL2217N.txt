

SQL2217N  The page size of the system temporary table space used by the
      REORG utility must match the page size of the table space(s) in
      which the table data resides (including the LONG or LOB column
      data). The cause is based on the following reason codes
      "<reason-code>".

Explanation: 

Following is a list of reason codes: 

1        The cause relates to selection of a temporary tablespace for
         the table's data.

2        The cause relates to selection of a temporary tablesace for the
         table's LONG or LOB data.

If the system temporary table was explicitly specified to the REORG
utility then the page size of the system temporary table space used by
the REORG utility must match the page size of the table space or table
spaces in which the table data, including LONG or LOB column data,
resides, or else an appropriate container for long data must be
specified. One of the following has violated this restriction: 
*  The table's data resides in a table space that has a different page
   size than that of the specified system temporary table space.
*  The table contains LONG or LOB columns whose data resides in a table
   space with a page size that is different than that of the system
   temporary table space and the table's regular data but no tablespace
   with the correct page size could be found for the LONG or LOB data
   objects.

If the system temporary table space or LONG temporary table space was
not specified to the REORG utility then the utility was looking
internally for a system temporary table space. A system temporary table
space that uses the same page size as the table data either did not
exist in the database or was not available at the time.

User response: 

If a system temporary table space that uses the same page size as the
table data does not exist in the database, please create a system
temporary table space using a page size that matches the page size of
the table data. If the page size of the table data differs from that of
the LOB or LONG data, ensure a system temporary tablespace using that
page size also exists.

If a system temporary table space that uses the same page size as the
table data exists in the database but was not available at the time of
your command, please reissue the command when the system temporary table
space is available.
