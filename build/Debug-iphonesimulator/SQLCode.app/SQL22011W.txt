

SQL22011W  Cannot find the configuration for "<object-name-or-type>".

Explanation: 

The object or object type does not have a specific or default
configuration of its own.

User response: 

No action required.
