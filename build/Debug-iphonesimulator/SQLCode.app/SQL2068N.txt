

SQL2068N  An invalid image was encountered on media "<media>". There was
      no media header.

Explanation: 

An invalid image was encountered during the processing of a database
utility. The utility was not able to locate a valid media header. The
utility stops processing.

User response: 

Resubmit the command with correct backup or copy images.
