

SQL27931I  Writing output partition map to file "<filename>".

Explanation: 

This is an informational message indicating that the output partition
map is written to "<filename>".

User response: 

No action is required.
