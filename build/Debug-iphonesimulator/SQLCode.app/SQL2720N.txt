

SQL2720N  Number of partition keys exceeded the maximum "256". This
      error was detected at line "<line>" of the configuration file.

Explanation: 

The number of partitioning keys defined cannot exceed the maximum limit:
256.

User response: 

Remove one or more partitioning keys defined in the configuration file.
