

SQL6047N  Database partition group cannot be redistributed because table
      "<name>" does not have a partitioning key.

Explanation: 

At least one table in the single-partition database partition does not
have a partitioning key. All tables in the single-partition database
must have a partitioning key before the database partition group can be
redistributed to a multiple-partition database partition group.

The operation was not performed.

User response: 

Use the ALTER TABLE statement to specify partitioning keys for tables
that do not have one. Then try the request again.

Alternatively, omit the tables that do not have partitioning keys by
specifying the EXCLUDE parameter in the REDISTRIBUTE DATABASE PARTITION
GROUP command.
