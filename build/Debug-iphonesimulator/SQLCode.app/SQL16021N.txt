

SQL16021N  The XQuery version declaration specifies a number
      "<version-number>" that is not supported. Error
      QName=err:XQST0031.

Explanation: 

DB2 XQuery does not support the XQuery "<version-number>" that is
specified in a version declaration.

The XQuery expression cannot be processed.

User response: 

Specify a "<version-number>" that is supported by DB2 XQuery. DB2 XQuery
currently supports XQuery version 1.0.

 sqlcode: -16021

 sqlstate: 10502
