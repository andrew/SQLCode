

SQL1792W  The statistics for the specified nicknames were not updated
      completely because of schema inconsistencies between the remote
      and local catalogs.

Explanation: 

The remote schema has changed. Either the remote table or view, or one
of its columns or the column data types have changed since the nickname
was created.

User response: 

Create a new nickname and resubmit the statement.

sqlcode: +1792

sqlcode: 01669
