

SQL6563N  Failed to retrieve the current user ID.

Explanation: 

The utility attempted to retrieve the current user ID for the ID, but an
error was encountered.

User response: 

Contact DB2 Service.
