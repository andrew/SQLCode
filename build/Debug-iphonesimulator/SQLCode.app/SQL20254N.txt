

SQL20254N  FOREIGN KEY "<name>" is not valid since it would cause table
      "<table-name>" to be delete-connected to itself through a cycle
      that contains a delete rule of either RESTRICT or SET NULL. Reason
      code = "<reason-code>".

Explanation: 

A referential cycle must not contain a delete rule of RESTRICT or SET
NULL. The delete rule specified for FOREIGN KEY "<name>" in the CREATE
TABLE or ALTER TABLE statement is not valid for the reason specified by
the "<reason-code>" as follows: 
1. The delete rule specified is RESTRICT or SET NULL and the referential
   relationship would cause table "<table-name>" to be delete-connected
   to itself.
2. The delete rule specified is CASCADE but the referential relationship
   would cause table "<table-name>" to be delete-connected to itself by
   a cycle that contains a delete rule of either RESTRICT or SET NULL.

 "<name>" is the constraint name, if specified, in the FOREIGN KEY
clause. If a constraint name was not specified, "<name>" is the first
column name specified in the column list of the FOREIGN KEY clause
followed by three periods.

The statement cannot be processed.

User response: 

The action corresponding to the reason code is: 
1. Change the delete rule to CASCADE or NO ACTION or eliminate the
   particular FOREIGN KEY clause from the CREATE TABLE or ALTER TABLE
   statement.
2. Change the delete rule to NO ACTION, RESTRICT, or SET NULL or
   eliminate the particular FOREIGN KEY clause from the CREATE TABLE or
   ALTER TABLE statement.

sqlcode: -20254

sqlstate: 42915
