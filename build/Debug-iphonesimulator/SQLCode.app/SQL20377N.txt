

SQL20377N  An illegal XML character "<hex-char>" was found in an SQL/XML
      expression or function argument that begins with string
      "<start-string>".

Explanation: 

An SQL/XML expression or function attempted to convert an SQL string
value from one of the arguments to an XML string, but the string
included a character at Unicode code point "<hex-char>" that is not a
legal XML 1.0 character. The character is included in a string that
begins with the string "<start-string>". The value for "<hex-char>"
represents the illegal character as a Unicode code point in the form
"#xH", where H is one or more hexadecimal characters. The following set
of Unicode characters (defined using a regular expression) are allowed:
#x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF].
Examples of the SQL/XML expression or function that may encounter this
error are XMLCAST, XMLELEMENT, XMLFOREST, XMLAGG, XMLDOCUMENT, XMLTEXT,
XMLATTRIBUTES, XMLQUERY, or XMLTABLE.

The statement cannot be processed.

User response: 

Remove the illegal character "<hex-char>" or replace it with a character
that is allowed.

sqlcode: -20377

sqlstate: 0N002
