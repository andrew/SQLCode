

SQL0604N  The length, precision, or scale attribute for column, distinct
      type, structured type, array type, attribute of structured type,
      routine, cast target type, type mapping, or global variable
      "<data-item>" is not valid.

Explanation: 

There is an error in the data type specification in a CREATE or ALTER
statement, or a CAST specification. An invalid length, precision or
scale attribute may have been specified, or it may be that the data type
itself is incorrect or not permitted in this context. The location of
the error is given by "<data-item>" as follows:

*  For a CREATE or ALTER TABLE statement, "<data-item>" gives the name
   of the column containing the error or the data type containing an
   error. If the column data type is a structured or XML data type, then
   the INLINE LENGTH value must be at least 292 and cannot exceed 32673.
   For a LOB data type, the INLINE LENGTH value must be at least the
   size of the LOB descriptor (see the CREATE TABLE statement) and
   cannot exceed 32673
*  For a CREATE FUNCTION statement, "<data-item>" is a token that
   identifies the area of the problem in the statement. For example,
   "PARAMETER 2" or "RETURNS" or "CAST FROM". In some cases, it may also
   be the data type containing the error.
*  For a CREATE DISTINCT TYPE statement, "<data-item>" gives the name of
   the type being defined or the source data type containing the error.
*  For a CREATE TYPE(array) statement, "<data-item>" gives the data type
   containing an error. The integer value specified inside the square
   brackets must be an integer greater than or equal to 1 and not larger
   than 2147483647.
*  For a CREATE or ALTER TYPE statement, "<data-item>" gives the type of
   the attribute containing the error or the name of the structured type
   having an incorrect inline length value. The inline length cannot be
   smaller than 292 and the size returned by the constructor function
   for the structured type.
*  For a CREATE VARIABLE statement, "<data-item>" gives the name of the
   variable having an incorrect data type. The data type of a global
   variable can be of any built-in data type with the exception of LONG
   types, LOBs, ARRAY, and structured types. For Version 10.1, the XML
   data type is still an exception. Support for XML data type was
   introduced in Version 10.1 Fix Pack 1. Distinct types and reference
   types are supported.
*  For CAST( expression AS data-type ), "<data-item>" is "CAST" or the
   data type containing the error.
*  For XMLCAST( expression AS data-type ), data-item is "XMLCAST" or the
   data type containing the error.
*  For a reverse type mapping, [p..p] expression cannot be used for the
   remote data type. For example, the following statement (reverse type
   mapping) is incorrect. 
   CREATE TYPE MAPPING tm1
          FROM SERVER drdasvr TYPE CHAR([1..255])
          TO SYSIBM.VARCHAR

    

   Whereas, the following statement (forward type mapping) is correct.

    

   CREATE TYPE MAPPING tm1
         TO SERVER drdasvr
         TYPE CHAR([1..255])
         FROM SYSIBM.VARCHAR

Federated system users: if the statement is a CREATE TYPE MAPPING
statement, an attempt was made to create a type mapping where a type
attribute for either the local data type or the remote data type is not
valid. Possible reasons include:

*  The local length/precision is set to 0 or a negative value.
*  The length/precision attribute is specified for data types such as
   date/time/timestamp, float, or integer.
*  The scale attribute is specified for data types such as character,
   date/time/timestamp, float, or integer.
*  The FOR BIT DATA clause is specified for a non-character type.
*  The remote precision is set to 0 for remote types other than Informix
   datetime.
*  An invalid field qualifier is being used in a type mapping for an
   Informix datetime type.
*  An ending value is lower than the starting value in a precision/scale
   range.

The statement cannot be processed.

User response: 

Correct the syntax and try again.

sqlcode: -604

sqlstate: 42611


   Related information:
   SQL statements
   XMLCAST specification
   CAST specification
