

SQL0193N  In an ALTER TABLE statement, the column "<column-name>" has
      been specified as NOT NULL and either the DEFAULT clause was not
      specified or was specified as DEFAULT NULL.

Explanation: 

When new columns are added to a table that already exists, a value must
be assigned to that new column for all existing rows. By default, the
null value is assigned. However, since the column has been defined as
NOT NULL, a default value other than null must be defined.

User response: 

Either remove the NOT NULL restriction on the column or provide a
default value other than null for the column.

 sqlcode: -193

 sqlstate: 42601
