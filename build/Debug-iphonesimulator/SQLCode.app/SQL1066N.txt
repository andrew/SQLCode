

SQL1066N  DB2START processing was successful. IPX/SPX protocol support
      was not successfully started.

Explanation: 

The IPX/SPX protocol support was not successfully started. Remote
clients cannot use IPX/SPX to connect to the server. Possible causes
are: 
*  The workstation is not logged in to the NetWare file server.
*  The workstation does not have authority to create an object in the
   NetWare file server bindery.
*  Another database manager on the network is using the same object name
   specified in the database manager configuration file.

User response: 

Ensure that the workstation is logged in to the NetWare file server, and
has sufficient authority to create an object in the bindery at the file
server. The user must be logged in as SUPERVISOR or equivalent. Also
ensure that the object name specified in the database manager
configuration file is unique for all database managers in the network.
Make any corrections, run DB2STOP, and then run DB2START again.

If the problem continues, at the operating system command prompt type
DB2TRC ON -L 0X100000. Run DB2START again, then at the command prompt,
type DB2TRC DUMP filename to save the trace information. To turn trace
off, type DB2TRC OFF. Contact your service coordinator with the trace
information.
