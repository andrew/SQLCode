

SQL4705N  The service subclass specified in the mapping work action
      "<work-action-name>" cannot be the default service subclass.

Explanation: 

The default service subclass cannot be specified when defining a work
action to map activities.

User response: 

Specify a different service subclass that is not the default service
subclass and try the request again.

sqlcode: -4705

sqlstate: 5U018
