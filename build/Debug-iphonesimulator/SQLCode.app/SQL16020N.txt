

SQL16020N  The context node in a path expression does not have an XQuery
      document node root. Error QName=err:XPDY0050.

Explanation: 

The root node above the context node in a path expression must be an
XQuery document node.

The XQuery expression cannot be processed.

User response: 

Change the each path expression so that the context node has a root node
that is an XQuery document node.

 sqlcode: -16020

 sqlstate: 10507
