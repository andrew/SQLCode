

SQL30074N  "<reply-identifier>" Reply is not supported.

Explanation: 

The client received a reply it does not recognize. The current
environment command or SQL statement cannot be processed successfully,
nor can any subsequent commands or SQL statements.

The current transaction is rolled back and the application is
disconnected from the remote database. The statement cannot be
processed.

User response: 

Record the message number (SQLCODE) and the reply identifier. Record all
error information from the SQLCA, if possible. Attempt to connect to the
remote database and rerun the application.

If sufficient memory resources exist and the problem continues, invoke
the Independent Trace Facility at the operating system command prompt.

Contact your technical service representative with the following
information:

Required information: 
*  Problem description
*  SQLCODE and reply identifier
*  SQLCA contents if possible
*  Trace file if possible.

 sqlcode: -30074

 sqlstate: 58018
