

SQL0650N  The ALTER statement was not executed. Reason code:
      "<reason-code>".

Explanation: 

The ALTER of an index or a table cannot be executed as specified.

The reason code indicates more specifically the nature of the problem:

23       

         Compression cannot be altered for the following indexes: MDC or
         ITC block indexes, global temporary table indexes, generated
         range-clustered table indexes, and index specifications.


30       

         An attempt was made to alter the replication-maintained
         materialized query table latency information table,
         SYSTOOLS.REPL_MQT_LATENCY.


31       

         An attempt was made to change a column in the base table of a
         replication-maintained materialized query table to a data type
         that is not supported in base tables of replication-maintained
         materialized query tables.

User response: 

Correct the ALTER statement to avoid the restriction indicated by the
reason code, and then issue the statement again.

sqlcode: -650

sqlstate: 56090


   Related information:
   ALTER TABLE statement
   ALTER INDEX statement
