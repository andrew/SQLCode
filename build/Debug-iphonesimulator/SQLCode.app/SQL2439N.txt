

SQL2439N  The routine could not be run because the routine definition
      has changed and the db2updvXX tool has not been used to update
      routine definitions.

Explanation: 

This message is returned when a new level has been applied but the
db2updvXX utility for that version of DB2 database (for example,
db2updv97, or db2updv10) has not be used to update the system routine
definitions.

User response: 

Run the appropriate db2updvXX command.
