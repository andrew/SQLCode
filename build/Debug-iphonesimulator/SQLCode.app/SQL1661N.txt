

SQL1661N  The query failed because the information you are trying to
      retrieve could not be found on the HADR standby database.

Explanation: 

The statement or command is trying to retrieve an XML value that is not
available on the HADR standby database. This might be because that value
has not yet been replayed on the standby.

User response: 

Retry your query on the read-enabled HADR standby later, or submit the
query against the HADR primary database.

sqlcode: -1661

sqlstate: 58004
