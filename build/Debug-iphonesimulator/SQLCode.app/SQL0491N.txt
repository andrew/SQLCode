

SQL0491N  The CREATE FUNCTION or ALTER MODULE statement used to define
      "<routine-name>" must have a RETURNS clause, and one of: the
      EXTERNAL clause (with other required keywords); an SQL function
      body; or the SOURCE clause.

Explanation: 

A required clause is missing in the definition of routine
"<routine-name>". If EXTERNAL was specified, one of the following
clauses must also be specified: LANGUAGE, PARAMETER STYLE.

If defining an SQL function, the SQL function body must be included
unless using the PUBLISH action of ALTER MODULE to define an SQL
function prototype.

User response: 

Add the missing clause, and then try again.

sqlcode: -491

sqlstate: 42601
