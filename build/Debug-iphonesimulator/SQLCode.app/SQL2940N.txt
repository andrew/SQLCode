

SQL2940N  The ingest utility does not support DB2 server versions
      earlier than version "<version>".

Explanation: 

The ingest utility does not support ingesting data into tables that are
on versions of the DB2 server earlier than the version shown in the
message.

User response: 

Connect to a DB2 server whose version is the same as or later than the
version shown in the message and re-run the utility.


   Related information:
   Supported languages and code pages
   INGEST command
