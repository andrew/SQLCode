

SQL5145W  The parameter update took effect however tuning of the
      AUTOMATIC memory parameters was deactivated because an
      insufficient number of parameters or buffer pools are set to
      AUTOMATIC.

Explanation: 

Self tuning of the AUTOMATIC memory parameters is active when 2 or more
tunable parameters or buffer pools are set to AUTOMATIC and
SELF_TUNING_MEM is ON. Self tuning was active but the configuration
update caused its deactivation.

User response: 

Self tuning of the AUTOMATIC memory parameters will resume when one or
more tunable parameters or buffer pools are set to AUTOMATIC.
