

SQL16153N  XML schema contains an attribute type definition with a
      default value or a fixed value that is different from the fixed
      value constraint of the reference type "<type-name>".

Explanation: 

While parsing an XML document, the parser encountered a mismatch in the
value constraint of the attribute and attribute reference type. Either
the attribute specified a default and the reference type is fixed or the
attribute specified a different fixed value than specified in reference
type "<type-name>".

Parsing or validation did not complete.

User response: 

Correct the value constraint of the attribute reference in the XML
document and try the operation again.

sqlcode: -16153

sqlstate: 2200M
