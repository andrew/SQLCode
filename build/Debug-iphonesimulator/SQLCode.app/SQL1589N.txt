

SQL1589N  The database connection failed because an operating system
      resource limit was reached.

Explanation: 

This message can be returned when there are more than 1024 simultaneous
local database connections, and as a result, an operating system limit
is reached. On AIX operating systems, no more than 1024 local database
connections established by a single process or application can exist
simultaneously.

This message can also be returned when the DB2 database manager
encounters an internal error while making operating system calls.

User response: 

Terminate any unneeded local database connections.

If terminating local database connections resolves the problem, prevent
reoccurrences of this error by modifying your applications to use fewer
simultaneous local connections. If your applications require many
database connections, use remote connections (or loopback connections
when the database server and client are on the same host machine) using
TCP/IP instead of local connections.

If reducing the number of local connections does not resolve this
problem, contact IBM software support for help with investigating the
cause of the problem.

sqlcode: -1589

sqlstate: 54067
