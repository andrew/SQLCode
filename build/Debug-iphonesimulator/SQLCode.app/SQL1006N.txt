

SQL1006N  The code page "<code-page>" of the application does not match
      the code page "<code-page>" of the database.

Explanation: 

The application could not connect to the database because the active
codepage is different from the one that was active when the database was
created.

The command cannot be processed.

User response: 

Exit the current application program and return to the operating system.
Change the code page for the process and restart the application
program.
