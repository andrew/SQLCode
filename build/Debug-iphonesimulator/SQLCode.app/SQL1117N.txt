

SQL1117N  A connection to or activation of database "<name>" cannot be
      made because of ROLL-FORWARD PENDING.

Explanation: 

The specified database is enabled for roll-forward recovery and it has
been restored but not rolled forward.

No connection was made.

Federated system users: this situation can also be detected by the data
source.

User response: 

Roll forward the database or indicate that you do not wish to roll
forward by using the ROLLFORWARD command. Note that if you do not roll
forward the database, the records written since the last backup of the
database will not be applied to the database.

Federated system users: if necessary isolate the problem to the data
source rejecting the request and take recovery action appropriate to
that data source to bring the data source to a point of consistency.

sqlcode: -1117

sqlstate: 57019


   Related information:
   Troubleshooting data source connection errors
