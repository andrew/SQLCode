

SQL1084C  The database manager failed to allocate shared memory because
      an operating system kernel memory limit has been reached.

Explanation: 

The default values for some kernel parameters on some operating systems
are not sufficient for running a DB2 database. If any other software is
running on the same system as the DB2 database, the other software will
compete for operating system resources, which makes configuring kernel
parameters more difficult. To simplify kernel parameter configuration,
the database manager automatically adjusts some kernel parameter
settings when an instance is started on some operating systems. Also,
there is a tool called db2osconf that you can use with some operating
systems to determine recommended minimum operating system kernel
settings for running a DB2 database system.

This message is returned when the database manager is unable to allocate
shared memory, during activities such as activating a database or
rolling a database forward, because an operating system kernel memory
limit, such SHMMAX on Linux, has been reached.

User response: 

Respond to this error by performing one or more of the following
troubleshooting steps:

*  Generate database configuration recommendations by running the
   following command: 
   DB2 AUTOCONFIGURE APPLY NONE

*  Increase the operating system kernel setting that limits the amount
   of operating system memory that is available for the database
   manager.
*  Decrease the amount of memory that the database uses by decreasing
   the database_memory configuration parameter.
*  On Linux operating systems only: the default value of some kernel
   configuration parameters, such as SHMMAX, are not large enough for
   DB2 LUW. Determine the current kernel configuration parameter
   settings and modify any kernel parameters that are not large enough.

If this error continues to happen after you have performed these
troubleshooting steps, collect diagnostic information using the
db2support utility and contact IBM software support.

sqlcode: -1084

sqlstate: 57019


   Related information:
   Configuring for good performance
   Database manager shared memory
   db2osconf - Utility for kernel parameter values command
   Modifying kernel parameters (Linux)
   Generating database configuration recommendations
   database_memory - Database shared memory size configuration parameter
