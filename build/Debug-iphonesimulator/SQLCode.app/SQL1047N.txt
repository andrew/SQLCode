

SQL1047N  The application is already connected to another database.

Explanation: 

An application cannot create a database while connected to another
database.

Binding a bind file to one database while already connected to another
database is not permitted.

The command cannot be processed.

User response: 

Disconnect from the active database and resubmit the command.
