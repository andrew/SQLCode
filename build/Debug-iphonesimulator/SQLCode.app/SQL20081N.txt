

SQL20081N  The method body cannot be defined for a LANGUAGE
      "<language-type>" method specification "<method-name>".

Explanation: 

The method specification"<method-name>" is defined with LANGUAGE
"<language-type>". If the LANGUAGE is SQL, the method body must be an
SQL control statement. For other languages, the EXTERNAL clause must be
specified.

The statement cannot be processed.

User response: 

Change the method body to match the LANGUAGE specified in the method
specification.

 sqlcode: -20081

 sqlstate: 428ES
