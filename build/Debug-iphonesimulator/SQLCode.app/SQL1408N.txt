

SQL1408N  The audit policy "<audit-policy>" is already in use for the
      object "<object-name>" of type "<object-type>".

Explanation: 

An AUDIT USING statement for object "<object-name>" of type
"<object-type>" attempted to associate the audit policy for the
specified object, but an audit policy "<audit-policy>" is already in
use. Only one audit policy can be associated to a particular object. The
statement could not be processed.

User response: 

Use the REPLACE option of the AUDIT statement to replace the existing
audit policy with the desired audit policy.

sqlcode: -1408

sqlstate: 5U041
