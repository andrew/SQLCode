

SQL1093N  The user is not logged on.

Explanation: 

A user must be logged on before any command requiring authorization can
be processed. Possible causes for this error include: 
*  Unable to get a user ID.
*  An unexpected operating system error occurred when attempting to log
   on.
*  The application is running in a background process.
*  The user cancelled an attempted logon.

The command cannot be processed.

User response: 

Log on with a valid user ID and resubmit the command. If several
concurrent processes are attempting to log on, wait a few seconds and
retry the logon procedure.

 sqlcode: -1093

 sqlstate: 51017
