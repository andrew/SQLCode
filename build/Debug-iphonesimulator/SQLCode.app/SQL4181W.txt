

SQL4181W  The number of TARGET SPECIFICATIONs does not match the DEGREE
      of cursor "<cursor>".

Explanation: 

The number of TARGET SPECIFICATIONs in the FETCH statement does not
match the DEGREE of the table specified.

Processing continues.

User response: 

Correct the SQL statement.
