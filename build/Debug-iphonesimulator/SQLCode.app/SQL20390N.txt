

SQL20390N  The security label component "<component-name>" is not
      defined in the security policy "<security-policy>" so that
      component cannot be used in the security label "<security-label>".

Explanation: 

Security labels can only contain values for those components that are
defined in the security policy that the label is part of. The security
label component "<component-name>" is not part of the security policy
"<security-policy>". The security label "<security-label>" is part of
that security policy so the component "<component-name>" cannot be used
in that security label.

User response: 

Provide a security label component that is part of the security policy
"<security-policy>". You can execute the following query to list the
security label components that are part of the security policy:

SELECT COMPNAME FROM 
  SYSCAT.SECURITYLABELCOMPONENTS
  WHERE COMPID=(SELECT COMPID FROM 
    SYSCAT.SECURITYPOLICYCOMPONENTRULES
    WHERE SECPOLICYID = (SELECT 
      SECPOLICYID FROM 
      SYSCAT.SECURITYPOLICIES 
      WHERE SECPOLICYNAME = 
        '<security-policy>') )

sqlcode: -20390

sqlstate: 4274G
