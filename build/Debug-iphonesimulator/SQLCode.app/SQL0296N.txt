

SQL0296N  The CREATE statement failed because a database limit for the
      object has been reached. Limit: "<limit-number>". Object type
      keyword: "<object-keyword>"

Explanation: 

This message is returned when an attempt is made to create a database
object when there are already the maximum number of that type of
database object defined for the database.

User response: 

Respond to this error in one of the following ways:

*  Delete any database objects of the same type that are not being used
   any more, and then reissue the CREATE statement.
*  For table spaces: 
   1. Move data from multiple, small table spaces into one, larger table
      space.
   2. Delete the original, small table spaces.
   3. Reissue the CREATE statement.

sqlcode: -296

sqlstate: 54035


   Related information:
   CREATE STOGROUP statement
   CREATE TABLESPACE statement
   SQL and XML limits
   Data movement options
   Authorization
