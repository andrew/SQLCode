

SQL0992C  The release number of the precompiled program is not valid.

Explanation: 

The release number of the precompiled program (package) is not
compatible with the release number of the installed version of the
database manager.

The precompiled program (package) cannot be used with the current
version of the database manager. The command cannot be processed.

User response: 

Use only programs that are precompiled with a compatible release level
of the database manager.

 sqlcode: -992

 sqlstate: 51008
