

SQL0123N  The parameter in position "<n>" in the function "<name>" must
      be a constant or a keyword.

Explanation: 

The parameter in position "<n>" in the function "<name>" is not a
constant when it is required to be a constant or a keyword when it is
required to be a keyword.

User response: 

Ensure that each argument of the function conforms to the definition of
the corresponding parameter.

 sqlcode: -123

 sqlstate: 42601
