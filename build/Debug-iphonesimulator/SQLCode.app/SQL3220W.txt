

SQL3220W  Volume "<volume-name>" was not found in the "<directory-name>"
      directory. Copy the volume into this directory and continue the
      LOAD/IMPORT.

Explanation: 

An attempt to LOAD/IMPORT a multiple IXF file was made, but one of the
files is missing from the directory specified. LOAD/IMPORT tries to find
the parts in the same directory as the first part.

The import will terminate.

User response: 


*  Find the part and put it in the same directory as the first part.
   Then call LOAD/IMPORT again with callerac of SQLU_CONTINUE. The
   LOAD/IMPORT will continue processing the file.
*  Terminate the LOAD/IMPORT by calling LOAD/IMPORT with a callerac of
   SQLU_TERMINATE.
