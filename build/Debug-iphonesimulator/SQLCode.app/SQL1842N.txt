

SQL1842N  Option "<option-name>" of type "<option-type>" for object
      "<object-name>" found near "<text>" is not valid. Reason code=
      "<reason-code>".

Explanation: 

The syntax of the specified option is not valid or the option cannot be
set to the specified value. The reason code provides more information
about the error. The reason codes are:

01       

         Unexpected character.


02       

         Element or attribute name expected but not found.


03       

         Min/max occurrence expected after reference.


04       

         More than one colon found in an attribute name.


05       

         Min/max occurrence is not an integer value.


06       

         Min/max occurrence out of range.


07       

         Min occurrence greater than max occurrence.


08       

         Reference in column template option was not "column".


09       

         '=' delimiter missing from namespace specification.


10       

         Opening or closing quotation mark missing from namespace
         specification.


11       

         Duplicate reference in template.


12       

         The wrapper option "<option-name>" is not allowed when the DB2
         instance is 32-bit.


13       

         The two wrapper options or option values are not compatible.
         The wrapper option "<option-name>" is only valid when
         "DB2_FENCED" is set to "Y".


14       

         The wrapper option "<option-name>" is not supported on this
         particular platform.


15       

         The wrapper cannot be loaded into the DB2 threaded engine on
         this particular platform as it is not threadsafe.


16       

         The minimum valid value for the wait-time parameter is 1000
         microseconds.

User response: 

See the federation documentation for this data source. Determine the
correct option syntax and recode the statement. The reason codes are:

01       

         Examine the option value near the specified position and change
         or remove the invalid character.


02       

         Examine the option value near the specified position and
         correct the syntax.


03       

         Ensure that a range specification "[min,max]" follows each
         reference in the template option value.


04       

         Templates support only one level of name qualification. Remove
         the extra qualifications.


05       

         Ensure that the min occurrence and max occurrence values of a
         range specification are integers.


06       

         Ensure that the values for the range specification "[min,max]"
         are within the allowed ranges for this data source.


07       

         Correct the range specification. Ensure that the first number
         is less than or equal to the second.


08       

         Replace the reference in the column template option value with
         the token 'column'.


09,10    

         Recode the namespace option value in the form
         'name="specification"'.


11       

         Recode the template. Ensure that no references are repeated.


12       

         Do not specify the "<option-name>" wrapper option for a 32-bit
         DB2 instance.


13       

         Examine the option value for the "DB2_FENCED" wrapper option.


14       

         Consult the SQL Reference to verify the wrapper option that you
         want. Install and use the 64-bit client for this data source.


15       

         Specify the value "Y" for the "DB2_FENCED" wrapper option.


16       

         Issue the statement again with a valid value for wait-time.

sqlcode: -1842

 sqlstate: 42616
