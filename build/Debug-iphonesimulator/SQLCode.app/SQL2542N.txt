

SQL2542N  No match for a database image file was found based on the
      source database alias "<database-alias>" and timestamp
      "<timestamp>" provided.

Explanation: 

The backup image file filename consists of a database alias and a
timestamp component. A filename is constructed from the source database
alias and timestamp parameters provided in the Database Restore call. No
filename existed in the source directory that matched based on the
source database alias and timestamp provided.

The following situations might apply:

1. The path to the backup was incorrectly specified in the restore
   command.
2. You do not have permission to access the backup image or the
   directory within which the backup image resides.
3. You are performing an automatic incremental restore and a required
   image was not found based on the timestamp and location in the
   database history.
4. You are restoring a database in a partitioned database environment,
   the database does not already exist, and the first database partition
   being restored is not the catalog partition.
5. You are restoring from TSM media, and the TSM API client
   configuration used by the current instance is not able to access the
   backup image.

User response: 

Appropriate responses corresponding to the situations described
previously are:

1. Ensure that the database backup image resides on the media source.
   Resubmit the operation by specifying a correct path to the backup
   image and correct timestamp to result in a match. For more
   information on using the restore command, search the DB2 Information
   Center using phrases such as "using restore database utility".
2. Ensure that you have permission to access the backup image and the
   directory in which it resides.
3. Check the database history for the corresponding backup entry and
   verify that the location listed matches the actual location of the
   backup image. Either update the database history and retry the
   operation to result in a match or issue a RESTORE INCREMENTAL ABORT
   command to cleanup any resources that may have been created during
   processing.
4. Always restore the catalog partition first when restoring a
   partitioned database. For more information about restoring in a
   partitioned database environment, search the DB2 Information Center
   using phrases such as "restore utility partitioned database".
5. Use db2adutl utility with QUERY option to check if the image could be
   retrieved from TSM. If you are restoring a backup image taken from a
   different instance on a different server, make sure to use the
   options NODENAME, OWNER, and optionally PASSWORD corresponding to the
   TSM settings of the TSM node on which the backup image was originally
   taken. Once you have verified that the image could be retrieved, you
   could pass the same options in the options string of the RESTORE
   command. For more information about the db2adutl utility, search the
   DB2 Information Center using phrases such as "db2adutl".

DB2 Information Center:
http://publib.boulder.ibm.com/infocenter/db2luw/v9
