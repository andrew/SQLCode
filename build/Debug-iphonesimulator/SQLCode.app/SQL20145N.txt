

SQL20145N  The decryption function failed. The password used for
      decryption does not match the password used to encrypt the data.

Explanation: 

The data must be decrypted using the same password that was used to
encrypt the data.

User response: 

Ensure that the same password is used to encrypt and decrypt the data.

 sqlcode: -20145

 sqlstate: 428FD
