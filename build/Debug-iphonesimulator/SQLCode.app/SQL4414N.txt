

SQL4414N  The DB2 Administration Server is not active.

Explanation: 

The request cannot be processed unless the DB2 Administration Server is
active.

User response: 

Start the DB2 Administration Server by issuing the command DB2ADMIN
START, and reissue the request.
