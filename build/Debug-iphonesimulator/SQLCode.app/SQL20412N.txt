

SQL20412N  Serialization of an XML value resulted in characters that
      could not be represented in the target encoding.

Explanation: 

Serialization of XML data may require conversion to an encoding
different from the source UTF-8 encoding. If characters exist in the
source encoding that cannot be represented in the target encoding, code
page conversion produces substitution characters which are not allowed
in the result of XML serialization.

User response: 

Choose a target encoding that can represent all characters in the XML
value. Unicode encodings are recommended as they can represent all
characters.

sqlcode: -20412

sqlstate: 2200W
