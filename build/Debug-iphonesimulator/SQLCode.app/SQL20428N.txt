

SQL20428N  URI specified in the the ACCORDING TO XMLSCHEMA clause is an
      empty string.

Explanation: 

The target namespace URI specified following the URI keyword or the
schema location URI specified following the LOCATION keyword is an empty
string.

User response: 

Ensure that every target namespace URIs and every schema location URI in
the ACCORDING TO XMLSCHEMA clause is a valid URI that is not an empty
string.

sqlcode: -20428

sqlstate: 428GV
