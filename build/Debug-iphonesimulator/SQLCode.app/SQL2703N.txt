

SQL2703N  Failed to open the log file "<log-file>".

Explanation: 

The utility cannot open the log file "<log-file>" for writing or
appending.

User response: 

Please ensure the log file exists and is writable.
