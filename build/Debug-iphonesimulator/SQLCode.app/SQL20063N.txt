

SQL20063N  TRANSFORM GROUP clause must be specified for type
      "<type-name>".

Explanation: 

The function or method includes a parameter or returns data type of
"<type-name>" that does not have a transform group specified.

The statement cannot be processed.

User response: 

Specify a TRANSFORM GROUP clause with a transform group name that is
defined for "<type-name>".

 sqlcode: -20063

 sqlstate: 428EM
