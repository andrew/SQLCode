

SQL20320N  The maximum size specified for the table space is not valid.

Explanation: 

The maximum size specified in the CREATE TABLESPACE or ALTER TABLESPACE
statement is not valid. If creating the table space, the maximum size
must be greater than or equal to the initial size specified. If altering
an existing table space, the maximum size must be greater than or equal
to the current size of the table space.

User response: 

Specify a larger value for the maximum size as described in this
message's Explanation.

sqlcode: -20320

sqlstate: 560B0
