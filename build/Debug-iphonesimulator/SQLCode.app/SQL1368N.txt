

SQL1368N  Invalid resource policy configuration.

Explanation: 

The resource policy file is invalid.

User response: 

Correct the policy definition specified by the file defined by the
DB2_RESOURCE_POLICY registry variable.

Disable resource policy support by clearing the DB2_RESOURCE_POLICY
registry variable or set DB2_RESOURCE_POLICY to AUTOMATIC for automatic
configuration.
