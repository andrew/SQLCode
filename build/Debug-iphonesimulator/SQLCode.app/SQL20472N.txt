

SQL20472N  The ALTER statement on the permission or mask "<object-name>"
      failed due to reason code "<reason-code>".

Explanation: 

The ALTER MASK or ALTER PERMISSION statement cannot be processed for one
of the following reasons:

1        

         A default row permission cannot be altered.


2        

         ENABLE cannot be specified because the object is invalid.

The statement cannot be processed.

User response: 

1        

         Specify the name of a row permission or column mask that can be
         changed.


2        

         For the invalid state, disable the row permission or column
         mask, then drop and recreate it.

sqlcode: -20472

sqlstate: 428H9
