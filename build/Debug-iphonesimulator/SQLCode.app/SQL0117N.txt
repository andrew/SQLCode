

SQL0117N  The number of values assigned is not the same as the number of
      specified or implied columns or variables.

Explanation: 

The number of values can be different when:

*  The number of insert values in the value list of the INSERT statement
   is not the same as the number of columns specified or implied. If no
   column list is specified, a column list that includes all columns of
   the table (except those implicitly hidden) or view is implied.
*  The number of values on the right hand side of an assignment in a SET
   statement or SET clause of an UPDATE statement does not match the
   number of columns or variables on the left hand side.

The statement cannot be processed.

User response: 

Correct the statement to specify one value for each of the specified or
implied columns or variables.

sqlcode: -117

sqlstate: 42802
