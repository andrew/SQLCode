

SQL20476N  The "<function-name>" function was invoked with an invalid
      format string "<format-string>".

Explanation: 

An invalid format string was specified for the "<function-name>"
function. The value for "<function-name>" could be VARCHAR_FORMAT or
DECFLOAT_FORMAT, even if the name used to invoke the function was
TO_CHAR or TO_NUMBER. A valid format string for the VARCHAR_FORMAT
function must:

*  Have an actual length of the data type that is not greater than 254
   bytes
*  Only contain supported format elements
*  Not result in a string with an actual length that is greater than the
   length attribute of the result

A valid format string for the DECFLOAT_FORMAT function must:

*  Have an actual length of the data type that is not greater than 254
   bytes
*  Contain at least one format element
*  Only contain supported format elements

The statement cannot be processed.

User response: 

Change the format string argument of the "<function-name>" function. For
more information, see the corresponding description of the function in
the SQL Reference.

sqlcode: -20476

sqlstate: 22018
