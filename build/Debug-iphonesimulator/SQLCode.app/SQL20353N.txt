

SQL20353N  An operation involving comparison cannot use operand "<name>"
      defined as data type "<type-name>".

Explanation: 

The use of the value identified by "<name>" defined as data type
"<type-name>" is not permitted in operations involving comparisons. An
expression resulting in a "<type-name>" data type is not permitted in: 
*  A SELECT DISTINCT statement
*  A GROUP BY clause
*  An ORDER BY clause
*  An aggregate function with DISTINCT
*  A SELECT or VALUES statement of a set operator other than UNION ALL.

The statement cannot be processed.

User response: 

The requested operation on the data type "<type-name>" is not supported.
You may be able to change the data type of the value to a data type that
is supported using a cast or some other function.

sqlcode: -20353

sqlstate: 42818
