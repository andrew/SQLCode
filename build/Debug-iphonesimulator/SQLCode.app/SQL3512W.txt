

SQL3512W  The file named in row "<row-number>" and column
      "<column-number>" cannot be found, but the target column is not
      nullable. The row was not loaded.

Explanation: 

The filename in the specified field cannot be found. A null cannot be
loaded because the output column in the table is not nullable.

For delimited ASCII (DEL) files, the value of the column number
specifies the field within the row that contains the value in question.
For ASCII files, the value of the column number specifies the byte
location within the row where the value in question begins.

The row is not loaded.

User response: 

Correct the input file and resubmit the command or edit the data in the
table.
