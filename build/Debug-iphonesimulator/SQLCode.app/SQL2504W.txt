

SQL2504W  Insert the first backup diskette into drive "<drive>".

Explanation: 

The RESTORE utility reads the first backup diskette to determine the
path of the database directory that was backed up. If the backup media
is a diskette and the diskette is not found in the specified input
drive, the utility returns to the caller with this prompt. The calling
program is expected to query the user and return to the utility with the
user response.

The utility waits for a response from the caller.

User response: 

Prompt the user for the diskette and return to the utility with the
callerac parameter, indicating if processing continues or ends.
