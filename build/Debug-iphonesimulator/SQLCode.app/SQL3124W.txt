

SQL3124W  The field value in row "<row-number>" and column
      "<column-number>" cannot be converted to a PACKED DECIMAL value,
      but the target column is not nullable. The row was not loaded.

Explanation: 

The value in the specified field cannot be converted to a PACKED DECIMAL
value. There may be a data type mismatch. A null cannot be loaded
because the output column in the table is not nullable.

For delimited ASCII (DEL) files, the value of the column number
specifies the field within the row that contains the value in question.
For ASCII files, the value of the column number specifies the byte
location within the row where the value in question begins.

The row is not loaded.

User response: 

Correct the input file and resubmit the command or edit the data in the
table.
