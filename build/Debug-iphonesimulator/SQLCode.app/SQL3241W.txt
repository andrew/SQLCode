

SQL3241W  Row "<row>" and column "<column>" in the input source contains
      an invalid security label for the target table.

Explanation: 

The value for the column of data type DB2SECURITYLABEL in the input
source is not a valid security label for the security policy that is
protecting the target table. Any security label inserted into a column
of data type DB2SECURITYLABEL must be a part of the security policy that
is protecting the table. The row is not loaded.

User response: 

Check the input source column and make sure that the value is valid for
the policy protecting the target table. If the values in the input
source are in string form, you must specify a security label related
file type modifier. If necessary, correct the input data source and
resubmit the command.

sqlcode: +3241

sqlstate: 01H53
