

SQL2938N  The beginning-ending location pair "<begin>", "<end>" for
      field "<field-name>" is not valid. Reason code "<reason-code>".

Explanation: 

The field specification for locating the input data for the indicated
database column within the input non-delimited ASCII file is not valid
for the reason indicated by the following reason codes:

1        

         The start position is 0.


2        

         The end position is less than the start position.


3        

         The end position is greater than 32 767.


4        

         Fields of type SMALLINT, INTEGER, BIGINT, DECIMAL, REAL, FLOAT,
         and DECFLOAT with the EXTERNAL modifier have a maximum length
         of 50.


5        

         If a format string is specified for fields of type DATE, TIME,
         and TIMESTAMP(p), the field length must be greater than or
         equal to length of the shortest value that matches the format
         string.


6        

         If a format string is not specified:

          
         *  For DATE fields, the field length must be between 8 and 10,
            inclusive.
         *  For TIME fields, the field length must be between 4 and 8,
            inclusive.
         *  For TIMESTAMP fields, the field length must be between 19
            and 32, inclusive.

The INGEST command failed.

User response: 

Follow the action indicated by the reason code and resubmit the command:

1        

         Specify a start position that is greater than 0.


2        

         Specify an end position that is greater than the start
         position.


3        

         Specify an end position that is less than or equal to 32 767.


4        

         Change the field length to a value less than or equal to 50.


5        

         Shorten the format string so that the length of shortest value
         that matches the format string is less than or equal the field
         length, or change the field length to a value greater than or
         equal to the the length of shortest value that matches the
         format string.


6        

         Change the field length to a value within the ranges given in
         the explanation of reason code 6.


   Related information:
   INGEST command
