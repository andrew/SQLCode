

SQL16130N  XML document contains an invalid or not terminated processing
      instruction. Reason code = "<reason-code>".

Explanation: 

While processing an XML document or XML schema the XML parser
encountered an processing instruction that is not valid. One or more of
the following reasons make the processing instruction not valid: 
1. The processing instruction was not terminated
2. The processing instruction starts with the characters 'xml' (in any
   combination of upper or lower case), which is prohibited.

Parsing or validation did not complete.

User response: 

Correct the XML processing instruction and try the operation again.

sqlcode: -16130

sqlstate: 2200M
