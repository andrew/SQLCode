

SQL3933W  The test synchronization session completed successfully. The
      release level of the satellite, however, is not supported by the
      release level of the satellite control server.

Explanation: 

The release level of the satellite must be within the range of one level
above to two levels below that of the satellite control server.

User response: 

Contact the help desk or your system administrator.
