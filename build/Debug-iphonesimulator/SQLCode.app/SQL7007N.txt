

SQL7007N  REXX variable "<variable>" does not exist.

Explanation: 

A REXX variable that did not exist in the REXX variable pool was passed.

The command cannot be processed.

User response: 

Verify that all variable names in the host-variable list are assigned
before the command that failed. Then run the procedure again.
