

SQL0385W  Assignment to an SQLSTATE or SQLCODE variable in an SQL
      routine may be over-written and does not activate any handler.

Explanation: 

The SQL routine includes at least one statement that assigns a value to
the SQLSTATE or SQLCODE special variables. These variables are assigned
values by processing of SQL statements in the SQL routine. The value
assigned may therefore be over-written as a result of the SQL statement
processing. Furthermore, the assignment of a value to the SQLSTATE
special variable does not activate any handlers.

The routine definition was processed successfully.

User response: 

None required. Remove any assignments to the SQLSTATE or SQLCODE special
variable to prevent the warning.

 sqlcode: +385

 sqlstate: 01643
