

SQL20461W  The procedure "<procedure-name>" returned output in the
      alternate locale, "<locale1>", instead of the locale, "<locale2>",
      specified in parameter "<number>".

Explanation: 

The locale, "<locale2>", specified in the parameter in ordinal position
"<number>" was not available for the output of the procedure
"<procedure-name>". The output is returned using locale "<locale1>".

User response: 

Install the message file support on the server for the specified locale,
"<locale2>", or specify a supported locale.

sqlcode: +20461

sqlstate: 01H57
