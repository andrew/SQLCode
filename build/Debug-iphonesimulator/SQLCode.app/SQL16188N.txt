

SQL16188N  XML document contains an invalid content annotation
      specification for type "<type-name>".

Explanation: 

While parsing an XML document, the parser encountered Content
(Annotation?...) which is incorrect for the data type "<type-name>".

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16188

sqlstate: 2200M
