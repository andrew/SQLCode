

SQL2958N  The INGEST command cannot restart because one of the following
      does not match the original INGEST command: the number of input
      sources, or the setting of NUM_FLUSHERS_PER_PARTITION. Original
      number of input sources: "<number-of-input-sources>". Original
      value of NUM_FLUSHERS_PER_PARTITION: "<number-of-flushers>".
      Current number of input sources: "<number-of-input-sources>".
      Current value of NUM_FLUSHERS_PER_PARTITION:
      "<number-of-flushers>".

Explanation: 

The INGEST command specified RESTART CONTINUE. In order to restart a
failed INGEST command, the restarted command must meet the following
requirements:

*  The NUM_FLUSHERS_PER_PARTITION configuration parameter must be the
   same as on the original command.
*  If the input is from files or pipes, the number of input files or
   pipes must be the same as on the original command.

For a comprehensive list of requirements, refer to the INGEST command
topic in the DB2 Information Center.

User response: 

One of the following:

*  Set the NUM_FLUSHERS_PER_PARTITION configuration parameter to the
   value it had when the original INGEST command was run.
*  Modify the restarted INGEST command to specify the same number of
   files or pipes as on the original command.
*  Remove the RESTART CONTINUE parameter. In this case, the command
   starts from the beginning instead of resuming from where the failed
   command left off.


   Related information:
   INGEST command
   Restarting a failed ingest operation
