

SQL27986W  A column name has been truncated in the PC/IXF file during
      Export. This file will not be supported in Import using Method N.

Explanation: 

Data has not been affected during Export but the file cannot be used in
Import with Method N because some column information is missing.

User response: 

If Method N will not be used in Import, then no action is necessary. If
Method N will be used, then export using Method N again, but explicitly
specify shorter column names. Even though the PC/IXF file will contain
the shorter column names, this will not affect the import of the data
into an existing table. Alternatively, rename the columns in the
original table to be shorter and export the data again.

sqlcode: +27986

sqlstate: 5U036
