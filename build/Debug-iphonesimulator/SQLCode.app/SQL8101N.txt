

SQL8101N  A Database segment may be incorrect.

Explanation: 

This error could occur in two ways: 
1. Every database segment has an identifier file. The file may be
   missing or the contents of the file may be incorrect.
2. One, or more, previously allocated database segments are missing.

User response: 


*  Check to ensure that the file systems are mounted correctly
*  Restore the database from a backup
*  Call your IBM service representative
