

SQL16102N  XML document contains a notation declaration with name
      "<notation-name>" that is not found, is not a valid notation
      declaration, or does not have a valid QName.

Explanation: 

While parsing an XML document, the declaration for the identified XML
notation identified by "<notation-name>" was not found in the document
or associated schema/DTD, is incorrectly declared, or does not have a
valid QName.

Parsing or validation did not complete.

User response: 

Correct the XML notation identified by "<notation-name>" and try the
operation again.

sqlcode: -16102

sqlstate: 2200M
