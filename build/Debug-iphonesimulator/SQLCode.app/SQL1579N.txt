

SQL1579N  Two or more log streams for database "<dbname>" on database
      partition "<dbpartitionnum>" are following different log chains.
      Log file "<file1>" on log stream "<stream1>" follows log chain
      "<chain1>" and log file "<file2>" on log stream "<stream2>"
      follows log chain "<chain2>".

Explanation: 

When a database rollforward to a point-in-time operation completes, or a
database restore operation without rolling forward is done, a new
history of the database, called a log chain, is created. The database
manager assigns log chain numbers to log extents and other database
objects so that it can verify that the set of objects belongs to the
same database history. The database manager has detected that two or
more log streams are following different log chains, and has halted the
current operation.

User response: 

Check the log streams that are identified in this message and determine
which log stream is following the correct log chain. Retrieve the
required log files from the invalid log stream into an overflow log
path, and attempt the operation again.
