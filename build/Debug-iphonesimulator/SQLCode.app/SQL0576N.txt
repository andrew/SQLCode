

SQL0576N  Alias "<name>" cannot be created for "<name2>" as it would
      result in a repetitive alias chain.

Explanation: 

The alias definition of "<name>" on "<name2>" would have resulted in a
repetitive alias chain which could never be resolved. For example,
"alias A refers to alias B which refers to alias A" is a repetitive
alias chain which could never be resolved.

The statement cannot be processed.

User response: 

Change the alias definition for "<name>" or revise the definition of one
of the other alias definitions in the alias chain to avoid a repetitive
chain.

 sqlcode: -576

 sqlstate: 42916
