

SQL1736N  The command failed to retrieve the keystore password. Reason
      code "<reason-code>".

Explanation: 

The command included an argument used to retrieve the keystore password
that is not valid.

1        

         The format of the argument was not recognized. Valid formats
         are: "fd:"<fd>"" and "filename:"<filename>"".


2        

         An error resulted in reading the specified argument. For
         example, the file name specified does not exist.

User response: 

1        

         Specify a valid format and retry the command.


2        

         Specify valid argument values and retry the command.


   Related information:
   BACKUP DATABASE command
