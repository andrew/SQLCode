

SQL3957N  Cannot connect to the satellite control database because of a
      communication failure: SQLCODE="<sqlcode>",
      SQLSTATES="<sqlstate>", Tokens ="<token1>", "<token2>",
      "<token3>".

Explanation: 

An error has been detected by the communication subsystem. Refer to
"<sqlcode>" for more details.

User response: 

 Contact the help desk or your system administrator.
