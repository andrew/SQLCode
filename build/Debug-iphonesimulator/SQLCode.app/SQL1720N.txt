

SQL1720N  Could not drop the only member.

Explanation: 

You cannot drop the only member that the database could be connected to
because dropping the member, results in dropping the database. The
database must be dropped first.

User response: 

First drop the database, then drop the member.


   Related information:
   DROP DATABASE command
   Dropping a member or cluster caching facility
