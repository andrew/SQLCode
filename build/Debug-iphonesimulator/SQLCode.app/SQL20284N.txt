

SQL20284N  No plan was possible to create for federated data source
      "<server-name>". Reason = "<reason-code>".

Explanation: 

While building a federated query access plan, the query fragment for one
or more data sources cannot be processed because of a missing predicate
or a problem with the query syntax as indicated by the following reason
code: 
1. A required predicate is missing.
2. A predicate that can be processed by the data source is combined with
   another predicate either using the OR operator or a BETWEEN
   predicate.

User response: 

See the federation documentation for this data source. Correct the query
syntax as needed and resubmit the statement. The action corresponding to
the reason code is: 
1. Supply the missing predicate.
2. Change the statement syntax so that predicates for one data source
   are separated from predicates for another data source using the AND
   operator and not the OR operator.

sqlcode: -20284

sqlcode: 429BO
