

SQL0297N  Path name for container or storage path is too long.

Explanation: 

 One of the following conditions is true: 
*  The full path specifying the container name exceeds the maximum
   length allowed (254 characters). If the container was specified as a
   path relative to the database directory, the concatenation of these
   two values must not exceed the maximum length. Details can be found
   in the administration notification log.
*  The storage path exceeds the maximum length allowed (175 characters).

User response: 

Shorten the path length.

 sqlcode: -297

 sqlstate: 54036
