

SQL27919I  The distribution file "<filename>" was opened successfully
      for writing.

Explanation: 

This informational message indicates the distribution file was opened
successfully for writing.

User response: 

No action is required.
