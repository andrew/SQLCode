

SQL6106N  The filetype modifier "NOHEADER" was specified, but the
      database partition group on which the table is defined is not a
      single-node database partition group.

Explanation: 

The data to be loaded was specified as having no header information.
However the target table specified is not a single-node table. The data
cannot be loaded.

User response: 

The data must be split using db2split, then loaded without the
"NOHEADER" option.
