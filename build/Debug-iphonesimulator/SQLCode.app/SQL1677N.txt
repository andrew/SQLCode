

SQL1677N  DB2START or DB2STOP processing failed due to a DB2 cluster
      services error.

Explanation: 

DB2 cluster services failed to perform the required operation.

User response: 

Troubleshoot the DB2 cluster services status using the db2cluster
command and the db2instance command.


   Related information:
   Frequently asked questions about installation, instance creation, and
   rollback problems with the DB2 pureScale Feature
   db2instance - Query state of DB2 instance command
