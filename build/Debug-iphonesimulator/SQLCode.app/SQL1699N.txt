

SQL1699N  The specified member subset attribute is not valid. Reason
      code = "<reason-code>" Attribute name: "<attribute-name>" Value:
      "<value>"

Explanation: 

The statement cannot be processed because of the following reason code:

1

The specified value is only valid in a data sharing environment.

2

You cannot change a member subsets catalogDatabaseAlias attribute
without also changing the databaseAlias attribute.

3

The database name cannot be used for the database alias attribute in a
member subset.

User response: 

The action corresponding to the reason code is as follows:

1

Reissue the member subset management routine with a different attribute
value.

2

Specify a new databaseAlias attribute value or do not change the
catalogDatabaseAlias attribute value.

3

Specify another name for the databaseAlias attribute value.

sqlcode: -1699

sqlstate: 530AA


   Related information:
   Member subsets overview
