

SQL0151N  The column "<name>" cannot be updated.

Explanation: 

The specified column cannot be updated because one of the following was
attempted.

*  The object table is a view, and the specified column is derived from
   a scalar function, expression, keyword, constant, or column of a view
   where that column cannot be updated.
*  The specified column is a non-updateable column of a system catalog,
   or a column explicitly marked as READ ONLY.
*  The BUSINESS_TIME period in the table includes a column that was
   specified to be updated. A column of a BUSINESS_TIME period must not
   be modified within a trigger body.
*  The BUSINESS_TIME period in the table includes a column that was
   specified to be updated. A column of a BUSINESS_TIME period must not
   be modified if the data change statement includes a period clause.

Federated system users should check to see if some other data source
specific limitation prevents the column from being updated.

The statement cannot be processed.

User response: 

If the specified column is derived from a scalar function, expression,
keyword, or non updatable column, omit the column from the set clause of
the update or the column of the insert. For a list of updatable catalogs
(and the updatable columns) see the SQL Reference.

Federated system users: if the reason is unknown, isolate the problem to
the data source failing the request and examine the object definition
and the update restrictions for that data source.

sqlcode: -151

sqlstate: 42808


   Related information:
   Troubleshooting data source connection errors
