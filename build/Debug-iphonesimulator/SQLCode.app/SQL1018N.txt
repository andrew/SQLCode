

SQL1018N  The node name "<name>" specified in the CATALOG NODE command
      already exists.

Explanation: 

The node name specified in the nodename parameter of the CATALOG NODE
command is already cataloged in the node directory on this file system.

The command cannot be processed.

User response: 

If the nodename parameter is typed correctly, continue processing.

Uncatalog the cataloged node in the node directory if the node cataloged
information is no longer valid and resubmit the command. If the node
cataloged information is valid, define a new node name and resubmit the
command using the new node name.
