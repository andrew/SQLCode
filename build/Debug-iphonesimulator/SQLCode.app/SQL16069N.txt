

SQL16069N  A regular expression argument "<value>" passed to the
      function "<function-name>" matches a zero-length string. Error
      QName=err:FORX0003.

Explanation: 

The "<value>" that was specified for the pattern parameter in a call to
the function "<function-name>" matches a zero-length string. A
zero-length string is not a valid pattern for this function because the
pattern will never match a substring in the input string.

User response: 

Pass a valid pattern to the function call, or remove the function call
from the expression.

 sqlcode: -16069

 sqlstate: 10609
