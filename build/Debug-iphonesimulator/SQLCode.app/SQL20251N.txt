

SQL20251N  The last data partition cannot be detached from the table
      "<table-name>".

Explanation: 

The ALTER TABLE statement would have resulted in the the last remaining
data partition being detached from the table. This operation is not
allowed. A partitioned table must have at least one data partition whose
status is normal or attached. A partitioned table cannot have only data
partitions whose status is detached. To determine the status of
partitions, query the catalog view, SYSCAT.DATAPARTITIONS.

The statement cannot be processed.

User response: 

Ensure that the ALTER TABLE statement leaves at least one data partition
in the table.

sqlcode: -20251

sqlstate: 428G2
