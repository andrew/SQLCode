

SQL2457W  CF self-tuning memory was not enabled.

Explanation: 

For CF self-tuning memory to be enabled, when registry variable
DB2_DATABASE_CF_MEMORY is set to AUTO, these database configuration
parameters must be set to AUTOMATIC: CF_GBP_SZ, and CF_LOCK_SZ,
CF_SCA_SZ.

One of the database configuration parameters is not set to AUTOMATIC.

User response: 

To enable CF self-tuning memory, set the specified parameter to
AUTOMATIC using the UPDATE DATABASE CONFIGURATION command.


   Related information:
   CF self-tuning memory parameter configuration
