

SQL20441N  A "<type-name>" data type is not supported in the context
      where it is being used.

Explanation: 

The data type can be specified in multiple contexts, including the
following.

*  Parameters to SQL functions: 
   *  Defined in a module
   *  With a compound SQL (compiled) statement as function body not
      defined in a module

*  Return types from SQL functions: 
   *  Defined in a module
   *  With a compound SQL (compiled) statement as function body not
      defined in a module

*  Parameters to SQL procedures
*  Local variables declared in SQL functions: 
   *  Defined in a module
   *  With a compound SQL (compiled) statement as function body not
      defined in a module

*  Local variables declared in SQL procedures
*  Local variables declared in triggers with a compound compiled SQL
   statement as trigger body
*  Expressions in SQL statements within compound compiled SQL statements
*  Global variables

The following is a list of some invalid contexts:

*  Parameters or variables in external routines
*  Return type of a function with a function body definition defined by
   a compound SQL (inlined) statement
*  Columns in tables
*  Data types in SQL statements outside of SQL PL contexts
*  Global variable (includes module-variable) being referenced outside
   of an SQL PL context.
*  Input or output parameter to a procedure or function being invoked
   from outside of an SQL PL context.
*  In a partitioned database environment (DPF) or symmetric
   multiprocessor (SMP) environment, only top level SET and CALL
   statements can reference objects defined in nested types. A sub-query
   cannot reference objects with nested types.
*  Nested types with global variables, module variables, or PL/SQL
   package variables in autonomous routines.

User response: 

Refer to the documentation for the most up to date list of supported
contexts and for restrictions on the use of this data type. Remove any
data types used in unsupported contexts.

If referencing the data type in a routine reference from a command
interface, invoke the routine from within an SQL PL context or provide a
global variable of the specified data type as the routine argument.

sqlcode: -20441

sqlstate: 428H2


   Related information:
   Restrictions on the row data type
   Restrictions on the array data type
   Restrictions on associative array data types
