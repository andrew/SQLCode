

SQL0336N  The scale of the decimal number must be zero.

Explanation: 

The decimal number is used in a context where the scale must be zero.
This can occur when a decimal number is specified in a CREATE or ALTER
SEQUENCE statement for START WITH, INCREMENT, MINVALUE, MAXVALUE or
RESTART WITH.

The statement cannot be processed.

User response: 

Change the decimal number to remove any non-zero digits from the right
side of the decimal delimiter.

 sqlcode: -336

 sqlstate: 428FA
