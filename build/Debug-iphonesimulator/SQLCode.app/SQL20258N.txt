

SQL20258N  Invalid use of INPUT SEQUENCE ordering.

Explanation: 

The ORDER BY clause specifies INPUT SEQUENCE and the FROM clause of the
fullselect does not specify an INSERT statement.

The statement cannot be processed.

User response: 

Use INPUT SEQUENCE when the FROM clause of the fullselect specifies an
INSERT statement.

 sqlcode: -20258

 sqlstate: 428G4
