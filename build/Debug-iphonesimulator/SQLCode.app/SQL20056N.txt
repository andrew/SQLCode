

SQL20056N  Processing on DB2 Data Links Manager "<name>" encountered an
      error. Reason code = "<reason-code>".

Explanation: 

The processing on the DB2 Data Links Manager for the statement
encountered an error as indicated by the following reason codes. 

01       An inconsistency was detected between the data on the DB2 Data
         Links Manager and a DATALINK value in a table.

02       The DB2 Data Links Manager reached a resource limit during the
         processing.

03       The DB2 Data Links Manager does not support file pathnames
         longer than 128 characters.

99       The DB2 Data Links Manager encountered an internal processing
         error.

The statement cannot be processed.

User response: 

The action is based on the reason code as follows. 

01       Run the reconcile utility on the table.

02       The DB2 Data Links Manager administrator should identify the
         resource from the diagnostic logs and take corrective action.

03       You should ensure that the file pathname (excluding the file
         system prefix) to be stored in the DATALINK column does not
         exceed 128 characters. For example, in the URL
         "http://server.com/dlfiles/dir1/.../file1" -- assuming the DLFS
         file system prefix is "/dlfiles" -- the file pathname
         "/dir1/.../file1" must not exceed 128 characters.

99       Save the diagnostic logs from the DB2 Data Links Manager and
         the database manager and contact IBM service.

 sqlcode: -20056

 sqlstate: 58004
