

SQL6043C  No FCM request blocks are available.

Explanation: 

No FCM request block is available. FCM is unable to increase the number
of request blocks automatically because the maximum value has been
reached.

The statement cannot be processed.

User response: 

Try the request again after other processes have freed up some of this
resource.

 sqlcode: -6043

 sqlstate: 57011
