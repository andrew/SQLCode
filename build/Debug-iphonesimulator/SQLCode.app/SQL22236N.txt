

SQL22236N  A file I/O error occurred. Reason code "<reason-code>".

Explanation: 

An error occurred while accessing the file system. The reasons codes are
as follows: 
1. An invalid directory was specified.
2. An attempt was made to open a non-existent file.
3. An attempt was made to create an existing file.

User response: 

The action is based on the reason code as follows. 
1. Specify a valid directory.
2. Specify a file that exists.
3. Specify a file that does not already exist.
