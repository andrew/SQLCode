

SQL0817N  The SQL statement cannot be executed because the statement
      will result in a prohibited update operation.

Explanation: 

The application attempted to execute an SQL statement that would result
in updates to user data or to the subsystem catalog. This is prohibited
for one of the following reasons: 
*  The application is running as an IMS inquiry-only transaction.
*  The application is an IMS or CICS application that is attempting to
   update data at a remote DBMS that does not support two-phase commit.
*  The application is attempting to update data at multiple locations
   and one of the locations does not support two-phase commit.

These SQL statements include INSERT, UPDATE, DELETE, CREATE, ALTER,
DROP, GRANT, and REVOKE.

The statement cannot be executed.

User response: 

If the application is running as an IMS inquiry-only transaction, see
your IMS system programmer about changing the inquiry-only status of the
transaction under which your application is running.

If the IMS or CICS application is attempting a remote update, either the
application must be changed to run as a local application on the server
DBMS, or the server DBMS must be upgraded to support two-phase commit.

If the application is attempting to update data at multiple locations,
either the application must be changed, or all DBMSs involved must be
upgraded to support two-phase commit.

 sqlcode: -817

 sqlstate: 25000
