

SQL0612N  "<name>" is a duplicate name.

Explanation: 

A statement was issued with the same name appearing more than once where
duplicates are not allowed. Where these names appear varies depending on
the type of statement.

*  CREATE TABLE statements cannot have the same column name defined for
   two columns.
*  CREATE TABLE statements specifying table partitioning cannot define
   an identical data partition name for two data partitions.
*  ALTER TABLE statements cannot add or attach a data partition to a
   table specifying a partition name that is identical to a partition
   name of an existing data partition in the table.
*  CREATE VIEW statements or common table expression definitions cannot
   have the same column name in the column name list. If no column name
   list is specified, then the column names of the columns in the select
   list of the view must be unique.
*  ALTER TABLE statement cannot add a column to a table using the name
   of a column that already exists or is the same as another column
   being added. Furthermore, a column name can only be referenced in one
   ADD, DROP COLUMN, or ALTER COLUMN clause in a single ALTER TABLE
   statement.
*  CREATE INDEX cannot have a column name specified more than once as
   part of the index key or the INCLUDE columns of the index.
*  CREATE TRIGGER cannot have a column name specified more than once in
   the list of columns that cause an update trigger to be activated.
*  CREATE TABLE OF statements cannot have the same name defined for the
   REF IS column and any attribute of the structured type.
*  CREATE TYPE statements cannot have the same name defined for two
   fields in a ROW data type or two attributes in a structured type.
   Field and attribute names must be unique within the type and all
   supertypes.
*  ALTER TYPE statements cannot add an attribute to a structured type
   using the name of an attribute that already exists in the type or any
   of its subtypes or is the same as another attribute being added.
   Also, the name of the attribute may not be the same as the REF IS
   column in any table created from the structured type. Furthermore, an
   attribute name can only be referenced in one ADD or DROP ATTRIBUTE
   clause in a single ALTER TYPE statement.
*  CREATE INDEX EXTENSION statements cannot have the same name defined
   for two parameters.
*  A column name can only be referenced in one ALTER COLUMN clause in a
   single ALTER NICKNAME statement.
*  An XMLQUERY, XMLEXISTS, or XMLTABLE argument list contained two
   arguments with the same name.
*  An XMLTABLE column list contains two columns with the same name.
*  The typed-correlation clause of a SELECT statement cannot contain two
   columns with the same name.
*  CREATE or ALTER TABLE statements must not specify the same period
   more than once in a unique key.
*  CREATE or ALTER TABLE statements must not define a period and a
   column with the same name.
*  A period cannot be dropped at the same time as it is being added or
   altered in the same ALTER TABLE statement. For example, the same
   period name cannot appear in both a DROP PERIOD clause and an ADD
   PERIOD clause in the same ALTER TABLE statement.

The statement cannot be processed.

User response: 

Specify unique names as appropriate for the type of statement.

sqlcode: -612

sqlstate: 42711


   Related information:
   subselect
