

SQL20316N  Invalid compilation environment. Reason code =
      "<reason-code>".

Explanation: 

The compilation environment provided is invalid for the reason specified
in the accompanying reason code. 
1. The format of the compilation environment provided is not correct.
2. The version of the compilation environment provided is not supported.
3. The size of the compilation environment provided is not valid.
4. The codepage used by the compilation environment provided is not
   compatible with this database.

The statement cannot be executed.

User response: 

The action is based on the reason codes as follows: 
1. Acquire the compilation environment again and ensure that it is not
   altered in any fashion prior to use.
2. Acquire the compilation environment again using a compatible level of
   software.
3. Acquire the compilation environment again and ensure that it is not
   altered in any fashion prior to use.
4. Acquire the compilation environment again using a database with the
   same codepage as this one.

sqlcode: -20316

sqlstate: 51040
