

SQL6581I  Load cannot restart on node "<node-num>".

Explanation: 

AutoLoader has acknowledged that LOAD cannot be restarted on a given
node.

User response: 

This is an informational message.
