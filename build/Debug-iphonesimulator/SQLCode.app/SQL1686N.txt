

SQL1686N  An error was encountered during DB2START processing on host
      "<host-name>" as the database manager failed to activate the host.

Explanation: 

The database manager tried to start up the instance on the host while
the DB2 cluster services failed to make the host active and rejoin the
DB2 pureScale cluster.

User response: 

Determine why the DB2 cluster services failed to make the host active
and rejoin the DB2 cluster.
