

SQL2037N  TSM could not be loaded.

Explanation: 

A call to a database utility specified TSM as the target or source of
the backup. An attempt was made to load the TSM client. Either the TSM
client is not available on the system or an error was encountered in the
load procedure.

User response: 

Ensure that the system has TSM available. Resubmit the command after TSM
is made available, or resubmit the command without utilizing TSM.
