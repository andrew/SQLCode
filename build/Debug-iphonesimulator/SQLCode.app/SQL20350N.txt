

SQL20350N  Authentication at the user mapping repository for plugin
      "<plugin-name>" failed.

Explanation: 

The user mapping from the user mapping repository for plugin
"<plugin-name>" cannot be accessed because authentication at the user
mapping repository failed.

User response: 

See the federation documentation for details on user mapping plugins.
Correct the repository connection credential parameters of the plugin.

sqlcode: -20350

sqlstate: 42516
