

SQL2900W  The ingest utility will not be able to pre-partition the input
      records. Reason code: "<reason-code>"

Explanation: 

The ingest utility uses the distribution key to determine the partition
number and routes input records to one of that partition's flushers.
Because of the condition that the reason code specifies, the ingest
utility will route records to a random flusher. The reason codes are:

1        

         The NUM_FLUSHERS_PER_PARTITION configuration parameter was set
         to 0, so there is only 1 flusher for all partitions.


2        

         The target table is a type that does not have a distribution
         key.


3        

         At least one of the distribution key columns has a type that is
         a user-defined type (UDT) or a DB2SECURITYLABEL.


4        

         If the SQL statement is INSERT or REPLACE, the column list does
         not specify all the distribution key columns. If the SQL
         statement is UPDATE, DELETE, or MERGE, the WHERE or ON clause
         does not specify all the distribution key columns.


5        

         If the SQL statement is INSERT or REPLACE, a value in the
         VALUES list is not a field name or constant. If the SQL
         statement is UPDATE, DELETE, or MERGE, one or more of the
         following is true:

          
         *  The comparison to a distribution key column is not an
            equality comparison.
         *  The value compared to a distribution key column is not a
            field name or constant.
         *  The distribution key column corresponds to more than one
            field name or constant.


6        

         The SQL statement is UPDATE, DELETE, or MERGE and the WHERE or
         ON clause is not of the form:

          

         (dist-key-col1 = value1) AND (dist-key-col2 = value2) AND ...
         (dist-key-colN = valueN)

          

         where dist-key-col1 to dist-key-colN includes all the
         distribution key columns and each value is a field name or
         constant.


7        

         There is at least one distribution key column that has a
         numeric type, but its corresponding field does not have the
         exact same numeric type, including the same precision and
         scale.

User response: 

If this has a negative performance impact, modify the configuration
parameter or INGEST command, depending on the reason code:

1        

         Set the NUM_FLUSHERS_PER_PARTITION configuration parameter to 1
         or greater.


2        

         Specify a target table that has a distribution key.


3        

         Specify a table whose distribution key does not contain a
         column whose type is a user-defined type (UDT) or a
         DB2SECURITYLABEL.


4        

         Ensure that all distribution keys are specified on the SQL
         statement and that each distribution key corresponds to exactly
         one field.


5        

         Ensure that all of the following are true:

          
         *  For INSERT or REPLACE statements, all value in the VALUES
            list are field names or constants.
         *  For UPDATE, DELETE, or MERGE statements: 
            *  Any comparison to a distribution key column is an
               equality comparison.
            *  Any value compared to a distribution key column is a
               field name or constant.
            *  Any distribution key column corresponds to one field name
               or constant.


6        

         Change the WHERE predicate or the ON predicate that it
         specifies all distribution key columns and it is of the form:

          

         (dist-key-col1 = value1) AND (dist-key-col2 = value2) AND ...
         (dist-key-colN = valueN)

          

         where dist-key-col1 to dist-key-colN includes all the
         distribution key columns and each value is a field name or
         constant.


7        

         Change the field definitions so that each field that
         corresponds to a numeric distribution key column has the exact
         same type as the distribution key column, including the same
         precision and scale.


   Related information:
   num_flushers_per_partition - Number of flushers per database
   partition configuration parameter
   INGEST command
