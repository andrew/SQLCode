

SQL0638N  Table "<name>" cannot be created because no column definitions
      were specified.

Explanation: 

The CREATE TABLE statement does not contain any column definitions.

The statement cannot be processed.

User response: 

Add one or more column definitions to the statement.

 sqlcode: -638

 sqlstate: 42601
