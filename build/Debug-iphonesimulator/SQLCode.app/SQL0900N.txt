

SQL0900N  The application state is in error. A database connection does
      not exist.

Explanation: 

A connection to a database does not exist. This may be due to one of the
following reasons: 
*  A serious error in the application state has caused the database
   connection to be lost.
*  The application may have disconnected from a database and not
   established a new current connection before executing the next SQL
   statement.
*  A request to switch the user in a trusted connection was
   unsuccessful.

User response: 

Reestablish a current connection by either switching to an existing
dormant connection (using CONNECT TO or SET CONNECTION), by establishing
a new connection (using CONNECT), or by successfully switching to
another user in a trusted connection.

 sqlcode: -900

 sqlstate: 08003
