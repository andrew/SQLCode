

SQL2094W  The rebalance of table space "<tablespace-name>" either did
      not add or drop containers, or there was insufficient disk space
      to create all of the containers. Reason code: "<reason-code>".

Explanation: 

A rebalance operation attempts to drop containers from storage paths in
the "drop pending" state, and to create new containers on recently added
storage paths. The operation also tries to create containers for stripe
sets that do not already include all of the database storage paths. This
warning occurred on one or more database partitions as a result of one
of the following reason codes:

1        

         No containers need to be added or dropped.


2        

         No containers need to be dropped, but some stripe sets in the
         table space do not contain containers for every storage path.
         However, the storage paths do not have enough disk space for
         any of the new containers or the table space has reached its
         maximum size.


3        

         No containers need to be dropped, but some stripe sets in the
         table space do not contain containers for every storage path.
         Some containers could not be created because some storage paths
         do not have enough disk space, or the table space has reached
         its maximum size. The new containers will be added to the table
         space and the data will be rebalanced.


4        

         Some containers need to be dropped and some stripe sets in the
         table space do not contain containers for every storage path.
         However, the corresponding storage paths do not have enough
         disk space for any of the containers to be created. The
         containers on the "drop pending" storage paths will be dropped
         and the data will be rebalanced.


5        

         Some containers need to be dropped and some stripe sets in the
         table space do not contain containers for every storage path.
         Sufficient disk space exists to create some of these
         containers, but not all. Containers in the "dropped pending"
         storage paths will be dropped and new containers will be added
         as needed to stripe sets. The data will be rebalanced.

User response: 

1        

         No rebalance is necessary.


2        

         Increase the amount of free space on the storage paths that are
         full or increase the maximum allowable size of the table space.
         Then try the request again.


3        

         Wait for the current rebalance to complete. Then, increase the
         amount of free space on the storage paths that are full or
         increase the maximum allowable size of the table space. Then
         try the request again.


4        

         Wait for the current rebalance to complete. Then increase the
         amount of free space on the storage paths that are full and try
         the request again.


5        

         Wait for the current rebalance to complete. Then increase the
         amount of free space on the storage paths that are full and try
         the request again.

To increase the amount of free space on a storage path either increase
the size of the file system or delete non-database data.

sqlcode: +2094

sqlstate: 01690
