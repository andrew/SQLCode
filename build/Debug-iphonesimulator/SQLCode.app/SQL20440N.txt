

SQL20440N  Array value with cardinality "<cardinality>" is too long. The
      maximum cardinality allowed is "<max-cardinality>".

Explanation: 

The array value required truncation from its cardinality of
"<cardinality>" to a maximum cardinality of "<max-cardinality>". A
system (built-in) cast or adjustment function was called to transform
the value in some way. The truncation is not allowed where the value is
used.

The array value being transformed is one of the following:

*  An argument to a stored procedure call
*  The result of a call to the ARRAY_AGG function
*  The result of an array constructor used in the right side of a SET
   statement
*  An argument to a cast function

User response: 

Examine the SQL statement to determine where the transformation is
taking place. Either the input to the transformation is too long, or the
target is too short. Explicitly reduce the cardinality of the input, or
increase the cardinality that the target can support.

sqlcode: -20440

sqlstate: 2202F
