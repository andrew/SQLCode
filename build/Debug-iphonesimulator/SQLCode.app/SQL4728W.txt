

SQL4728W  A priority setting was assigned to a service class that is
      higher than the priority setting of the default system service
      class SYSDEFAULTSYSTEMCLASS and this might negatively impact
      performance.

Explanation: 

To ensure that system work can take precedence over user work, the
priority settings of the default system service class
SYSDEFAULTSYSTEMCLASS should always be higher than the priorities that
were set for all other service classes. Failure to assign higher
priority settings to the default system service class can result in a
negative impact on performance because system-type activities run in the
default system service class.

User response: 

Raise the priority setting of the default system service class, or lower
the priority setting of other service classes that have a higher
priority setting than the default system service class.

sqlcode: +4728

sqlstate: 01HN1
