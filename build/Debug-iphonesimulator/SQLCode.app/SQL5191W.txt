

SQL5191W  The target of the data change operation is a table
      "<table-name>", which includes a period "<period-name>". The
      operation caused an adjustment to a recorded time value for the
      period.

Explanation: 

Table "<table-name>" is a system-period temporal table. The table
includes a period "<period-name>". The requested data change operation
was performed and resulted in an adjustment to a recorded time value for
the period. This can occur for one of the following reasons:

*  Two transactions are accessing the same row. The transaction that
   started first has a statement that is updating or deleting the row
   after a statement in the second transaction has already changed that
   row. This results in the timestamp value in the row begin column
   being the start time of the second transaction. The first transaction
   cannot update or delete a row if the row begin time stamp on that row
   is later than when the first transaction started.
*  Data has been loaded into the system-period temporal table with
   values for the row begin column that override the generated values.
   The row begin column value that was loaded is in the future relative
   to the timestamp that the executing transaction would use.

For an update operation, the adjustment can affect both the begin and
end columns for the period. For a delete operation, the adjustment only
affects the end column for the period.

The statement was processed with an adjustment to the recorded time for
the period.

User response: 

If you did not want the adjustment, issue a rollback and retry the
transaction. If data has been loaded into the system-period temporal
table with values for the row begin column that override the generated
values, clean the data such that the row-begin time values are less than
or equal to the value of CURRENT TIMESTAMP. Otherwise, no action is
required.

The systime_period_adj configuration parameter can be set to block the
adjustment of the affected values.

sqlcode: +5191

sqlstate: 01695


   Related information:
   systime_period_adj - Adjust temporal SYSTEM_TIME period database
   configuration parameter
