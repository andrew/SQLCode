

SQL9303N  No "<option>" is specified.

Explanation: 

The required option "<option>" is not specified.

The command cannot be processed.

User response: 

Resubmit the command with the required option.
