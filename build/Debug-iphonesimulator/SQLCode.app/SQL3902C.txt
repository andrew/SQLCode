

SQL3902C  A system error occurred. Further processing is not possible.
      Reason code = "<reason-code>".

Explanation: 

A system error occurred.

User response: 

If trace was active, invoke the Independent Trace Facility at the
operating system command prompt. Then contact your technical service
representative with the following information: 
*  Problem description
*  SQLCODE and embedded reason code
*  SQLCA contents if possible
*  Trace file if possible.
