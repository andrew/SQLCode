

SQL16233N  XML document contains a duplicate ID value "<ID-value>".

Explanation: 

While parsing an XML document the parser encountered duplicate ID value
"<ID-value>".

Parsing or validation did not complete.

User response: 

Change the duplicate ID values to unique ID values in the XML document
and try the operation again.

sqlcode: -16233

sqlstate: 2200M
