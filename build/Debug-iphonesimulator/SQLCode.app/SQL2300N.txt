

SQL2300N  The identifier for the table name is too long or it was not
      specified as part of the table name.

Explanation: 

The table name must be fully qualified. The format is authid.tablename
where authid contains 1 to 128 bytes and tablename contains 1 to 128
bytes in.

The utility stops processing.

User response: 

Resubmit the command with the fully qualified table name, including the
correct qualifier.
