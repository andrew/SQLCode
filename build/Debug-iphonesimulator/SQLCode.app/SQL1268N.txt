

SQL1268N  Rollforward recovery processing has stopped because of error
      "<error>" while retrieving log file "<logfile>" for database
      "<database-name>" on database partition "<dbpartitionnum>" and log
      stream "<log-stream-ID>".

Explanation: 

Rollforward processing is unable to retrieve a required log file. This
error is sometimes returned because the target system to which you are
attempting to restore the backup image does not have access to the
facility being used by the source system to archive its transaction
logs.

(Note: If you are using a partitioned database server, the database
partition number indicates the database partition on which the error
occurred. Otherwise, this value is not pertinent and should be ignored.)

User response: 

*  Ensure that log archiving is being performed correctly by checking
   the administration notification log. Correct any errors and resume
   rollforward recovery.
*  If the database is configured to use log archiving through a userexit
   program, check the userexit diagnostic logs to determine whether an
   error occurred while executing the userexit program. Correct the
   error and resume rollforward recovery.
*  Ensure that the target system to which you are attempting to restore
   the backup image has access to the facility being used by the source
   system to archive its transaction logs. Make appropriate changes and
   resume rollforward recovery.
