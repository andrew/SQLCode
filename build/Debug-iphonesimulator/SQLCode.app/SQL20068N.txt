

SQL20068N  The structured type "<type-name>" may not be defined so that
      one of its attribute types directly or indirectly uses itself. The
      attribute "<attribute-name>" causes the direct or indirect use.

Explanation: 

Direct use: Type A is said to directly use type B if one of the
following is true: 
*  Type A has an attribute of type B
*  Type B is a subtype of A, or a supertype of A.

Indirect use: Type A is said to indirectly use type B if the following
is true: 
*  Type A uses type C, and type C uses type B.

You cannot define a type so that one of its attribute types directly or
indirectly uses itself. The type for attribute "<attribute-name>" is the
cause of the direct or indirect use.

User response: 

Evaluate the type and remove the attribute type that causes the direct
or indirect use.

 sqlcode: -20068

 sqlstate: 428EP
