

SQL0801N  Division by zero was attempted.

Explanation: 

The processing of a column function or arithmetic expression resulted in
division by zero.

The statement cannot be processed. For the INSERT, UPDATE, or DELETE
statements, no inserts or updates are performed.

User response: 

Examine the SQL statement to determine the cause of the problem. If the
problem is data dependent, it is necessary to examine the data processed
when the error occurred. Refer to the SQL Reference to see the valid
ranges for the data types.

Federated system users: examine the SQL statement to determine the cause
of the problem. If the problem is data dependent, examine the data being
processed at the data sources when the error occurred.

 sqlcode: -801

 sqlstate: 22012
