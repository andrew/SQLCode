

SQL0581N  The data types of the result-expressions of a CASE expression
      or DECODE function are not compatible.

Explanation: 

There is a CASE expression or a DECODE function in the statement that
has result-expressions (expressions following THEN and ELSE keywords for
CASE expressions) that are not compatible.

The data type of a CASE expression or the result of a DECODE function is
determined using the "Rules for Result Data Types" on the
result-expressions.

The statement cannot be processed.

User response: 

Correct the result-expressions so that they are compatible.

sqlcode: -581

 sqlstate: 42804
