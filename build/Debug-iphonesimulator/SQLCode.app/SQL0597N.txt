

SQL0597N  Unable to retrieve a DATALINK value. Reason code =
      "<reason-code>".

Explanation: 

A DATALINK value could not be retrieved. The possible reason codes are
as follows: 

01       The DB2 Data Links Manager does not authorize the DB2 user to
         retrieve a DATALINK value embedded with a write token for
         modifying the DATALINK value referenced file.

User response: 

The action is based on the reason code as follows. 

01       Contact the DB2 Data Links Manager administrator to grant the
         write access privilege to this file.

sqlcode: -0597

sqlstate: 42511
