

SQL2905I  The following error occurred issuing the SQL "<sql-statement>"
      statement on table "<table-name>" using data from line
      "<line-number>" of input file "<file-name>".

Explanation: 

This message provides the identification of the line and input file
where the error occurred for the following message listed.

The table is either the one specified on the SQL statement of the INGEST
command or the exception table. If the table is the one specified on the
SQL statement, and the SQL statement is INSERT or REPLACE, and the
INGEST command specified an exception table, the ingest utility attempts
to insert the record into the exception table. Otherwise, the ingest
utility discards the record.

User response: 

If the error occurred issuing the SQL statement on the target table of
the INGEST command and the ingest utility successfully inserted the row
into the exception table, correct the data in the exception table and
copy it from the exception table to the target table. Otherwise, verify
that the data on the specified line in the input file is correct. If
needed, correct the data in the input file and re-run the ingest utility
using an input file with only the corrected lines.


   Related information:
   INGEST command
