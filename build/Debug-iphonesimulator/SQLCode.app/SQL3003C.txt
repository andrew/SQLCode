

SQL3003C  An I/O error occurred while closing the output data file.

Explanation: 

A system I/O error occurred while closing the output data file.

The file is not closed.

User response: 

If the output data file is incomplete, erase it and resubmit the
command.
