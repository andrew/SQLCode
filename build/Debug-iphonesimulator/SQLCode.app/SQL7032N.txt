

SQL7032N  SQL procedure "<procedure-name>" not created. Diagnostic file
      is "<file-name>".

Explanation: 

SQL Procedure "<procedure-name>" was not created. One of the following
has occurred: 
*  Support for SQL stored procedures is not installed and configured on
   this server. You need to have the IBM Data Server Client and a C
   compiler installed on the server to create SQL procedures. You may
   need to set the DB2 registry variable DB2_SQLROUTINE_COMPILER_PATH to
   point to a script or batch file that contains environment settings
   for the C compiler on your platform.
*  DB2 failed to precompile or compile the SQL stored procedure. DB2
   creates an SQL procedure as a C program that contains embedded SQL.
   Errors not found during the initial parsing of the CREATE PROCEDURE
   statement can be found during the precompile or compile stage.

For UNIX platforms, the full path of the file that contains diagnostic
information is:

 $DB2PATH/function/routine/sqlproc/ \
 $DATABASE/$SCHEMA/tmp/"<file-name>"

 where $DATABASE represents the name of the database, and $SCHEMA
represents the schema name of the SQL procedure.For Windows operating
systems, the full path of the file that contains diagnostic information
is:

 %DB2PATH%\function\routine\sqlproc\ \
 %DATABASE%\%SCHEMA%\tmp\"<file-name>"

 where %DATABASE% represents the name of the database, and %SCHEMA%
represents the schema name of the SQL procedure.User response: 

Ensure that both a compatible C compiler and a DB2 Application
Development Client are installed on the server. If a precompile or
compile error occurred, refer to the messages from the precompiler or
compiler in the diagnostic file "<file-name>".

Ensure that the DB2 registry variable DB2_SQLROUTINE_COMPILER_PATH is
set to point to a script or batch file that sets up the C compiler
environment. On a UNIX operating system, for example, you may create a
script called "sr_cpath" in the
/home/DB2INSTANCE/sqllib/function/routine directory. To set the DB2
registry variable DB2_SQL_ROUTINE_COMPILER_PATH accordingly, issue the
following command: 

db2set DB2_SQLROUTINE_COMPILER_PATH =
/home/DB2INSTANCE/sqllib/function/routine/sr_cpath

 sqlcode: -7032

 sqlstate: 42904
