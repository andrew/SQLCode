

SQL10005N  The mode parameter "<parameter>" in the CONNECT TO statement
      is not valid. It must be SHARE for shared access, EXCLUSIVE for
      exclusive use or EXCLUSIVE MODE ON SINGLE NODE for exclusive use
      on single node. For DB2 Connect connections, only SHARE mode is
      supported. EXCLUSIVE MODE ON SINGLE NODE is only supported in MPP
      configuration.

Explanation: 

The mode parameter of the CONNECT TO statement must be SHARE for shared,
EXCLUSIVE for exclusive use or EXCLUSIVE MODE ON SINGLE NODE for
exclusive use on single node. If connecting to a database usingDB2
Connect , only shared access is allowed. EXCLUSIVE MODE ON SINGLE NODE
is only supported in MPP configuration.

The command cannot be processed.

User response: 

Resubmit the command with a valid mode parameter.
