

SQL0121N  The target name "<name>" is specified more than once for
      assignment in the same SQL statement.

Explanation: 

The same target name "<name>" is specified more than once as an OUT or
INOUT argument of a CALL statement, or in the list of columns of an
INSERT statement, the left hand side of assignments in the SET clause of
an UPDATE statement, or the left hand side of the assignment statement.
The target name identifies a column, SQL parameter, or variable.

Note that this error may occur when updating or inserting into a view
where more than one column of the view is based on the same column of a
base table.

The statement cannot be processed.

User response: 

Correct the syntax of the statement so each column name is specified
only once.

 sqlcode: -121

 sqlstate: 42701
