

SQL2435N  Database activation failed because there is insufficient
      memory available in the CF for this database. Database name:
      "<database-name>".

Explanation: 

This message is returned when an attempt is made to have multiple active
databases in a DB2 pureScale environment, but the activation of one
database fails because there is not enough memory in the cluster caching
facility, also known as CF, for that database.

You can configure the total amount of memory that is available for the
CF using the cf_mem_sz database manager configuration parameter. You can
configure the percentage of the total CF memory that is assigned to each
database that has the cf_db_mem_sz database configuration parameter set
to AUTOMATIC using the DB2_DATABASE_CF_MEMORY registry variable. (Any
database that has cf_db_mem_sz set to a specific value will ignore the
DB2_DATABASE_CF_MEMORY registry variable.)

Use of the DB2_DATABASE_CF_MEMORY registry variable must be coordinated
with the cf_db_mem_sz database configuration paremter and the numdb
database manager configuration parameter.

Example  

         If there are four databases to be active at once, then the
         configuration parameters should be coordinated like this:

          
         *  The database manager configuration parameter numdb should be
            set to 4
         *  If the database configuration parameter cf_dm_mem_sz is set
            to AUTOMATIC for each of the four database, then the
            registry variable DB2_DATABASE_CF_MEMORY should be set to 25

One reason this error can occur is because the DB2_DATABASE_CF_MEMORY
registry variable is set to 100 when numdb is greater than 1.

User response: 

Respond to this error in one of the following ways:

*  Determine the amount of CF memory used by other databases and see if
   any settings need to be changed in order to accommodate the CF memory
   requirements of this database.
*  If the cf_db_mem_sz database configuration parameter is set to
   automatic for this database, increase the amount of memory available
   in the CF for this database by modifying the value of the
   DB2_DATABASE_CF_MEMORY registry variable.


   Related information:
   Multiple active databases in a DB2 pureScale environment
   DB2 pureScale CF memory parameter configuration
   cf_mem_sz - CF memory configuration parameter
   numdb - Maximum number of concurrently active databases including
   host and System i databases configuration parameter
   cf_db_mem_sz - Database memory configuration parameter
   Miscellaneous variables
