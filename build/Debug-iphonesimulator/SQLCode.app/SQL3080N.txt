

SQL3080N  The value in the length field of the C record is too small.

Explanation: 

The value in the length field of a C record is not large enough so the C
record is not valid.

The utility stops processing. No data is loaded.

User response: 

Examine the length field in the C records.
