

SQL3186W  Data was not loaded into the database, because the log was
      full or the lock space was exhausted. SQLCODE "<sqlcode>" was
      returned. A commit will be attempted and the operation will
      continue if the commit is successful.

Explanation: 

The utility could not insert a row of data into the database because
either the database transaction log is full or the lock space available
to the application is full.

The completed database transactions are committed and the insert is
tried again. If the repeated insert continues to indicate the same
failure, the utility stops processing.

User response: 

Be aware that a subsequent failure of the utility causes the database to
roll back to the state after the last commit, not to the state before
the utility was initially called.
