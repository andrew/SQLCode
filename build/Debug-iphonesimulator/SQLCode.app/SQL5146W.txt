

SQL5146W  "<Parameter_1>" must be set to "<Parameter_3>" when
      "<Parameter_2>" is "<Parameter_3>". "<Parameter_1>" has been set
      to "<Parameter_3>".

Explanation: 

If "<Parameter_2>" is AUTOMATIC then "<Parameter_1>" must be set to
AUTOMATIC as well. To this end, "<Parameter_1>" was set to AUTOMATIC.

User response: 

"<Parameter_1>" was set to AUTOMATIC by DB2. To set this parameter to a
different value, first set "<Parameter_2>" to a value and then set
"<Parameter_1>" to a value using the MANUAL option of the db2 update
database configuration command.
