

SQL0283N  System temporary table space "<tablespace-name>" cannot be
      dropped because it is the only system temporary table space with a
      "<page-size>" page size in the database.

Explanation: 

A database must contain at least one system temporary table space with
the same page size as the page size of the catalog tablespace. Dropping
table space "<tablespace-name>" would remove the last system temporary
tablespace with a "<page-size>" page size from the database.

User response: 

Ensure there will be another system temporary table space with a
"<page-size>" page size in the database before attempting to drop this
table space.

 sqlcode: -283

 sqlstate: 55026
