

SQL3083N  The D-record-id field in the C record for column "<name>"
      cannot be converted to a numeric value.

Explanation: 

The D-record-id field in the C record for the indicated column is not an
ASCII representation of a number.

The utility stops processing. No data is loaded.

User response: 

Examine the D-record-id field in the C record.
