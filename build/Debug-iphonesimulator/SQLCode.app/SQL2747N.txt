

SQL2747N  A record was too long while reading record "<rec-no>" from the
      input data file.

Explanation: 

For a positional ASC input data file or a delimited data file with
parameter 32KLIMIT on, the maximum record length can not exceed the 32k
(bytes) limit.

User response: 

Check your input data file and make sure the record length is less than
32k bytes.
