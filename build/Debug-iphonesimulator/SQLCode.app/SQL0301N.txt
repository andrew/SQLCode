

SQL0301N  The value of input variable, expression or parameter number
      "<number>" cannot be used because of its data type.

Explanation: 

A variable, expression, or parameter in position "<number>" could not be
used as specified in the statement because its data type is incompatible
with the intended use of its value.

This error can occur as a result of specifying an incorrect host
variable or an incorrect SQLTYPE value in a SQLDA on an EXECUTE or OPEN
statement. In the case of a user-defined structured type, the associated
built-in type of the host variable or SQLTYPE might not be compatible
with the parameter of the TO SQL transform function defined in the
transform group for the statement. In the case of performing an implicit
or explicit cast between character and graphic data types, this error
indicates that such a cast was attempted with a non-Unicode character or
graphic string.

The statement cannot be processed.

User response: 

Verify that the data types of all host variables in the statement are
compatible with the manner in which they are used.

sqlcode: -301

sqlstate: 07006
