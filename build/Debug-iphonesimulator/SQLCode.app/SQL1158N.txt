

SQL1158N  The directory "<directory-name>" is restricted from access.

Explanation: 

The directory "<directory-name>" may contain database sensitive
information and so access to it is restricted. In order to maintain
database integrity, applications cannot create a new file, read from a
file, write to a file, or delete a file under this directory.

User response: 

Specify a different directory name.

 sqlcode: -1158

 sqlstate: 42832
