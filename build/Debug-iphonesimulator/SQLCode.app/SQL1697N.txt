

SQL1697N  The statement cannot be precompiled because an indicator
      variable structure was specified that contains fewer members than
      the corresponding host variable structure. Indicator variable
      structure name: "<variable-name>". Number of members in the host
      variable structure: "<number-of-members>".

Explanation: 

In embedded SQL applications, information about the contents of host
variable structures can be stored in corresponding indicator variable
structures. When an indicator variable structure is specified, the
number of members in the indicator variable structure must be equal to
the corresponding host variable structure.

This message is returned when the number of members in an indicator
variable structure does not equal the number of members in the
corresponding host variable structure.

User response: 

1. Modify the embedded SQL application to declare equal number of
   members in the indicator variable structure as that of the host
   variable structure.
2. Recompile and rerun the embedded SQL application.


   Related information:
   GDPC High Availability and Disaster Recovery
