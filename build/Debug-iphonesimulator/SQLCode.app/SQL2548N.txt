

SQL2548N  Database code page indicated within the backup image
      "<code-page>" is invalid or not supported. The restore operation
      has failed.

Explanation: 


1. The backup image you are restoring may have been created on a server
   with a newer FixPak level than the FixPak level that has been applied
   to this server. In this case, it is possible that the image contains
   a newer code page that is unsupported.
2. The backup image is corrupt and contains invalid code page
   information.

User response: 

If attempting to restore an image from a server with a newer FixPak
level to a server with a lesser FixPak level, then ensure that the code
page is supported by both servers. Alternatively, consider applying the
newer FixPak to the server you are restoring to.
