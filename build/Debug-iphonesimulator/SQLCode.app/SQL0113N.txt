

SQL0113N  "<identifier>" contains a character that is not allowed or
      does not contain any characters.

Explanation: 

An SQL-variable-name, parameter-name, security label component element,
or condition-name "<identifier>" contains an invalid character.

for SQL-variable-name, parameter-name, and condition-name, only
characters that are valid for an SQL ordinary identifier are allowed.
Note that because the identifier is delimited, folding is not performed
and uppercase and lowercase letters are treated as distinct from each
other.

For security label component element, only characters that are valid for
element values are allowed.

User response: 

Correct the identifier and resubmit the statement.

 sqlcode: -113

 sqlstate: 42601
