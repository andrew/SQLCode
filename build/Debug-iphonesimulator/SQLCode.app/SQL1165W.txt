

SQL1165W  A value cannot be assigned to a host variable because the
      value is not within the range of the host variable's data type.

Explanation: 

A FETCH, VALUES, or SELECT into a host variable list failed because the
host variable was not large enough to hold the retrieved value.

The statement processing continued returning a null indicator of -2.

User response: 

Verify that table definitions are current and that the host variable has
the correct data type. For the ranges of SQL data types, refer to the
SQL Reference.

 sqlcode: +1165

 sqlstate: 01515
