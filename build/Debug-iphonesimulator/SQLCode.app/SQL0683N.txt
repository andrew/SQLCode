

SQL0683N  The specification for column, attribute, user-defined type or
      function "<data-item>" contains incompatible clauses.

Explanation: 

There is an error in a data item specification in a CREATE statement,
ALTER statement, an XMLTABLE expression, or a typed-correlation clause
of a SELECT statement which is referencing a generic table function.
Incompatible specifications are present, such as: "INTEGER and FOR BIT
DATA". If the column is of type DB2SECURITYLABEL, incompatible
specifications include NOT NULL WITH DEFAULT. The location of the error
is given by "<data-item>" as follows:

*  For a CREATE TABLE statement, ALTER TABLE statement, an XMLTABLE
   expression, or a typed-correlation clause of a SELECT statement,
   "<data-item>" gives the name of the column containing the error.
*  For a CREATE FUNCTION statement, "<data-item>" is a token that
   identifies the area of the problem in the statement. For example,
   "PARAMETER 3" or "RETURNS" or "CAST FROM".
*  For a CREATE DISTINCT TYPE statement, "<data-item>" gives the name of
   the type being defined.
*  For a CREATE or ALTER TYPE statement, "<data-item>" identifies the
   clause containing the error or gives the name of the attribute
   containing the error.
*  For a CREATE or ALTER TABLE statement, the columns for a
   BUSINESS_TIME period must be defined as DATE, or TIMESTAMP(p) where p
   is between 0 and 12 (inclusive).
*  For a CREATE or ALTER TABLE statement, "<data-item>" for a ROW BEGIN,
   ROW END, or TRANSACTION START ID column must be TIMESTAMP(12).

The statement cannot be processed.

User response: 

Remove the incompatibility and try the statement again.

sqlcode: -683

sqlstate: 42842


   Related information:
   subselect
