

SQL3222W  ...COMMIT of any database changes was successful.

Explanation: 

The COMMIT was successful.

User response: 

None necessary if you get this message.
