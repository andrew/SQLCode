

SQL2765W  The utility failed to open output partition map file
      "<out-map-file>".

Explanation: 

The utility cannot open the output partition map file for writing. It
will write the output to stdout.

User response: 

Check your file access permissions.
