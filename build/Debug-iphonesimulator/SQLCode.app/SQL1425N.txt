

SQL1425N  A password has been supplied without a userid.

Explanation: 

Any command/API that accepts a userid and password will not accept a
password without a userid.

User response: 

Resubmit the command/API and supply a userid if you are also supplying a
password.
