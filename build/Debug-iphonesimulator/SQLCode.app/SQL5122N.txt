

SQL5122N  Access to the database was denied because of a machine
      dependent check.

Explanation: 

The database and database configuration file cannot be accessed because
of copy protection.

The user request is denied.

User response: 

Have a user with SYSADM authority return to the original database,
modify the configuration file to turn off the copy protection, and
create a new backup, which can be used to restore the database. If the
original database is no longer available, contact your service
representative.
