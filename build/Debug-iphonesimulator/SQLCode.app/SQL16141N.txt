

SQL16141N  XML schema contains an invalid derivation by RESTRICTION or
      EXTENSION. Reason code = "<reason-code>".

Explanation: 

While processing an XML schema the XML parser encountered a problem with
a type derived by RESTRICTION or EXTENSION. The possible reasons are
described by the "<reason-code>" values that follow. 

1        There is an invalid child following the RESTRICTION or
         EXTENSION element in a simpleContent definition.

2        There is an invalid child following the RESTRICTION or
         EXTENSION element in a complexContent definition.

3        The BASE attribute was not specified for the RESTRICTION or
         EXTENSION. Every derivation by RESTRICTION or EXTENSION must
         include an identification of the base type for that derivation.

4        Derivation by RESTRICTION or EXTENSION is prohibited by the
         base type or by XML schema.

5        The schema contains a forbidden restriction of 'any'. Valid
         restrictions for 'any' include 'choice', 'sequence', 'all', and
         'element'

6        The schema contains a forbidden restriction of 'all'. Valid
         restrictions for 'all' include 'choice', 'sequence', and
         'element'

7        The schema contains a forbidden restriction of 'choice'. Valid
         restrictions for 'choice' include 'sequence', 'all', and 'leaf'

8        The schema contains a forbidden restriction of 'sequence'.
         Valid restrictions for 'sequence' include 'element'.

9        The schema attempts to use a simple type in a derivation by
         RESTRICTION for a complexType

10       The schema attempts to use a simple type with a value of
         'final' in a derivation by EXTENSION.

Parsing or validation did not complete.

User response: 

Correct the XML schema and try the operation again.

sqlcode: -16141

sqlstate: 2200M
