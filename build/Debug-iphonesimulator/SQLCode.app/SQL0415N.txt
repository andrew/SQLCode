

SQL0415N  The data types of corresponding columns are not compatible in
      a fullselect that includes a set operator or in the multiple rows
      of a VALUES clause of an INSERT or fullselect.

Explanation: 

There are various statements where this error may occur.

*  It may occur within a SELECT or VALUES statement that includes set
   operations (UNION, INTERSECT, or EXCEPT). The corresponding columns
   of the subselects or fullselects that make up the SELECT or VALUES
   statements are not compatible.
*  It may occur within an INSERT statement that is inserting multiple
   rows. In this case, the corresponding columns of the rows specified
   in the VALUES clause are not compatible.
*  It may occur within a SELECT or VALUES statement where the VALUES
   clause is used with multiple rows. In this case, the corresponding
   columns of the rows specified in the VALUES clause are not
   compatible.

It may occur within the array constructor, when the data type of the two
values listed in the constructor are not compatible.

See Assignments and Comparisons in the SQL Reference for details on data
type compatibility.

The statement cannot be processed.

User response: 

Correct the column names used in the SELECT statements or the
expressions in the VALUES clause so that all corresponding columns are
compatible types.

sqlcode: -415

 sqlstate: 42825
