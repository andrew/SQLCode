

SQL20012N  Type "<type-name>" does not have any associated transform
      groups to drop.

Explanation: 

There are no transforms defined for "<type-name>". There is nothing to
drop.

The statement did not drop any transform groups.

User response: 

Ensure the name of the type (including any required qualifiers) is
correctly specified in the SQL statement and that the type exists.

 sqlcode: -20012

 sqlstate: 42740
