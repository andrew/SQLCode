

SQL3534I  The Load DELETE phase is approximately "<number>" percent
      complete.

Explanation: 

This is an informational message returned if the Load currently being
queried is in the DELETE phase.

User response: 

No action is necessary.
