

SQL20542N  The statement was not executed because the connection to the
      database server was dropped, and the automatic client reroute
      (ACR) feature failed to successfully re-execute the statement.

Explanation: 

The ACR feature attempts to reconnect to the database when a connection
to a database server is dropped. This message is returned when the ACR
feature is unable to seamlessly reconnect and execute the statement.

When this message is returned, the database connection is in an open
state.

User response: 

Execute the statement again.

If the error persists, perform the following troubleshooting steps:

1. Collect information from diagnostic logs on the client and the
   server.
2. Investigate why the connection to the database server is being lost.

sqlcode: -20542

sqlstate: 54068


   Related information:
   Automatic client reroute roadmap
