

SQL3159W  The double-byte code page field in the C record for column
      "<name>" is not valid. Data from the column will not be loaded.

Explanation: 

The double-byte code page field in the C record for the indicated column
is not valid.

Data from the indicated column is not loaded.

User response: 

Change the double-byte code page field in the C record and resubmit the
command.
