

SQL3095N  The specified column position "<position>" is not in the valid
      range of 1 to 256.

Explanation: 

A column position was specified that was not within the range of 1 to
256.

The utility stops processing. No data is loaded. The column names
processed before the error occurred are in the database.

User response: 

Verify that the specified column position is within the range 1 to 256.
