

SQL0602N  The index was not created or altered because too many columns,
      periods, or expression-based keys were specified.

Explanation: 

There is an upper limit to the number of columns that can be included in
an index. The number of columns that are allowed in a given index is
influenced by factors such as: the type of table, and whether the
columns that are being included in the index use random ordering.

This message is returned when an attempt is made to create an index,
alter an index, or create an index extension, and the resulting number
of columns in the index would exceed the upper limit.

Federated system users:

*  The limit on the number of columns that are allowed in an index
   varies for different data sources.
*  This problem might be detected on the federated server or on the data
   source.

User response: 

Review the restrictions related to the maximum number of columns in an
index. Then modify the index definition to have fewer columns.

Federated system users: change the index definition to conform to the
column limit for the data source.

sqlcode: -602

sqlstate: 54008


   Related information:
   CREATE INDEX statement
   CREATE INDEX EXTENSION statement
   ALTER INDEX statement
