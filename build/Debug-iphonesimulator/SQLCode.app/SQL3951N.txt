

SQL3951N  The satellite ID cannot be found locally.

Explanation: 

Either the operating system logon was bypassed or the DB2SATELLITEID
registry variable was not set.

User response: 

If you are using the operating system logon ID as the satellite ID, log
on to the operating system. If you use the DB2SATELLITEID registry
variable, ensure that it is set to the unique ID for the satellite.
