

SQL6566N  The LOAD command is missing from the AutoLoader configuration
      file.

Explanation: 

The LOAD command is missing from the AutoLoader configuration file. The
parameter must be specified.

User response: 

Ensure that you have specified the correct configuration file for the
AutoLoader, and that the LOAD command is specified inside.
