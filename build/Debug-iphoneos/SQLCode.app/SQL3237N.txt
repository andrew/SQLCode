

SQL3237N  The supplied EXPORT Action String cannot be used with the
      XMLSAVESCHEMA option. Reason code: "<reason-code>"

Explanation: 

Reason codes:

1         The Action String is too long due to XML specific processing.

2         The Action String contains a "WITH" clause, which is
         incompatible with the XMLSAVESCHEMA option.

User response: 

Resubmit the command without the XMLSAVESCHEMA option or modify the
Action String.
