

SQL0463N  Routine "<routine-name>" (specific name "<specific-name>") has
      returned an invalid SQLSTATE "<state>", with diagnostic text
      "<text>".

Explanation: 

The valid SQLSTATEs that a routine can return are 38xxx (error), 38502
(error) and 01Hxx (warning). This routine "<routine-name>" (specific
name "<specific-name>") returned an invalid SQLSTATE "<state>", along
with message text "<text>". The routine is in error.

User response: 

The routine will need to be corrected. See your database administrator,
or the author of the routine. The application significance of the bad
SQLSTATE can also be learned from the routine author.

 sqlcode: -463

 sqlstate: 39001
