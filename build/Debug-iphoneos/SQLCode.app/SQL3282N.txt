

SQL3282N  The supplied credentials are not valid.

Explanation: 

Either or both the distinguished name (DN) of the user and the password
that were specified were not valid.

This error may occur when a user is running in a Windows 2000 domain
environment , which supports LDAP, and logs into a local account which
may not have sufficient authority.

User response: 

Resubmit the command using valid values for both the distinguished name
(DN) of the user and the password.

If you are working in a Windows 2000 domain environment, ensure that
logon with an account that has sufficient authority.
