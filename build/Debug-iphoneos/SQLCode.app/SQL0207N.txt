

SQL0207N  A column name is not allowed in the ORDER BY clause of a
      SELECT statement used with a set operator.

Explanation: 

A SELECT statement with a set operator contains an ORDER BY clause,
which specifies column names. In this case, the list of columns in the
ORDER BY clause must contain only integers.

The statement cannot be processed.

User response: 

Specify only integers in the list of columns in the ORDER BY clause.

NOTE: This error is only applicable to releases of DB2 prior toVersion 2
and hosts accessed through DB2 Connect.

 sqlcode: -207

 sqlstate: 42706
