

SQL16275N  A name starting with "<string>" is not a valid QName. It was
      intended as a QName for "<structure-type>".

Explanation: 

During parsing of an XML document or XML schema for a QName, a string
starting with "<string>" was encountered that is not a valid QName. It
may have started with an invalid character or contained invalid
characters for a QName.

Parsing or validation did not complete.

User response: 

Correct the invalid QName in the XML document or XML schema and try the
operation again.

sqlcode: -16275

sqlstate: 2200M
