

SQL20387N  Two or more elements are specified for the security label
      component "<component-name>".

Explanation: 

A security label cannot have multiple elements for a component of type
ARRAY.

User response: 

Specify only one element for the security label component
"<component-name>".

sqlcode: -20387

sqlstate: 428GP
