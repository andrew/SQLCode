

SQL0629N  SET NULL cannot be specified because either the column or
      FOREIGN KEY "<name>" cannot contain null values.

Explanation: 

The SET NULL option of the ALTER TABLE statement or indicated FOREIGN
KEY clause is not valid. For an ALTER TABLE statement, the column
"<name>" cannot be altered to not allow NULL values because this column
was the last nullable column in the foreign key constraint with action
type 'on delete set NULL'.

For the FOREIGN KEY clause, no column of the key allows null values.
"<name>" is the constraint name, if specified. If a constraint name was
not specified, "<name>" is the first column name specified in the column
list of the FOREIGN KEY clause followed by three periods.

The statement cannot be processed.

User response: 

For an ALTER TABLE statement, either remove the foreign constraint or
change at least one other column in this constraint to be nullable. For
the FOREIGN KEY clause, change either a column of the key to allow an
assignment to the null value or change the delete rule.

 sqlcode: -629

 sqlstate: 42834
