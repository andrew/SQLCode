

SQL0293N  Error accessing a table space container.

Explanation: 

This error may be caused by one of the following conditions:

*  A container (directory, file or raw device) was not found.
*  A container is not tagged as being owned by the proper table space.
*  A container tag is corrupt.

This error can be returned during database startup and during the
processing of the ALTER TABLESPACE SQL statement.

User response: 

Try the following actions:

1. Check that the directory, file, or device exists and that the file
   system is mounted (if it is on a separate file system). Containers
   must be readable and writable by the database instance owner.
2. If you have a recent backup, try restoring the table space or
   database. If that fails because of the bad container and the
   container is not a DEVICE type, try manually removing the container
   first.

If the error was returned from the processing of an ALTER TABLESPACE SQL
statement with the SWITCH ONLINE option, then re-issue the statement
after correcting the problem.

If the error persists, call your IBM service representative.

sqlcode: -293

sqlstate: 57048
