

SQL20515N  A dynamic statement name cannot be used in the cursor value
      constructor.

Explanation: 

A dynamic statement name is specified in a cursor value constructor
that:

*  also specifies one or more named parameters in a cursor value
   constructor parameter list.
*  is assigned to a variable with a strongly typed cursor data type.

The statement cannot be processed.

User response: 

Do one of the following and try the request again:

*  Replace the dynamic statement name with a SELECT statement.
*  Remove the parameter list from the cursor value constructor.
*  Change the cursor variable to a weakly typed cursor data type.

sqlcode: -20515

sqlstate: 428HU
