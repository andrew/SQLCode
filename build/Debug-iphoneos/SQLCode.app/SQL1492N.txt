

SQL1492N  The database "<name>" was not deactivated because it was not
      activated.

Explanation: 

The database cannot be deactivated because the specified database was
not active.

User response: 

No action is required.
