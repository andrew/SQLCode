

SQL27922I  Output data files are not created because the run type is
      ANALYZE.

Explanation: 

This is an informational message indicating that the output data files
are not created because the mode of operation is ANALYZE.

User response: 

No action is required.
