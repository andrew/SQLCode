

SQL2980I  The ingest utility completed successfully at timestamp
      "<timestamp>"

Explanation: 

The ingest utility completed with no warnings or errors.

User response: 

No user response required.
