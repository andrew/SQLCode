

SQL29003N  DB2 Query Patroller could not load Java class "<class-name>",
      reason code "<reason-code>".

Explanation: 

An error occurred while trying to load the Java class "<class-name>".
The reason codes are: 

1        The class was not found on the CLASSPATH.

User response: 

Ensure the "<class-name>" is installed in the CLASSPATH.
