

SQL1621N  The transaction in which the specified event monitor or usage
      list was created has not yet been committed. The event monitor or
      usage list cannot be activated.

Explanation: 

An event monitor or usage list cannot be activated until the transaction
in which it was created has been committed.

User response: 

Commit the transaction in which the event monitor or usage list was
created and then reissue the SET EVENT MONITOR or SET USAGE LIST
statement.

sqlcode: -1621

sqlstate: 55033
