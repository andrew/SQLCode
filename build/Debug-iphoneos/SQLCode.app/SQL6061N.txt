

SQL6061N  Roll-forward recovery on database "<name>" cannot reach the
      specified stop point (end-of-log or point-in-time) because of
      missing log file(s) on node(s) "<node-list>".

Explanation: 

The Rollforward Database utility cannot find the necessary log file(s)
in the logpath.

User response: 

Do one of the following: 
*  Use the ROLLFORWARD DATABASE command with the QUERY STATUS option to
   determine which log files are missing. When you find the log files,
   put them in the log path and resume forward recovery.
*  If you cannot find the missing log files, restore the database on all
   nodes, then do point-in-time recovery using a timestamp that is
   earlier than that of the earliest missing log file.
