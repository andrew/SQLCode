

SQL3172W  The specified input column "<name>" was not found. The
      corresponding database column will contain null values.

Explanation: 

The specified input column was not found in the input spreadsheet file.
The database column is nullable and contains null values.

User response: 

Verify the specified input column name.
