

SQL22009N  There is no health contact information for this instance.

Explanation: 

There is no health contact information for this instance.

User response: 

If the current contact information is correct, no action has to be
taken.
