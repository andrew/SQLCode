

SQL3133W  The field in row "<row-number>" and column "<column-number>"
      contains invalid DATALINK value. A null was loaded.

Explanation: 

The DATALINK value in the specified field is invalid. For delimited
ASCII (DEL) files, the value of the column number specifies the field
within the row that contains the value in question. For ASCII files, the
value of the column number specifies the byte location within the row
where the value in question begins.

A null value is loaded.

User response: 

Examine the input value. If necessary, correct the input file and
resubmit the command or edit the data in the table.
