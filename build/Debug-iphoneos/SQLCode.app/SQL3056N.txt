

SQL3056N  The input file is not a valid PC/IXF file. The value in the
      length field of the H record is too small.

Explanation: 

The value in the length field of the H record is not large enough for a
valid H record. The file may not be a PC/IXF file.

The utility stops processing. No data is loaded.

User response: 

Verify that the input file is correct.
