

SQL20262N  Invalid usage of WITH ROW MOVEMENT in view "<view-name>".
      Reason code = "<reason-code>".

Explanation: 

The view "<view-name>" has been defined with the WITH ROW MOVEMENT
clause. This clause is not applicable for the view because of one of the
following:

1. The view's outermost fullselect is not a UNION ALL.
2. The view contains nested UNION ALL operations other than in the
   outermost fullselect.
3. Not all view columns are updatable.
4. Two columns of the view are based on the same column of the base
   table.
5. One of the underlying views has an INSTEAD OF UPDATE trigger defined
   on it.
6. The view contains references to system-period temporal tables or
   application-period temporal tables.

The view cannot be created.

User response: 

Depending on the reason code, do the following:

1. Omit the WITH ROW MOVEMENT clause. It is not applicable for views
   without UNION ALL.
2. Rewrite the view body so that UNION ALL only occurs on the outermost
   fullselect.
3. Omit columns that are not updatable from the view definition.
4. Rewrite the view body so that each column of a base table is only
   referenced once in the view definition.
5. Omit the WITH ROW MOVEMENT clause and use an INSTEAD OF UPDATE
   trigger on the newly defined view.
6. Remove any reference to a system-period temporal table or an
   application temporal table.

sqlcode: -20262

sqlstate: 429BJ
