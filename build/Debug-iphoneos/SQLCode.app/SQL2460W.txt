

SQL2460W  The database was started with settings that might impact
      future growth of your database.

Explanation: 

In a multiple database environment, when one of the databases was
activated, the database could not get as much memory as it wanted. This
will affect how large that database can grow in the future. This message
can be returned when the CF self-tuning memory is enabled and database
memory configuration parameter CF_DB_MEM_SZ is set to AUTOMATIC. If the
instance is hosting more than one database, either:

*  Too many database were activated at the same time, or
*  The instance is hosting databases with both fixed and automatic
   CF_DB_MEM_SZ setting. The databases with fixed CF_DB_MEM_SZ are not
   leaving enough CF memory for the databases with AUTOMATIC
   CF_DB_MEM_SZ.

User response: 

Perform one of the following actions:

*  Rerun the ACTIVATE DATABASE command,
*  Manually configure database parameter CF_DB_MEM_SZ using the UPDATE
   DATABASE CONFIGURATION command, or
*  Configure a fixed CF_DB_MEM_SZ setting for all databases.


   Related information:
   CF self-tuning memory parameter configuration
   Configuring cluster caching facility (CF) memory for a database
