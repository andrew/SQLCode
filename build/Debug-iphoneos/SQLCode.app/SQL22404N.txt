

SQL22404N  The action mode "<action-mode>" specified when creating or
      dropping the database objects for function or feature
      "<function-or-feature-name>" is not valid.

Explanation: 

The action mode "<action-mode>" is invalid. To create the database
objects for "<function-or-feature-name>", specify action mode C. To drop
the database objects for "<function-or-feature-name>", specify action
mode D.

User response: 

Specify a valid action mode, then try the request again.

sqlcode: -22404

sqlstate: 5U005
