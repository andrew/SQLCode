

SQL1577N  The START command failed because the STANDALONE parameter was
      specified, and the current environment is a DB2 pureScale
      environment.

Explanation: 

The STANDALONE parameter is not supported with the START command in a
DB2 pureScale environment.

User response: 

Call the command again without specifying the STANDALONE parameter.


   Related information:
   db2start - Start DB2 command
