

SQL16143N  XML document contains an unexpected end of entity
      "<entity-name>".

Explanation: 

While parsing an XML document, the parser encountered an end of entity
named "<entity-name>" that was not expected.

Parsing or validation did not complete.

User response: 

Correct the entity and try the operation again.

sqlcode: -16143

sqlstate: 2200M
