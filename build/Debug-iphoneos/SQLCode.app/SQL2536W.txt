

SQL2536W  Warning! The backup image on device "<device>" contains an
      incorrect sequence number. Sequence number "<number>" is expected.

Explanation: 

The tape is positioned on a backup image file that is out of sequence.
The tape containing the backup image must be positioned to the file of
sequence number "<sequence>" of the backup image.

User response: 

Position the tape containing the backup image to the correct file and
resubmit the Restore command with the callerac parameter set to continue
or end.
