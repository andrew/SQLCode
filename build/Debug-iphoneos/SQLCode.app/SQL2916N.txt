

SQL2916N  The ingest utility failed because the specified SQL statement
      does not reference any fields.

Explanation: 

You can stream data from files and pipes into DB2 database tables by
using the ingest utility. How the ingest utility modifies the target
table is controlled by specifying an SQL statement with the INGEST
command or db2Ingest API call.

This message is returned when an attempt is made to perform an ingest
operation with an SQL statement that references no fields.

User response: 

Modify the SQL statement to include at least one field, and then perform
the ingest operation again.


   Related information:
   db2Ingest API- Ingest data from an input file or pipe into a DB2
   table.
   INGEST command
   Ingest utility restrictions and limitations
   Comparison between the ingest, import, and load utilities
