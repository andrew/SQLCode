

SQL4005N  An invalid token "<token>" was found starting in position
      "<position>" on line "<line>".

Explanation: 

A syntax error in the SQL statement was detected at the specified token
"<token>".

The statement cannot be processed.

User response: 

Examine the statement, especially around the specified token. Correct
the syntax.
