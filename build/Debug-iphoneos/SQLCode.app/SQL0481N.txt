

SQL0481N  The GROUP BY clause contains "<element-1>" nested within
      "<element-2>".

Explanation: 

The following types of nesting are not allowed within a GROUP BY clause:

*  CUBE within CUBE, ROLLUP, or GEL
*  ROLLUP within CUBE, ROLLUP, or GEL
*  () within CUBE, ROLLUP, or GEL
*  GROUPING SETS within GROUPING SETS, CUBE, ROLLUP, or GEL
*  CUBE, ROLLUP, (), GROUPING SETS within any function, CASE expression,
   or CAST specification where GEL represents the element shown as
   grouping-expression-list in the syntax diagram of the GROUP BY
   clause.

where GEL represents the element shown as grouping-expression-list in
the syntax diagram of the GROUP BY clause.

In some instances the value "---" will be shown for "<element 2>". In
this case "---" represents one of CUBE, ROLLUP, GROUPING SETS, or GEL.

 The statement cannot be processed.

User response: 

Modify the GROUP BY clause to remove the nesting.

 sqlcode: -481

 sqlstate: 428B0
