

SQL27953I  Usage: db2split [-c configuration-file-name] [-d
      distribution-file-name] [-i input-file-name] [-o output-file-name]
      [-h help message]

Explanation: 



-c       option will run this program using a user specified
         configuration file

-d       option specifies the distribution file

-i       option specifies the input file

-o       option specifies the output file

-h       option will generate the help message
