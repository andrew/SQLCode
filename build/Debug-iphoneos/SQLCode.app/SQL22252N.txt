

SQL22252N  DAS migration failed with reason code "<reason-code>".

Explanation: 

An error has occurred while migrating the DB2 Administration Server. The
reason codes are as follows: 
1. The system resources available were insufficient to complete the
   migration.
2. The DB2 Administration Server's configuration parameters do not
   identify a valid tools catalog.
3. A non-severe error occurred during migration.

User response: 


1. Verify that enough system resources are available for DAS migration.
2. Verify that a tools catalog is created and is identified correctly by
   the DB2 Administration Server's configuration parameters.
3. Refer to the DB2 Administration Server's First Failure Data Capture
   Log for additional information.
