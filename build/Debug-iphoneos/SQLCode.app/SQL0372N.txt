

SQL0372N  Column types of ROWID, IDENTITY, security label, row change
      timestamp, row-begin, row-end, or transaction start-ID can only be
      specified once for a table.

Explanation: 

An attempt was made to do one of the following:

*  Create a table with more than one IDENTITY column.
*  Add an IDENTITY column to a table which already has one.
*  Create a table with more than one ROWID column.
*  Add a ROWID column to a table which already has one.
*  Create a table with more than one security label column.
*  Add a security label column to a table that already has one.
*  Create a table with more than one row change timestamp column.
*  Add a row change timestamp column to a table that already has one.
*  Create a table with more than one row-begin column.
*  Add a row-begin column to a table that already has one.
*  Create a table with more than one row-end column.
*  Add a row-end column to a table that already has one.
*  Create a table with more than one transaction start-ID column.
*  Add a transaction start-ID column to a table that already has one.
*  Define a period more than once in a table.

The ROWID data type is supported in DB2 for z/OS and DB2 for iSeries.

The statement cannot be processed.

User response: 

For a CREATE TABLE statement, specify the specified attribute only once
for the table. For an ALTER TABLE statement, the specified column
already exists for the table. Do not attempt to define a period more
than once in a table.

sqlcode: -372

sqlstate: 428C1
