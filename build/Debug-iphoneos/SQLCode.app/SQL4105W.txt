

SQL4105W  An SQL syntax deviation has occurred. The statement is not
      complete.

Explanation: 

The SQL statement has terminated before all required elements have been
found.

Processing continues.

User response: 

Correct the SQL statement.
