

SQL16201N  XML document contains invalid Unicode character "<hex-value>"
      in a public identifier.

Explanation: 

While parsing an XML document the parser encountered an invalid Unicode
character in a public id. The invalid character is identified by the
hexadecimal value "<hex-value>".

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16201

sqlstate: 2200M
