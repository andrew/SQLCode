

SQL5192W  The ADMIN_SET_INTRA_PARALLEL procedure failed because either
      shared sort heap memory is not available or the application is
      assigned to a workload that has a value assigned to MAXIMUM
      DEGREE. Application name: "<application-name>"

Explanation: 

There are multiple ways to configure intrapartition parallelism,
including by setting the MAXIMUM DEGREE workload attribute or by calling
the ADMIN_SET_INTRA_PARALLEL procedure:

*  You can enable or disable intrapartition parallelism for a specified
   workload by setting the MAXIMUM DEGREE workload attribute.
*  You can enable or disable intrapartition parallelism for a database
   application by calling the ADMIN_SET_INTRA_PARALLEL procedure.

The MAXIMUM DEGREE workload attribute overrides any call to the
ADMIN_SET_INTRA_PARALLEL procedure.

This message can be returned when the ADMIN_SET_INTRA_PARALLEL procedure
is called in either of the following two scenarios:

*  The shared sort heap memory is not available
*  The application is assigned to a workload for which intrapartition
   parallelism is already configured with the MAXIMUM DEGREE workload
   attribute

User response: 

Control the degree of intrapartition parallelism either at the workload
level or from within the application, but not both.


   Related information:
   ADMIN_SET_INTRA_PARALLEL procedure - Enables or disables
   intrapartition parallelism
   max_querydegree - Maximum query degree of parallelism configuration
   parameter
   SET RUNTIME DEGREE command
   database_memory - Database shared memory size configuration parameter
