

SQL0489N  The function "<function-name>" in a SELECT or VALUES list item
      has produced a BOOLEAN result.

Explanation: 

The function "<function-name>" is defined for use as a predicate,
returning a boolean result. Such a result is not valid in a select list.

The statement cannot be processed.

User response: 

Correct the function name or remove the use of the function.

 sqlcode: -489

 sqlstate: 42844
