

SQL3179W  Row "<row>" in the input file is missing data for inserting
      into a non-nullable column in the database. The row was not
      inserted.

Explanation: 

The row of data from the input file has missing or not valid data for a
non-nullable column. The values in the remaining database columns in
that row are not inserted.

Processing continues with the next row. The row is not inserted.

User response: 

Either edit the data in the table or verify that the data in the
spreadsheet file is valid for inserting into a database manager
database.
