

SQL1888N  The port number "<port-number>" is not valid.

Explanation: 

The port number as specified on the Update Alternate Server command is
not valid. The value is either not numeric or its length is not valid.
The value must be 1 to 14 characters in length and cannot be all blanks.

User response: 

Verify that the port number is specified as a numeric value and that it
is no more than 14 characters in length.

Resubmit the command with a valid port number.
