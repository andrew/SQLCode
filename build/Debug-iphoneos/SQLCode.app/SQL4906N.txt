

SQL4906N  The list of table space names specified is an incomplete set
      for the rollforward operation.

Explanation: 

The list of table space names is incomplete for one of the following
reasons: 
*  For point-in-time table space recovery, a table space list must be
   specified.
*  For point-in-time table space recovery, a self-contained list of
   table space names must be specified. The table spaces in the list
   must contain all objects of every table included in the table spaces.
*  Point-in-time table space recovery is not allowed for the system
   catalogs.
*  End-of-logs table space recovery is allowed for the system catalogs
   but it can be the only table space name in the list.
*  The CANCEL option for rollforward must have a table space list if
   there are no table spaces in "rollforward-in-progress" state.

User response: 

Check the table space list and resubmit the rollforward command with the
complete table space list.
