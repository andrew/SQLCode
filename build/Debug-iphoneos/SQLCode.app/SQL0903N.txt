

SQL0903N  COMMIT statement failed, transaction rolled back. Reason code:
      "<reason-code>".

Explanation: 

One or more of the servers participating in the current unit of work was
unable to prepare the database to be committed. The COMMIT statement has
failed and the transaction has been rolled back. If there is only one
server participating in the current unit of work, the transaction may
have been committed instead.

Possible reason codes are:

01       

         A connection to one of the databases participating in the unit
         of work was lost.


02       

         One of the databases or nodes participating in the unit of work
         was accessed, but unable to prepare to commit.

          

         Federated system users: if the database you connected to is a
         federated server database where nicknames are used, one of the
         data sources required for a nickname is unable to prepare to
         commit.


03       

         A DB2 Data Links Manager participating in the unit of work was
         unable to prepare to commit.


04       

         One or more created temporary tables, or declared temporary
         tables are in an inconsistent state.


05       

         An unexpected error occurred. Check the administration
         notification log for details.

          

         Federated system users: if the database you connected to is a
         federated server database where nicknames are used, a
         connection to one of the data sources required for a nickname
         within the database was lost.


06       

         Unable to send Resync Information to one of the participants.
         The participant only supports IPv4. Please enable dual-stack
         mode for the participant.

User response: 

If a connection to a database was lost, reestablish the connection. If
the failure was not connection related, reference the error diagnostic
logs on the remote system to determine the nature of the failure and
what action might be required. Rerun the application.

sqlcode: -903

 sqlstate: 40504
