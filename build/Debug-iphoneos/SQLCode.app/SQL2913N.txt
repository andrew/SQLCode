

SQL2913N  Field "<field-name>" does not specify the end position or the
      length.

Explanation: 

INGEST commands that specify FORMAT POSITIONAL must specify or imply the
field length, or the POSITION parameter must specify the end position.
The command failed.

User response: 

Reissue the INGEST command and specify either the end position of the
field or the field length.
