

SQL16129N  XML document expected end of tag "<tag-name>".

Explanation: 

While parsing an XML document the parser expected to encounter the end
of a tag with name "<tag-name>" and did not.

Parsing or validation did not complete.

User response: 

Add or correct the missing end tag and try the operation again.

sqlcode: -16129

sqlstate: 2200M
