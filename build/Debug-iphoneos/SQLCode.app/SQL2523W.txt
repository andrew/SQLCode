

SQL2523W  Warning! Restoring to an existing database that is different
      from the database on the backup image, but have matching names.
      The target database will be overwritten by the backup version. The
      Roll-forward recovery logs associated with the target database
      will be deleted.

Explanation: 

The database alias and name of the target database are the same as the
backup image database alias and name. The database seeds are not the
same indicating that the databases are not the same. The target database
will be overwritten by the backup version. The Roll-forward recovery
logs associated with the target database will be deleted. The current
configuration file will be overwritten with the backup version.

User response: 

Return to the utility with the callerac parameter indicating processing
to continue or end.
