

SQL6511N  Load failed to create output for the partitioning agent at
      partition "<partition-num>".

Explanation: 

The program can not create temporary output pipes for the partitioning
agent at partition "<partition-num>".

User response: 

Please ensure your working space is clean.
