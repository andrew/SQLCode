

SQL0979N  COMMIT has failed against "<num>" databases for an application
      process running with SYNCPOINT of NONE. The failures include the
      following database alias and SQLSTATE pairs (a maximum of four can
      be returned): "<alias/SQLSTATE1>", "<alias/SQLSTATE2>",
      "<alias/SQLSTATE3>", "<alias/SQLSTATE4>".

Explanation: 

An application was connected to multiple databases, and a COMMIT was
issued which failed for one or more of these connections.

Federated system users: if one of the failed connections is a federated
server database where nicknames are used, then a commit against one of
the data sources required for a nickname has failed.

User response: 

Depending upon the nature of the application and the data being updated,
the user might wish to discontinue the processing being done, log the
failure, and issue the appropriate SQL to ensure that the changes
intended by the application are consistently reflected across all
databases involved.

If a full list of databases affected by COMMIT errors could not be
returned please refer to the diagnostic log for a full list.

 sqlcode: -979

 sqlstate: 40003
