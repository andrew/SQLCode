

SQL0509N  The table specified in the UPDATE or DELETE statement is not
      the same table specified in the SELECT for the cursor.

Explanation: 

The program attempted to execute an UPDATE or DELETE WHERE CURRENT OF
cursor statement where the table named did not match the name of the
table specified in the SELECT statement that declared the cursor.

The statement cannot be processed.

User response: 

Correct the application program to ensure that the table identified in
the UPDATE or DELETE statement is the same table identified in the
cursor declaration.

 sqlcode: -509

 sqlstate: 42827
