

SQL0263N  Member range from "<member-number-1>" to "<member-number-2>"
      is not valid. The second member number must be greater than or
      equal to the first member number.

Explanation: 

The specified member range is not valid.

The statement cannot be processed.

User response: 

Correct the member range in the statement, then try the request again.

sqlcode: -263

sqlstate: 428A9
