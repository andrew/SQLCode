

SQL1604N  The parameter "<parameter>" is not null terminated.

Explanation: 

A null character is expected at the end of the character string
parameter.

The command can not be processed.

User response: 

Add a null character at the end of the character string parameter and
reissue the command.
