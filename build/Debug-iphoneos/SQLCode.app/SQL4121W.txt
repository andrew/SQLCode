

SQL4121W  The WHERE clause, GROUP BY clause or HAVING clause is invalid
      for the grouped view "<schema-name>"."<view>".

Explanation: 

If the table identified in the FROM clause is a grouped view, then the
TABLE EXPRESSION must not contain a WHERE clause, GROUP BY clause, or
HAVING clause.

Processing continues.

User response: 

Correct the SQL statement.
