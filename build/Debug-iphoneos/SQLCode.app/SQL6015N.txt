

SQL6015N  The keyword is not recognized.

Explanation: 

The keyword parameter indicator ("/") was followed by a value that is
not a keyword.

The command cannot be processed.

User response: 

Retry the command using a different keyword value.
