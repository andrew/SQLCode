

SQL2088W  Automatic statistics profiling has been disabled for the
      specified connection.

Explanation: 

You can apply workload management (WLM) configuration settings to a
specific database connection using the WLM_SET_CONN_ENV stored
procedure. Including the <sectionactuals> name in the "settings"
parameter to the stored procedure with a value other than NONE will
enable the collection of section actuals for the specified connection.

This message is returned when the WLM_SET_CONN_ENV stored procedure is
used to enable section actuals collection.

Section actuals collection and automatic statistics profiling (enabled
using the AUTO_STATS_PROF database configuration parameter) cannot be
used together. When you enable section actuals collection on a
connection, automatic statistics profiling for the connection is
disabled.

User response: 

No response to this message is required.

To restore automatic statistics profiling for this connection, disable
section actuals collection by executing the WLM_SET_CONN_ENV stored
procedure again and specifying the following name-value pair in the
"settings" parameter:

<sectionactuals>NONE</sectionactuals>

sqlcode: +2088

sqlstate: 01HN2
