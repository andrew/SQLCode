

SQL20160W  The authorizations were granted to USER "<userid>". Groups
      were not considered because the authorization name is more than 8
      bytes.

Explanation: 

The authorization name has a length of more than 8 bytes. The privilege
is granted to the user with authorization name "<userid>" without
considering groups defined in the system that might have a matching
name. Processing continues.

User response: 

If the grant was intended for a user, no action is required. If the
grant was intended for a group, consider choosing an alternate group
because group names greater than 8 bytes are not supported. To avoid
this warning message, specify the USER keyword before the authorization
name.

 sqlcode: +20160

 sqlstate: 01653
