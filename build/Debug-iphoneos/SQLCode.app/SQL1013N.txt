

SQL1013N  The database alias name or database name "<name>" could not be
      found.

Explanation: 

The database name or alias specified in the command is not an existing
database or the database could not be found in the (client or server)
database directories or the db2dsdriver.cfg configuration file.

User response: 

Ensure that the specified database name exists in the system database
directory. If the database name does not exist in the system database
directory, then the database either does not exist or the database name
has not been cataloged.

If the database name appears in the system database directory and the
entry type is INDIRECT, ensure that the database exists in the specified
local database directory. If the entry type is REMOTE, then ensure that
the database exists and is cataloged on the database directories for the
server.

For CREATE DATABASE with the AT DBPARTITIONNUM clause, ensure that the
database name is in the system database directory with an entry type of
INDIRECT and with a catalog database partition number that does not
equal -1.

Federated system users: in addition to the previous responses, verify
that the database names specified in SYSCAT.SERVERS are all valid.
Correct any SYSCAT.SERVERS entry for which the database specified in
that entry does not exist.

sqlcode: -1013

sqlstate: 42705


   Related information:
   CREATE DATABASE command
   IBM data server driver configuration file
