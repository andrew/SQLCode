

SQL1273N  An operation reading the logs on database "<database-name>"
      cannot continue because of a missing log file "<log-file-name>" on
      database partition "<dbpartitionnum>" and log stream
      "<log-stream-ID>".

Explanation: 

The operation cannot find the specified log file in the archive,
database log directory, or overflow log directory for the given log
stream. The operation reading the logs has been stopped. This operation
was either a recovery operation, a rollforward operation, a call to the
db2ReadLog API, or any other operation that requires access to the
transaction logs.

If the operation was a crash recovery, the database is left in an
inconsistent state. If the operation was a rollforward operation, the
operation has stopped and the database is left in rollforward pending
state.

User response: 

Recover the missing log file by taking one of the following actions:

*  Move the specified log file into the database log directory and
   restart the operation.
*  If an overflow log path can be specified, restart the operation with
   the overflow log path, specifying the path that contains the log
   file.

If the missing log file cannot be found, determine whether one of the
following special cases applies:

*  If the operation is a ROLLFORWARD DATABASE command to maintain a
   standby system through log shipping, this error might be normal,
   because some files that are available on the primary site might not
   yet be available on the standby system. To ensure that your standby
   system is up-to-date, issue a ROLLFORWARD DATABASE command with the
   QUERY STATUS option after each rollforward operation to verify that
   the log replay is progressing properly. If you find that a
   rollforward operation on the standby system is not making progress
   over an extended period of time, determine why the log file that is
   reported as missing is not available on the standby system, and
   correct the problem. Note that the ARCHIVE LOG command can be used to
   truncate currently active log files on the primary system, making
   them eligible for archiving and subsequent replay on the standby
   system.
*  If a ROLLFORWARD DATABASE command with the TO END OF LOGS option was
   issued following a restore operation from an online backup image in
   which the only available logs are those that were included in the
   backup image, there are two possible scenarios to consider: 
   *  Scenario 1: All of the log files contained in the backup image are
      found by the rollforward operation. However, the rollforward
      operation still looks for log files that were updated following
      the original backup operation. Bring the database to a consistent
      state by issuing the ROLLFORWARD DATABASE command with the STOP
      option (without the TO END OF LOGS option). To avoid this scenario
      in the future, use the END OF BACKUP option instead of the END OF
      LOGS option, because the rollforward operation will not look for
      log files that were updated after the backup was taken.
   *  Scenario 2: One or more log files that were contained in the
      backup image were not found by the rollforward operation. These
      log files are required to bring the database to a consistent
      state. Attempting to bring the database to a consistent state by
      issuing the ROLLFORWARD DATABASE command with the STOP option
      (without the TO END OF LOGS option) will fail with SQL1273N;
      recover the missing log file, as described earlier in this
      section.

If the missing log file cannot be recovered:

*  If the operation is a ROLLFORWARD DATABASE command, you can issue the
   ROLLFORWARD DATABASE command again with the STOP option (without the
   END OF LOGS option or the END OF BACKUP option) to bring the database
   to a consistent state. If this consistency point (immediately prior
   to the missing log file) is not acceptable, you can restore the
   database and roll forward to any point in time that is prior to the
   missing log file by providing an earlier time stamp to the
   ROLLFORWARD DATABASE command.
*  If the operation is a ROLLFORWARD DATABASE command with the STOP or
   COMPLETE option (without the END OF LOGS option or the END OF BACKUP
   option), the missing log file is needed to bring the database to a
   consistent state. Because you cannot recover the missing log file,
   you must restore and roll forward to an earlier point in time (as
   long as that point in time is not prior to the minimum recovery
   time).
*  If the operation is a call to the db2ReadLog or db2ReadLogNoConn API
   for the purpose of replicating data, resynchronize the replicated
   tables and terminate the current connection being used by the API. If
   the API is being used to maintain a disaster recovery site, a backup
   image that was created after the last time stamp in the missing log
   file must be restored to the disaster recovery site before any
   subsequent API calls can complete successfully. If the API is being
   used for any other purpose, terminate the connection being used by
   the API and deactivate the database. In all cases, restart the scan
   by creating a new connection to the database and calling the API with
   the query action. Use the nextStartLRI as the piStartLRI on the next
   call to the API.
