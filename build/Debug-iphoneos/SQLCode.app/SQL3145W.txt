

SQL3145W  The data for row "<row-number>", column "<column-number>" is
      being truncated to 240 bytes.

Explanation: 

The Lotus 1-2-3** and Symphony** programs have a limit of 240 bytes for
label records. Whenever a character field longer than 240 bytes is
written to a worksheet format (WSF) file, the data is truncated to 240
bytes. This message is preceded by message SQL3143 associated with the
column.

Processing continues. The data is truncated.

User response: 

Verify the output. If significant data from the column is lost because
of truncation, investigate selecting the data from the column in several
fields by substringing, or redesign the database.
