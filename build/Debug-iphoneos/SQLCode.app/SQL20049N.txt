

SQL20049N  Type of an operand following the comparison operator in the
      AS PREDICATE WHEN clause does not exactly match the RETURNS type.

Explanation: 

The definition of the user-defined predicate is not valid. In the AS
PREDICATE WHEN clause, the type of the operand following the the
comparison operator is not an exact match with the RETURNS type of the
function.

The statement cannot be processed.

User response: 

Specify an the operand with the correct data type.

 sqlcode: -20049

 sqlstate: 428E7
