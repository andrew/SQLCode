

SQL2419N  The target disk "<disk>" has become full.

Explanation: 

During the processing of a database utility, the target disk became
full. The utility has stopped and the target is deleted.

User response: 

Ensure enough disk space is available for the utility or direct the
target to other media, such as tape.

On unix-based systems, this disk full condition may be due to exceeding
the maximum file size allowed for the current userid. Use the chuser
command to update fsize. A reboot may be necessary.

On non unix-based systems, this disk full condition may be due to
exceeding the maximum file size allowed for the operating system. Direct
the target to other media, such as tape, or use multiple targets.
