

SQL0423N  Locator variable "<variable-position>" does not currently
      represent any value.

Explanation: 

A locator variable is in error. Either it has not had a valid result set
locator or LOB locator variable value assigned to it, the locator
associated with the variable has been freed, or the result set cursor
has been closed.

If "<variable-position>" is provided, it gives the ordinal position of
the variable in error in the set of variables specified. Depending on
when the error is detected, the database manager may not be able to
determine "<variable-position>".

Instead of an ordinal position, "<variable-position>" may have the value
"function-name RETURNS", which means that the locator value returned
from the user-defined function identified by function-name is in error.

User response: 

Correct the program or routine so that the locator variables used in the
SQL statement have valid values before the statement is executed.

A LOB value can be assigned to a locator variable by means of a SELECT
INTO statement, a VALUES INTO statement, or a FETCH statement.

Result set locator values are returned by the ASSOCIATE LOCATORS
statements. Result set locator values are only valid as long as the
underlying SQL cursor is open. If a commit or rollback operation is run,
the result set locator associated with the cursor is no longer valid. If
this was a WITH RETURN cursor, ensure the cursor is opened before
attempting to allocate it.

If the following statements are all true about the application code:

*  the application contains a cursor declared for a query that defines a
   result set containing LOB columns
*  the cursor declaration contains the WITH HOLD clause
*  LOB locators are used to reference the LOB values in the result set
   of the cursor
*  the unit of work is committed before the cursor is closed

Do one of the following actions to remove a factor contributing to this
warning case so as to successfully upgrade your application:

*  Precompile your application again using the PREP command making sure
   to include the SQLRULES STD option.
*  If possible, alter the application so that the LOB columns are
   retrieved as values instead of locators
*  If possible, alter the application so that the cursor is no longer
   declared with the WITH HOLD option and remove the commit before the
   cursor is closed.

sqlcode: -423

 sqlstate: 0F001
