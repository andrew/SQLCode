

SQL4016N  The preprocessor specified cannot be found.

Explanation: 

The preprocessor specified through the PREPROCESSOR option cannot be
found.

The command cannot be processed.

User response: 

Ensure that the preprocessor can be executed from the current directory,
and also check the syntax of the PREPROCESSOR option.
