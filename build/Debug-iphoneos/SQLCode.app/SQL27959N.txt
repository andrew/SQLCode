

SQL27959N  The partitioned database configuration option "<option-name>"
      is invalid. Reason code = "<reason-code>".

Explanation: 

The partitioned database configuration option named in the error message
is incorrectly specified or is incompatible with one of the other load
options specified.

The possible reason codes are as follows: 

1        Partitioned database configuration options cannot be specified
         in a non-partitioned database environment or when the
         DB2_PARTITIONEDLOAD_DEFAULT registry variable is set to OFF.

2        Partitioned database configuration options may not be specified
         more than once.

3        Invalid pointer detected in the piPartLoadInfoIn input
         structure passed to the db2Load API.

4        Invalid pointer detected in the poPartLoadInfoOut output
         structure passed to the db2Load API.

5        The argument supplied to the MODE option must be one of the
         following: 
         *   PARTITION_AND_LOAD
         *   PARTITION_ONLY
         *   LOAD_ONLY
         *   LOAD_ONLY_VERIFY_PART
         *   ANALYZE


6        The maximum number of partitioning agents must be less than or
         equal to the maximum number of partitions allowable in a
         cluster.

7        The maximum number of partition numbers in a partition list
         must be less than or equal to the maximum number of partitions
         allowable in a cluster.

8        The arguments supplied to the ISOLATE_PART_ERRS option must be
         one of the following: 
         *   SETUP_ERRS_ONLY
         *   LOAD_ERRS_ONLY
         *   SETUP_AND_LOAD_ERRS
         *   NO_ISOLATION


9        The value supplied to the STATUS_INTERVAL option must be in the
         range 1-4000.

10       The maximum port number must be greater than or equal to the
         minimum port number.

11       The only legal arguments for the CHECK_TRUNCATION, NEWLINE and
         OMIT_HEADER options are TRUE and FALSE.

12       The argument supplied to RUN_STAT_DBPARTNUM must be a legal
         partition number.

13       If the mode is ANALYZE, the MAP_FILE_OUTPUT option must be
         specified.

14       If the mode is PARTITION_ONLY or LOAD_ONLY and a remote client
         is being used, then the PART_FILE_LOCATION option must be
         specified. If the mode is PARTITION_ONLY or LOAD_ONLY then if
         the file type is CURSOR, the PART_FILE_LOCATION option must be
         used and must specify a file name.

15       The load actions RESTART and TERMINATE can only be used when
         the mode is PARTITION_AND_LOAD, LOAD_ONLY or
         LOAD_ONLY_VERIFY_PART.

16       The HOSTNAME option cannot be specified unless the
         FILE_TRANSFER_CMD option is also specified.

17       The partition isolation error modes LOAD_ERRS_ONLY and
         SETUP_AND_LOAD_ERRS cannot be used when both the ALLOW READ
         ACCESS or COPY YES options of the load command are used.

18       The LOAD_ONLY and LOAD_ONLY_VERIFY_PART modes are not
         compatible with the CLIENT option of the load command.

User response: 

Resubmit the LOAD command with the correct partitioned database
configuration options.
