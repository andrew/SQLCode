

SQL16183N  XML document contains an invalid child in a complexType.
      Reason code = "<reason-code>".

Explanation: 

While parsing an XML document an invalid child was found in a
complexType. Possible reasons given by "<reason-code>" are: 
1. Found an invalid child following the simpleContent child in the
   complexType
2. Found an invalid child following the complexContent child in the
   complexType

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16183

sqlstate: 2200M
