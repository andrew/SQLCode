

SQL7008N  REXX variable "<variable>" contains inconsistent data.

Explanation: 

A variable that contained inconsistent data was passed to REXX.

The command cannot be processed.

User response: 

If the variable is an SQLDA, verify that the data and length fields have
been assigned correctly. If it is a REXX variable, verify that the type
of data is appropriate to the command where it is being used.
