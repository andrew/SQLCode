

SQL1272N  Table space level roll-forward recovery for database "<name>"
      has stopped before completion on members or nodes "<node-list>".

Explanation: 

Table space level roll-forward recovery has stopped on the specified
members or nodes before all qualifying table spaces could be rolled
forward.

This message can be returned for the following types of reasons:

*  The transaction table is full.
*  All table spaces being rolled forward received I/O errors.
*  Point-in-time table space level roll-forward encountered an I/O error
   on one of the table spaces being rolled forward.
*  Point-in-time table space level roll-forward encountered an active
   transaction that made changes to one of the table spaces being rolled
   forward. This transaction could be an indoubt transaction.
*  The table space level roll-forward was interrupted and, before it was
   resumed, all table spaces that were being rolled forward are restored
   again.
*  The table space rollforward state information has become corrupted or
   been lost.
*  A lock timeout or deadlock occurred.

If ",..." is displayed at the end of the member or node list, see the
administration notification log for the complete list of members or
nodes.

Note: The member or node numbers provide useful information only in DB2
pureScale environments and partitioned database environments. Otherwise,
the information should be ignored.

User response: 

Check the administration notification log for the cause. Do one of the
following depending on the cause:

*  Use the MON_GET_TABLESPACE table function to determine if the table
   spaces received I/O errors. If the table spaces received I/O errors,
   repair them.
*  If the transaction table became full, increase the MAXAPPLS database
   configuration parameter or try running table space level roll-forward
   recovery offline.
*  If the cause is an active or indoubt transaction, complete the
   transaction.
*  If the table spaces were restored after a previous table space level
   roll forward was interrupted, the previous table space level
   roll-forward is now canceled. The next table space level roll-forward
   command will check for table spaces in rollforward pending state.
*  Cancel the rollforward operation, which will put the table space in
   restore pending state. Before reissuing the ROLLFORWARD command,
   restore a table space level backup image.

Run table space level roll-forward recovery again.


   Related information:
   Rollforward overview
