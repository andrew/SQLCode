

SQL1206N  PRUNE LOGFILE is not supported in this database configuration.

Explanation: 

The PRUNE LOGFILE request is not supported if: 
1. the database is not in recoverable mode. A database is in recoverable
   mode if LOGARCHMETH1 is set to DISK, TSM, VENDOR, USEREXIT, or
   LOGRETAIN, or if LOGARCHMETH2 is set to DISK, TSM, or VENDOR.
2. the active logfile path is set to a raw device.

User response: 

Do not issue the PRUNE LOGFILE command for this database.
