

SQL2721N  Invalid partitioning key specification (PARTITION) at line
      "<line>" of the configuration file. Reason code "<reason-code>".

Explanation: 

A partitioning key specification (PARTITION) in the configuration file
is not valid. Valid format: 

 PARTITION=<key name>,
           <position>,
           <offset>,
           <len>,
           <nullable>,
           <datatype>

If a delimited-data file, <position> must be defined; otherwise,
<offset> and <len> must be defined.

User response: 

Given for each reason code: 

1        Fields must be delimited by the ',' character.

2        <position>, <offset> and <len> must be positive integers.

3        <nullable> must take a value from {N,NN,NNWD}.

4        Valid <data type> includes: SMALLINT, INTEGER, CHARACTER,
         VARCHAR, FOR_BIT_CHAR, FOR_BIT_VARCHAR, FLOAT (for binary
         numerics only), DOUBLE (for binary numerics only), DATE, TIME,
         TIMESTAMP, DECIMAL(x,y).

5        For DECIMAL data type, precision (x) and scale (y) must be
         specified and they must be positive integers.

6        For CHARACTER or VARCHAR data type, <len> must be specified.
