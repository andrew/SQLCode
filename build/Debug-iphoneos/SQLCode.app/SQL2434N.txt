

SQL2434N  The redirected restore operation failed because a table space
      operation could not be replayed during the rollforward phase of
      the restore operation.

Explanation: 

A redirected restore is a restore in which the set of table space
containers for the restored database is different from the set of
containers for the original database at the time the backup was done.

When you define new table space containers that are to be used by the
restored database, you can define an architecture for the new table
space containers that is not the same as the architecture of the
original table space containers. For example, you can use a redirected
restore operation to change a database that currently uses two table
space containers to use a single table space container.

If the architecture of the table spaces to be used by the restored
database is different from the architecture of the original database, it
is possible that there are table space altering operations in the
database log files that will have to be replayed during rollforward that
are not valid in the new table space architecture.

This message is returned during a redirected restore operation when the
rollforward utility attempts to replay a table space operation that is
invalid for the target table space container architecture. Specifically,
this message is returned when the rollforward utility attempts to
perform a table space operation on a REGULAR or USER TEMPORARY DMS table
space that causes the size of the table space to increase beyond the
defined maximum allowed size, as determined by the PAGESIZE database
configuration parameter.

User response: 

1. Begin the redirected restore again using the RESTORE command with the
   REDIRECT parameter.
2. Define the new table space containers to be used by the restored
   database using the SET TABLESPACE CONTAINERS command, forcing the
   rollforward operation to omit replaying table space operations by
   specifying the IGNORE ROLLFORWARD CONTAINER OPERATION parameter.
3. Complete the restore operation using the RESTORE command with the
   CONTINUE parameter.

sqlcode: -2434

sqlstate: 58004


   Related information:
   Performing a redirected restore operation
   SET TABLESPACE CONTAINERS command
   Table spaces
