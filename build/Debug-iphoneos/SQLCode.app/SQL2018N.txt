

SQL2018N  The utility encountered an error "<error>" while attempting to
      verify the user's authorization ID or database authorizations.

Explanation: 

The user attempted to execute a utility and one of the following has
occurred: 
*  The user authorization ID is not valid.
*  An error occurred attempting to access the user's authorizations for
   the database.

The utility stops processing.

User response: 

Look at the error number in the message for more information. Make
changes and resubmit the command.
