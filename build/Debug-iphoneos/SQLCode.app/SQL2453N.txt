

SQL2453N  Rebinding the package failed because the SQL object that
      generated the package needs to be revalidated. SQL object name:
      "<object-name>". SQL object type: "<object-type>".

Explanation: 

Rebinding is the process of recreating a package for an application
program that was previously bound. You can rebind a package by using the
REBIND command.

This message is returned when an attempt is made to rebind a package
that is generated for an SQL object (such as an SQL procedure or a
compiled function, among others) that is invalid. The SQL object might
have become invalid for one of multiple reasons, such as because
something in the body of the SQL object has changed since the SQL object
and the associated package was created.

User response: 

1. Revalidate the SQL object that generated the package by using one of
   the following methods: 
   *  Enable automatic revalidation and then execute and SQL statement
      that uses the SQL object.
   *  Call the ADMIN_REVALIDATE_DB_OBJECTS procedure
   *  Replace the SQL object using a CREATE OR REPLACE statement or by
      dropping the SQL object and creating it again.

2. Call the REBIND command again.

sqlcode: -2453

sqlstate: 560D6


   Related information:
   ADMIN_REVALIDATE_DB_OBJECTS procedure - Revalidate invalid database
   objects
   Automatic revalidation of database objects
