

SQL16062N  The argument passed to the function fn:zero-or-one is not
      valid because the sequence contains more than one item. Error
      QName=err:FORG0003.

Explanation: 

A sequence that was passed as an argument to the function fn:zero-or-one
contains more than one item.

The XQuery expression cannot be processed.

User response: 

Modify the expression to ensure that the sequence passed to the function
fn:zero-or-one contains only one item or is an empty sequence.

 sqlcode: -16062

 sqlstate: 10608
