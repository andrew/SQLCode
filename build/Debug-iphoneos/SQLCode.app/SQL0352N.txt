

SQL0352N  An unsupported SQLTYPE was encountered in position
      "<position-number>" of the input list (SQLDA).

Explanation: 

The element of the SQLDA at position "<position-number>" is for a data
type that either the application requestor or the application server
does not support. If the application is not using the SQLDA directly,
"<position-number>" could represent the position of an input host
variable, parameter marker, or a parameter of a CALL statement.

The statement cannot be processed.

User response: 

Change the statement to exclude the unsupported data type.

 sqlcode: -352

 sqlstate: 56084
