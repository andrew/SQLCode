

SQL3165W  The column type field "<type>" in the C record for column
      "<name>" is not valid. Data from the column will not be loaded.

Explanation: 

The column type in the C record for the indicated column is not valid.

Data from the indicated column is not loaded.

User response: 

Change the column type field in the C record and resubmit the command.
