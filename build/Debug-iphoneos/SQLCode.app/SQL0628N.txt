

SQL0628N  Multiple or conflicting keywords involving the "<clause-type>"
      clause are present.

Explanation: 

There are several possible reasons why this condition might have been
diagnosed for the statement. The cause is indicated by the value of
"<clause-type>". The possible reasons include:

*  The keyword might not be specified in the same statement as some
   other keyword.
*  The keyword might be part of a sequence of keywords where the order
   in which they are specified is not enforced. A keyword in such a
   sequence might have been specified with the contradicting keyword
   also specified.
*  The keyword might appear more than once with different associated
   values.
*  The keyword might require the specification of other particular
   keywords in the same statement which were not specified.
*  When ALTERING an object, a keyword was specified that conflicts with
   an existing property of the object.
*  When ALTERING a partitioned table, the ADD, ATTACH and DETACH clauses
   are not supported with any other clauses.
*  When CREATING or DECLARING a table with the LIKE table-name clause
   where table-name identifies a nickname, the INCLUDING COLUMN DEFAULTS
   clause has no effect and column defaults are not copied.
*  The parameter mode OUT or INOUT was used to define a parameter in a
   function that does not support OUT or INOUT parameters.
*  A generic table function was being created using the CREATE PROCEDURE
   statement, with the RETURNS GENERIC TABLE clause, and one of the
   following errors occurred: 
   *  A language other than JAVA was specified with the LANGUAGE clause
   *  A parameter style other than DB2GENERAL was specified with the
      PARAMETER STYLE clause

*  When CREATING a trigger, the same trigger event was specified more
   than once.
*  When CREATING a table space, the 'USING STOGROUP' clause cannot be
   used with DMS or SMS table spaces
*  When CREATING a table space, STOGROUP cannot be changed for temporary
   automatic storage table spaces
*  When CREATING a table space, DATA TAG cannot be set for a temporary
   table space
*  When CREATING a table space, DATA TAG cannot be set for the system
   catalog table space
*  When CREATING a global variable in Version 10.1 Fix Pack 1 or later
   fix pack releases, a default value (DEFAULT) or constant value
   (CONSTANT) cannot be specified for the global variable. The default
   value for all global variables is the null value.
*  When CREATING or ALTERING a table VERSIONING cannot be specified with
   LIKE for as-result-table or materialized-query-definition.

User response: 

Check that the statement conforms to the syntax and rules defined for
the statement. Correct any invalid occurrences of duplicate or
conflicting keywords.

sqlcode: -628

sqlstate: 42613


   Related information:
   CREATE FUNCTION (external table) statement
