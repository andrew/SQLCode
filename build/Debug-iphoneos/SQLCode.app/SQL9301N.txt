

SQL9301N  An invalid option is specified or an option parameter is
      missing.

Explanation: 

Either the option specified is invalid or an option parameter is not
specified.

The command cannot be processed.

User response: 

Correct the option and resubmit the command.
