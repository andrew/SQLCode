

SQL1829W  The federated server received the warning message
      "<warning-code>" from the data source "<server-name>". The
      associated text and tokens are "<tokens>".

Explanation: 

An unknown warning "<warning-code>" occurred at the data source
"<server-name>". The tokens for the message are "<tokens>".

User response: 

Use the diagnostic information for the data source to determine what, if
any, corrective action to take.

sqlcode: +1829

sqlstate: 0168O
