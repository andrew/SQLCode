

SQL3271N  The LDAP user's Distinguished Name (DN) and/or password is not
      defined for the current logon user.

Explanation: 

When setting user preferences such as CLI configuration or DB2 registry
variable, the LDAP user's DN and password must be defined for the
current logon user.

User response: 

Refer to the IBM eNetwork LDAP documentation on how to configure the
LDAP user's DN and password for the current logon user.
