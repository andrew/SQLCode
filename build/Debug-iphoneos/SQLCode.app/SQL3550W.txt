

SQL3550W  The field value in row "<row-number>" and column
      "<column-number>" is not NULL, but the target column has been
      defined as GENERATED ALWAYS.

Explanation: 

A non NULL field value was encountered in the input file. Because the
target column is of type GENERATED ALWAYS, the value cannot be loaded.
The column number specifies the field within the row of the missing
data.

User response: 

For LOAD, an explicit, non NULL field value can be loaded into a
GENERATED ALWAYS identity column only if the identityoverride file type
modifier is used. For non-identity GENERATED ALWAYS columns, the
generatedoverride file type modifier can be used to load explicit, non
NULL values into a row. If using these modifiers is not appropriate, the
field value must be replaced with a NULL if LOAD is to accept the row.

For IMPORT there is no way to override a GENERATED ALWAYS column. The
field value must be removed and replaced with a NULL if the utility is
to accept the row.
