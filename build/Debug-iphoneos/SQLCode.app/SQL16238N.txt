

SQL16238N  XML schema contains a prefix "<prefix-name>" that is not
      bound to a namespace URI in an XPath value.

Explanation: 

While processing an XML schema, the XML parser encountered a prefix
"<prefix-name>" without a corresponding URI in an XPath value.

Parsing or validation did not complete.

User response: 

Correct the XML schema and try the operation again.

sqlcode: -16238

sqlstate: 2200M
