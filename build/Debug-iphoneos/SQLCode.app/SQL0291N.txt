

SQL0291N  State transition not allowed on table space.

Explanation: 

An attempt was made to change the state of a table space. Either the new
state is not compatible with the current state of the table space, or an
attempt was made to turn off a particular state and the table space was
not in that state.

User response: 

Table space states change when a backup is taken, the load completes,
the rollforward completes, etc., depending on the current state of the
table spaces. Refer to the systems administration guide for further
information about the table space states.

 sqlcode: -291

 sqlstate: 55039
