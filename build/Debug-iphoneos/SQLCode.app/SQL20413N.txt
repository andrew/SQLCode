

SQL20413N  The built-in function SECLABEL_TO_CHAR could not be executed
      because authorization ID "<auth-id>" has had its security label
      for READ access revoked.

Explanation: 

To execute the built-in function SECLABEL_TO_CHAR, the authorization ID
must have a security label for READ access. The security label for READ
access has been revoked from authorization ID "<auth-id>".

User response: 

Contact the database security administrator or a user with SECADM
authority and ask that the security label be granted again.

sqlcode: -20413

 sqlstate: 42520
