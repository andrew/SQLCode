

SQL2517W  The database was successfully restored and then upgraded to
      the current release. However, the database operation returned
      warning or error code "<warn-err-code>" and runtime tokens
      "<tokens>".

Explanation: 

You can upgrade a database from one version of DB2 database to a newer
version of DB2 database using the restore utility. You can upgrade by
recreating your old database manager instance in the new version and
then restoring a backup image from the the old instance into the new
instance. In this scenario, after restoring the database in the new
database instance, the restore utility automatically performs the
upgrade operation. This message is returned when the upgrade operation
that the restore utility automatically runs returns a warning or error.

User response: 

Respond to this message by performing the following troubleshooting
steps:

1. Review the text of the warning or error code listed in the runtime
   token "<warn-err-code>".
2. Respond to the warning or error code listed in the runtime token
   "<warn-err-code>" before accessing the upgraded database.

The user who issued the RESTORE DATABASE command is given DBADM
authority to the database. If other users had DBADM authority prior to
the restore, work with a user who has SECADM authority, to grant DBADM
authorization to these users.


   Related information:
   Upgrading a DB2 server by using online backups from a previous
   release
   Upgrading to a new DB2 server
