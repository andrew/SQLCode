

SQL3706N  A disk full error was encountered on "<path/file>".

Explanation: 

A disk full error was encountered during the processing of a database
utility. The utility stops.

User response: 

Ensure enough disk space is available for the utility or direct the
output to other media such as tape.
