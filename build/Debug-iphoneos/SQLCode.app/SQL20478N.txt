

SQL20478N  The statement failed because the column mask "<mask-name>"
      defined for column "<column-name>" exists and the column mask
      cannot be applied or the column mask conflicts with the failed
      statement. Reason code "<reason-code>"

Explanation: 

A reference to a column mask is not supported. The cause of the error is
described by the following reason codes:

1        

         The result table of the select is derived from a set operation
         that involves a EXCEPT ALL, or INTERSECT ALL set operator.
         Change the query to not reference the column in the select list
         or do not use the set operation in this context.


4        

         Each column that is in the same table as the "<column-name>"
         column that is referenced in the "<mask-name>" mask must also
         be referenced as a simple column reference in the GROUP BY
         clause. Such columns must not be referenced in a grouping
         expression in the GROUP BY clause. Change the query to not
         reference the column in the select list or change the GROUP BY
         clause to include only a simple column reference for each
         column of the same table that is referenced in the mask-name
         mask.


22       

         The column "<column-name>" is input to a table or row function
         in the statement. Some references to the function result
         require the column mask "<mask-name>" be applied to the
         function input, and some references to the function result do
         not require the column mask. If the specified column is input
         to a table or row function, all references to the function
         result must have the same mask requirement.


30       

         An INSERT or UPDATE operation uses a masked value returned from
         the column mask "<mask-name>" for "<column-name>". The
         expression specified in the THEN or ELSE clause of the column
         mask definition that is used to return the masked value is not
         a simple reference to the column "<column-name>". For the
         specified INSERT or UPDATE operation, the return expression in
         the column mask definition must be a simple reference to the
         column for which the mask is defined.

The statement cannot be processed.

User response: 

*  Remove the reference to the column in the INSERT or UPDATE operation
   and retry the operation.
*  Contact the Security Administrator to have the return expression in
   the column mask definition modified.

sqlcode: -20478

sqlstate: 428HD
