

SQL2315N  The RUNSTATS utility was called with the "<option-name>"
      option. However, a statistics profile for this table does not
      exist.

Explanation: 

The statistics profile for this table does not exist in the catalog
table SYSIBM.SYSTABLES.

The utility stops processing.

User response: 

To create a statistics profile for this table, use the SET PROFILE or
SET PROFILE ONLY options. Refer to the RUNSTATS utility documentation
for information about the utility options.

"<option-name>" is a token which can be "USE PROFILE", "UNSET PROFILE",
or "UPDATE PROFILE".
