

SQL8025W  The connection to the server "<server-name>" is not allowed. A
      valid WebSphere Federated Server license could not be found.

Explanation: 

Your current WebSphere Federated Server license does not allow
connection to the specified data source.

User response: 

Refer to the Administration Notification log for possible causes of this
error. If this problem persist, contact IBM Support.
