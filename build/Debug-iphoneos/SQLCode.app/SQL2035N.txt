

SQL2035N  The warning condition "<warn>" was encountered while executing
      the utility in non-interrupt mode.

Explanation: 

The calling application invoked the utility with no interrupt mode.
During the operation a warning condition was encountered.

User response: 

Resubmit the operation without specifying the no interrupt condition in
the callerac parameter or take actions to prevent the warning and
resubmit the operation.
