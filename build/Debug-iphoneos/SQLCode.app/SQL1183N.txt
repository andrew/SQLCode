

SQL1183N  User defined function "<function-name>" received an OLE DB
      error from specified OLE DB provider. HRESULT="<hresult>".
      Diagnostic text: "<message-text>".

Explanation: 

The specified OLE DB provider returned an OLE DB error code. "<hresult>"
is the returned OLE DB error code, and "<message-text>" is the retrieved
error message.

The following shows a partial list of HRESULTS and possible causes.

0x80040E14
         The command contained one or more errors, for example, syntax
         error in pass-through command text.

0x80040E21
         Errors occurred, for example, the supplied columnID was invalid
         (DB_INVALIDCOLUMN).

0x80040E37
         The specified table does not exist.

User response: 

Consult Microsoft OLE DB Programmer's Reference and Data Access SDK for
a complete documentation of HRESULT codes.

 sqlcode: -1183

 sqlstate: 38506
