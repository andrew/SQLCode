

SQL1635N  The snapshot operation failed because the size of the snapshot
      is "<snapshot-size>" bytes, which exceeds the maximum allowable
      size of "<max-size>" bytes.

Explanation: 

You can collect DB2 database and related operating system information by
using the snapshot monitor. In multiple database partition environments,
you can take a snapshot of the current database partition, a specified
database partition, or all database partitions. Taking a snapshot of all
database partitions at once is referred to as a taking global snapshot.

In general, the size of the data buffer that the snapshot operation
allocates is restricted by system resource constraints. For global
snapshots, you can optionally specify the maximum allowed size of the
data buffer by setting the DB2_MAX_GLOBAL_SNAPSHOT_SIZE registry
variable.

This message can be returned in two types of scenarios:

*  In general, this message is returned when the requested snapshot size
   exceeds the maximum possible snapshot size.
*  For global snapshot operations, this message can also be returned
   when the requested snapshot size exceeds the value that is set in the
   DB2_MAX_GLOBAL_SNAPSHOT_SIZE registry variable.

User response: 

Respond to this message by performing one or more of the following
actions.

General: 
         *  Create a smaller snapshot by reducing the amount of
            information that is collected. 

            Two examples of ways to collect smaller snapshots by
            changing the parameters that are specified with the GET
            SNAPSHOT command:

             
            *  Collect information for only one database by using the
               DATABASE parameter instead of using the ALL DATABASES
               parameter.
            *  Collect information for only the applications that are
               connected to a particular database by using the
               APPLICATIONS ON parameter instead of using the ALL
               APPLICATIONS parameter.


Global snapshots only:
         
         *  Collect multiple, smaller snapshots by running the GET
            SNAPSHOT command separately on each database partition
            instead of creating a global snapshot.
         *  If the DB2_MAX_GLOBAL_SNAPSHOT_SIZE registry variable is
            set, increase DB2_MAX_GLOBAL_SNAPSHOT_SIZE to a value that
            is at least as large as the actual size given in this
            message in the "<snapshot-size>" runtime variable, and then
            run the GET SNAPSHOT command again.


   Related information:
   GET SNAPSHOT command
   Snapshot monitor
