

SQL3315N  The volume fields in an A record of subtype C are not valid.

Explanation: 

During the load of a PC/IXF file that was created by Database Services,
an A record was found in the PC/IXF file, that contained volume
information (in the volume fields) that is not valid.

The input file has probably been damaged.

The utility stops processing.

User response: 

Re-create the damaged file, or repair the damaged file to recover as
much data as possible. Resubmit the command.
