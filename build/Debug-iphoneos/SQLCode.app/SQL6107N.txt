

SQL6107N  The partitioning key information in the data file is not
      correct.

Explanation: 

Either the data was not split with db2split or the db2split operation
was not successful.

User response: 

Use the db2split program to partition the data and then try the request
again with the partitioned data. If the insert-column option is being
used, ensure that all of the partitioning columns are specified in the
column list.

If the problem persists, contact your technical service representative
with the following information: 
*  Problem description
*  SQLCODE and embedded reason code
*  SQLCA contents if possible
*  Trace file if possible
