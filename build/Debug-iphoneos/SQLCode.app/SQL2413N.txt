

SQL2413N  Online backup is not allowed because the database is not
      recoverable or a backup pending condition is in effect.

Explanation: 

Online backup cannot be performed while the database is not logging for
forward recovery as forward recovery will be required at restore time.
Forward recovery is placed into effect by setting either the database
configuration LOGARCHMETH1 or LOGARCHMETH2 and then performing an
offline backup of the database.

User response: 

Execute an offline backup or reconfigure the database for roll-forward
recovery and issue an offline backup so that subsequent online backups
will be allowed.
