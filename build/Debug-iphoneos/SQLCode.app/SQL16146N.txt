

SQL16146N  The main XML document is empty.

Explanation: 

While parsing an XML document the parser encountered an empty main XML
document. The main XML document cannot be empty.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16146

sqlstate: 2200M
