

SQL7010N  Invalid scan ID "<ID>".

Explanation: 

The scan ID "<variable>" passed to REXX did not exist or contained
inconsistent or missing data.

The command cannot be processed.

User response: 

Verify that the data contained in the scan ID has been assigned
correctly, and run the procedure again.
