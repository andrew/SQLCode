

SQL16114N  XML document contains an ID with a duplicate value "<value>".

Explanation: 

While parsing an XML document the parser encountered an ID value
"<value>" that had previously been declared with the document. ID values
must be unique with a document.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16114

sqlstate: 2200M
