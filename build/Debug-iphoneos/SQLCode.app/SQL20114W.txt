

SQL20114W  Column "<column-name>" in table "<table-name>" is not long
      enough for the defined length of the USER default value.

Explanation: 

The column "<column-name>" is defined with a length that is less than
128 bytes. The clause DEFAULT USER has been specified for this column.
Since the USER special register is defined as VARCHAR(128), any attempt
to assign the default value for "<table-name>" by a user with a user ID
longer than the column length results in an error. A user with a user ID
longer than the column length would never be able to insert or update
this column to the default value.

User response: 

If your system standards would not allow a user ID to exceed the length
of the column, then this warning may be ignored. To prevent this warning
from occurring, the length of the column must be at least 128 bytes. You
can change the column length by dropping and creating the table again,
or, if the data type is VARCHAR, by using ALTER TABLE to increase the
length of the column.

 sqlcode: +20114

 sqlstate: 01642
