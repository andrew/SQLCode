

SQL1399N  Operation "<operation-name>" is not valid for option
      "<option-name1>" because of option "<option-name2>" for object
      "<object-name>". Reason code= "<reason-code>".

Explanation: 

Two wrapper or server options have dependency on each other. Whether an
option can be dropped or added depends on the existence of another
option. Please view section Changes Related to DDL Statements for the
examples of how this sqlcode is used.

The reason codes are: 

01       Option "<option-name1>" cannot be added because option
         "<option-name2>" for object "<object-name>" does not exist.

02       Option "<option-name1>" cannot be dropped because option
         "<option-name2>" for object "<object-name>" exists.

User response: 

Two wrapper or server options have dependency on each other. Whether an
option can be dropped or added depends on the existence of another
option.

The reason codes are: 

01       Add option "<option-name2>" for object "<object-name>". Then
         add option "<option-name1>".

02       Drop opotion "<option-name2>" for object "<object-name>". Then
         drop option "<option-name1>".
