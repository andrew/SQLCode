

SQL2213N  The specified table space is not a system temporary table
      space.

Explanation: 

 The REORG utility requires that any table space specified is a system
temporary table space. The table space name provided is not a table
space defined to hold system temporary tables.

The utility stops processing.

User response: 

Resubmit the command with the name of a system temporary table space or
do not use the table space name parameter. In the latter case, the REORG
utility will use the table space(s) in which the table itself resides.
