

SQL1332N  The Host Name "<name>" is not valid.

Explanation: 

The host name in the TCP/IP protocol structure of the Catalog Node
command is either not specified or longer than the allowable length. The
name must be 1 to 255 characters in length and cannot be all blanks.

User response: 

Verify that the host name is specified and that it is not longer than
255 characters in length. Resubmit the command with a valid host name.
