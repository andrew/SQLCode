

SQL3310N  Column "<name>" in the PC/IXF file is not valid.

Explanation: 

The CREATE or REPLACE_CREATE option was specified on the IMPORT command.
A column with an invalid C record was found while importing the PC/IXF
file.

The IMPORT utility stops processing. The table is not created.

User response: 

Verify the column definition information in the input file.
