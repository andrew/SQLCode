

SQL6560N  Node "<node-number>" which is an execution node for
      partitioning does not appear in the db2nodes.cfg file.

Explanation: 

A node specified as an execution node for partitioning does not appear
to be a member in the db2nodes.cfg file. The work intended to complete
on this node can not be started.

User response: 

Add the node to the node list definition in the db2nodes.cfg file, or
specify an alternate node for the partition operation which is a member
of the node configuration.
