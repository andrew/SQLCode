

SQL1642N  The database manager failed to connect to an internet socket
      on a remote computer because the connection request was refused by
      the remote computer.

Explanation: 

To interact with a remote database across a network, the DB2 database
manager must use a communication protocol, such as TCP/IP, and use
operating system interfaces, such as an internet socket, to connect to
database-related operating system services on the computer on which the
remote database is located.

This message is returned when the DB2 database manager attempts to
connect to a remote computer using the TCP/IP communication protocol and
receives the error code ECONNREFUSED or WSAECONNREFUSED from the TCP/IP
function called CONNECT. Usually, the connection is refused because the
database-related operating system service to which the database manager
is attempting to connect on the remote computer is inactive.

There are multiple scenarios that can lead to this error being returned,
including the following scenarios:

*  The database manager on the remote computer is stopped
*  There is a problem with the way the remote database is cataloged
*  There is a problem with the way the remote database server is
   configured
*  The DB2COMM registry variable at the remote database server is not
   set to the communication protocol that is being used by the client
*  Firewall software on the remote computer is blocking the database
   manager connection attempt
*  There are more TCP/IP connection requests than the remote computer
   can handle

User response: 

Respond to this error by systematically eliminating the possible causes:

1. Ensure that the database manager on the remote computer has been
   started successfully.
2. Ensure that the database is cataloged correctly.
3. Ensure that the entries in the database manager configuration file
   for the remote database are valid and consistent.
4. Ensure that the DB2COMM environment variable at the remote database
   server is set to the communication protocol that is being used by the
   client.
5. Ensure that firewall software is not blocking the TCP/IP connection
   to the remote computer.
6. Ensure the number of connection requests being sent to the remote
   computer by all applications is less than the number of request that
   can be handled by the computer.

If you have eliminated the possible causes listed here, collect DB2 and
system diagnostic information using the db2support command and then
contact IBM software support.


   Related information:
   Protocol-specific error codes for the SQL30081N message
   Starting instances (Windows)
   Cataloging databases
   Updating the database manager configuration file on the server for
   TCP/IP communications
   Configuring DB2 server communications (TCP/IP)
   Setting communication protocols for a DB2 instance
   Introduction to firewall support
