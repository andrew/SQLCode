

SQL22286N  Unable to execute the required task.

Explanation: 

The scheduler failed to run the task because an unexpected error
occurred.

User response: 

Refer to the DB2 Administration Server's First Failure Data Capture Log
for additional information.

If trace was active, invoke the Independent Trace Facility at the
operating system command prompt.
