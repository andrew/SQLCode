

SQL2955N  The ingest utility could not find file "<filename>".

Explanation: 

A file that the ingest utility requires could not be found. The
installation of the DB2 Data Server Client or DB2 Data Server Runtime
client might be incomplete or damaged.

User response: 

Reinstall the DB2 Data Server client or the DB2 Data Server Runtime
client.
