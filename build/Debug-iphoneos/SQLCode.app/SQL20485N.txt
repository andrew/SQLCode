

SQL20485N  The CREATE statement for routine "<routine-name>" defines a
      parameter without a DEFAULT after a parameter that has been
      defined with a DEFAULT.

Explanation: 

During the creation of procedure routine-name, a parameter is specified
without a defined DEFAULT value after the specification of a parameter
with a defined DEFAULT value. All parameters without a defined DEFAULT
must be specified before those which have a defined DEFAULT value. The
statement cannot be processed.

User response: 

Provide a DEFAULT value for all parameters that follow the first
parameter that is defined with a DEFAULT value or re-order the parameter
list such that all parameters defined with a DEFAULT value are after all
parameters defined without a DEFAULT value.

sqlcode: -20485

 sqlstate: 428HG
