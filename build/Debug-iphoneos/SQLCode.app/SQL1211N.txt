

SQL1211N  The computer name "<name>" is not valid.

Explanation: 

The computer name specified in the NPIPE protocol structure for the
Catalog command is not valid. The size of the computer name must be 15
characters or less.

The command cannot be processed.

User response: 

Verify that the computer name is valid and resubmit the command.
