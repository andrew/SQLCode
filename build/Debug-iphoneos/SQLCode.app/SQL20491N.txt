

SQL20491N  The statement failed because the specification of a period
      "<period-name>" is not valid. Reason code "<reason-code>".

Explanation: 

The specification of a period attribute in a CREATE or ALTER statement
is invalid for the reason code indicated:

1        

         The row-begin column name must not be the same as the row-end
         column name for the period.


2        

         The name of a column in a period must not be the same as a
         column used in the definition of another period for the table.


3        

         The data type, length, precision, and scale for the row-begin
         column must be the same as for the row-end column.


4        

         The type of timestamp specified for the row-begin column must
         be the same as the type of timestamp specified for the row-end
         column.


5        

         For a BUSINESS_TIME period, the column must not be a column
         defined with a GENERATED clause.


6        

         For a SYSTEM_TIME period, the row-begin column must be defined
         as ROW-BEGIN and the row-end column must be defined as ROW-END.

The statement cannot be processed.

User response: 

Correct the syntax and resubmit the statement.

sqlcode: -20491

sqlstate: 428HN
