

SQL0997W  General informational message for transaction processing.
      Reason Code = "<XA-reason-code>".

Explanation: 

The SQLCODE 997 is only passed between components of the database
manager and will not be returned to an application. It is used to carry
XA return codes for non-error situations. The possible reason codes are:

*  XA_RDONLY (3) - the transaction branch was read-only and has been
   committed.
*  64 - TM database indicates transaction to be committed on DUOW
   resynchronization
*  65 - TM database indicates transaction to be rolled back on DUOW
   resynchronization

User response: 

No action required.
