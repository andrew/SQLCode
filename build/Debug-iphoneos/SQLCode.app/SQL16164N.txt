

SQL16164N  XML document contains duplication <annotation> elements in
      the content.

Explanation: 

While parsing an XML document, the parser encountered duplicate
<annotation> elements in the contents. At most one <annotation> element
is expected.

Parsing or validation did not complete.

User response: 

Correct the XML document to remove the duplicate <annotation> and try
the operation again.

sqlcode: -16164

sqlstate: 2200M
