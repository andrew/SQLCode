

SQL3046N  The number of columns in the METHOD parameter is less than
      one.

Explanation: 

For METHOD methods other than Default, the number of columns specified
must be a positive number (greater than 0).

The command cannot be processed.

User response: 

Resubmit the command with a valid number of columns in the METHOD
parameter.
