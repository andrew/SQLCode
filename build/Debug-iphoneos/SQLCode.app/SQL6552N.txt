

SQL6552N  An error occurred while attempting to open a temporary
      configuration file "<filename>" for writing.

Explanation: 

The file name and path for a temporary file could not be opened. An
error occurred.

User response: 

Confirm that the utility temporary file storage path was specified
correctly and that the path allows for files to be opened for writing.
