

SQL4109W  The SET FUNCTION SPECIFICATION references column "<column>".

Explanation: 

One of the following conditions has not been met: 
*  The COLUMN REFERENCE of a DISTINCT SET FUNCTION cannot reference a
   column derived from a SET FUNCTION SPECIFICATION.
*  COLUMN REFERENCEs in the VALUE EXPRESSION of an ALL SET FUNCTION
   cannot reference a column derived from a SET FUNCTION SPECIFICATION.

Processing continues.

User response: 

Correct the SQL statement.
