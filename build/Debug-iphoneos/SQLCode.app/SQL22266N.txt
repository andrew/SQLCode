

SQL22266N  The DB2 Administration Server is already installed under the
      current DB2 Copy.

Explanation: 

The DB2 Administration Server is already installed under the DB2 Copy
from which the DB2 Administration Server update command was run.

User response: 

Run the DB2 Administration Server update command from the DB2 Copy to
which you would like the DB2 Administration Server to be moved.
