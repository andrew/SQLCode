

SQL1616N  The limit on the maximum number of active event monitors has
      already been reached.

Explanation: 

A maximum of 128 event monitors can be active simultaneously on each
database partition.

In a multiple partition database environment, a maximum of 32 GLOBAL
event monitors can be active simultaneously on each database.

One of these limits has already been reached. The specified event
monitor cannot be activated.

User response: 

If possible, deactivate one of the active event monitors and resubmit
the SET EVENT MONITOR statement. Use the following query to determine
all the active event monitors and whether or not they are global:

SELECT EVMONNAME, MONSCOPE FROM SYSCAT.EVENTMONITORS WHERE
EVENT_MON_STATE(EVMONNAME) = 1

sqlcode: -1616

 sqlstate: 54030
