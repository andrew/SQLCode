

SQL0667N  The FOREIGN KEY "<name>" cannot be created because the table
      contains rows with foreign key values that cannot be found in the
      parent key of the parent table.

Explanation: 

The definition of the indicated foreign key failed because the table
being altered contains at least one row where the foreign key does not
match the parent key value in the parent table.

"<name>" is the constraint name, if specified. If a constraint name was
not specified, "<name>" is the first column name specified in the column
list of the FOREIGN KEY clause followed by three periods.

The statement cannot be processed. The specified table is not altered.

User response: 

Remove the erroneous table rows and define the foreign key.

 sqlcode: -667

 sqlstate: 23520
