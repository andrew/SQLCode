

SQL22264N  The DB2 Administration Server migrate command has encountered
      an unexpected error.

Explanation: 

An unexpected error occurred while attempting to migrate the DB2
Administration Server.

User response: 

Contact IBM Support and, if possible supply a trace file.
