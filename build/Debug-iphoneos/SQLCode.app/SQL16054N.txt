

SQL16054N  The normalization form "<form>" that is specified as an
      argument of the function fn:normalize-unicode is not supported.
      Error QName=err:FOCH0003.

Explanation: 

The effective value "<form>" that was passed as the normalization form
argument to the fn:normalize-unicode function is not supported. The
effective value of the normalization form is computed by removing
leading and trailing blanks, if present, and converting to upper case.

The XQuery expression cannot be processed.

User response: 

Pass a supported normalization form to the fn:normalize-unicode
function.

 sqlcode: -16054

 sqlstate: 10603
