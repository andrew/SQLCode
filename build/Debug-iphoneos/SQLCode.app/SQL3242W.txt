

SQL3242W  Row "<row>" and column "<column>" in the input source contains
      an invalid security label string for the target table.

Explanation: 

The SECLABELCHAR file type modifier was specified, but the value for the
DB2SECURITYLABEL column is not in the proper format for a security label
string. The row is not loaded.

User response: 

Check that the values in the input source for the DB2SECURITYLABEL
column are in the correct format. If necessary, correct the input data
source and resubmit the command.

sqlcode: +3242

sqlstate: 01H53
