

SQL16131N  XML document contains a start tag "<tag-name>" that is not
      terminated.

Explanation: 

While parsing an XML document the parser encountered a start tag
identified by "<tag-name>" that is not terminated. The content following
the start tag may be missing the end tag or the content is not
well-formed between the start tag and the end tag.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16131

sqlstate: 2200M
