

SQL16035N  A validate expression is not supported in DB2 XQuery. Error
      QName=err:XQST0075.

Explanation: 

The validation feature is not supported in DB2 XQuery, so a validate
expression cannot be used as an XQuery expression.

The XQuery expression cannot be processed.

User response: 

Remove all validate expressions from the XQuery expression.

 sqlcode: -16035

 sqlstate: 10509
