

SQL1044N  Processing was cancelled due to an interrupt.

Explanation: 

The user may have pressed the interrupt key sequence.

Processing is stopped.

Federated system users: this situation can also be detected by the data
source.

User response: 

Continue processing to handle the interrupt.

If installing the sample database, drop it and install the sample
database again.

If starting the database manager, issue a db2stop before issuing any db2
commands.

 sqlcode: -1044

 sqlstate: 57014
