

SQL2084N  Only one work action set can be defined for the following
      database, workload, or service superclass: "<db-or-ssc-name>".

Explanation: 

There is already a work action set defined for the following specified
database, workload, or service superclass: "<db-or-ssc-name>". Only one
work action set can be defined for any one database, workload, or
service superclass at any given time.

User response: 

1. Do one of the following: 
   *  Specify a different database, workload, or service superclass.
   *  Drop the work action set currently defined for the database,
      workload, or service superclass.

2. Issue the request again.

sqlcode: -2084

sqlstate: 5U017
