

SQL1432N  A request has been sent to a server using a database protocol
      which the server does not recognize.

Explanation: 

This error is caused by sending a DB2 request to a server which does not
understand the database protocol being used to transmit the request.
This situation will occur most frequently when sending a DB2 ATTACH
request to a server listed in your node directory which is not a DB2
Version 2 or greater server. This error will also arise if you send an
attach request to DB2 for AS/400, DB2 for MVS, or DB2 for VM and VSE
servers.

User response: 

Do not attempt to attach to any of the servers listed in the
Explanation.
