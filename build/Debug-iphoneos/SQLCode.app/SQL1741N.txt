

SQL1741N  The command or operation failed because the master key for
      master key label "<label>" has changed.

Explanation: 

The data encryption key cannot be decrypted because the master key
identified by the specified label has changed. The key identified by the
master key label must never change.

User response: 

Ensure the keystore contains the correct master key and master key
label. If required, restore the keystore from the most recent backup
image and retry the command or operation.


   Related information:
   BACKUP DATABASE command
