

SQL0462W  Command or routine "<command-or-routine-name>" (specific name
      "<specific-name>") has returned a warning SQLSTATE, with
      diagnostic text "<text>".

Explanation: 

An SQLSTATE of the form 01Hxx was returned to DB2 by the command or
routine "<command-or-routine-name>" (with specific name
"<specific-name>"), along with message text "<text>". If
"<command-or-routine-name>" is a command, then "<specific-name>" will
contain the value ''*N''.

User response: 

The user will need to understand the meaning of the warning. See your
database administrator, or the author of the routine.

sqlcode: +462

sqlstate: Valid warning SQLSTATEs returned by a user-defined function,
external procedure CALL, or command invocation.
