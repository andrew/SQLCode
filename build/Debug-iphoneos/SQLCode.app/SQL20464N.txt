

SQL20464N  An attempt to revoke the SECADM authority from
      "<authorization-ID>" was denied because it is the only external
      authorization ID of type user with SECADM authority.

Explanation: 

The SECADM authority must be held by at least one external authorization
ID of type user, and the REVOKE statement is attempting to revoke the
authority from the last authorization ID of type user that holds this
authority. The statement cannot be executed. The SECADM authority is not
revoked.

User response: 

Grant the SECADM authority to another external authorization ID of type
user to allow the SECADM authority to be revoked from
"<authorization-ID>".

sqlcode: -20464

 sqlstate: 42523
