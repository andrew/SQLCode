

SQL3244W  Row "<row>" and column "<column>" in the input source contains
      the security label named "<security-label-name>", which cannot be
      found for the security policy "<policy-name>" protecting the
      target table.

Explanation: 

The SECLABELNAME file type modifier was specified, but the security
label named "<security-label-name>" cannot be found for the security
policy "<policy-name>". The row is not loaded.

User response: 

Check the input source column and confirm that the value appears to be
valid for the security policy protecting the target table. If necessary,
correct the input data source and resubmit the command.

sqlcode: +3244

sqlstate: 01H53
