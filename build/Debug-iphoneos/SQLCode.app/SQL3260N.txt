

SQL3260N  An unexpected error occurred when accessing the LDAP
      directory. Error code = "<error-code>".

Explanation: 

An unexpected error occurred when accessing the LDAP directory. The
command can not be processed.

User response: 

Record the message number (SQLCODE) and the error code. Use the
Independent Trace Facility to obtain the DB2 trace. Then contact your
IBM service representative.
