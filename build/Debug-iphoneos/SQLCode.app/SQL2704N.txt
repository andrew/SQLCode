

SQL2704N  Failed to open the input data file "<input-data-file>".

Explanation: 

The utility cannot read the input data file "<input-data-file>".

User response: 

Please ensure the input data file exists and is readable.
