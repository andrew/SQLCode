

SQL20481N  The creation or revalidation of object "<object-name>" would
      result in an invalid direct or indirect self-reference.

Explanation: 

The definition of the object being created or replaced contains a direct
or indirect reference to itself. This self-reference can either be
explicit in the definition, or implicit by a reference to another object
that explicitly or implicitly references the object. An object
definition can only contain a valid reference to itself when it is
created using the CREATE SCHEMA statement. Such an object can only be
replaced or revalidated if the new definition does not contain the
self-reference.

User response: 

Remove the self-reference or use the CREATE SCHEMA statement to create
the object that has a valid reference to itself.

sqlcode: -20481

sqlstate: 429C3
