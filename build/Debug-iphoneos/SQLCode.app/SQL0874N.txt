

SQL0874N  The CCSID of all the parameters must match the PARAMETER CCSID
      of the routine.

Explanation: 

All the parameters of a routine must use the same encoding scheme as the
routine itself. If the CCSID is specified for a parameter, it must match
the implicitly or explicitly specified PARAMETER CCSID option for the
routine.

User response: 

Remove the CCSID option from the parameter or change the statement so
the same CCSID value is specified throughout.

sqlcode: -874

sqlstate: 53091
