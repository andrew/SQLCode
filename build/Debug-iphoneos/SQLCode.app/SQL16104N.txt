

SQL16104N  Internal error encountered in XML parser. Parser error is
      "<parser-error>".

Explanation: 

While parsing an XML document the parser encountered an internal error
"<parser-error>". The value of "<parser-error>" is the internal parser
error code.

Parsing or validation did not complete.

User response: 

Try the operation again and if the error persists, contact IBM service.

sqlcode: -16104

sqlstate: 2200M
