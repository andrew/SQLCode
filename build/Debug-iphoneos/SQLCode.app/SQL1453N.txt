

SQL1453N  The entry in the database manager configuration file for file
      server name is missing or invalid.

Explanation: 

The file server name specified in the configuration command/API or in
the database manager configuration file is missing or invalid.

User response: 

Verify that a file server name was specified, that the name does not
contain invalid characters, and is not longer than 48 characters in
length. Update the file server name in the database manager
configuration file and resubmit the command/API.
