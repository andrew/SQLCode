

SQL20338N  The data type of either the source or target operand of an
      XMLCAST specification must be XML.

Explanation: 

The XMLCAST specification must have one operand that has the data type
XML. The XMLCAST operation can be from an XML type value to an SQL type
value or from an SQL type value to and XML type value. The XMLCAST
specification also accepts having both the source and target operands as
XML but no actual casting operation is performed in this case.

User response: 

If both operands are SQL data types other than XML, use the CAST
specification. Otherwise, change the XMLCAST specification so that at
least one operand is the XML data type.

sqlcode: -20338

sqlstate: 42815
