

SQL22214N  The admin node "<node-name>" does not exist in the DB2 node
      directory.

Explanation: 

The admin node "<node-name>" is invalid. The node name does not exist in
the DB2 node directory.

User response: 

Verify that the node name "<node-name>" is cataloged in the admin node
directory using the LIST ADMIN NODE DIRECTORY command. If the admin node
is not listed in the admin node directory, submit a CATALOG ADMIN ...
NODE command to catalog the admin node. If you continue to receive this
error message after attempting the suggested response, please contact
IBM Support.
