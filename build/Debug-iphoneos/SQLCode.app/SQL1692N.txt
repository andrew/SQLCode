

SQL1692N  An error was encountered during DB2STOP processing. The
      database manager failed to stop a component of the DB2 pureScale
      environment.

Explanation: 

A component of the DB2 pureScale environment failed to respond to the
DB2 cluster services and did not shut down.

User response: 

1. Determine which components of the DB2 database manager instance
   failed to stop using the following command: 
   db2instance -list

2. Stop the components that failed to stop by performing the following
   steps: 
   a. Collect more information about the reasons why these components
      failed to stop from the diagnostic information collected in the
      db2diag log files.
   b. Resolve the problems that caused the components to fail to stop.
   c. Stop the components manually.

3. [Optional] Clean up interprocess communications for the instance by
   running the following command as the instance owner at each physical
   partition: 
   $HOME/sqllib/bin/ipclean


   Related information:
   Interpretation of status information
   Stopping all DB2 processes (Linux and UNIX)
   db2instance - Query state of DB2 instance command
   Summary of instance status information from the db2instance -list
   command
