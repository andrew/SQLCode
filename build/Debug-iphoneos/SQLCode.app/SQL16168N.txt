

SQL16168N  XML document contained an invalid XML declaration. Reason
      code = "<reason-code>".

Explanation: 

While processing an XML document or XML schema the XML parser
encountered a missing or invalid XML declaration. The "<reason-code>"
indicates which of the following conditions was found. 

1        The XML declaration strings must be in the order 'version',
         'encoding', and 'standalone'.

2        The declaration must start <?xml, not <?XML. The string 'xml'
         must be in lower case.

3        The XML or text declaration must start at the first column of
         the first line.

4        The XML declaration must include the 'version=' string.

5        The XML declaration is required and not present.

6        The specified XML version is unsupported, or invalid.

7        The specified document encoding was invalid or contradicts the
         automatically sensed encoding.

8        The XML declaration is not terminated.

9        The value of the standalone attribute is invalid or
         unsupported.

10       Only attributes 'version', encoding', and 'standalone' are
         supported.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16168

sqlstate: 2200M
