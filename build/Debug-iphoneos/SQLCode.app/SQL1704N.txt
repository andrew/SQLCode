

SQL1704N  Database upgrade failed. Reason code "<reason-code>".

Explanation: 

Database upgrade failed. The reason codes are as follows:

2        

         database cannot be upgraded because it could be in one of the
         following states:

          
         *  backup pending state
         *  restore pending state
         *  roll-forward pending state
         *  transaction inconsistent state
         *  HADR has marked the database inconsistent


3        

         database logs are full.


4        

         insufficient disk space.


5        

         cannot update database configuration file.


7        

         failed to access the database subdirectory or one of the
         database files.


8        

         failed to update database container tag.


9        

         table space access is not allowed.


17       

         Failure to allocate new page from the system catalog table
         space.


21       

         Database upgrade is complete on the catalog partition but not
         on all the other database partitions. There are database
         partitions that cannot be upgraded due to system errors, such
         as node failure or connection failure.


22       

         Database upgrade failed because the catalog partition cannot be
         upgraded due to system errors, such as database partition
         failure or connection failure.


24       

         Error creating the dbpath/db2event/db2detaildeadlock event
         monitor directory where dbpath is the file path used to create
         the database.

User response: 

Possible solutions based on the reason code are:

2        

         Reverse the database upgrade and correct the database state by
         performing any necessary corrective action in the DB2 copy
         version where the database resided prior to the upgrade. For
         HADR systems, stop HADR should be issued prior to attempting to
         upgrade the HADR primary database. Resubmit the UPGRADE
         DATABASE command from the DB2 copy version to which you want to
         upgrade the database.


3        

         Increase the database configuration parameters logfilsiz or
         logprimary to a larger value. Resubmit the UPGRADE DATABASE
         command.


4        

         Ensure that there is sufficient disk space and resubmit the
         UPGRADE DATABASE command.


5        

         There was a problem in updating the database configuration
         file. Ensure that the database configuration file is not being
         held exclusively by any users and is updatable. Resubmit the
         UPGRADE DATABASE command. If the problem persists, inform your
         IBM service representatives.


7        

         Restore the database from the database backup.


8        

         Resubmit the UPGRADE DATABASE command. If the problem persists,
         contact your IBM service representative.


9        

         Reverse the database upgrade and correct the table space
         access. Resubmit the UPGRADE DATABASE command from the DB2 copy
         version to which you want to upgrade the database. Refer to
         message SQL0290N for the suggested actions to take to correct
         the table space.


17       

         If the system catalog table space is an Automatic Storage DMS
         table space or SMS table space, ensure that there is at least
         50% free disk space available for the system catalog table
         space, then upgrade the database. If the system catalog table
         space is a DMS table space. Reverse the database upgrade and
         add more containers to the system catalog table space from the
         DB2 copy version where the database resided prior to upgrade.
         You should allocate 50% free space for database upgrade.
         Resubmit the UPGRADE DATABASE command from the DB2 copy version
         to which you want to upgrade the database.


21       

         Check the administration notification log to determine the
         database partitions that cannot be upgraded. Correct the
         situation and resubmit the UPGRADE DATABASE command. As
         database upgrade only takes place on database partitions that
         require upgrade, you can submit the UPGRADE DATABASE command
         from any database partition.


22       

         Correct the database partition failure situation on the catalog
         partition. Resubmit the UPGRADE DATABASE command.


24       

         Remove the dbpath/db2event/db2detaildeadlock event monitor
         directory where dbpath is the file path used to create the
         database. Resubmit the UPGRADE DATABASE command.
