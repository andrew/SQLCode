

SQL8010W  The number of concurrent users of the DB2 Connect product has
      exceeded the defined entitlement of "<number>". Concurrent user
      count is "<number>".

Explanation: 

The number of concurrent users has exceeded the number of defined DB2
concurrent user entitlements.

User response: 

Contact your IBM representative or authorized dealer to obtain
additional DB2 user entitlements and update the DB2 license information
using the db2licm command.

sqlcode: +8010

sqlstate: 01632


   Related information:
   db2licm - License management tool command
