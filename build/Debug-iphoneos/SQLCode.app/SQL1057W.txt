

SQL1057W  The system database directory is empty.

Explanation: 

An attempt was made to read the contents of the system database
directory, but no entries existed.

User response: 

No action is required.

sqlcode: +1057

sqlstate: 01606
