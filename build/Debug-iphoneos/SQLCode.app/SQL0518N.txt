

SQL0518N  The statement named in the EXECUTE statement is not in a
      prepared state or is a SELECT or VALUES statement.

Explanation: 

The application program tried to EXECUTE a statement that

1. was never prepared,
2. is a SELECT or VALUES statement,
3. was made not valid by either an explicit or implicit rebind of the
   package, or
4. was prepared in a previous transaction and the application's package
   is bound with KEEPDYNAMIC NO.

The statement cannot be processed.

User response: 

1. Prepare the statement before attempting the EXECUTE.
2. Ensure that the statement is not a SELECT or VALUES statement.
3. The prepare for the cursor must be reissued.
4. The statement should be prepared again after COMMIT or ROLLBACK.
   Alternatively, either bind the package with KEEPDYNAMIC YES or use
   the ALTER PACKAGE statement to change the KEEPDYNAMIC property to
   YES.

sqlcode: -518

sqlstate: 07003
