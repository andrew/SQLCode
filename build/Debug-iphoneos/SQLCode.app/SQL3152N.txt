

SQL3152N  The double-byte code page value "<value>" in the H record is
      not compatible with the double-byte code page value "<value>" for
      the application. Data will be inserted because the FORCEIN option
      was specified.

Explanation: 

The double-byte code page values in the record and the application are
not compatible. Because the FORCEIN option was used, the data is
inserted.

User response: 

No action is required.
