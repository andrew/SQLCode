

SQL3212N  The TERMINATE option of the LOAD command is not currently
      supported for tables with DATALINK column(s), or table spaces in
      delete pending state.

Explanation: 

An attempt was made to terminate a crashed, interrupted, or forced LOAD
operation against a table which contains DATALINK column(s), or a table
which resides in table space(s) in delete pending state. These are not
currently supported.

User response: 

Use the RESTART option of the LOAD command to recover a crashed,
interrupted, or forced LOAD operation.
