

SQL1758W  The containers not designated for specific nodes are not used
      by the table space on any node.

Explanation: 

The ALTER TABLESPACE and CREATE TABLESPACE statement includes container
specification for all the database partition in the database partition
group. The specification of the containers that is not followed by an ON
DATABASE PARTITION clause is redundant and has been ignored.

The statement has been processed.

User response: 

If the containers are needed on some of the nodes, issue an ALTER
TABLESPACE statement to add the necessary containers.

 sqlcode: +1758

 sqlstate: 01589
