

SQL20322N  The database name provided does not match "<server-name>",
      the name of the database that the application is connected to.

Explanation: 

The database name does not match "<server-name>", the name of the
database that the application is currently connected to. The database
name was either explicitly specified or determined by the specified
database alias name.

User response: 

If the intention is to alter the database that you are currently
connected to, either remove the database name from the statement or
specify the correct name. If the intention is to alter the database with
the given name, and not the database you are currently connected to,
disconnect from the current database and connect to the specified
database before resubmitting the statement. If you are backing up or
restoring a database, connect to that database and provide the correct
database name or database alias name.

sqlcode: -20322

sqlstate: 42961
