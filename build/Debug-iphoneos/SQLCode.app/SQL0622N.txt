

SQL0622N  The clause "<clause>" is invalid for this database.

Explanation: 

The clause indicated is invalid because it is incompatible with the
defined characteristics for this database.

Possible reasons: 
*  CCSID ASCII and PARAMETER CCSID ASCII cannot be specified when
   connected to a database created using a Unicode code page.
*  CCSID UNICODE and PARAMETER CCSID UNICODE cannot be specified when
   connected to a database created using a non-Unicode code page before
   the alternate collating sequence is specified in the database
   configuration.
*  CCSID UNICODE or PARAMETER CCSID UNICODE cannot be specified in this
   statement when connected to a database created using a non-Unicode
   code page.
*  FOR SBCS DATA can only be specified when connected to a database
   created using a single byte code page.
*  FOR MIXED DATA can only be specified when connected to a database
   created using a double byte or Unicode code page.
*  The IN "<database-name.table-space-name>" or IN DATABASE
   "<database-name>" clause was specified using a database name that
   does not match the name of the database to which the application is
   currently connected.

The statement cannot be processed.

User response: 

Change or remove the clause and re-issue the SQL statement.

To allow Unicode objects in a non-Unicode database, update the database
configuration to specify the alternate collating sequence (ALT_COLLATE).

 sqlcode: -622

 sqlstate: 56031
