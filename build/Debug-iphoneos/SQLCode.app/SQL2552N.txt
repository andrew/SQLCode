

SQL2552N  Invalid report file name specified in the restore command.

Explanation: 

The length of the report file name exceeded the allowed limit of 255.

User response: 

Specify a report file name whose length is within the allowed limit and
resubmit the restore command.
