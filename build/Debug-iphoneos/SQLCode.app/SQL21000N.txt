

SQL21000N  A text search option is not installed or not properly
      configured.

Explanation: 

1. DB2 Text Search or DB2 Net Search Extender is not installed or not
   correctly configured on this server. One of the text search features
   must be correctly configured and started on your system if you want
   to use a text search function such as CONTAINS, SCORE, or
   NUMBEROFMATCHES, or the DESCRIBE TEXT SEARCH INDEXES command.
2. DB2 Text Search is not supported in a DB2 pureScale environment.

User response: 

1. Ensure that either DB2 Text Search or DB2 Net Search Extender is
   correctly installed and configured and that the database is enabled
   for text search.
2. Disable the DB2 pureScale Feature to use DB2 Text Search.

sqlcode: -21000

sqlstate: 42724
