

SQL6101N  This data file contains data for node "<node-1>" but the Load
      utility is connected to node "<node-2>".

Explanation: 

The data to be loaded is associated with a node number that differs from
the node number of the node to which the application is connected. The
data cannot be loaded.

User response: 

Either find the data file associated with this node and try the request
again with that data file, or connect to the node associated with this
data file and issue the request at that node.
