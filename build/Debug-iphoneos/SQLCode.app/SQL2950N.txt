

SQL2950N  The base tables of view "<view-name>" are protected by more
      than one security policy.

Explanation: 

When you ingest into an updateable view and the view has multiple base
tables, all the base tables that are protected by a security policy must
be protected by the same security policy.

User response: 

One of the following:

*  Specify a different table or view.
*  Alter the base tables so that all the base tables that are protected
   by a security policy are protected by the same security policy.
