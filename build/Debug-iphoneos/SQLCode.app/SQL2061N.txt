

SQL2061N  An attempt to access media "<media>" is denied.

Explanation: 

An attempt to access a device, file, named pipe, TSM or the vendor
shared library is denied during the processing of a database utility.
The utility stops processing.

User response: 

Ensure the device, file, named pipe, TSM or vendor shared library used
by the utility allows the access requested and resubmit the utility
command.
