

SQL3193N  The specified view or materialized query table cannot be
      updated. You cannot LOAD/IMPORT into this view or LOAD into this
      materialized query table.

Explanation: 

The LOAD/IMPORT utility can be run against a view only if the view can
be updated. The specified view is defined such that data in it may not
be changed.

The LOAD utility can be run against a materialized query table only if
the materialized query table is not replicated. The specified table is a
replicated materialized query table.

The IMPORT utility can be run against a materialized query table only if
the materialized query table is a user maintained materialized query
table. The specified table is a system maintained materialized query
table.

The LOAD/IMPORT utility stops processing. No data is inserted.

User response: 

Resubmit the command with the name of a table or a view that can be
updated.
