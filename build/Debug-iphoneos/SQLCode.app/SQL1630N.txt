

SQL1630N  The specified event monitor has already reached its
      PCTDEACTIVATE limit.

Explanation: 

The specified Write to Table event monitor was created with a
PCTDEACTIVATE limit, specifying how full a DMS table space must be
before the event monitor automatically deactivates, and this limit has
already been reached. The specified event monitor cannot be activated.

User response: 

Reduce the space used in the table space, and resubmit the SET EVENT
MONITOR statement. Alternatively, drop the event monitor and recreate it
with a higher PCTDEACTIVATE limit value.

 sqlcode: -1630

 sqlstate: 54063
