

SQL3216W  The table's index object was not compatible with INCREMENTAL
      index maintenance at the time the load utility began. INCREMENTAL
      indexing cannot be performed during this load utility operation.
      The REBUILD indexing mode will be used instead.

Explanation: 

INCREMENTAL indexing can only be used on tables that have a compatible
index object at the time the load utility begins. Loading with indexing
mode REBUILD causes the table index to be rebuilt in a consistent
manner.

User response: 

No action required.
