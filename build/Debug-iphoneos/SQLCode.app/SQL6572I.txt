

SQL6572I  The LOAD operation has begun on partition "<node-number>".

Explanation: 

The LOAD operation has begun on the specified partition.

User response: 

This is an informational message.
