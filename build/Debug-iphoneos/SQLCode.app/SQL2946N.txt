

SQL2946N  The INGEST command must include the field list for this file
      format.

Explanation: 

The INGEST command can omit the field list only when the format is
delimited. If the format is positional, you must specify the field list.

User response: 

Add the field list to the INGEST command.
