

SQL1442N  The context is not in use or is not in use by the current
      thread. Reason code "<code>".

Explanation: 

The call failed because: 

1        The context is not in use by any thread (no attach was done)

2        The context is not in use by the current thread.

3        The current thread is not using a context.

User response: 

For a detach call, ensure that the context is the one in use by the
current thread, and that a corresponding attach was done.

For a get current context call, ensure that the thread is currently
using a context.
