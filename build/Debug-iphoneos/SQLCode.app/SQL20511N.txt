

SQL20511N  The attempt to put data into the message buffer failed
      because there is not enough available space in the message buffer.
      Message buffer name: "<buffer-name>".

Explanation: 

The attempt to put data into the message buffer failed because the
available free space in the buffer is not large enough for the data.

User response: 

Respond to this message in one of the following ways:

*  For the DBMS_OUTPUT buffer, take one of the following actions: 
   *  Call the DBMS_OUTPUT.GET_LINE procedure or DBMS_OUTPUT.GET_LINES
      procedure to retrieve the data from the local message buffer to
      free up space.
   *  Increase the size of the buffer using the DBMS_OUTPUT.ENABLE
      procedure.

*  For the DBMS_PIPE buffer, invoke DBMS_OUTPUT.SEND_MESSAGE function to
   send the content in the message buffer through the pipe.
*  For the UTL_TCP.READ_LINE buffer, decrease the amount of data per
   line transmitted by the sender.

sqlcode: -20511

sqlstate: 5UA0P


   Related information:
   ENABLE procedure - Enable the message buffer
   GET_LINE procedure - Get a line from the message buffer
   GET_LINES procedure - Get multiple lines from the message buffer
