

SQL3217W  The INCREMENTAL indexing mode is supported only when using
      LOAD to append data using the INSERT INTO action. The current LOAD
      action is "<action>". The utility will use indexing mode of
      "<mode>" instead.

Explanation: 

INCREMENTAL indexing can only be used when appending data to a table
using the load INSERT action. This feature is not supported with when
loading with REPLACE, RESTART, or TERMINATE actions.

User response: 

No action necessary.
