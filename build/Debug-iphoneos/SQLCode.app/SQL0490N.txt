

SQL0490N  Number "<number>" directly specified in an SQL statement or
      command is outside the range of allowable values in this context
      ("<minval>","<maxval>").

Explanation: 

A number ("<number>") was specified that is not valid in the context in
which it was specified. The minimum allowed value in this context is
"<minval>". The maximum allowed value in this context is "<maxval>". n
must be within the range specified by "<minval>" and "<maxval>"
("<minval>" =< n => "<maxval>").

If creating or altering a table space, the minimum and maximum values
may be dependant on the page size of the table space. Refer to the SQL
Reference for more details on table space limits.

User response: 

Change the value n to a valid value in the statement or command.

 sqlcode: -490

 sqlstate: 428B7
