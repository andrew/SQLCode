

SQL1841N  The value of the "<option-type>" option "<option-name>" cannot
      be changed for the "<object-name>" object.

Explanation: 

The option value cannot be changed. Some options are set by the object
and cannot be added or overridden by you.

User response: 

See the federation documentation for this data source. Determine the
actions that are valid for the options that are associated with this
object. It might be necessary to drop the associated object and create
it again with a new option value. If this message is produced in
response to a SET SERVER OPTION statement, it might be necessary to use
an ALTER SERVER statement.

sqlcode: -1841

sqlstate: 428GA
