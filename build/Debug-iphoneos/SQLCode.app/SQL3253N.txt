

SQL3253N  The utility is beginning to load data from the SQL statement
      "<statement>" in database "<database>".

Explanation: 

This is an informational message indicating that a load from an SQL
statement fetching from a cataloged database has begun.

User response: 

No action is required.
