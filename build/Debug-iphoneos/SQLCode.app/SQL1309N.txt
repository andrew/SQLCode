

SQL1309N  Invalid server principal name.

Explanation: 

The server principal name specified in the database catalog statement
does not exist in the DCE registry. Because of this a DCE ticket could
not be obtained for the DB2 server.

User response: 

Make sure that the principal name in the database catalog entry
corresponds to the DCE principal being used by the DB2 server. It may be
necessary to fully qualify the principal name.

 sqlcode: -1309

 sqlstate: 08001
