

SQL4138W  The data type "<type1>" of the target is not compatible with
      the data type "<type2>" of the source.

Explanation: 

Data types should match: 
*  In a FETCH statement, between source and target.
*  In a SELECT statement, between source and target.

Processing continues.

User response: 

Correct the SQL statement.
