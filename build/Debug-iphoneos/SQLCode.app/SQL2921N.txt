

SQL2921N  The ingest utility failed because a specified field name is
      not defined in the input source. Field name: "<field-name>".

Explanation: 

You can stream data from files and pipes into DB2 database tables by
using the ingest utility. You can specify to the ingest utility how the
data is defined in the input stream by describing the fields into which
the data is separated.

This message is returned when an attempt is made to perform an ingest
operation with an SQL statement that references a data field that does
not exist in the input source data.

User response: 

Perform the ingest operation again, specifying fields that correspond to
the fields in the input data stream.


   Related information:
   db2Ingest API- Ingest data from an input file or pipe into a DB2
   table.
   INGEST command
   Ingest utility restrictions and limitations
   Comparison between the ingest, import, and load utilities
