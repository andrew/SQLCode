

SQL4146W  Internal error occurred causing the semantics processing to
      stop. Module name = "<module-name>". Internal error code =
      "<error-code>".

Explanation: 

The FLAGGER has encountered a severe internal error in a semantics
analysis routine.

Processing continues but with flagger syntax checking only.

User response: 

Record this message number (SQLCODE), module name and error code in the
message. Contact your technical service representative with the
information.
