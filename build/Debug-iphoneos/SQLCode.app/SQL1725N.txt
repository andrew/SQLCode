

SQL1725N  Could not perform the specified action because the status of
      the indoubt transaction changed after you issued the LIST INDOUBT
      TRANSACTIONS command.

Explanation: 

When you run the LIST INDOUBT TRANSACTIONS command on the client, it
returns the current state of the indoubt transactions. However, the
output is not refreshed on the client if the status changes on the
server. If the status of an indoubt transaction is not synchronized
between the server and the client, you might not be able to perform all
of the actions that are listed as valid options in the LIST INDOUBT
TRANSACTIONS command output.

User response: 

Quit the interactive window and reissue the LIST INDOUBT TRANSACTIONS
command to obtain the most recent status of the indoubt transactions
from the server.


   Related information:
   LIST INDOUBT TRANSACTIONS command
   Resolving indoubt transactions manually
