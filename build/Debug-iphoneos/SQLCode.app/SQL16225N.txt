

SQL16225N  XML document contains a namespace of "<namespace-uri>" that
      is not allowed by a wildcard in the base type.

Explanation: 

While parsing an XML document, the parser encountered an invalid
namespace caused by a type which is derived from a base type with an
"any" element that has an element definition with a namespace URI that
does not match "<namespace-uri>".

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16225

sqlstate: 2200M
