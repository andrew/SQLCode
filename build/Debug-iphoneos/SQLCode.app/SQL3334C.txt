

SQL3334C  There is not enough storage available.

Explanation: 

This message may accompany another error message. It indicates there is
not enough storage available to open the file.

The command cannot be processed.

User response: 

Stop the application. Possible solutions include: 
*  Verify that your system has sufficient real and virtual memory.
*  Remove background processes.
