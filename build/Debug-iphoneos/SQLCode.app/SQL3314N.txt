

SQL3314N  The date and time fields in an A record do not match the date
      and time fields in the H record.

Explanation: 

During the load of a PC/IXF file, an A record was found in the PC/IXF
file, that contained run identification information (in the date and
time fields) that differs from the run identification information in the
header (H) record. This action does not apply to an A record at the
start of a continuation file.

The input file has probably been damaged.

The utility stops processing.

User response: 

Re-create the damaged file, or repair the damaged file to recover as
much data as possible. Resubmit the command.
