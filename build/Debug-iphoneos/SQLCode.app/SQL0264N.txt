

SQL0264N  Partitioning key cannot be added or dropped because table
      resides in a table space defined on the multi-partition database
      partition group "<name>".

Explanation: 

You can only add or drop a partitioning key on a table in a single
database partition group.

The statement cannot be processed.

User response: 

Do one of the following and try the request again:

*  Define an identical table with a partitioning key.
*  Redistribute the database partition group to a single database
   partition group.

sqlcode: -264

sqlstate: 55037
