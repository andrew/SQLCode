

SQL20422N  The statement failed because the table "<table-name>" would
      only contain hidden columns.

Explanation: 

A CREATE TABLE or ALTER TABLE statement attempted to create or alter
table "<table-name>" in which all the columns are considered implicitly
hidden. This can occur when:

*  A CREATE TABLE statement specifies IMPLICITLY HIDDEN as part of the
   definition of all columns.
*  An ALTER TABLE statement alters any not hidden columns to implicitly
   hidden

Change the column definition ensuring it includes at least one column
that is defined as not hidden.

User response: 

Ensure that the table definition includes at least one column that is
not defined as implicitly hidden.

sqlcode: -20422

sqlstate: 428GU
