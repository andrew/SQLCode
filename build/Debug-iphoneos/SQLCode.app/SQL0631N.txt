

SQL0631N  FOREIGN KEY "<name>" is too long or has too many columns.

Explanation: 

The sum of the column internal lengths identified in the FOREIGN KEY
clause in a CREATE TABLE statement exceeds the index key length limit,
or the number of columns identified exceeds 64. Also, a foreign key
cannot be defined using a LONG VARCHAR column.

"<name>" is the constraint name, if specified, in the FOREIGN KEY
clause. If a constraint name was not specified, "<name>" is the first
column name specified in the column list of the FOREIGN KEY clause
followed by three periods.



The index key length limit is based on the page size of the tablespace
used by the index: 

Max Key Length  Page size
--------------  ---------
   1K              4K
   2K              8K
   4K              16K
   8K              32K

The statement cannot be processed.

User response: 

To modify the foreign key definition, eliminate one or more key columns
and conform to the 64 column limit and the key length limit.

 sqlcode: -631

 sqlstate: 54008
