

SQL2947N  Ingest job with identifier "<job-id>" not found.

Explanation: 

The command failed because the ingest job with the specified identifier
was not found.

User response: 

*  If you are issuing the INGEST GET STATS command, specify the
   identifier of an ingest job that is still running. To display these
   identifiers, issue the INGEST LIST command.
*  If you are issuing the INGEST command with the RESTART parameter,
   specify the identifier of an ingest job whose restart data is still
   in the ingest restart table. To display these identifiers, issue the
   following query: 
   SELECT jobid FROM systools.ingestrestart
