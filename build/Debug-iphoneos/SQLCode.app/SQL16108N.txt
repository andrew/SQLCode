

SQL16108N  XML schema contains an invalid combination of facets
      "<facet1>" and "<facet2>". One reason is "<reason-code>".

Explanation: 

While parsing an XML document the parser found specified facets
identified by "<facet1>" and "<facet2>" that are either mutually
exclusive or have conflicting or invalid values. The "<reason-code>"
indicates one of the possible reasons as described here.



1        The length facet and maxLength facet are both specified and are
         mutually exclusive.

2        The length facet and minLength facet are both specified and are
         mutually exclusive.

3        The maxInclusive facet and maxExclusive facet are both
         specified and are mutually exclusive.

4        The minInclusive facet and minExclusive facet are both
         specified and are mutually exclusive.

5        The value of the maxLength facet must be greater than the value
         of the minLength facet.

6        The value of the maxExclusive facet must be greater than the
         value of the minExclusive facet.

7        The value of the maxExclusive facet must be greater than the
         value of the minInclusive facet.

8        The value of the maxInclusive facet must be greater than the
         value of the minExclusive facet.

9        The value of the maxInclusive facet must be greater than the
         value of the minInclusive facet.

10       The value of the totalDigits facet must be greater than the
         value of the fractionDigits facet.

11       The value of the maxInclusive facet for a derived type is
         greater than or equal to the value of maxExclusive facet for
         the base type.

12       The value of the maxInclusive facet for a derived type is
         greater than the value of the maxInclusive facet for the base
         type.

13       The value of the maxInclusive facet for a derived type is less
         than the value of the minInclusive facet for the base type.

14       The value of the maxInclusive facet for a derived type is less
         than or equal to the value of the minExclusive facet for the
         base type.

15       The value of the maxExclusive facet for a derived type is
         greater than the value of the maxExclusive facet for the base
         type.

16       The value of the maxExclusive facet for a derived type is
         greater than the value of the maxInclusive facet for the base
         type.

17       The value of the maxExclusive facet for a derived type is less
         than or equal to the value of the minInclusive facet for the
         base type.

18       The value of the maxExclusive facet for a derived type is less
         than or equal to the value of the minExclusive facet for the
         base type.

19       The value of the minExclusive facet for a derived type is
         greater than or equal to the value of the maxExclusive facet
         for the base type.

20       The value of the minExclusive facet for a derived type is
         greater than the value of the maxInclusive facet for the base
         type.

21       The value of the minExclusive facet for a derived type is less
         than or equal to the value of the minInclusive facet for the
         base type.

22       The value of the minExclusive facet for a derived type is less
         than or equal to the value of the minExclusive facet for the
         base type.

23       The value of the minInclusive facet for a derived type is
         greater than or equal to the value of the maxExclusive facet
         for the base type.

24       The value of the minInclusive facet for a derived type is
         greater than the value of the maxInclusive facet for the base
         type.

25       The value of the minInclusive facet for a derived type is less
         than the value of the minInclusive facet for the base type.

26       The value of the minInclusive facet for a derived type is less
         than or equal to the value of the minExclusive facet for the
         base type.

27       The value of the maxInclusive facet for a derived type is not
         equal to the value of the maxInclusive facet for the base type.
         The maxInclusive facet for the base type was defined with the
         fixed attribute set to "true".

28       The value of the maxExclusive facet for a derived type is not
         equal to the value of the maxExclusive facet for the base type.
         The maxExclusive facet for the base type was defined with the
         fixed attribute set to "true".

29       The value of the minInclusive facet for a derived type is not
         equal to the value of the minInclusive facet for the base type.
         The minInclusive facet for the base type was defined with the
         fixed attribute set to "true".

30       The value of the minExclusive facet for a derived type is not
         equal to the value of the minExclusive facet for the base type.
         The minExclusive facet for the base type was defined with the
         fixed attribute set to "true".

31       The value of the minOccurs attribute exceeds the value of the
         maxOccurs attribute.

32       The totalDigits facet value of a derived type must be less than
         or equal to the totalDigits facet value for the corresponding
         base type.

33       The fractionDigits facet value of a derived type must be less
         than or equal to the totalDigits facet value for the
         corresponding base type.

34       The fractionDigits facet value of a derived type must be less
         than or equal to the fractionDigits facet value for the
         corresponding base type.

35       The totalDigits facet value of a derived type must be equal to
         the value of the totalDigits facet with the fixed attribute set
         to "true" for the corresponding base type.

36       The fractionDigits facet value of a derived type must be equal
         to the value of the fractionDigits facet with the fixed
         attribute set to "true" for the corresponding base type.

37       The maxLength facet value of a derived type must be equal to
         the value of the maxLength facet with the fixed attribute set
         to "true" for the corresponding base type.

38       The minLength facet value of a derived type must be equal to
         the value of the minLength facet with the fixed attribute set
         to "true" for the corresponding base type.

39       The length facet value of a derived type must be equal to the
         value of the length facet with the fixed attribute set to
         "true" for the corresponding base type.

40       The whiteSpace facet value of a derived type must be equal to
         the value of the whiteSpace facet with the fixed attribute set
         to "true" for the corresponding base type.

41       A fractionDigits facet value exceeded the totalDigits facet
         value. The fractionDigits facet value represents the number of
         digits to the right of the decimal and cannot exceed that
         totalDigits facet value.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16108

sqlstate: 2200M
