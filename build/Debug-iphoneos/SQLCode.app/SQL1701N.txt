

SQL1701N  The database cannot be upgraded because the database
      terminated abnormally.

Explanation: 

The database terminated abnormally (for example, due to a power failure)
before attempting to upgrade the database. You must restart the database
before you can upgrade the database successfully.

The command cannot be processed.

User response: 

You must issue the RESTART DATABASE command using the DB2 copy where the
database resided prior to attempting to upgrade the database. Then,
re-issue the UPGRADE DATABASE command from the DB2 copy version to which
you want to upgrade.
