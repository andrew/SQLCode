

SQL0857N  Conflicting options have been specified ("<option1>",
      "<option2>").

Explanation: 

Conflicting options have been specified. Either "<option1>" and
"<option2>" must be specified together, or the two options must not be
specified together.

If IMPLICITLY HIDDEN is specified when defining a column, the column
must also be defined as a ROW CHANGE TIMESTAMP column.

The statement cannot be executed.

User response: 

Depending on the options, either specify the options together or do not
specify the options together.

sqlcode: -857

sqlstate: 42867
