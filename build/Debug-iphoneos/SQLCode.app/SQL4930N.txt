

SQL4930N  The bind, rebind, alter, or precompile option or option value
      "<option-name>" is invalid.

Explanation: 

Either "<option-name>" is an invalid bind, rebind, alter, or precompile
option or the value specified for this option is invalid. The bind,
rebind, alter, or precompile cannot continue.

User response: 

Correct the bind, rebind, alter, or precompile option or option value
and retry the command or statement.

sqlcode: -4930

sqlstate: 56095
