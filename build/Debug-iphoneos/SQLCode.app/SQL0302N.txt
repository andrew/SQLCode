

SQL0302N  The value of a host variable in the EXECUTE or OPEN statement
      is out of range for its corresponding use.

Explanation: 

The value of an input host variable was found to be out of range for its
use in the SELECT, VALUES, or prepared statement.

One of the following occurred:

*  The corresponding host variable or parameter marker used in the SQL
   statement is defined as string, but the input host variable contains
   a string that is too long.
*  The corresponding host variable or parameter marker used in the SQL
   statement is defined as numeric, but the input host variable contains
   a numeric value that is out of range.
*  The terminating NUL character is missing from the C language
   NUL-terminated character string host variable.
*  Federated system users: in a pass-through session, a data
   source-specific restriction might have been violated.

This error occurs as a result of specifying either an incorrect host
variable or an incorrect SQLLEN value in an SQLDA on an EXECUTE or OPEN
statement.

The statement cannot be processed.

User response: 

Ensure that the input host variable value is the correct type and
length.

If the input host variables supply values to parameter markers, match
values with the implied data type and length of the parameter marker.

Federated system users: for a pass-through session, determine what data
source is causing the error.

Examine the SQL dialect for that data source to determine which specific
restriction has been violated, and adjust the failing statement as
needed.

sqlcode: -302

sqlstate: 22001, 22003


   Related information:
   Troubleshooting data source connection errors
