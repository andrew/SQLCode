

SQL29001N  This database client level is not compatible with the level
      of DB2 Query Patroller server you are running on.

Explanation: 

The client and server code are not compatible.
