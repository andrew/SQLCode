

SQL3256N  The Load utility encountered an error while processing data
      for the specified file type.

Explanation: 

The Load utility has encountered an error while processing data for the
specified file type. The format of the data is invalid. The utility has
stopped processing.

User response: 

Ensure the data is in the correct file type format.
