

SQL20287W  The environment of the specified cached statement is
      different than the current environment. The current environment
      will be used to reoptimize the specified SQL statement.

Explanation: 

The Explain facility has been invoked to explain a statement that has
been previously reoptimized with REOPT ONCE, but the current environment
is different than the environment in which the cached statement was
originally compiled. The current environment will be used to reoptimize
the specified statement.

The statement will be processed.

User response: 

In order to ensure that the plan matches the plan in the cache, reissue
the EXPLAIN in an environment that matches the one in which the original
statement was reoptimized and cached.

sqlcode: -20287

sqlstate: 01671
