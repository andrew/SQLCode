

SQL20189W  The buffer pool operation (CREATE/ALTER) will not take effect
      until the next database startup due to insufficient memory.

Explanation: 

The CREATE or ALTER BUFFERPOOL statement was issued and completed
successfully, however due to insufficient memory the create/alter was
done DEFERRED. The changes will take effect on the next database
startup.

User response: 

If you do not want to wait until the next startup to activate or resize
the buffer pool, you can free memory resources and try again (with the
same or different size). Memory resources that can be reduced include
other buffer pools, the database heap, the catalog cache, the package
cache, and the utility heap. These can be reduced using the ALTER/DROP
BUFFERPOOL or UPDATE DATABASE CONFIGURATION command depending on the
resource. In the future, to reserve extra memory for the dynamic
allocation of buffer pool memory, you can increase the DATABASE_MEMORY
database configuration parameter.

If you do not try again: 
1. If it is an ALTER BUFFERPOOL that failed, you will continue to run
   with the current runtime size of the buffer pool. You can use the
   database monitor to see the current runtime size of the buffer pool.
2. If it is a CREATE BUFFERPOOL that failed, any table spaces that are
   created in the buffer pool will temporarily (until next startup) be
   put in a hidden buffer pool with the matching pagesize. Since the
   hidden buffer pools are small, this can result in lower than desired
   performance.

To try again: 
1. For ALTER BUFFERPOOL, resubmit the command.
2. For CREATE BUFFERPOOL drop the buffer pool and then resubmit the
   command.

sqlcode: +20189

sqlstate: 01657
