

SQL1184N  One or more EXPLAIN tables were not created using the current
      version of DB2.

Explanation: 

EXPLAIN will not be able to insert into these tables until they are
migrated using DB2EXMIG, or dropped and created with the EXPLAIN.DDL CLP
script for the current version of DB2.

User response: 

Either migrate the tables using DB2EXMIG, or drop and re-create them
with the EXPLAIN.DDL CLP script for the current version of DB2. Re-issue
the command.

 sqlcode: -1184

 sqlstate: 55002
