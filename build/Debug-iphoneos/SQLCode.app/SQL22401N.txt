

SQL22401N  The application with agent ID "<agent-ID>" does not exist.

Explanation: 

The application with agent ID "<agent-ID>" does not currently exist. To
see all active database applications, use LIST APPLICATIONS command.

User response: 

Specify an agent ID for an active application, then try the request
again.

sqlcode: -22401

sqlstate: 5U002
