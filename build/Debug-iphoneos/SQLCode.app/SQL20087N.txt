

SQL20087N  DEFAULT or NULL cannot be used in an attribute assignment.

Explanation: 

The UPDATE statement is using an attribute assignment to set the value
of an attribute in a structured type column. This form of assignment
statement does not allow the use of the keyword DEFAULT or the keyword
NULL as the right hand side of the assignment.

The statement cannot be processed.

User response: 

Specify an expression for the right hand side of the attribute
assignment or change the assignment so it is not using the attribute
assignment syntax.

 sqlcode: -20087

 sqlstate: 428B9
