

SQL10003C  There are not enough system resources to process the request.
      The request cannot be processed.

Explanation: 

The database manager could not process the request due to insufficient
system resources. The resources that can cause this error include:

*  The amount of memory in the system.
*  The number of message queue identifiers available in the system.
*  The number of connections at the cluster caching facility (CF).

User response: 

Stop the application. Possible solutions include:

*  Remove background processes.
*  Terminate other applications using the resources listed in this
   message's Explanation.
*  If you are using Remote Data Services, increase the Remote Data
   Services heap size (rsheapsz) in the server and client configuration
   because at least one block is used per application. 

   NOTE: This is applicable only for releases of DB2 prior to Version 2.

*  Decrease the values of the configuration parameters that define
   allocation of memory, including ASLHEAPSZ if UDFs are involved in the
   failing statement.
*  Avoid accessing large files or use non-buffered I/O. To use
   non-buffered I/O, set the DB2 registry variable DB2NTNOCACHE to YES.
*  Deactivate another connected database before activating this
   database.

sqlcode: -10003

sqlstate: 57011
