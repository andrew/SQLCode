

SQL1413N  Invalid specification of a row change timestamp column for
      table "<table-name>".

Explanation: 

The specification of a row change timestamp column is invalid. A row
change timestamp column cannot be:

*  A column of a foreign key.
*  A column of a functional dependency DEPENDS ON clause.
*  A column of a database partitioning key.
*  Defined for a temporary table.

The statement cannot be executed.

User response: 

Correct the syntax and resubmit the statement.

sqlcode: -1413

 sqlstate: 429BV
