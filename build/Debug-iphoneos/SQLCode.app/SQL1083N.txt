

SQL1083N  The database cannot be created because the database
      description block cannot be processed, reason code =
      "<reason-code>".

Explanation: 

The application issued a CREATE DATABASE command, but the database
descriptor block (DBDB) could not be processed for one of the following
reason codes: 

1        The address of the DBDB is not valid.

2        The value of the SQLDBDID field of the DBDB is not valid. It
         should be set to the value SQLDBDB1.

4        The value of the SQLDBCSS field of the DBDB is not valid. When
         using the CREATE DATABASE CLP command, the value specified in
         the COLLATE USING option is invalid.

5        The collation value specified in SQLDBUDC is invalid for
         collation type SQL_CS_UNICODE. When using the CREATE DATABASE
         CLP command, the value specified in the COLLATE USING option is
         not valid for UTF-8 code set.

6        A database cannot be created with an explicit collation type,
         and implicit code set. You must either specify the code set
         desired, or leave collation as SQL_CS_SYSTEM. When using the
         CREATE DATABASE CLP command, you must either specify the code
         set desired with an explicit collation type, or use implicit
         collation without COLLATE USING option.

The command cannot be processed.

User response: 

Correct the error and resubmit the command.
