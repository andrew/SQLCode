

SQL3521N  The input source file "<sequence-num>" was not provided.

Explanation: 

A load was invoked with multiple input files but not all the files were
provided. For the DB2CS filetype, all the input source files that were
created originally must be provided. For the IXF filetype, all the input
source files must be provided in the correct order.

The utility terminates.

User response: 

Restart the utility providing all the input source files and setting the
RESTARTCOUNT appropriately for the data that was already loaded.
