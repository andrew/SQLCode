

SQL5001N  "<authorization-ID>" does not have the authority to change the
      database manager configuration file.

Explanation: 

The user attempted to update or reset the database manager configuration
file without having SYSADM authority.

The requested change is not made.

User response: 

Do not attempt to change the database manager configuration file without
appropriate authorization. Contact a user with SYSADM authority if a
change is required.
