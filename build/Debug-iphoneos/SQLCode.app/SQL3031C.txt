

SQL3031C  An I/O error occurred while reading from the input file.

Explanation: 

A system I/O error occurred while reading from the input file. This
error can refer to a problem on either the client or the server.

The command cannot be processed.

User response: 

Verify that the input file is readable.
