

SQL16082N  A target node in one or more "<expression-type>" expressions
      is not a node that was newly created in the copy clause of the
      transform expression. Error QName=err:XUDY0014.

Explanation: 

A target node of a basic updating expression must be a node that was
newly created by the copy clause of the transform expression. One or
more "<expression-type>" expressions has a target node that is not newly
created.

The XQuery expression cannot be processed.

User response: 

Ensure that the target nodes for every "<expression-type>" expression
and any other basic updating expressions are newly created by the copy
clause of the transform expression.

sqlcode: -16082

sqlstate: 10703
