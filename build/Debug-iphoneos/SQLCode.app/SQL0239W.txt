

SQL0239W  SQLDA has only provided "<integer1>" SQLVAR entries. Since at
      least one of the columns being described is a distinct type or
      reference type, "<integer2>" SQLVAR entries are required for
      "<integer3>" columns. No SQLVAR entries have been set.

Explanation: 

If any of the columns in the result set is a distinct type or reference
type, then space should be provided for twice as many SQLVAR entries as
the number of columns in the result set.

The database manager has not set any SQLVAR entries (and the SQLDOUBLED
flag has been set off (i.e. to the space character).

User response: 

If the distinct type or reference type information is needed, the value
of the SQLN field in the SQLDA should be increased to the value
indicated in the message (after making sure the SQLDA is large enough to
support that amount) and the statement should be resubmitted. If there
is no need for the additional information about the distinct type(s) or
reference type(s) in the result set, then it is possible to resubmit the
statement only providing enough SQLVAR entries to accommodate the number
of columns in the result set.

 sqlcode: +239

 sqlstate: 01005
