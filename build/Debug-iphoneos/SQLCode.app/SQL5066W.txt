

SQL5066W  The database configuration parameter value for token
      "<token-name>" has been truncated.

Explanation: 

The database configuration parameter value is larger than the specified
token can contain.

A new token now represents this database configuration parameter value
and should be used if the value is larger than can be contained by the
old token.

User response: 

Use the new token for this database configuration parameter.
