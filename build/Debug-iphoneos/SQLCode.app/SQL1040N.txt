

SQL1040N  The statement failed because the maximum number of
      applications are already connected to the database.

Explanation: 

Each application that attaches to a database causes some private memory
to be allocated. To manage memory use, you can configure the maximum
number of concurrent applications that can be connected (both local and
remote) to a database by setting the maxappls configuration parameter.

This message is returned when the number of applications connected to
the database is equal to the maximum value defined in the configuration
file for the database.

User response: 

Respond to this message in one of the following ways:

*  Wait for other applications to disconnect from the database, and then
   resubmit the statement.
*  If more applications are required to run concurrently, increase the
   value of the maxappls database configuration parameter or set
   maxappls to "AUTOMATIC".

sqlcode: -1040

sqlstate: 57030


   Related information:
   maxappls - Maximum number of active applications configuration
   parameter
