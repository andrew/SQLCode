

SQL0171N  The statement was not processed because the data type, length
      or value of the argument for the parameter in position "<n>" of
      routine "<routine-name>" is incorrect. Parameter name:
      "<parameter-name>".

Explanation: 

This message can be returned in two scenarios:

*  The data type, length, or value of the argument for a parameter is
   incorrect for the routine.
*  A conflicting or unsupported combination of parameters has been
   specified.

If the position is not applicable or unknown, a value of 0 is returned
for "<n>". If the parameter name is not applicable or unknown, the empty
string is returned for "<parameter-name>".

User response: 

Determine what is incorrect about the parameter given in the token
"<parameter-name>" by reviewing the rules for the parameters of the
routine:

*  Required and supported parameters
*  Valid range of arguments for parameters
*  Valid combinations of parameters

Invoke the routine again, specifying valid parameters.

sqlcode: -171

sqlstate: 5UA0J, 5UA05, 5UA06, 5UA07, 5UA08, 5UA09, 10608, 22546, 42815


   Related information:
   Routines
