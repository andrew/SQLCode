

SQL3953N  This satellite has been disabled at the satellite control
      server.

Explanation: 

The satellite ID has been disabled at the satellite control server.

User response: 

Contact the help desk or your system administrator.
