

SQL1125N  The adapter number "<number>" is not valid.

Explanation: 

The adapter number specified in the NETBIOS protocol structure for the
Catalog command is not valid.

The command cannot be processed.

User response: 

Verify that the adapter number is valid and resubmit the command.
