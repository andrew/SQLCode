

SQL0434W  An unsupported value for clause "<clause>" has been replaced
      by the value "<value>".

Explanation: 

The value that was specified for clause "<clause>" is not supported and
has been replaced with the identified supported value "<value>".

User response: 

No change is required if the selected value is acceptable. Otherwise,
specify a value that is valid for "<clause>".

 sqlcode: +434

 sqlstate: 01608
