

SQL0501N  The cursor specified in a FETCH statement or CLOSE statement
      is not open or a cursor variable in a cursor scalar function
      reference is not open.

Explanation: 

The program attempted to do one of:

*  FETCH using a cursor at a time when the specified cursor was not
   open.
*  CLOSE a cursor at a time when the specified cursor was not open.
*  Reference a cursor variable in an OPEN statement and the cursor
   variable is not open.
*  Reference a cursor scalar function, such as CURSOR_ROWCOUNT function,
   and the cursor variable is not open.

The statement cannot be processed.

User response: 

Check for a previous message (SQLCODE) that may have closed the cursor.
Note that after the cursor is closed, any fetches or close cursor
statements receive SQLCODE -501.

If no previous SQLCODEs have been issued, correct the application
program to ensure that the cursor is open when the FETCH or CLOSE
statement is executed.

If a cursor variable is referenced in a cursor scalar function, verify
that that the cursor is not null, is defined, and is open, else replace
the cursor variable with one that is in that state.

sqlcode: -501

sqlstate: 24501
