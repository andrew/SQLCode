

SQL20404N  The security label object "<policy-name.object-name>" cannot
      be dropped because it is currently in use. Reason code
      "<reason-code>".

Explanation: 

The security label object "<object-name>" could not be dropped. The
reason it could not be dropped is specified by the reason code
"<reason-code>":

1. It is granted to one or more users, groups, or roles.
2. It is being used to protect one or more columns.

User response: 

The user response corresponding to the reason code is:

1. Revoke this label from all users, groups, or roles who have been
   granted this security label for this security policy. The following
   query can be used to find all the users who has been granted with
   this label. 
   SELECT GRANTEE FROM SYSCAT.SECURITYLABELACCESS 
     WHERE SECLABELID = (SELECT SECLABELID FROM 
     SYSCAT.SECURITYLABELS  
     WHERE SECLABELNAME = '<object-name>' AND   
       SECPOLICYID = (SELECT SECPOLICYID FROM 
       SYSCAT.SECURITYPOLICIES 
     WHERE SECPOLICYNAME = '<policy-name>' ) )

2. For all the tables that use this security label to protect a column,
   either alter the table to drop this security label or drop the table.
   The following query can be used to find all protected tables and all
   the columns that are protected with this label. 
   SELECT TABNAME, COLNAME FROM SYSCAT.COLUMNS
     WHERE SECLABELNAME = '<object-name>' AND
       TABNAME = (SELECT TABNAME FROM 
       SYSCAT.TABLES
     WHERE SECPOLICYID = (SELECT SECPOLICYID FROM 
     SYSCAT.SECURITYPOLICIES
     WHERE SECPOLICYNAME = '<policy-name>' ) )

sqlcode: -20404

sqlstate: 42893
