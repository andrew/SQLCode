

SQL3173N  The inserted data for column "<name>" will always contain
      fewer characters than the column width.

Explanation: 

The database column width is larger than the maximum worksheet format
(WSF) label record.

Continue processing.

User response: 

No action is required.
