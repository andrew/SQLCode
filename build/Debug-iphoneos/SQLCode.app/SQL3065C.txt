

SQL3065C  The value for the application codepage cannot be determined.

Explanation: 

The system encountered an error while determining the code page of the
application.

The utility stops processing. No data is loaded or unloaded.

User response: 

Contact your technical service representative.
