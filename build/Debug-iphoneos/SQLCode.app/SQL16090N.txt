

SQL16090N  The target of a rename expression is a processing instruction
      node and the namespace prefix of the QName "<qname-string>" is not
      empty. Error QName=err:XUDY0025.

Explanation: 

A transform expression includes a rename expression that has a target
node that is a processing instruction node. The new name expression in
the rename expression results in the QName "<qname-string>" that has a
prefix that is not empty. The name of a processing instruction must not
include a prefix.

User response: 

Change the new name expression of the rename expression so that the
resulting QName has an empty prefix.

sqlcode: -16090

 sqlstate: 10709
