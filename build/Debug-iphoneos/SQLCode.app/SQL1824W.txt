

SQL1824W  Some base tables in the operands of this UNION ALL may be the
      same table.

Explanation: 

A nickname can refer to a remote base table, a remote view, a remote
alias/synonym, or a remote nickname. If two operands of a UNION ALL view
refer to different nicknames, they may potentially be pointing to the
same table (if not both of them are known to be remote base tables).
This message is issued to warn the user that potentially one remote base
table can get updated/deleted twice via updates/deletes through two
operands.

User response: 

Verify if all operands point to different remote tables. If two operands
point to the same remote base table, consider issuing a rollback to
reverse the update/delete operation.

 sqlcode: +1824

 sqlstate: 01620
