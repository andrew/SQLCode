

SQL16006N  XML schemas cannot be imported. Error QName=err:XQST0009.

Explanation: 

DB2 XQuery does not support the Schema Import Feature.

The schema cannot be imported.

User response: 

Remove the schema import statement from the query prolog.

 sqlcode: -16006

 sqlstate: 10502
