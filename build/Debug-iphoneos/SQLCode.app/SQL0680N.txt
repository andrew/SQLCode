

SQL0680N  The statement failed because too many columns were specified
      for a table, view, table function, or nickname, or because too
      many fields were specified for a user-defined row data type.

Explanation: 

The maximum number of columns that you can define for a table depends on
the type of object.

The maximum number of columns that you can define for a view, table
function, or nickname and the maximum number of fields permitted for a
user-defined row data type are specified in SQL and XML limits.

For tables, the maximum number of columns is affected by the number of
LOB columns specified. The section Row size limit in the CREATE TABLE
statement includes a formula to calculate the row size of a table.

For relational database sources in federated environments, nicknames
might also be limited by the maximum number of columns for tables or
views on their data source when these limits are less than the limits
specified in SQL and XML limits.

This message is returned for the following scenarios:

*  An attempt is made to create an object such as a table, a view, a
   table function, or a nickname for a table with more columns than the
   maximum number of permitted columns.
*  An attempt is made to create a user-defined row data type with more
   fields than than the maximum number of permitted fields.

User response: 

Ensure that the number of columns or fields does not exceed the limit.

To resolve the issue reported by this message, perform the following
types of troubleshooting actions:

*  Creating a table 
   *  Review database manager SQL and XML limits. Particularly review
      database manager SQL and XML limits that are specific to page
      size.
   *  Redesign the table to have fewer columns, use a data type
      different than LOBs to reduce the row size, or indicate a table
      space with a page size that allows a higher number of columns and
      issue the CREATE TABLE statement again.

*  Creating a view or table function on a table 
   *  Review the database manager limits, redesign the table to meet the
      SQL limits, and then create the view or table function again.

*  Creating a nickname on a table or view in a federated data source 
   *  Review the database manager limits and the data source SQL limits
      for tables or views, redesign the nickname to meet these limits,
      and then create it again.

*  Creating a row data type 
   *  Review SQL and XML limits, redesign the data type to have fewer
      fields in order to satisfy the SQL limits, and then create the row
      data type again.

sqlcode: -680

sqlstate: 54011


   Related information:
   CREATE TYPE statement
   CREATE NICKNAME statement
   CREATE VIEW statement
   CREATE TABLE statement
   CREATE FUNCTION statement
   SQL and XML limits
