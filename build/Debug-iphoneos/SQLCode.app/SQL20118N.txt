

SQL20118N  Structured type "<type-name>" has more than the maximum
      number of allowable attributes. The maximum is "<max-value>".

Explanation: 

The maximum number of attributes, including inherited attributes,
allowed for each structured type has been exceeded with the definition
of structured type "<type-name>". The maximum number of attributes,
including inherited attributes, is "<max-value>".

The statement cannot be processed.

User response: 

Ensure that the number of attributes for the structured type does not
exceed the limit.

 sqlcode: -20118

 sqlstate: 54050
