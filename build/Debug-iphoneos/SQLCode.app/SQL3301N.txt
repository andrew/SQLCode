

SQL3301N  A BOF record was found in the middle of the input file.

Explanation: 

A beginning-of-file (BOF) record must be the first record of a worksheet
format (WSF) file. It cannot occur at any other location in the file.
Some damage has occurred to the WSF file or it was generated
incorrectly, possibly with a level of the Lotus product not supported by
the database manager.

The IMPORT utility stops processing.

User response: 

Regenerate the WSF file with a supported level of the Lotus product.
