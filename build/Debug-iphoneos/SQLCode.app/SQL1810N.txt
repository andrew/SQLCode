

SQL1810N  The insert or update statement was not executed or the cursor
      could not be opened because the statement contains some elements
      that must be evaluated locally and some elements that must be
      evaluated remotely.

Explanation: 

In a federated environment, many factors affect whether parts of an SQL
statement are evaluated locally or remotely, including the following
examples:

*  Sometimes parts of a SQL statement are executed locally instead of
   remotely because the query optimizer determines that it is more
   efficient to do so.
*  Other times, parts of a SQL statement are executed locally because
   they cannot be evaluated at the remote data source. For example: 
   *  A statement would have to be evaluated locally if an expression in
      the statement contains a function or syntax that is not supported
      by the remote data source.
   *  An INSERT statement with a VALUES clause and an UPDATE statement
      with a SET clause must both be evaluated locally.

This message is returned in the following cases:

*  An attempt is made to execute a positioned update on a result set of
   a query that references tables at the federated data source, but an
   expression in the query can only be evaluated locally.
*  An attempt is made to execute a statement that contains some elements
   that can only be evaluated locally and some elements that can only be
   evaluated remotely.

User response: 

Respond to this error in one of the following ways:

*  Alter the statement to use neither the INSERT statement with the
   VALUES clause nor the UPDATE statement with the SET clause, and then
   execute the statement again.
*  If this message is returned when an attempt is made to open a cursor
   on a result set and update, or delete operations are not required,
   reopen the cursor as read-only.
*  Cause the federated database to send the statement to the federated
   data source without evaluating the statement locally by alter the
   statement so that the statement contains no elements that are not
   supported by the data source.


   Related information:
   Why is a positioned UPDATE statement not completely evaluated
   remotely?
   Understanding access plan evaluation decisions
   Analyzing where a query is evaluated
   The query optimizer
   Why is a remote INSERT with VALUES clause statement not completely
   evaluated remotely?
   Why isn't the SET operator evaluated remotely?
