

SQL1209W  The partner_lu name "<name>" specified in the CATALOG NODE
      function does not exist. One was created.

Explanation: 

The logical partner unit name specified in the CATALOG NODE function
does not exist in the Communications Manager configuration file located
in the CMLIB directory on the default drive.

A logical unit profile of the specified name was created.

User response: 

No action is required.
