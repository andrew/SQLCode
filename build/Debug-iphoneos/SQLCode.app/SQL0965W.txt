

SQL0965W  There is no message text corresponding to SQL warning
      "<SQLCODE>" in the message file on this workstation. The warning
      was returned from module "<name>" with original tokens
      "<token-list>".

Explanation: 

Database server returned code "<SQLCODE>" to your application. The
warning code does not correspond to a message in the DB2 message file on
this workstation.

User response: 

Refer to your database server documentation for more information about
the specified "<SQLCODE>".
