

SQL2310N  The utility could not generate statistics. Error "<sqlcode>"
      was returned.

Explanation: 

An error occurred while the utility was gathering statistics.

The utility stops processing.

User response: 

Look at the message error number for more information. Make changes and
resubmit the command.
