

SQL1645N  The database manager failed to connect to or send data to an
      internet socket on a remote computer because the connection was
      terminated by the remote computer.

Explanation: 

To interact with a remote database across a network, the DB2 database
manager must use a communication protocol, such as TCP/IP, and use
operating system interfaces, such as an internet socket, to connect to
database-related operating system services on the computer on which the
remote database is located.

There are multiple scenarios that can lead to this error being returned,
including the following scenarios:

*  A database agent could not be started at the remote computer because
   of a memory allocation failure.
*  A database agent was forced off of the remote computer.
*  A database agent on the remote computer was terminated.
*  The connection was closed by the remote gateway or server at the
   TCP/IP level.

User response: 

Systematically investigate and resolve the possible causes:

Failure to start a new database agent
         
         1. Investigate diagnostic logs at the remote computer to
            determine whether any memory limits have been exceeded and
            memory allocation failures happened as a result.
         2. If any memory limits have been reached at the remote
            computer, or if there were memory allocation failures at the
            remote computer, work with a database or system
            administrator to resolve the cause of the memory allocation
            problems, and then retry the unit of work.


Database agent forced off
         
         1. Investigate whether any events might have forced a database
            agent off of the remote computer, such as an administrator
            forcing all users and agents off the remote computer to
            perform maintenance.
         2. If database agents have been forced off the remote computer,
            work with you database or system administrator to bring the
            database server back online and ready to process requests,
            and then retry the unit of work.


Database agent terminated
         
         1. Investigate whether any failures on the remote computer
            might have terminated a database agent. For example, the
            termination of a key database manager process might cause
            the termination of a database agent.
         2. If failures on the remote computer have terminated a key
            database manager process, work with you database or system
            administrator to bring the database server back online and
            ready to process requests, and then retry the unit of work.


Connection closed by the remote gateway or server
         

         Resolve any problems external to the DB2 database product might
         have caused a TCP/IP connection to be closed on the remote
         gateway or server. Some examples of problems that could cause
         the connection to be closed include the following problems:

          
         *  Firewall software errors or failure
         *  Power failure
         *  Network failure

If you have eliminated the possible causes listed here, collect DB2 and
system diagnostic information using the db2support command and then
contact IBM software support.

sqlcode: -1645

sqlstate: 08001


   Related information:
   Database agent management
   Database agents
   sqlefrce API - Force users and applications off the system
   FORCE APPLICATION command
   Protocol-specific error codes for the SQL30081N message
