

SQL3117W  The field value in row "<row-number>" and column
      "<column-number>" cannot be converted to a SMALLINT value. A null
      was loaded.

Explanation: 

The value in the specified field cannot be converted to a SMALLINT
value. There may be a data type mismatch. The value may be larger than a
2-byte integer.

For delimited ASCII (DEL) files, the value of the column number
specifies the field within the row that contains the value in question.
For ASCII files, the value of the column number specifies the byte
location within the row where the value in question begins.

A null value is loaded.

User response: 

Examine the input value. If necessary, correct the input file and
resubmit the command or edit the data in the table.
