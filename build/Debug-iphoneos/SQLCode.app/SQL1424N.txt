

SQL1424N  Too many references to transition variables and transition
      table columns or the row length for these references is too long.
      Reason code="<rc>".

Explanation: 

The trigger includes a REFERENCING clause that identifies one or more
transition tables and transition variables. The triggered action of the
trigger contains references to transition table columns or transition
variables with one of the following conditions identified by the reason
code: 

1        references total more than the limit of the number of columns
         in a table

2        sum of the lengths of the references exceeds the maximum length
         of a row in a table.

User response: 

Reduce the number of references to transition variables and transition
table columns in the trigger action of the trigger so that the length is
reduced or the total number of such references is less than the maximum
number of columns in a table.

 sqlcode: -1424

 sqlstate: 54040
