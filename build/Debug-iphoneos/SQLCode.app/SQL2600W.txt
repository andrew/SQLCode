

SQL2600W  The input parameter pointer to the authorization block is
      invalid or the block's size is incorrect.

Explanation: 

The pointer to the authorization structure parameter is NULL, the
pointer to the authorization structure points to an area that is smaller
than the length indicated in the structure length field, or the
authorization structure length field is not set to the correct value.

The command cannot be processed.

User response: 

Correct the value of the input parameter and resubmit the command.
