

SQL3407N  The beginning-ending location pair for inserting into column
      "<name>" is not valid for a timestamp.

Explanation: 

The field specification for locating the data for the indicated database
column within the input non-delimited ASCII file is not valid. The
location pair defines a field length that is not valid for an external
representation of a timestamp.

The command cannot be processed.

User response: 

Resubmit the command with a valid set of locations for the columns in
the file.
