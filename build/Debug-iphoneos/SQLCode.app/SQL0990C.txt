

SQL0990C  An index error occurred. Reorganize the table.

Explanation: 

An index has had considerable activity that used all the free space for
indexes.

Federated system users: this situation can also be detected by the data
source.

The statement cannot be processed.

User response: 

Commit your work and retry the command. If the error continues, roll
back your work. If errors still persist, reorganize the table, if
possible.

Federated system users: isolate the problem to the data source failing
the request and follow the index re-creation procedures for that data
source.


   Related information:
   Troubleshooting data source connection errors
