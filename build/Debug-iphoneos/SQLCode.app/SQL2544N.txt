

SQL2544N  The directory where the database is being restored has become
      full.

Explanation: 

While the database was being restored the directory to which it was
being restored had become full. The database being restored is unusable.
The Restore terminates and if the database being restored is a new
database, then it is deleted.

User response: 

Free up sufficient space on the directory for the database, and reissue
the Restore, or if restoring to a new database supply a directory with
sufficient space to contain the database.
