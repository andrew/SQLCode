

SQL3419W  The specified sort option is not supported by the vendor sort.
      The default DB2 sort will be used to continue with the operation.

Explanation: 

The vendor sort library is activated by setting the DB2 registry
variable DB2SORT. The current sort specification requires a feature that
is not supported by this vendor sort library. DB2 will use the default
sort to continue with the operation. Possible features not supported by
vendor sort:

*  Database created using IDENTITY_16BIT collation.
*  Database configuration parameter ALT_COLLATE is set to
   IDENTITY_16BIT.
*  Load target table has XML columns.
*  Load target table has data partitioning and local indexes.
*  Load target table has indexes with RANDOM ordering.

User response: 

No action required.


   Related information:
   REORG INDEXES/TABLE command
