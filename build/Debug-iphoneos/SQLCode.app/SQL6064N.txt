

SQL6064N  SQL error "<sqlcode>" occurred during data redistribution.

Explanation: 

An error occurred during data redistribution.

The utility stops processing.

User response: 

Look at the SQLCODE (message number) in the message for more
information. Make any required changes and try the request again.
