

SQL6052N  Cannot roll forward database "<name>" because it is not in
      roll-forward pending state on node(s) "<node-list>".

Explanation: 

The specified database is not in the roll-forward pending state on the
specified node(s). This may be because the database has not been
restored, or was restored with the WITHOUT ROLLING FORWARD option, or
roll-forward recovery is complete on these nodes.

The database is not rolled forward.

If ",..." is displayed at the end of the node list, see the syslog file
for the complete list of nodes.

User response: 

Do the following: 
1. Ensure that recovery is required on the specified node(s).
2. Restore a backup version of the database on these nodes.
3. Issue the ROLLFORWARD DATABASE command.
