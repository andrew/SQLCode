

SQL0408N  A value is not compatible with the data type of its assignment
      target. Target name is "<name>".

Explanation: 

The data type of the value to be assigned to the column, parameter, SQL
variable, or transition variable by the SQL statement is incompatible
with the declared data type of the assignment target.

The statement cannot be processed.

User response: 

Examine the statement and possibly the target table or view to determine
the target data type. Ensure the variable, expression, or literal value
assigned has the proper data type for the assignment target.

For a user-defined structured type, also consider the parameter of the
TO SQL transform function defined in the transform group for the
statement as an assignment target.

sqlcode: -408

sqlstate: 42821
