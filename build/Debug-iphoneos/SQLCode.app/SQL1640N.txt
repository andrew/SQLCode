

SQL1640N  A usage list cannot be created for the object "<object-name>".

Explanation: 

Usage lists can be created only for regular tables and indexes. For more
information about the types of objects for which usage lists can be
created, refer to the Related topics section.

User response: 

Use the name of a valid table or index object.

sqlcode: -1640

sqlstate: 42809


   Related information:
   Usage lists
