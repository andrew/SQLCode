

SQL16036N  The URI that is specified in a namespace declaration cannot
      be a zero-length string. Error QName=err:XQST0085.

Explanation: 

The URI that is specified in a namespace declaration must be a valid URI
that conforms to the generic URI syntax specified by the World Wide Web
Consortium (W3C). The URI cannot be a zero-length string.

The XQuery expression cannot be processed.

User response: 

Specify a valid URI in the namespace declaration.

 sqlcode: -16036

 sqlstate: 10504
