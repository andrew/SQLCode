

SQL1192W  Too many input sources were specified for the current
      filetype. The maximum number allowed is "<max-input-sources>".

Explanation: 

The filetype specified does not allow more than "<max-input-sources>"
input sources to be specified for a single load.

User response: 

Resubmit the command with a number of input sources that does not exceed
"<max-input-sources>".
