

SQL27948I  The output data file specified at line "<linenum>" of the
      configuration file is ignored.

Explanation: 

The output data file specified at line "<linenum>" of the configuration
file is ignored. The command line option will be used if it is
specified; otherwise, the first specification of output data in the
configuration file will be used.

User response: 

No action is required.
