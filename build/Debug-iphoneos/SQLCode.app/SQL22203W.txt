

SQL22203W  The DB2 Administration Server cannot be unquiesced. Reason
      code "<reason-code>".

Explanation: 

The DB2 Administration Server unquiesce operation failed for one of the
following reasons: 
1. The DB2 Administration Server is not quiesced.
2. There is at least one administration request in progress.

User response: 

Depending on the reason for the failure, attempt one of the following: 
1. No action required.
2. Wait until the DB2 Administration Server has completed processing all
   administration requests, or resubmit the unquiesce request and
   specify the force option. Forcing the unquiesce operation will allow
   normal requests to be handled concurrently with the administration
   requests, which may prevent the administration requests from
   completing successfully.
