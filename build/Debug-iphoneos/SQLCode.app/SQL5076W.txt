

SQL5076W  The update completed successfully. The current value of
      NOTIFYLEVEL will cause some health monitor notifications not to be
      issued to the notification log.

Explanation: 

Health Monitor issues notifications to the notification log and to
specified e-mail and page contacts. The current value of NOTIFYLEVEL is
set too low to have notifications be issued for alarms and warnings.
NOTIFYLEVEL must be set to 2 or higher for alarm notifications and 3 or
higher for warning notifications.

User response: 

Increase the value of the database manager configuration parameter
NOTIFYLEVEL.
