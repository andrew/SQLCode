

SQL0951N  The object "<object-name>" of type "<object-type>" cannot be
      altered because it is currently in use by the same application
      process.

Explanation: 

An ALTER statement, SET INTEGRITY, or TRUNCATE statement statement for
an object cannot be issued when it is either locked or in use.

The statement cannot be processed. The object is not altered.

User response: 

Close any cursors that depend either directly or indirectly on the
object "<object-name>" and resubmit the statement.

 sqlcode: -951

 sqlstate: 55007
