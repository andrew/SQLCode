

SQL3057N  The input file is not a valid PC/IXF file. The type field in
      the first record is not H.

Explanation: 

The type field in the first record is not H. The first record is not a
valid H record. The file may not be a PC/IXF file.

The utility stops processing. No data is loaded.

User response: 

Verify that the input file is correct.
