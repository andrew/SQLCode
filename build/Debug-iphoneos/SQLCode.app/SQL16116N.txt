

SQL16116N  XML document contains a duplicate value "<value>" in a type
      declaration.

Explanation: 

While parsing an XML document the parser encountered a duplicate value
in the type declaration. A type declaration can only specify
'substitution', 'union', 'extension', 'list', or 'restriction' once.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16116

sqlstate: 2200M
