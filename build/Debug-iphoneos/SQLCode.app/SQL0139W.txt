

SQL0139W  A redundant clause appears in the specification for column
      "<column>".

Explanation: 

The clause in the column specification is redundant.

The statement was processed successfully, but the redundant clause was
ignored.

User response: 

Correct the column specification.

 sqlcode: +139

 sqlstate: 01589
