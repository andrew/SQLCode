

SQL0314N  The host variable "<name>" is incorrectly declared.

Explanation: 

The host variable "<name>" is not declared correctly for one of the
following reasons:

*  The type specified is not one that is supported.
*  The length specification is 0, negative, or too large.
*  An initializer is used.
*  An incorrect syntax is specified.
*  A host variable array with cardinality larger than the maximum
   allowed value is specified.

The variable remains undefined.

User response: 

Ensure that you correctly specify only the declarations the database
manager supports.


   Related information:
   Enabling compatibility features for migration
