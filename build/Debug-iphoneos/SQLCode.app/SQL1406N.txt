

SQL1406N  Shared sort memory cannot be allocated for this utility.

Explanation: 

Shared sort memory is not available and is required for this operation.

User response: 

Do any of the following: 
*  Configure the value of the SHEAPTHRES_SHR configuration parameter to
   allow for sorts to occur in shared memory.
*  Enable intra-partition parallelism by setting the INTRA_PARALLEL
   configuration parameter to "YES".
*  Activate the connection concentrator.
