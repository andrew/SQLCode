

SQL3182W  Insert diskette "<number>" into drive "<drive>". The diskette
      that is currently inserted is not the right diskette or the
      continuation diskette is not valid.

Explanation: 

During the load of a PC/IXF file that is contained on more than one
diskette, a request to insert a diskette was sent to the application, a
confirmation that the diskette is in the drive was returned but the
continuation file is not there or is not valid. This action does not
apply to the first diskette.

The utility waits for a response from the application to either continue
processing or stop processing.

User response: 

Have the user verify that the correct diskette is in the drive. If the
correct diskette is in the drive, call the utility again with the
callerac parameter set to stop processing.
