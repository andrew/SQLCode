

SQL1714N  The ROLLFORWARD command failed because it was run on a member
      that did not exist in the source member topology.

Explanation: 

Following a backup and restore, if the target member topology contains
all the member identifiers contained in the source member topology, you
can roll forward through one or more add member events in the
transaction logs. However, the ROLLFORWARD command must be run from a
member that existed in the source member topology at the time of the
backup operation.

User response: 

Rerun the ROLLFORWARD command from a member that existed in the original
source member topology.


   Related information:
   Backup, restore, and rollforward operations within DB2 pureScale
   Feature
