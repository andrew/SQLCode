

SQL22211N  An error occurred setting or retrieving the DB2
      Administration Server configuration parameter "<parameter-token>".
      Reason code "<reason-code>".

Explanation: 

The following error occurred while updating or reading the DB2
Administration Server configuration: 
1. Configuration parameter is unknown.
2. Configuration parameter value is not in the correct range.
3. A system error occurred while updating the DB2 Administration Server
   configuration parameters.

User response: 

Depending on the reason code, verify the following: 
1. The configuration parameter exists.
2. The value specified for the configuration parameter is in the correct
   range. See the description of the configuration parameter in the DB2
   Administration Guide: Performance volume for the range of allowable
   values.
3. Refer to the DB2 Administration Server's First Failure Data Capture
   Log for additional information or contact IBM Support.
