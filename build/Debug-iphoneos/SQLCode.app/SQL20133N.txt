

SQL20133N  Operation "<operation-name>" cannot be performed on external
      routine "<routine-name>". The operation can only be performed on
      SQL routines.

Explanation: 

You attempted to perform operation "<operation-name>" on external
routine "<routine-name>". However, you can only perform that operation
on SQL routines. The operation did not complete successfully.

User response: 

Ensure the name you provide identifies an SQL routine.

 sqlcode: -20133

 sqlstate: 428F7
