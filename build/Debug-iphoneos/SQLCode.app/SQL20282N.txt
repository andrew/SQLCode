

SQL20282N  .NET procedure or user-defined function "<name>", specific
      name "<specific-name>" could not load .NET class "<class>". Reason
      code "<reason-code>".

Explanation: 

The .NET class given by the EXTERNAL NAME clause of a CREATE PROCEDURE
or CREATE FUNCTION statement could not be loaded. The reason codes are: 
1. The assembly of the .NET routine was not found.
2. Class was not found in the assembly specified.
3. A method with types matching those specified in the database catalogs
   could not be found in the class specified.

User response: 


1. Ensure that correct assembly file is given, including any file
   extensions. If the full path is not specified, ensure that only one
   instance of the assembly exists in the system PATH, as the first
   instance of the assembly found in the PATH will be loaded.
2. Ensure that the assembly was specified correctly as described in
   response 1. Ensure that the case sensitive class name was specified
   correctly and that it exists in the specified assembly.
3. Ensure that the class was specified correctly as described in
   response 2. Ensure that the case sensitive method name was specified
   correctly, and that it exists in the specified class as a "public
   static void" method.

sqlcode: -20282

sqlstate: 42724
