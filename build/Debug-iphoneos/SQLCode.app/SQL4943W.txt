

SQL4943W  The number of host variables in the INTO clause is not the
      same as the number of items in the SELECT clause.

Explanation: 

The number of host variables specified in both the INTO clause and the
SELECT clause must be the same.

The function is processed.

User response: 

Correct the application program to specify the same number of host
variables as SELECT list expressions.
