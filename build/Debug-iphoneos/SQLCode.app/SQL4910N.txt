

SQL4910N  Overflow log path "<log-path>" is not valid.

Explanation: 

The overflow log path specified on the ROLLFORWARD command is not valid.
The overflow log path must be a directory in a file system. This
directory must be accessible by the instance owner id.

User response: 

Resubmit the command with a valid overflow log path.
