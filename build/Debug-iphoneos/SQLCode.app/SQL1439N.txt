

SQL1439N  Could not retrieve automated maintenance policy configuration
      information.

Explanation: 

You can use the stored procedures SYSPROC.AUTOMAINT_SET_POLICY or
SYSPROC.AUTOMAINT_SET_POLICYFILE and SYSPROC.AUTOMAINT_GET_POLICY or
SYSPROC.AUTOMAINT_GET_POLICYFILE to configure DB2 server automated
maintenance activities such as automatic backup, automatic
reorganization, and automatic statistics collection.

The stored procedure SYSPROC.AUTOMAINT_GET_POLICY or
SYSPROC.AUTOMAINT_GET_POLICYFILE was unable to collect existing
automated maintenance configuration information because it could not
find any default automated maintenance policies.

No automatic maintenance configuration information was collected.

User response: 

Default automated maintenance policies are created by the DB2 health
monitor. If the health monitor has not yet created the default
maintenance policies, then you can manually create them:

1. Connect to the database for which you wish to create default
   automated maintenance policies
2. Call the system stored procedure called SYSPROCS.SYSINSTALLOBJECTS
   with the following parameters: 
   SYSPROCS.SYSINSTALLOBJECTS( 'POLICY','C','','')

For more information about the SYSINSTALLOBJECTS system stored
procedure, see the topic called "SYSINSTALLOBJECTS procedure" in the DB2
Information Center.

For more information, see the topic called "Collecting automated
maintenance configuration information using SYSPROC.AUTOMAINT_GET_POLICY
or SYSPROC.AUTOMAINT_GET_POLICYFILE" in the DB2 Information Center.

sqlcode: -1439

 sqlstate: 5U0ZZ
