

SQL0724N  The activation of "<object-name>" of type "<object-type>"
      would exceed the maximum level of indirect SQL cascading.

Explanation: 

Cascading of indirect SQL occurs when a trigger activates another
trigger (possibly through referential constraint delete rules) or a
routine, containing SQL, invokes another routine. The depth of this
cascading is limited to 16 for triggers and 64 for routines.

Note that recursive situations where a trigger includes a triggered SQL
statement that directly or indirectly causes the same trigger to be
activated, or where a routine directly or indirectly invokes itself, is
a form of cascading that is very likely to cause this error if there are
no conditions to prevent cascading from exceeding the limit.

The "<object-type>" is one of TRIGGER, FUNCTION, METHOD, or PROCEDURE.

The "<object-name>" specified is one of the objects that would have been
activated at the seventeenth level of cascading.

User response: 

Start with the objects that are activated or invoked by the statement
that received this error. If any of these objects are recursive, ensure
that there is some condition that prevents the object from being
activated or invoked more than the limit allows. If this is not the
cause of the problem, follow the chain of objects that are activated or
invoked to determine the chain that exceeds the cascading limit.

 sqlcode: -724

 sqlstate: 54038
