

SQL1354N  An SQL variable in routine "<routine-name>" is not available
      for reference due to a recent commit or rollback operation.

Explanation: 

During the execution of routine "<routine-name>" an attempt was made to
reference an SQL variable or SQL parameter of data type XML that cannot
be referenced because a recent commit or rollback operation has caused
its value to no longer be available.

Referencing variables or parameters of data type XML in SQL procedures
after a commit or rollback operation occurs without first assigning new
values to these variables is not supported.

User response: 

To prevent the error from occurring, do one or more of the following: 
*  Move the commit or rollback operation to after the SQL statement that
   references SQL variables or SQL parameters of data type XML.
*  Remove references to SQL variables or SQL parameters of data type XML
   that follow commit or rollback operations.
*  Assign values to SQL variables or SQL parameters of data type XML
   that will be referenced in SQL statements that follow a commit or
   rollback operation.
*  Retry the transaction if the rollback was implicit as a result of
   conditions such as deadlocks or system failures.

sqlcode: -1354

sqlstate: 560CE
