

SQL16041N  An implicit or explicit invocation of the fn:boolean function
      in the XQuery expression could not compute the effective boolean
      value of the sequence. Error QName=err:FORG0006.

Explanation: 

This error occurs if the effective boolean value of the sequence operand
of an explicit or implicit invocation of the fn:boolean function cannot
be computed for the sequence operand. The effective boolean value can be
computed only if the sequence operand is one of the following sequences:

*  An empty sequence
*  A sequence where the value of the first item is a node
*  A singleton sequence with a value of type xs:string,
   xdt:untypedAtomic, or a type derived from one of these types
*  A singleton sequence with a value of any numeric type or derived from
   any numeric type

The XQuery expression cannot be processed.

User response: 

Determine the possible expressions within the XQuery expression where an
effective boolean value is calculated either implicitly or explicitly.
An implicit invocation of the fn:boolean function can occur when
processing the following types of expressions:

*  Logical expressions (and, or)
*  An fn:not function invocation
*  The where clause of a FLWOR expression
*  Certain types of predicates, such as a[b]
*  Conditional expressions, such as if
*  Quantified expressions (some, every)

Ensure that the sequence operand of each effective boolean value
calculation would have a valid sequence operand (one described in the
explanation).

 sqlcode: -16041

 sqlstate: 10608
