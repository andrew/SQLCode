

SQL0282N  Table space "<tablespace-name>" cannot be dropped because at
      least one of the tables in it, "<table-name>", has one or more of
      its parts in another table space.

Explanation: 

A table in the specified table space does not contain all of its parts
in that table space. If more than one table spaces are specified, then a
table in one of the specified table spaces does not contain all of its
parts in the list. The base table, indexes, or long data may be in
another table space, so dropping the table space(s) will not completely
drop the table. This would leave the table in an inconsistent state and
therefore the table space(s) cannot be dropped.

User response: 

Ensure that all objects contained in table space "<tablespace-name>"
contain all their parts in this table space before attempting to drop
it, or include those table spaces containing the parts in the list to be
dropped.

This may require dropping the table "<table-name>" before dropping the
table space.

 sqlcode: -282

 sqlstate: 55024
