

SQL20397W  Routine "<routine-name>" execution has completed, but at
      least one error, "<error-code>", was encountered during the
      execution. More information is available.

Explanation: 

Routine "<routine-name>" execution has completed. At least one error was
encountered during the internal execution of the requested function. The
last error encountered was "<error-code>". More detailed information on
the errors encountered is available.

For the ADMIN_CMD routine, its output parameter and result set, if any,
have been populated.

User response: 

Retrieve the output parameter and result set, if any, for more
information on the errors encountered. If message files were generated
during the execution, examine their content and resolve the error
situations. If appropriate, reinvoke the routine again.

sqlcode: +20397

sqlstate: 01H52
