

SQL4978N  The dropped table can not be accessed.

Explanation: 

The dropped table can not be accessed. This may be due to the table
being put into unavailable state because of a LOAD without copy or a NOT
LOGGED INITIALLY operation.

User response: 

The table can not be recovered using the DROPPED TABLE RECOVERY option.
