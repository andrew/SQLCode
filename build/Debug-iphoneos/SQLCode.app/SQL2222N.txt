

SQL2222N  The specified data partition "<data-partition-name>" is not
      valid. Reason code: "<reason-code>"

Explanation: 

The specified data partition name is not valid for the request as
indicated by the following reason code:

1        

         The data partition name does not exist for the specified table.


2        

         The ON DATA PARTITION clause is not supported for the REORG
         INDEX command.


3        

         The operation cannot be performed because the data partition is
         in the attached or detached state.

User response: 

Based on the reason code listed in the message, perform the following
actions.

1        

         Resubmit the request with a valid data partition name or
         without a data partition name.


2        

         Resubmit the command without the ON DATA PARTITION clause.


3        

         Query the SYSCAT.DATAPARTITIONS catalog view to check the value
         of the STATUS column for the partition.

          

         If the STATUS is 'A', the partition is newly attached, perform
         the following steps:

          
         1. Issue the SET INTEGRITY statement to bring the attached
            partition into the normal status, the STATUS is an empty
            string.
         2. Resubmit the request after SET INTEGRITY completes
            successfully.

          

         If the STATUS value is 'D', 'L', or 'I', the partition is being
         detached, but the detach operation has not completed.
         Reorganization of the data or indexes of a detached partition
         is not permitted.

          

         Once the detach operation completes the partition will no
         longer be part of the source table. You can reorganize the data
         or indexes of the newly created target table after detach
         completes.
