

SQL3151N  No data conversion will be done from the single-byte code page
      value "<code-page>" in the H record to the application single-byte
      code page value "<code-page>" because the FORCEIN option was
      specified.

Explanation: 

No data conversion will be performed from the IXF code page to the
application code page because the FORCEIN option was specified.

User response: 

No action is required. If the conversion from the IXF file code page to
the application code page is supported by the database manager, the
operation can be resubmitted without the FORCEIN option and the data
will be converted.
