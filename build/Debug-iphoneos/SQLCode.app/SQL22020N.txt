

SQL22020N  Health monitor processing was halted because its shared
      memory segment upper limit was reached. The current size is
      "<size>".

User response: 

If the problem persists, contact IBM Support.
