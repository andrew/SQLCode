

SQL20059W  The materialized query table "<table-name>" may not be used
      to optimize the processing of queries.

Explanation: 

The materialized query table is defined with REFRESH DEFERRED and a
fullselect that is currently not supported by the database manager when
optimizing the processing of queries. The rules are based on the
materialized query table options (REFRESH DEFERRED or REFRESH
IMMEDIATE). The fullselect in the CREATE TABLE statement that returned
this condition violates at least one of the rules as described in the
SQL Reference.

The materialized query table is successfully created but will only be
routed to for queries that reference the materialized query table
directly.

User response: 

No action is required. However, if the materialized query was intended
to optimize the processing of queries that do not reference the
materialized query table directly, then this can be achieved as follows.
Create a view using the fullselect specified for "<table-name>" and then
re-create the materialized query table "<table-name>" using a fullselect
that simply does a "SELECT *" from the view. This way, the materialized
query table can be routed to by queries that reference the view.

sqlcode: +20059

 sqlstate: 01633
