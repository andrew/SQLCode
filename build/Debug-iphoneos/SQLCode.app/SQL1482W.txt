

SQL1482W  The BUFFPAGE parameter will only be used if one of the buffer
      pools is defined with a size of -1.

Explanation: 

This is a warning that the BUFFPAGE database configuration parameter
will be ignored if none of the database's buffer pools is defined with a
size of -1. -1 indicates that the buffer pool is to use the BUFFPAGE
parameter as the number of buffer pool pages.

User response: 

You can select from the SYSCAT.BUFFERPOOLS to review the buffer pool
definitions. If none of the buffer pools are defined with size -1
(NPAGES), then setting the BUFFPAGE parameter will not change the size
of the buffer pools for the database.
