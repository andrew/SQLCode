

SQL5040N  One of the socket addresses required by the TCP/IP server
      support is being used by another process.

Explanation: 

One of the socket addresses required by the server either is in use by
another program, or has not been freed completely by the TCP/IP
subsystem after the database manager has been stopped.

User response: 

If you have just issued db2stop, wait for a couple of minutes so that
the TCP/IP subsystem has enough time to clean up its resources.
Otherwise, make sure no other programs on your workstation are using the
port numbers reserved for your service name in the /etc/services file.
The port number is a component of a socket address.
