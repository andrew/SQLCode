

SQL16111N  XML Document contains an invalid CDATA section. Reason code =
      "<reason-code>".

Explanation: 

While parsing an XML document the parser encountered an invalid or ill
formed CDATA section indicated by "<reason-code>". Possible reason codes
are: 
1. There are nested CDATA sections,
2. A CDATA section is not terminated
3. a CDATA section contains the sequence ']]<'
4. a CDATA section is found outside the root element.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16111

sqlstate: 2200M
