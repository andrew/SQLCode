

SQL1522N  The deactivate command failed at one or more members where
      indoubt transactions were detected for the given database.

Explanation: 

This message is returned when an attempt is made to explicitly
deactivate a database using the DEACTIVATE DATABASE command or the
sqle_deactivate_db API but the database could not be deactivated because
indoubt transactions were detected for the given database at one or more
members.

The database has been deactivated at members where no indoubt
transactions were detected. At members where indoubts were detected, the
database will be left activated or in the same state it was in, prior to
the deactivate command being issued.

User response: 

Respond to this error in one of the following ways:

*  Wait for the transaction manager to resolve the indoubt transactions:
   
   1. Identify the indoubt transaction that involve the database using
      the LIST INDOUBT TRANSACTIONS command.
   2. Monitor the indoubt transactions until the transaction manager
      automatically resolve the indoubt transactions.
   3. Resubmit the DEACTIVATE DATABASE command or call the
      sqle_deactivate_db API again.

*  Resolve the transactions manually: 
   1. Identify the indoubt transaction that involve the database using
      the LIST INDOUBT TRANSACTIONS command.
   2. Manually resolve the indoubt transactions. Resubmit the DEACTIVATE
      DATABASE command or call the sqle_deactivate_db API again.

*  Force the database deactivation by calling the DEACTIVATE DATABASE
   command with the FORCE option: 
   1. Determine on which members the deactivation operation failed by
      reviewing the db2diag log files.
   2. On each member on which the deactivation operation failed, call
      the DEACTIVATE DATABASE command with the FORCE option. 

      Any indoubt transactions will be left unresolved.


   Related information:
   Resolving indoubt transactions manually
   LIST INDOUBT TRANSACTIONS command
   sqle_deactivate_db API - Deactivate database
   DEACTIVATE DATABASE command
