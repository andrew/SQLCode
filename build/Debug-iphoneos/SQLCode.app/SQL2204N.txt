

SQL2204N  The iname parameter is not valid. The name of the index is too
      long, only an authorization ID was specified, or the address of
      the index is not valid.

Explanation: 

If an index is specified, the name must be 1 to 128 bytes in length. The
index must be located at a valid application address.

The utility stops processing.

User response: 

Resubmit the command with a valid index name.
