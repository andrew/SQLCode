

SQL22205C  The DB2 Administration Server encountered an unexpected error
      while executing a request.

Explanation: 

An unexpected error occurred in the DB2 Administration Server while it
was processing a request.

User response: 

Refer to the DB2 Administration Server's First Failure Data Capture Log
for additional information.

If trace was active, invoke the Independent Trace Facility at the
operating system command prompt. Contact IBM Support with the following
required information: 
*  Problem description
*  SQLCODE or message number
*  SQLCA contents if possible
*  Trace file if possible
