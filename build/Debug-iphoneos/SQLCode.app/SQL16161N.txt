

SQL16161N  XML document contains an element with an information item
      that is not expected.

Explanation: 

While parsing an XML document, the parser encountered an information
item that does not match (annonation?, (simpletype | complextype)?,
(unique | key | keyref)*).

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16161

sqlstate: 2200M
