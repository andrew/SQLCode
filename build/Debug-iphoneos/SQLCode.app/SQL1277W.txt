

SQL1277W  A redirected restore operation is being performed. During a
      table space restore, only table spaces being restored can have
      their paths reconfigured. During a database restore, storage group
      storage paths and DMS table space containers can be reconfigured.

Explanation: 

The Restore utility validates that the containers needed by each table
space being restored are currently accessible on the system. Where
possible, the Restore utility will create the containers if they do not
exist. If they cannot be created, are currently in use by another table
space, or are inaccessible for any other reason, then the list of
containers needed must be corrected before the restore operation can
continue.

In a redirected restore, the state of each container of a non-automatic
storage table space being restored is set to "storage must be defined".
This makes it possible to use the SET TABLESPACE CONTAINERS command or
API against the containers to redefine their storage.

For table spaces using automatic storage, the only way to redefine
container paths is to use the SET STOGROUP PATHS command for a specific
storage group or the ON keyword when you issue the RESTORE DATABASE
command to redefine all the storage groups paths. You cannot use the SET
TABLESPACE CONTAINERS command or API against automatic storage table
spaces.

User response: 

To determine the list of containers for each table space being restored,
use the MON_GET_CONTAINER table function. To specify an updated list for
each table space, use the SET TABLESPACE CONTAINERS command or API. This
API or command lets you specify whether this should be the initial list
of containers (meaning that a subsequent rollforward will redo any "add
container" operations described in the database log) or the final list
(meaning that the rollforward operation will not redo "add container"
operations).

It is also possible that the container(s) or storage path(s) are
read-only, in which case the only action required before continuing with
the restore is to give read/write access to the container(s) or storage
path(s).

To reconfigure the paths of specific storage groups during a database
restore, use the SET STOGROUP PATHS command.

When you are ready to proceed with the restore, issue a RESTORE DATABASE
command with the CONTINUE keyword to perform the actual restore.


   Related information:
   MON_GET_CONTAINER table function - Get table space container metrics
   RESTORE DATABASE command
   SET STOGROUP PATHS command
   SET TABLESPACE CONTAINERS command
   sqlbstsc API - Set table space containers
