

SQL2055N  Unable to access memory from memory set "<memory-heap>".

Explanation: 

A database utility was unable to access memory during processing.

The utility stops processing.

User response: 

Stop the database manager, then restart it and resubmit the utility
command.
