

SQL1004C  There is not enough storage on the file system to process the
      command.

Explanation: 

There is not enough storage on the specified file system to process the
command.

In a partitioned database environment on Windows environments, each node
in the partitioned database group must have the exact same physical hard
drive specification (letter) available and useable (must contain useable
space) for the CREATE DATABASE command to succeed. The physical hard
drive letter is specified in the database manager configuration. If
DFTDBPATH is left blank the default will be the hard drive where DB2 is
installed on the instance owning machine (db2 installation path).

The command cannot be processed.

User response: 

Choose a different file system or erase some non-database files from the
specified file system to make space for the database manager functions.

In a partitioned database environment on Windows environments, follow
the following steps:

*  Determine which hard drive specification (letter) is required. The
   drive letter is specified in the error message.
*  Determine which node/s of the database partition are experiencing the
   problem. You can usually find this information in the db2diag log
   file of the instance owning node.
*  Correct the drive problem on the individual node that is experiencing
   the problem or change the drive specification in the database manager
   configuration so that the same drive is available (and has sufficient
   space) on each and every node of the partitioned database group.
*  re-issue the command.
