

SQL2211N  The specified table does not exist.

Explanation: 

The table does not exist in the database. Either the table name or the
authorization ID is incorrect.

The command cannot be processed.

User response: 

Resubmit the command with a valid table name.
