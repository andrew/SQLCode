

SQL0773N  The case was not found for the CASE statement.

Explanation: 

A CASE statement without an ELSE clause was found in the routine body of
an SQL routine. None of the conditions specified in the CASE statement
were met.

User response: 

Change the CASE statement to handle all conditions that can occur.

 sqlcode: -773

 sqlstate: 20000
