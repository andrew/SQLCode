

SQL3300N  The records in the input file are not in the correct sequence.

Explanation: 

The records in the worksheet format (WSF) file are expected to be in
ascending order (row 1, col 1 ... row 1, col 256; row 2, col 1 ... row
2, col 256, and so on). Some damage has occurred to the WSF file or it
was generated incorrectly, possibly with a level of the Lotus product
not supported by the database manager.

The IMPORT utility stops processing.

User response: 

Regenerate the WSF file with a supported level of the Lotus product.
