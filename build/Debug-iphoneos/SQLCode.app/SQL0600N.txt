

SQL0600N  A routine "<routine-name>" could not be generated because of a
      duplicate signature or because it would override an existing
      routine.

Explanation: 

During the CREATE or ALTER operation, a system-generated cast function,
observer method, mutator method, or constructor function could not be
created because another function or method with the same name and
signature already exists in the schema, or because the method or
function would override an existing method.

User response: 

Choose another name for the user-defined type, attribute, or cast
function that causes the conflict, or drop the function or method with
the same name as the function or method that could not be generated.

 sqlcode: -600

 sqlstate: 42710
