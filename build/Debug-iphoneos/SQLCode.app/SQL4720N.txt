

SQL4720N  The work action type specified is not valid for work action
      "<work-action-name>". Reason code "<reason-code>".

Explanation: 

The type of work action specified is not valid for one of the following
reasons:

1        

         Duplicate work action type specified for the work class of the
         same work action set.


2        

         The work action type is MAP ACTIVITY, but the work action set
         is associated with a database or a workload. Mapping work
         actions are only valid for work action sets applied to service
         classes.


3        

         The work action type is a threshold, but the work action set is
         associated with a service class. Threshold work actions are
         only valid for work action sets applied to a database or a
         workload.


4        

         The work action type is COLLECT AGGREGATE ACTIVITY DATA, but
         the work action set is associated with a database. COLLECT
         AGGREGATE ACTIVITY DATA work actions are only valid for work
         action sets applied to service classes or workloads.

User response: 

Specify a different type of work action.

sqlcode: -4720

sqlstate: 5U034
