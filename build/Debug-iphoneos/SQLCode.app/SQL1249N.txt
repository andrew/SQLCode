

SQL1249N  The DATALINK data type is not supported. "<db-object>" must be
      dropped or altered to avoid using the DATALINK data type.

Explanation: 

The db2ckupgrade command has identified an occurrence of the DATALINK
data type which will cause database upgrade to fail.

Database objects which use the DATALINK data type include tables, views,
functions, methods, distinct types and structured data types.

User response: 

Remove or update the database object identified by "<db-object>" which
references the DATALINK data type before attempting to upgrade the
database.
