

SQL2749N  Partitioning key "<key-no>" of record "<rec-no>" was not in
      the first 32k bytes of the record.

Explanation: 

If a record is greater than 32k bytes long in a delimited data file, all
partitioning keys of each record have to be within the first 32k bytes
of the record.

User response: 

Check record "<rec-no>" in the input data file.
