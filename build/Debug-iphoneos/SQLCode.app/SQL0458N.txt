

SQL0458N  In a reference to routine "<routine-name>" by signature, a
      matching routine could not be found.

Explanation: 

In a reference to function, method, or stored procedure "<routine-name>"
by signature, no matching function, method, or stored procedure could be
found.

If a data type is used that can accept a parameter, then the type
parameter is optional. For example, for CHAR(12), you can either specify
the parameter (CHAR(12)) or omit it (CHAR()). If you specify the
parameter, then the DBMS will only accept an exact match on the data
type and the data type parameter. If you omit the parameter, then the
DBMS will accept a match on data type only. The CHAR() syntax provides a
way to tell the DBMS to ignore data type parameters when finding a
matching function.

Note also that in the DROP FUNCTION/PROCEDURE, COMMENT ON
FUNCTION/PROCEDURE and TRANSFER OWNERSHIP FUNCTION/PROCEDURE/METHOD
statements, an unqualified reference is qualified with the statement
authorization ID, and this is the schema where the problem can be found.
In the SOURCE clause of a CREATE function, the qualification comes from
the current path. In this case, there is no matching function in the
entire path.

A function cannot be sourced on the COALESCE, DBPARTITIONNUM, GREATEST,
HASHEDVALUE, LEAST, MAX (scalar), MIN (scalar), NULLIF, NVL, RID,
RAISE_ERROR, TYPE_ID, TYPE_NAME, TYPE_SCHEMA, or VALUE built-in
functions.

The statement cannot be processed.

User response: 

Possible responses include:

*  Changing the path to include the correct schema.
*  Removing parameters from the specifications of data types.
*  Using a SPECIFIC name to refer to the function or procedure instead
   of a signature.

sqlcode: -458

 sqlstate: 42883
