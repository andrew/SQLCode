

SQL1085N  The application heap cannot be allocated.

Explanation: 

The application could not connect to the database because the database
manager could not allocate the number of application heap 4K pages
specified in the database configuration file. The system is out of 4K
pages. The command cannot be processed.

User response: 

Possible solutions are: 
*  Lower the size of the application heap (applheapsz) in the database
   configuration file.
*  Reduce the maximum number of applications in the database
   configuration file.
*  Remove background processes.
*  Install more memory.

 sqlcode: -1085

 sqlstate: 57019
