

SQL3705N  The buffer size parameter specified is not valid. The buffer
      size must be specified as 0 or be between 8 and 250000 inclusive.
      For multiple buffers, the total buffer size must not exceed
      250000.

Explanation: 

The application calling the utility has supplied a buffer size parameter
that is not valid. The buffer size is used to determine the internal
buffer size. The value is the number of 4K pages that are obtained for
this buffer. The value must be specified as 0 or be between 8 and 250000
inclusive. If there are multiple buffers, the number of buffers
multiplied by the buffer size must not exceed 250000.

When 0 is specified: 
*  for a table whose regular data is in a database managed storage table
   space, the default buffer size chosen is the extent size for the
   table space or 8, whichever is larger.
*  for a table whose regular data is in a system managed storage table
   space, the default buffer size chosen is 8.

User response: 

Reissue the command with a valid buffer size.
