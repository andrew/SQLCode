

SQL1860N  Table space "<tablespace-name>" is not compatible with table
      space "<tablespace-name>". Reason code = "<reason-code>".

Explanation: 

The table spaces specified are not compatible for one of the following
reasons:

1        

         All table spaces (data, long, index) for a partitioned table
         must be in the same database partition group.


2        

         The data table spaces for a partitioned table must be either
         all SMS, all regular DMS, or all large DMS. The index table
         spaces for a partitioned index must be all regular DMS or all
         large DMS.


3        

         The page size for all data table spaces must be the same. The
         page size for all index table spaces must be the same. The page
         size for all long table spaces must be the same. However, the
         page sizes of data table spaces, index table spaces, and long
         table spaces can be different from each other.


4        

         The extent size for each data table space must be the same as
         the extent sizes of other data table spaces. The extent size
         for each index table spaces must be the same as the extent
         sizes of other index table spaces. The extent sizes for all
         table space used storing long data must also be the same.
         However, the extent sizes of table spaces used for different
         purposes do not need to match.


5        

         Long data for a partitioned table must be stored in the same
         table space as the data for all data partitions or it must be
         stored in large table spaces, each of which is distinct from
         the table space of the corresponding data partition. This error
         will occur if a LONG IN clause is used that specifies a table
         space that is different from the data table space but is not a
         large table space. LONG IN can only be used to specify regular
         table spaces if they are identical to the data table spaces
         (i.e. in the case where the LONG IN clause is redundant because
         it merely specifies the data table spaces, which is the same as
         the default behavior if LONG IN were omitted entirely).

User response: 

Specify a table space that matches the other table spaces for the table.

sqlcode: -1860

sqlstate: 42838
