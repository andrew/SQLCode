

SQL0412N  Multiple columns are returned from a subquery that is allowed
      only one column.

Explanation: 

In the context of the SQL statement, a fullselect is specified that can
have only one column as a result.

The statement cannot be processed.

User response: 

Specify only one column when only a scalar fullselect is allowed.

 sqlcode: -412

 sqlstate: 42823
