

SQL27983N  The LOAD utility is unable to rebuild the index.

Explanation: 

The LOAD target table has detached data partitions and has dependent
materialized query tables or dependent staging tables that have not been
incrementally refreshed with respect to the detached data partition.
This condition prevents the LOAD utility running in insert mode or
restart mode from rebuilding a unique index.

User response: 

Do not specify indexing mode REBUILD with LOAD insert until the
dependent materialized query tables or dependent staging tables are
refreshed. Use a different LOAD indexing mode, or execute the SET
INTEGRITY statement with the IMMEDIATE CHECKED option to maintain the
dependent materialized query tables or dependent staging tables with
respect to the detached data partition. If a LOAD restart cannot
incrementally maintain indexes, the previously failed LOAD operation
must be terminated before dependent materialized query tables or
dependent staging tables can be refreshed.
