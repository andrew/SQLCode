

SQL1509N  The statement cannot be processed because all available
      transports are in use, and no more transports can be created.
      Reason code: "<reason-code>".

Explanation: 

A transport is a physical connection to a database. For more information
about transports, refer to the topic called "Transaction-level load
balancing" in the DB2 Information Center.

The reason that no more transports can be created is indicated by the
given reason code:

1        

         No memory is available for additional transports.


2        

         The value set for the configuration parameter maxTransports has
         been reached.

User response: 

Respond to this error according to the reason code:

1        

         Resolve the memory shortage:

          
         1. Make more memory available to the application. For example,
            terminate any unused connections.
         2. Rerun the statement.


2        

         Change the database configuration to allow more transports:

          
         1. Set the maxTransports configuration parameter to a higher
            value in the db2dsdriver.cfg configuration file. 

            For more information about maxTransports or the
            db2dsdriver.cfg configuration file, refer to the DB2
            Information Center.

         2. Restart the application.

sqlcode: -1509

sqlstate: 57060
