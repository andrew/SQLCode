

SQL3704N  The num_buffers parameter specified is invalid.

Explanation: 

The num_buffers parameter determines the number of buffers that the
utility will use. The minimum is 2 if the lobpaths parameter is not
specified and 3 if the lobpaths parameter is specified. This is the
minimum required to allow the utility to work. There is, however, an
optimal number of buffers that the utility will use if this parameter is
not specified. This optimal number is based on the number of internal
processes that the utility will have running and whether or not the
lobpaths parameter was specified. If the number of buffers specified is
less than the optimal number, some processes will be waiting for buffers
to use. Therefore, it is recommended to specify 0 for this parameter and
let the utility choose the number of buffers. Only specify this
parameter if, due to the size of the utility storage heap, it is
necessary to limit the amount of memory that the utility uses.

User response: 

Resubmit the command using a valid num_buffers parameter.
