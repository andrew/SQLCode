

SQL2917N  The ingest operation failed because the SHM_MAX_SIZE
      configuration parameter is too small.

Explanation: 

You can specify the maximum size of Inter Process Communication (IPC)
shared memory by setting the SHM_MAX_SIZE ingest utility configuration
parameter.

This message is returned when the ingest operation fails because the
SHM_MAX_SIZE configuration parameter is not large enough.

User response: 

1. Set the SHM_MAX_SIZE configuration parameter to a larger value (by
   issuing the INGEST SET command, for example.)
2. Run the ingest operation again.


   Related information:
   shm_max_size - Maximum size of shared memory configuration parameter
   INGEST SET command
   INGEST command
   db2Ingest API- Ingest data from an input file or pipe into a DB2
   table.
