

SQL20157N  User with authorization ID "<authorization-ID>" failed to
      attach to a quiesced instance, or connect to a quiesced database
      or a database in a quiesced instance which is in the following
      quiesce mode: "<quiesce-mode>"

Explanation: 

The specified authorization ID does not have the authority to attach to
a quiesced instance or connect to a database in a quiesced instance when
the instance is in QUIESCE RESTRICTED ACCESS mode. To be able to attach
to the instance or connect to the database, the authorization ID must
satisfy one of the following listed criteria:

*  Hold SYSADM, SYSCTRL, or SYSMAINT authority
*  Be the user that was specified using the USER option of the QUIESCE
   INSTANCE or START DATABASE MANAGER commands (or db2InstanceQuiesce or
   db2InstanceStart APIs)
*  Be a member of the group that was specified using the GROUP option of
   the QUIESCE INSTANCE or START DATABASE MANAGER commands (or
   db2InstanceQuiesce or db2InstanceStart APIs)

In the event the quiesced database is in QUIESCE DATABASE mode, then the
authorization ID must satisfy one of the previously listed criteria or
hold either the DBADM authority or QUIESCE_CONNECT privilege before the
user can successfully connect to a quiesced database.

In the event the database in a quiesced instance is in QUIESCE INSTANCE
mode, then the authorization ID must satisfy one of the above listed
criteria or hold the DBADM authority before the user can successfully
connect to a database in a quiesced instance.

In the event the instance is in QUIESCE INSTANCE mode, then the
authorization ID must satisfy one of the above listed criteria before
the user can successfully attach to the quiesced instance.

User response: 

You can take one of the following actions to successfully connect to a
quiesced database or a database of a quiesced instance, or attach to a
quiesced instance:

*  To connect to the database, wait for the database to be unquiesced.
*  To attach to the instance or connect to a database in the instance,
   wait for the instance to be unquiesced.
*  Retry connecting to the database or attaching to the instance using
   an authorization ID that has sufficient authority.

sqlcode: -20157

sqlstate: 08004


   Related information:
   db2InstanceStart API - Start instance
   db2InstanceQuiesce API - Quiesce instance
   START DATABASE MANAGER command
   QUIESCE command
