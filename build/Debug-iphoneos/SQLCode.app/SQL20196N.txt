

SQL20196N  The one or more built-in types which are returned from the
      FROM SQL function or method does not match the corresponding
      built-in types which are parameters of the TO SQL function or
      method.

Explanation: 

The built-in types which are returned by a FROM SQL transform function
or method have to match the types in the parameter list of the
corresponding TO SQL transform function or method.

User response: 

Choose a different FROM SQL transform function or method or TO SQL
transform function or method or alter either the FROM SQL transform
function or method or TO SQL transform function or method to make sure
that each built-in type returned from the FROM SQL function or method
matches the corresponding built-in type that is a parameter of the TO
SQL transform function or method.

sqlcode: -20196

sqlstate: 428FU
