

SQL2449W  The db2convert command converted no tables because no tables
      that satisfied the specified matching criteria were row-organized.

Explanation: 

You can convert row-organized tables into column-organized tables by
using the db2convert command.

You can control which tables to convert by specifying matching criteria
with db2convert parameters:

*  All tables in the database
*  Tables created by a specific user (-u parameter)
*  Tables in a specified schema (-z parameter)
*  One specified table (-t parameter)

The db2convert utility will attempt to convert all row-organized tables
that match the specified criteria. This message is returned when none of
the tables that match the specified criteria are row-organized tables.

User response: 

To convert row-organized tables to column-organized tables, call the
db2convert command specifying table criteria that match existing
row-organized tables.


   Related information:
   db2convert - Convert row-organized tables into column-organized
   tables
