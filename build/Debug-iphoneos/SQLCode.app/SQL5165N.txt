

SQL5165N  The value "<value>" indicated by the database configuration
      parameter "<parameter>" is not valid. Reason code="<reason-code>"

Explanation: 

The update of the indicated database configuration parameter failed. The
explanation corresponding to the reason code is:

1        

         The hadr_target_list is invalid for one of the following
         reasons:

          
         *  The host entry is longer than 255 characters.
         *  The service entry is longer than 40 characters.
         *  The host entry contains invalid characters. Host entries can
            contain either alphanumerics, dashes, and underscores, or
            they can be in a numeric IPv4 or IPv6 address format.
         *  The values were not formatted correctly. The valid
            delimiters are: ":" "." "[" "]" "{" "}" "|"


2        

         The number of databases specified in hadr_target_list exceeds
         the maximum allowed (one in a DB2 pureScale environment and
         three in other environments).


4        

         The entries contained duplicate host:port pairs.


5        

         In a DB2 pureScale environment, each standby cluster specified
         in hadr_target_list must be enclosed with braces ("{...}"),
         even if the cluster only has one address listed.


6        

         In an environment other than DB2 pureScale, only one address is
         allowed for each standby database specified in
         hadr_target_list.


7        

         In a DB2 pureScale environment, a maximum of 128 addresses are
         allowed for each standby cluster specified in hadr_target_list.

User response: 

The user response corresponding to the reason code is:

1        

         Update the hadr_target_list configuration parameter using a
         string with a valid length or set of characters. Check the
         db2diag.log for more details.


2        
         *  If your HADR setup is in a DB2 pureScale environment,
            specify no more than one database in hadr_target_list.
         *  If your HADR setup is not in a DB2 pureScale environment,
            specify no more than three databases in hadr_target_list.


4        

         Update the hadr_target_list configuration parameter using
         unique host:port combinations.


5        

         Enclose the address list of each standby cluster with braces
         ("{...}").


6        

         List only one address for each standby database specified in
         hadr_target_list.


7        

         List no more than 128 addresses for each standby cluster
         specified in hadr_target_list.


   Related information:
   hadr_target_list - HADR target list database configuration parameter
