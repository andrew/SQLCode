

SQL5156N  The value of the database manager configuration parameter
      "trust_allclnts" must be one of NO, YES, or DRDAONLY.

Explanation: 

The allowed values for the configuration parameter "trust_allclnts" are:

*  NO = 0
*  YES = 1
*  DRDAONLY = 2

The requested change is not made.

User response: 

Resubmit the command with a valid value for "trust_allclnts".
