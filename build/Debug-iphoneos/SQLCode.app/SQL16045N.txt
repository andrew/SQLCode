

SQL16045N  An unidentified error was issued from an XQuery expression.
      Error QName=err::FOER0000.

Explanation: 

An XQuery expression resulted in an unidentified error.

User response: 

Fix the problem in the XQuery expression.

 sqlcode: -16045

 sqlstate: 10611
