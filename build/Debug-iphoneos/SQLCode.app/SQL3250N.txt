

SQL3250N  COMPOUND="<value>" is invalid. Reason code: "<reason-code>".

Explanation: 

The COMPOUND=x option was specified for the import utility and cannot be
processed due to the reason with reason code "<reason-code>": 

1        It is invalid when the INSERT_UPDATE option is used.

2        It is invalid with the following file type modifiers:
         IDENTITYIGNORE, IDENTITYMISSING, GENERATEDIGNORE,
         GENERATEDMISSING.

3        The database being imported to is accessed through a server or
         gateway of a previous release.

4        The value is not within the allowed range of 1 to 100 (On DOS
         or Windows, the maximum value is 7).

5        The table being imported is a hierarchy or typed table.

6        The table being imported has generated columns.

7        It is invalid when the XMLVALIDATE USING XDS option is used.

8        It is invalid when the USEDEFAULTS file type modifier is
         specified.

User response: 

The action corresponding to the reason code:

For reason code 1: 
*  Remove COMPOUND=x from the file type modifier option, or use INSERT
   option.

 For reason codes 2, 3, 5, 6, 8: 
*  Remove COMPOUND=x from the file type modifier option.

 For reason code 4: 
*  Set x in COMPOUND=x to a correct value.

 For reason code 7: 
*  Remove COMPOUND=x or remove XMLVALIDATE USING XDS option.
