

SQL0455N  In routine "<routine-name>", the schema name "<schema-name1>"
      provided for the SPECIFIC name does not match the schema name
      "<schema-name2>" of the routine.

Explanation: 

If the SPECIFIC name is specified as a two part name, the
"<schema-name1>" portion must be the same as the "<schema-name2>"
portion of the "<routine-name>". Note that the "<schema-name2>" portion
of "<routine-name>" may have been specified directly or it may have
defaulted to the authorization ID of the statement. If the routine is a
method, "<schema-name>" refers to the schema name of the subject type of
the method.

User response: 

Correct the statement and try again.

 sqlcode: -455

 sqlstate: 42882
