

SQL1454N  The entry in the database manager configuration file for
      object name is missing or invalid.

Explanation: 

The object name specified in the configuration command/API or in the
database manager configuration file is missing or invalid.

User response: 

Verify that an object name was specified, that the name does not contain
invalid characters, and is not longer than 48 characters in length.
Update the object name in the database manager configuration file and
resubmit the command/API.
