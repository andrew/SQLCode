

SQL1633W  The activity identified by application handle
      "<application-handle>", unit of work ID "<unit-of-work-id>", and
      activity ID "<activity-id>" could not be captured because there is
      no active activity event monitor.

Explanation: 

An attempt was made to capture an activity identified by an application
handle, a unit of work identifier, and an activity identifier. This
requires that an activity event monitor be created and its state set to
active. There currently is no activity event monitor in the active
state.

User response: 

If there already is an activity event monitor but it is not in active
state, set its state to active. If there are no activity event monitors
in this database, create one, set its state to active. Reinvoke this
procedure.

 sqlcode: +1633

 sqlstate: 01H53
