

SQL4125W  A HAVING clause must contain the WHERE clause when the WHERE
      clause has a column function.

Explanation: 

If a VALUE EXPRESSION directly contained in the SEARCH CONDITION is a
column function, then the WHERE clause must be contained in a HAVING
clause.

Processing continues.

User response: 

Correct the SQL statement.
