

SQL20453N  The task "<task-name>" cannot be removed because it is
      currently executing.

Explanation: 

An attempt to remove task "<task-name>" failed because it is currently
executing.

User response: 

Wait until the task completes and then use the SYSPROC.ADMIN_TASK_REMOVE
procedure to remove the task. The SYSTOOLS.ADMIN_TASK_STATUS view can be
used to check the execution status of the task.

sqlcode: -20453

sqlstate: 5UA01
