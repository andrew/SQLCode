

SQL20131N  Object number "<object-number>" of type "<object-type>" was
      specified more than once in a list of objects.

Explanation: 

In a list of object names of type "<object-type>", the object numbered
"<object-number>" was specified more than once. The operation of the
statement cannot be performed on the object more than once.

User response: 

Correct the duplicated object in the list removing duplicate
occurrences. (In the MDC case, the object type will be "dimension".)

sqlcode: -20131

sqlstate: 42713
