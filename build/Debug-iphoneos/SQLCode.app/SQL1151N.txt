

SQL1151N  Load REMOTEFETCH media options are invalid. Reason Code:
      "<reason-code>".

Explanation: 

The Load utility was invoked using the REMOTEFETCH media type, but one
or more specified arguments is incompatible or invalid. This is due to
one of the following reasons: 

1        The server does not support the REMOTEFETCH media type.

2        The source database name was not specified.

3        The password field was specified without the user-id field.

4        One of the source table-name or schema fields was specified
         without the other.

5        Both the source table-name and source statement fields were
         specified.

6        Neither the source table-name nor source statement fields were
         specified.

7        The isolation-level specified is invalid.

8        One of the specified arguments exceeds the maximum applicable
         size for that argument.

9        The SQLU_REMOTEFETCH_ENTRY API structure was not setup
         correctly.

User response: 

The responses corresponding to each reason code are: 

1        Ensure that the server level is version 9 or greater.

2        Ensure that the source database-name is specified. This field
         is mandatory.

3        Do not specify the password field without specifying the
         user-id field.

4         If providing a source table-name instead of a source query,
         ensure that you specify both the source table-name and schema.

5        Specify either the source table-name and schema, or the source
         statement, not both.

6        Specify the source table-name and schema, or the source
         statement.

7        Ensure that the isolation level specified is correct.

8        Ensure that the arguments provided and their length tokens
         (API) are all valid.

9        Ensure that the SQLU_REMOTEFETCH_ENTRY API structure is set-up
         and initialized correctly. Unused fields should be NULL. Length
         values should be set.
