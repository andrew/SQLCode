

SQL20016N  The value of the inline length associated with type or column
      "<type-or-column-name>" is too small.

Explanation: 

For the definition of structured type "<type-or-column-name>", it has an
INLINE LENGTH value specified that is smaller than the size returned by
the constructor function (32 + 10 * number_of_attributes) for the type
and is less than 292. For the altering of column
"<type-or-column-name>", the INLINE LENGTH specified is smaller than the
current inline length.

The statement cannot be processed.

User response: 

Specify an INLINE LENGTH value that is large enough. For a structured
type, that is at least the size returned by the constructor function for
the type or is at least 292. For a column, it must be larger than the
current inline length. If this error occurs when altering the type (or
some supertype of this type) to add an attribute, either the attribute
cannot be added or the type must be dropped and re-created with a larger
INLINE LENGTH value.

 sqlcode: -20016

 sqlstate: 429B2
