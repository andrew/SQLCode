

SQL3073N  The machine format field in the T record is not PCbbb (where b
      = blank).

Explanation: 

The machine format field in the T record is a value other than PC bbb,
where each b is a blank.

The utility stops processing. No data is loaded.

User response: 

Examine the machine format field in the T record.
