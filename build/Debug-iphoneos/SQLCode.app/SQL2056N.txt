

SQL2056N  An invalid media type was encountered on media "<media>".

Explanation: 

An invalid media type was encountered during the processing of a
database utility.

The utility stops processing.

User response: 

Ensure the media used is among of the types supported by the utility.
Resubmit the command with a valid media list.
