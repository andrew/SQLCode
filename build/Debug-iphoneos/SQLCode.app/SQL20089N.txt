

SQL20089N  A method name cannot be the same as a structured type name
      within the same type hierarchy.

Explanation: 

A specified method name is the same as the structured type which is
defined for one of the supertypes or subtypes of the structured type.

The statement cannot be processed.

User response: 

Specify a different name for the method.

 sqlcode: -20089

 sqlstate: 42746
