

SQL6011N  The data type "<number>" ("<type-text>") of column "<name>"
      (positioned at column "<number>") cannot be processed.

Explanation: 

The QMF file contains a column with data type that is not supported.

The SQLQMF facility does NOT support the following data types: 
*  LONG VARCHAR
*  LONG VARGRAPHIC.

Only the SQLQMF facility SQLQMFDB supports graphic data types.

The command cannot be processed.

User response: 

Return to your QMF host session and rerun the query without selecting
the named column. Then run the SQLQMF facility command again.
