

SQL2038N  A database system error "<errcode>" occurred during
      processing.

Explanation: 

A database system error was encountered during the processing of one of
the utilities.

User response: 

1. Examine the error code in the message.
2. Review the DB2 diagnostic (db2diag) log files for more information.
3. Take corrective action.
4. If appropriate resubmit the command.


   Related information:
   DB2 diagnostic (db2diag) log files
