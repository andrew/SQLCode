

SQL4179W  Table "<schema-name>"."<table>" must not be identified in a
      FROM clause of any subquery contained in a SEARCH CONDITION.

Explanation: 

A table specified in a DELETE or an UPDATE cannot be used in a FROM
clause of any subquery contained in its SEARCH CONDITION.

Processing continues.

User response: 

Correct the SQL statement.
