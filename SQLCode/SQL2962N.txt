

SQL2962N  When restart is on, the nickname specified on the INGEST
      command must have server option DB2_TWO_PHASE_COMMIT set to 'Y'.

Explanation: 

The ingest utility issues this message when all of the following are
true:

*  The INGEST command omits the RESTART parameter or specifies RESTART
   NEW or RESTART CONTINUE.
*  The target table is a nickname.
*  When trying to insert into or update the nickname, the utility
   receives message SQL30090N with reason code 18.

The SQL30090N error occurs because the utility needs to update both the
remote table that the nickname refers to and the restart log table,
which is local. This requires the two-phase commit protocol, but the
server definition that contains the nickname is not defined with server
option DB2_TWO_PHASE_COMMIT set to 'Y'.

User response: 

Any one of the following:

*  Alter the the server definition that contains the nickname so it
   specifies server option DB2_TWO_PHASE_COMMIT 'Y'. 

   Note that you cannot use command SET SERVER OPTION to set the
   DB2_TWO_PHASE_COMMIT option for the ingest utility because the SET
   SERVER OPTION command affects only the CLP connection, whereas the
   ingest utility establishes its own connection. You must set the
   server option in the server definition in the catalog.

*  Specify a nickname that has server option DB2_TWO_PHASE_COMMIT set to
   'Y'.
*  Specify RESTART OFF so that the ingest utility does not maintain
   restart information in the restart log table.
*  Specify a target table that is not a nickname.


   Related information:
   Performing two-phase commit transactions
