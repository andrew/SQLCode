

SQL16221N  XML document contains a mismatch in the definition of the
      base type "<base-type-name>" and the derived type
      "<derived-type-name>".

Explanation: 

While parsing an XML document, the parser encountered a mismatch in the
definition of a base type "<base-type-name>"and the derived type
"<derived-type-name>". If the content type of the base type is mixed
then the derived type must also be mixed content. If the content of the
base type is element-only, then the derived type must also be
element-only content.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16221

sqlstate: 2200M
