

SQL16157N  XML schema contains invalid relationships involving inclusion
      or exclusion facet value "<value>" of a derived type and the value
      space of the base type. Reason code = "<reason-code>".

Explanation: 

While processing an XML schema the XML parser encountered an invalid
relationship between inclusion facet values or exclusion facet values
for a derived type and the value space of the base type. The
"<reason-code>" indicates which of the following conditions was found.
One or more of the following conditions hold true: 
1. The maxInclusion value "<value>" for the derived type is not in the
   value space for the base type.
2. The maxExclusion value "<value>" for the derived type is not in the
   value space for the base type.
3. The minInclusion value "<value>" for the derived type is not in the
   value space for the base type.
4. The minExclusion value "<value>" for the derived type is not in the
   value space for the base type.

Parsing or validation did not complete.

User response: 

Correct the facet value that is outside the value space of the base type
and try the operation again.

sqlcode: -16157

sqlstate: 2200M
