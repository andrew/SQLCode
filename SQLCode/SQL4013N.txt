

SQL4013N  An END COMPOUND statement was found without a previous BEGIN
      COMPOUND statement.

Explanation: 

This error is returned when an END COMPOUND statement has been found
without a preceding BEGIN COMPOUND.

User response: 

Either remove the END COMPOUND, or add a BEGIN COMPOUND, and resubmit
the precompilation.
