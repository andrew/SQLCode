

SQL3019N  No Action String parameter was specified in the command.

Explanation: 

No Action String (for example, "REPLACE into ...") parameter is
specified for this utility call. This parameter is required.

The command cannot be processed.

User response: 

Resubmit the command with an Action String parameter.
