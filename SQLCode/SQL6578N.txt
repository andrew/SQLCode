

SQL6578N  Invalid AutoLoader option. RESTART/TERMINATE option can only
      be specified with SPLIT_AND_LOAD or LOAD_ONLY mode.

Explanation: 

RESTART/TERMINATE option in AutoLoader can only work with SPLIT_AND_LOAD
or LOAD_ONLY mode.

User response: 

Please check the AutoLoader configuration file or the autloader option
flags.
