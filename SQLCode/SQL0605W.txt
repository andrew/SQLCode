

SQL0605W  The index was not created because an index "<name>" with a
      matching definition already exists.

Explanation: 

A CREATE INDEX statement attempted to create a new index which matches
an existing index definition.

Two index definitions match if they identify the same columns in the
same order, with the same ascending or descending specifications, and
both enforce uniqueness or only the new index does not enforce
uniqueness.

Two index definitions also match if they identify the same columns in
the same order, with the same or reverse ascending or descending index
key order, and at least one index supports both forward and reverse
scans.

For partitioned tables, two index definitions do not match if one is
partitioned and the other is nonpartitioned, even if all other
specifications match. Partitioned and nonpartitioned indexes of
otherwise similar definitions can coexist on the same table.

The new index was not created.

User response: 

No action is required unless the existing index "<name>" is not a
suitable index. For example, the existing index "<name>" is not a
suitable index if it does not allow reverse scans, and the required one
does (or vice versa). In this case, the index "<name>" must be dropped
before the required index can be created.

sqlcode: +605

sqlstate: 01550
