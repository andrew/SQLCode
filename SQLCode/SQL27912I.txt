

SQL27912I  Reading of input partitioning map is in progress.

Explanation: 

This informational message indicates that reading of the input partition
map file is in progress.

User response: 

No action is required.
