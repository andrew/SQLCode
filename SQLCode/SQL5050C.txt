

SQL5050C  The database manage encountered an error while processing the
      contents of the database manager configuration file.

Explanation: 

The database manager configuration file, db2systm, is created when a DB2
instance is created. The parameters in the database manager
configuration file affect system resources at the instance level.

This message is returned when the database manager encounters an error
reading information from or updating information in the database manager
configuration file.

This error can be caused by different reasons, including the following
examples:

*  The database manager configuration file might have been altered by a
   text editor or a program other than the database manager.
*  Environment variables (such as LIBPATH or LD_LIBRARY_PATH) might be
   set incorrectly, and that is preventing the database manager from
   finding the proper versions of libraries or other files.
*  After the DB2 database product has been upgraded, processes (such as
   the monitoring process called kuddb2 or db2fmcd) or resources (such
   as old directories) that were associated with the previous version of
   DB2 database might be interfering with files the current DB2 database
   manager is accessing.

User response: 

Perform one or more of the following troubleshooting steps:

*  Verify that DB2 database-related environment variables are set
   correctly.
*  Stop all DB2 processes.
*  Drop and recreate the DB2 database manager instance.
*  Reinstall the DB2 database product.


   Related information:
   Linux and UNIX environment variable settings
   Setting up the application development environment (Windows)
   Stopping all DB2 processes (Linux and UNIX)
