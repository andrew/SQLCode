

SQL20102N  The CREATE or ALTER statement for the routine
      "<routine-name>" specified the "<option-name>" option which is not
      allowed for the routine.

Explanation: 

The option "<option-name>" was specified when creating or altering the
routine "<routine-name>". The option does not apply to the routine
because of other characteristics of the routine. For sourced procedures,
only ALTER PARAMETER can be specified, and ALTER PARAMETER can only be
specified for sourced procedures.

User response: 

For an ALTER statement, ensure that the correct routine is being
specified. Otherwise, remove the failing option and reissue the
statement.

sqlcode: -20102

sqlstate: 42849
