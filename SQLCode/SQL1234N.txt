

SQL1234N  The table space "<tablespace-name>" cannot be converted to a
      LARGE table space.

Explanation: 

Only REGULAR DMS table spaces can be converted to a LARGE DMS table
space. The system catalog table space, SMS table spaces, and temporary
table spaces cannot be converted to be a LARGE table space.

User response: 

Verify the attributes of the table space by issuing SELECT TBSPACE,
TBSPACETYPE, DATATYPE FROM SYSCAT.TABLESPACES WHERE TBSPACE =
'"<tablespace-name>"'. The table space cannot be the system catalog
table space (TBSPACE cannot be 'SYSCATSPACE'), must be a DMS table space
(TBSPACETYPE must be 'D'), and also must be a REGULAR table space
(DATATYPE must be 'A').

 sqlcode: -1234

 sqlstate: 560CF
