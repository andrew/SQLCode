

SQL16213N  XML schema contains an element "<element-name>" that must not
      have a "<constraint-type>" constraint because the type is derived
      from ID.

Explanation: 

While parsing an XML schema, the parser encountered an element that must
not have a "<constaint-type>" constraint because it is derived from ID.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16213

sqlstate: 2200M
