

SQL1513W  ddcstrc has not been turned off.

Explanation: 

Because of an error condition ddcstrc has not been turned off. This was
done to ensure that the trace information will not be lost before being
safely placed in a file.

User response: 

Correct the ddcstrc error condition reported prior to this error and
attempt to turn off the trace again.
