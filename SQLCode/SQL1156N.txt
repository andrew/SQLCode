

SQL1156N  The utility operation ID is not valid.

Explanation: 

The utility operation ID provided is not valid. It cannot be null and it
must be a value previously returned from the SYSPROC.ADMIN_CMD procedure
for a utility that was executed by it. However, once the messages have
been removed from the server by the SYSPROC.ADM_REMOVE_MSGS procedure,
the utility operation ID can no longer be used to refer to the messages
on the server.

User response: 

The messages were neither retrieved nor removed from the server. Verify
that the correct value was provided. The SYSPROC.ADMIN_CMD procedure
returns SQL statements in the MSG_RETRIEVAL and MSG_REMOVAL columns of
the first result set. They contain the operation ID that corresponds to
the utility being executed. Use these SQL statements for message
retrieval or removal.

 sqlcode: -1156

 sqlstate: 5U008
