

SQL16144N  XML document contains an open angle bracket character ('<')
      in the attribute "<attribute-name>" without specifying it as an
      entity.

Explanation: 

While parsing an XML document, the parser encountered an open angle
bracket character ('<') in an attribute value for attribute named
"<attribute-name>". If the open angle bracket character is desired, it
must be specified as the entity '&lt;'. It cannot be specified as the
character literal '<'.

Parsing or validation did not complete.

User response: 

Correct the attribute value and try the operation again.

sqlcode: -16144

sqlstate: 2200M
