

SQL1012N  No node name was specified in the CATALOG DATABASE command for
      a remote entry.

Explanation: 

There was no nodename parameter specified in the CATALOG DATABASE
command for a remote entry. Remote entries must specify the node name of
the database.

The command cannot be processed.

User response: 

Resubmit the command with the nodename parameter or a different type.
