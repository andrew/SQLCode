

SQL4006N  The structures are nested too deeply.

Explanation: 

The number of nested structures exceeded the maximum of 25.

The statement cannot be processed.

User response: 

Reduce the number of nested structures.
