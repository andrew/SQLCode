

SQL16247N  Source XML type "<source-data-type>" cannot be mapped to
      target SQL type "<target-data-type>" in the annotation at or near
      line "<lineno>" in XML schema document "<uri>".

Explanation: 

Annotation at or around line "<lineno>" in XML schema document "<uri>"
maps a XML schema type "<source-data-type>" to an incompatible SQL type
"<target-data-type>". The XML schema document can be determined by
matching "<uri>" to the SCHEMALOCATION column of catalog view
SYSCAT.XSROBJECTCOMPONENTS.

The XML schema is not enabled for decomposition.

User response: 

Consult annotated XML schema documentation on compatibility between XML
schema types and SQL types. Correct the annotation appropriately.

sqlcode: -16247

sqlstate: 225DE
