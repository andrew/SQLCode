

SQL3028N  The export method indicator is not valid. It must be either
      'N' or 'D'.

Explanation: 

The export method indicator must be either N for Names or D for Default.

The command cannot be processed.

User response: 

Resubmit the statement with a valid method indicator.
