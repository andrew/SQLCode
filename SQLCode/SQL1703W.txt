

SQL1703W  The db2event directory could not be created during database
      upgrade.

Explanation: 

The database was upgraded successfully but the db2event directory could
not be created.

This is a warning only.

User response: 

The db2event directory must be created to use the event monitor. The
db2event directory must be created in the database directory where the
upgraded database resides. The database directory of the upgraded
database can be determined by issuing the LIST DATABASE DIRECTORY
command.
