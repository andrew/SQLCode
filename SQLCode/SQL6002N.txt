

SQL6002N  Both the program name and CS:IP parameters must be specified.

Explanation: 

Communications Manager encountered an error during the download of the
host file.

The command cannot be processed.

User response: 

Review the Communications Manager message log.
