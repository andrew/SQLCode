

SQL20207N  The install or remove jar procedure for "<jar-id>" specified
      the use of a deployment descriptor.

Explanation: 

The DEPLOY or UNDEPLOY parameter of the install or replace jar procedure
was non-zero; this parameter is not supported and must be zero.

User response: 

Reissue the procedure with the DEPLOY or UNDEPLOY parameter set to zero.

 sqlcode: -20207

 sqlstate: 46501
