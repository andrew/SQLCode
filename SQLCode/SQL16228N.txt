

SQL16228N  The content of <all> is restricted to <xs:element> but
      "<tag-name>" was encountered.

Explanation: 

While parsing an XML schema document, the parser encountered the element
"<tag-name>". XML Schema restricts the content of <all> to <xs:element>.

User response: 

Correct the XML schema document and try the operation again.

sqlcode: -16228

sqlstate: 2200M
