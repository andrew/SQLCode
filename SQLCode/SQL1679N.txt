

SQL1679N  DB2START was unable to start a CF with identifier
      "<identifier>" on host "<host-name>". Reason code "<reason-code>".

Explanation: 

The reason code indicates why this error was returned:

1        

         This message can be returned with reason code 1 for multiple
         reasons, including the following reasons:

          
         *  The host on which the cluster caching facility (CF) is
            located is unavailable.
         *  A TCP/IP communication error occurred while trying to
            establish a connection with the host on which the CF is
            located.
         *  DB2 cluster services was unable to allocate memory for the
            CF because the CF memory database manager configuration
            parameter CF_MEM_SZ is set to a value that is larger than
            the amount of memory that is available.

User response: 

Respond to this message according to the reason code:

1        
         1. Perform the following troubleshooting activities: 
            *  Ensure that the host has the proper authorization defined
               in the .rhosts or the host.equiv files to execute remote
               commands.
            *  Ensure that the application is not using more than the
               maximum allowed file descriptors at the same time: 500 +
               (1995 - 2* total_number_of_nodes)
            *  Ensure all the Enterprise Server Edition environment
               variables are defined in the profile file.
            *  Ensure the profile file is written in valid Korn Shell
               script format.
            *  Ensure that all the host names defined in the
               db2nodes.cfg file in the sqllib directory are defined on
               the network and are running.
            *  Ensure that the DB2FCMCOMM registry variable specifies
               the correct IP format to use (TCPIP4 or TCPIP6).
            *  Ensure that the CF_MEM_SZ database manager configuration
               parameter is set to a valid value.

         2. Then, rerun the DB2START command.


   Related information:
   Communications variables
   Enabling the execution of remote commands (Linux and UNIX)
   cf_mem_sz - CF memory configuration parameter
