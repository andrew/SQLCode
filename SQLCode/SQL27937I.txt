

SQL27937I  Throughput: "<throughput>" records/sec.

Explanation: 

This is an informational message indicating the throughput of a given
DB2 agent.

User response: 

No action is required.
