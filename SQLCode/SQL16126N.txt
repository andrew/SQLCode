

SQL16126N  Document Type Definition (DTD) contains an element
      "<element-name>" in the content model that was not declared.

Explanation: 

While parsing a DTD the parser encountered an element with name
"<element-name>" in the content model that was not declared.

Parsing or validation did not complete.

User response: 

Correct the DTD and try the operation again.

sqlcode: -16126

sqlstate: 2200M
