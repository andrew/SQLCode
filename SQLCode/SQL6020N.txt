

SQL6020N  An import option was specified without also providing a
      database name.

Explanation: 

No database name was provided and an import option was specified.

The command has terminated.

User response: 

Retry the command and include a database name.
