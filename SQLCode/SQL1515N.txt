

SQL1515N  The user mapping cannot be created for server "<server-name>"
      because of a conflict with an existing user mapping or federated
      server option. Reason code "<reason-code>".

Explanation: 

A user mapping defined for PUBLIC cannot coexist on a server with user
mappings defined for individual users or on a server defined with the
federated server option FED_PROXY_USER. The attempt to create the user
mapping failed.

User response: 

The reason code "<reason-code>" indicates the particular situation. The
following actions can resolve the situation.

1        

         A user mapping for an individual user is being defined but
         server "<server-name>" already has a user mapping defined for
         PUBLIC. Drop the user mapping defined for PUBLIC from the
         server and create the user mapping again.


2        

         A user mapping for PUBLIC is being defined but server
         "<server-name>" already has a user mapping defined for an
         individual user. Drop all user mappings defined for individual
         users from the server and create the user mapping for PUBLIC
         again.


3        

         A user mapping for PUBLIC is being defined but server
         "<server-name>" is already defined with federated server option
         FED_PROXY_USER. Alter the server to remove the FED_PROXY_USER
         option and create the user mapping for PUBLIC again.

sqlcode: -1515

 sqlstate: 428HE
