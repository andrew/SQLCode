

SQL5103N  The entry in the database configuration file for the size of
      the buffer pool (buffpage) is too small for the maximum number of
      active applications (maxappls).

Explanation: 

The requested change would cause the maximum number of active
applications to be too large for the size of the buffer pool. The
following condition must always be true: 

  bufferpool_size > 
    (number of active_processes * 2)

The requested change is not made.

User response: 

Do one or both of the following: 
*  Increase the size of the buffer pool.
*  Decrease the maximum number of active processes allowed.
