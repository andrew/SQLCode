

SQL16088N  An "<expression-type>" expression has a binding of a
      namespace prefix "<prefix-string>" to namespace URI
      "<uri-string>", introduced to an element named "<element-name>",
      that conflicts with an existing namespace binding of the same
      prefix to a different URI in the in-scope namespaces of that
      element node. Error QName=err:XUDY0023.

Explanation: 

An "<expression-type>" expression has introduced a new namespace binding
for prefix "<prefix-string>" using URI "<uri-string>" into an element
node named "<element-name>" that conflicts with one of the existing
namespace bindings of the in-scope namespaces of that node. The element
node could be the target in the updating expression or the parent of the
target in the updating expression. For example, an insert expression
might insert an attribute into an existing element. If the QName of the
inserted attribute binds prefix P to some URI, but an in-scope namespace
of the element node binds the same prefix P to a different URI, a
conflict is detected and this error is raised.

User response: 

If the "<expression-type>" expression is intentionally introducing a new
namespace binding, modify the expression so that it uses a namespace
prefix that is different from all existing prefixes of the in-scope
namespaces of the element named "<element-name>". Alternatively, modify
the expression so that the binding of "<prefix-string>" uses the same
URI as the existing namespace binding of the in-scope namespaces of the
element named "<element-name>".

sqlcode: -16088

 sqlstate: 10708
