

SQL6111N  Cannot create a subdirectory under the path specified by
      newlogpath.

Explanation: 

When the newlogpath parameter is updated, the system attempts to create
a subdirectory under the specified path using the node name as the name
of the subdirectory. One of the following operating system errors
prevented the creation of the subdirectory: 
*  The file system or path does not have the appropriate permissions for
   file creation.
*  The file system does not have enough disk space.
*  The file system does not have enough file blocks or inodes.

The requested change is not made.

User response: 

Do one of the following, then try the request again: 
*  Ensure that the specified path exists and that the filesystem and
   path have read/write permissions.
*  Specify a different newlogpath.

If the problem persists, see your system administrator.
