

SQL22001W  Cannot find the default configuration for object
      "<object-name>". Returning install configuration for
      "<object-type>".

Explanation: 

The object does not have a specific configuration of its own so the
install configuration for that object type will be returned.

User response: 

If the behavior of the install configuration is correct, no action has
to be taken.
