

SQL5180N  DB2 is unable to read the federation configuration file
      "<file-name>".

Explanation: 

The federated configuration file could not be found, or it could not be
opened for reading.

User response: 

Specify the federation configuration file in the DB2_DJ_INI registry
variable. Be sure that the file exists and is readable. Ensure a fully
qualified path is specified for the location of the file.
