

SQL0390N  The function "<function-name>" resolved to specific function
      "<specific-name>" that is not valid in the context where it is
      used.

Explanation: 

A function resolved to a specific function that is not valid in the
context where it is used. If specific-name is an empty string, then the
function resolved to the built-in function identified by function-name.
Here is a list of some of the possible situations in which this message
can be returned:

*  The specific function is a table function where only a scalar,
   column, or row function is expected (such as creating a sourced
   scalar function).
*  The specific function is a scalar, column, or row function where only
   a table function is expected (such as in the FROM clause of a query).
*  The specific function is a row function where only a scalar or column
   function is expected.
*  The specific function is allowed only in restricted contexts but is
   referenced in a context that is not allowed for the function. The
   description of the function specifies the contexts in which the
   function is allowed.
*  The specified function has OUT or INOUT parameters and the context in
   which the function is used is not supported. A compiled function can
   be invoked only if the function invocation is the sole expression on
   the right hand side of a SET variable statement that is in a compound
   SQL (compiled) statement.
*  The specified function is a compiled SQL function and the context in
   which the function is used is not supported in a partitioned database
   environment. A compiled function can only be invoked in a partitioned
   database environment if the function invocation is the sole
   expression on the right hand side of a SET variable statement that is
   not in a compound SQL (inlined) statement.
*  The specified function is a generic table function, but a
   typed-correlation clause was not specified.
*  The specified function is not a generic table function, but a
   typed-correlation clause was specified.

The statement cannot be processed.

User response: 

Ensure that the correct function name and arguments are specified and
that the current path includes the schema where the correct function is
defined. You might need to change the function name, the current path
(using SET CURRENT FUNCTION PATH or the FUNCPATH bind option), or change
the context in which the function is used.

sqlcode: -390

sqlstate: 42887


   Related information:
   subselect
