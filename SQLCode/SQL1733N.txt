

SQL1733N  The command or operation failed because the specified master
      key label is too long.

Explanation: 

The maximum length of a master key label is 255 bytes. A command or
operation included a master key label with a length greater than 255
bytes.

User response: 

Check the command reference, specify a master key label with a valid
length, and retry the command.


   Related information:
   BACKUP DATABASE command
