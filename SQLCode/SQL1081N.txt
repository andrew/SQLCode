

SQL1081N  The address of the partner_lu name parameter is not valid.

Explanation: 

The application program has used an address that is not valid for the
partner_lu parameter. Either the address points to an unallocated buffer
or the character string in the buffer does not have a null terminator.

The command cannot be processed.

User response: 

Ensure that a valid address is used in the application program and the
input string is null terminated.
