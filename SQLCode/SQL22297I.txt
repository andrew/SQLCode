

SQL22297I  Configuration changes will not be effective until the DB2
      Administration Server is restarted.

Explanation: 

The DB2 Administration Server has been successfully configured, but the
changes will not take effect immediately. The changes will take effect
when the DB2 Administration Server is restarted.

User response: 

To have the change take effect at the next DB2 Administration Server
restart, no further action is required.
