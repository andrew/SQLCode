

SQL27993W  The STATISTICS USE PROFILE option of the Load utility was
      specified, however, a statistics profile does not exist.

Explanation: 

The statistics profile does not exist in the catalog table
SYSIBM.SYSTABLES. The statistics profile must be created before the load
is executed.

The load utility continues processing.

User response: 

To create a statistics profile, use the SET PROFILE or SET PROFILE ONLY
options of the RUNSTATS utility. Refer to the RUNSTATS documentation for
a detailed description of the utility options.
