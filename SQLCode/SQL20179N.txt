

SQL20179N  An INSTEAD OF trigger cannot be created because of how the
      view "<view-name>" is defined.

Explanation: 

An INSTEAD OF trigger cannot be defined on:

*  a view defined using WITH CHECK OPTION
*  a view on which such a view has been defined either directly or
   indirectly
*  a view on which such a view has been defined either directly or
   indirectly
*  a view that references an unfenced nickname and the Database
   Partitioning Feature is enabled

An INSTEAD OF UPDATE trigger cannot be defined on:

*  A view nested in a view defined with the WITH ROW MOVEMENT clause
*  The view may be the target view of the INSTEAD OF trigger or it may
   be a view that depends directly or indirectly on the target view of
   the trigger.
*  The statement cannot be processed. The INSTEAD OF trigger was not
   created.

User response: 

*  If the view is defined using WITH CHECK OPTION, remove the WITH CHECK
   OPTION clause.
*  If the view is nested in a view defined with the WITH ROW MOVEMENT
   clause, remove the WITH ROW MOVEMENT clause.
*  If the view references an unfenced nickname, specify a different
   view.

sqlcode: -20179

sqlstate: 428FQ


   Related information:
   INSTEAD OF triggers
