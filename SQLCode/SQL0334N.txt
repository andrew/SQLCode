

SQL0334N  Overflow occurred while performing conversion from codepage
      "<source>" to codepage "<target>". The maximum size of the target
      area was "<max-len>". The source string length was "<source-len>"
      and its hexadecimal representation was "<string>".

Explanation: 

During the execution of the SQL statement, a code page conversion
operation has resulted in a string that is longer than the maximum size
of the target object.

User response: 

Modify the data to avoid the overflow condition, depending on the
circumstances, by:

*  decreasing the length of the source string or increasing the size of
   the target object (refer to the note that follows this list),
*  altering the operation,
*  casting the encrypted data value to a VARCHAR string with a larger
   number of bytes before using it in a decryption function, or
*  ensuring that the application codepage and the database codepage are
   the same. This eliminates the need for codepage conversions for most
   connections.

Note: Automatic promotion of character or graphic string data types will
not occur as part of character conversion. If the resultant string
length exceeds the maximum length of the data type of the source string
then an overflow has occurred. To correct this situation either change
the data type of the source string or use data type casting to allow for
an increase in the string length due to conversion.

sqlcode: -334

sqlstate: 22524
