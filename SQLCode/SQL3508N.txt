

SQL3508N  Error in accessing a file or path of type "<file-type>" during
      load or load query. Reason code: "<reason-code>". Path:
      "<path/file>".

Explanation: 

An error occurred while trying to access a file during load or load
query processing. The utility stops processing.

User response: 

If doing a load and the table space is not in load pending state,
correct the problem and invoke the load utility again. If the table is
in load pending state, then invoke the load utility in RESTART or
REPLACE mode, or restore a backup of the table space(s). The state of
the table space can be determined using the LOAD QUERY command.

The following is a list of reason codes:

1        

         Unable to open the file.

          

         This could be caused by an incorrect file name or insufficient
         authority to access the file/directory. Correct the problem and
         either restart or rerun the load.

          

         The load temporary file could have been destroyed or the
         database could have been restored from an earlier backup. Load
         restart is not supported under these circumstances. Use load
         terminate to bring the table out of load pending state.


2        

         Unable to read/scan the file.

          

         This could be the result of a hardware error. If the error is a
         hardware error, take the appropriate action and restart or
         rerun the load.


3        

         Unable to write to or change size of the file.

          

         This could be the result of a disk full condition or a hardware
         error. Refer to the file type list provided later in this
         message, and either ensure there is enough space to run the
         load or specify a different location to be used. Restart or
         rerun the load. If the error is a hardware error, take the
         appropriate action an restart or rerun the load.


4        

         The file contains invalid data.

          

         A file required by the load contains incorrect data. See the
         action described for TEMPFILES_PATH.


5        

         Unable to close the file.

          

         If the load cannot be restarted or rerun, contact your IBM
         service representative.


6        

         Unable to delete the file.

          

         If the load cannot be restarted or rerun, contact your IBM
         service representative.


7        

         Parameter specified incorrectly. Refer to the list of file
         types to determine the parameter in error and rerun the load
         with a valid parameter.

The following is a list of file types:

SORTDIRECTORY
         

         Ensure that the workdirectory parameter is specified properly.
         There must be enough combined space in all the directories to
         hold twice the size of the index keys for the loaded data. For
         load insert and load restart there must also be room for twice
         the size of the index keys of the existing data in the table.


MSGFILE  

         Ensure that the messagefile parameter is specified properly.
         There must be enough disk space to write out the messages that
         occur during the load.

          

         If this is a load query, ensure that the local message file
         parameter is NOT the same as the messagefile parameter used for
         the load whose status is being queried.


TEMPFILES_PATH
         

         Ensure that the tempfiles path parameter is specified properly.


   Related information:
   LOAD command
