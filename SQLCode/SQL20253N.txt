

SQL20253N  The BEFORE trigger or generated column "<name>" cannot be
      created, altered, or executed because doing so would cause the
      table on which the BEFORE trigger or generated column is defined
      to be delete-connected to at least one ancestor table through
      multiple relationships with conflicting delete rules. The conflict
      is between the delete rules of constraints "<constraint-name1>"
      and "<constraint-name2>". Reason code = "<reason-code>".

Explanation: 

The definition for the BEFORE trigger or generated column "<name>" in
the CREATE TRIGGER, CREATE TABLE or ALTER TABLE statement is not valid
for the reason specified by the "<reason-code>" as follows:

1. The execution of the delete rule of constraint "<constraint-name1>"
   will fire the BEFORE trigger "<name>" and the body of this BEFORE
   trigger modifies a column that is part of the foreign key of
   constraint "<constraint-name2>" or modifies a column that is
   referenced by a generated column which is part of the foreign key of
   constraint "<constraint-name2>".
2. The execution of the delete rule of constraint "<constraint-name1>"
   will trigger the update of the generated column "<name>" and the
   generated column itself is part of the foreign key of constraint
   "<constraint-name2>".
3. With the addition of the BEFORE trigger or generated column,
   "<name>", the execution of both constraints "<constraint-name1>" and
   "<constraint-name2>" would cause an update of the same column.

The statement cannot be processed.

User response: 

The action corresponding to the reason code is:

1. Change the BEFORE trigger definition so that the BEFORE trigger will
   not be fired when the delete rule of constraint "<constraint-name1>"
   is executed, or change the body of the BEFORE trigger so that it does
   not modify a column that is part of the foreign key of constraint
   "<constraint-name2>" nor modify a column that is referenced by a
   generated column which is part of the foreign key of constraint
   "<constraint-name2>".
2. Change the generated column expression so that the generated column
   will not be updated when the delete rule of constraint
   "<constraint-name1>" is executed or change foreign key of constraint
   "<constraint-name2>" so that it does not include the generated
   column.
3. Change the BEFORE trigger definition or generated column expression
   so that the execution of both constraints "<constraint-name1>" and
   "<constraint-name2>" would not cause an update of the same column.

sqlcode: -20253

sqlstate: 42915


   Related information:
   BEFORE triggers
   Generated columns
