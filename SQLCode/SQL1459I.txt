

SQL1459I  Tools catalog migrated successfully to the current level.

Explanation: 

The db2tdbmgr command successfully migrated the database to the current
level.

User response: 

No response is required.
