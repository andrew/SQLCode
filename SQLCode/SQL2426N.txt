

SQL2426N  The database has not been configured to allow the incremental
      backup operation. Reason code = "<reason-code>".

Explanation: 

Incremental backups are not enabled for a table space until after
modification tracking has been activated for the database and a
non-incremental backup has been performed on the table space.

Possible reason codes:

1. The configuration parameter TRACKMOD has not been set for the
   database.
2. The TRACKMOD configuration parameter has been set but at least one
   table space has not had a non-incremental backup taken since the
   TRACKMOD parameter was set.

User response: 

The action is based on the reason code as follows:

1. Activate modification tracking for the database by setting the
   TRACKMOD database configuration parameter to on, then perform a full
   database backup.
2. Consult the db2diag log file to determine the name of the table
   space, then perform a full backup of that table space.
