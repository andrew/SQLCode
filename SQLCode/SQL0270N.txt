

SQL0270N  Function not supported (Reason code = "<reason-code>").

Explanation: 

The statement cannot be processed because it violates a restriction as
indicated by the following reason code:

1        

         The primary key, each unique constraint, and each unique index
         must contain all distribution columns of the table.


2        

         Updating the distribution key column value is not supported.


3        

         A foreign key cannot include any nullable distribution key
         columns when defined with ON DELETE SET NULL. This is a special
         case of reason code 2 because defining such a constraint would
         result in attempting to update a distribution key column.


5        

         Views created with the WITH CHECK OPTION clause should not use
         functions (or reference views that use functions) that:

          
         *  are non-deterministic
         *  have side effects
         *  are related to the placement of data, for example,
            nodenumber or partition functions

          

         These functions must also not be present within a referenced
         view if the new view is created with the CASCADED check option.


6        

         A transform cannot be defined for a user-defined distinct type.


7        

         Long fields can only be defined using a table space with a page
         size that is 4 KB. A LARGE TABLESPACE can only be created using
         a 4 KB page size.


8        

         Structured types are not supported as columns of a table or
         structured type attribute data types prior to DB2 Version 7.1.


9        

         Triggers are not supported on typed tables.


10       

         A single default table space cannot be selected because the
         table has one or more LOB columns that must be placed in a
         table space with a 4 KB page size and the rowsize or number of
         columns in the table requires a table space with an 8 KB page
         size.


11       

         A typed table or typed view cannot be created using a
         structured type that has no attributes.


12       

         The type of a source key parameter must be a user-defined
         structured type or a distinct type that is not sourced on the
         LOB, XML, LONG VARCHAR, or LONG VARGRAPHIC data type.


13       

         Check constraints cannot be defined on a typed table or the
         WITH CHECK OPTION clause cannot be specified on a typed view.


14       

         Referential constraints cannot be defined on a typed table or
         to a parent table that is a typed table.


15       

         A default value cannot be defined for reference type columns.


16       

         A reference data type or structured data type cannot be used as
         a parameter data type or a returns data type of a user-defined
         function prior to DB2 UDB Version 7.1. Otherwise, a scoped
         reference data type or an array data type cannot be used as a
         parameter data type or returns data type of a routine. A
         structured data type or an array data type cannot be used as a
         return column of a table or row function.


17       

         The SET INTEGRITY statement cannot be used for a typed table.


18       

         Column-level UPDATE and REFERENCES privileges cannot be granted
         on a typed table, typed view, or nickname.


19       

         A default value must be specified when defining a default for a
         column of a typed table.


20       

         ALTER TABLE is not supported for a materialized query table.


21       

         A column cannot be dropped or have its length, data type,
         security, nullability, or hidden attribute altered on a table
         that is a base table for a materialized query table.


22       

         Materialized query tables cannot be defined in a CREATE SCHEMA
         statement.


23       

         REPLICATED can be specified only for a materialized query table
         defined with REFRESH DEFERRED.


24       

         The triggered-action in a BEFORE trigger cannot reference a
         materialized query table defined with REFRESH IMMEDIATE.


25       

         Only one materialized query table can be specified for a SET
         INTEGRITY statement.


26       

         The database partition group being redistributed contains at
         least one replicated materialized query table.


27       

         A replicated materialized query table cannot be defined on a
         table that does not have a unique index existing on one or more
         columns that make up the replicated materialized query table.


28       

         A typed table or materialized query table cannot be renamed.


29       

         The FOR EXCEPTION clause cannot be specified with a
         materialized query table in the SET INTEGRITY statement.


30       

         Typed tables and typed views cannot be defined in a CREATE
         SCHEMA statement.


31       

         A distribution key cannot be defined with more than 500
         columns.


32       

         A table defined using a multipartition database partition group
         or a single-partition database partition group on other than
         the catalog partition does not support DATALINK columns defined
         with FILE LINK CONTROL.


33       

         An underlying table of a materialized query table defined with
         REFRESH IMMEDIATE cannot be the child of a referential
         constraint with a cascading effect (that is, with the option ON
         DELETE CASCADE or ON DELETE SET NULL).


34       

         The underlying object relational feature is not supported in
         the current release.


35       

         A sequence or an identity column cannot be created in a Version
         7 multinode database environment.


36       

         Activation of a multinode Version 7 database that contains
         sequences or identity columns is not allowed.


38       

         An index using an index extension is not supported in a
         multiple partition database partition group prior to DB2 UDB
         Version 8.1 FixPak 6.


39       

         Nicknames or OLE DB table functions cannot be referenced
         directly or indirectly in the body of an SQL function or SQL
         method.


40       

         The function IDENTITY_VAL_LOCAL cannot be used in a trigger or
         SQL function.


41       

         A single SQL variable statement cannot assign values to both a
         local variable and a transition variable.


42       

         The execution of a trigger, method, or function using SQL
         control statements and the execution of a dynamic compound
         statement in a multinode database are not allowed.


43       

         One or more of the specified options are currently not
         supported.


44       

         The following EXPLAIN MODES are not supported in MPP, SMP, and
         Data Joiner:

          
         *  COUNT CARDINALITIES
         *  COMPARE CARDINALITIES
         *  ESTIMATE CARDINALITIES


45       

         APPEND mode is not supported for multidimensional clustering
         (MDC) or insert time clustering (ITC) tables.


46       

         INPLACE table reorganization is not supported for
         multidimensional clustering (MDC) or insert time clustering
         (ITC) tables.


47       

         Index extensions are not supported for multidimensional
         clustering (MDC) or insert time clustering (ITC) tables.


48       

         Changes to the dimension specification of a multidimensional
         clustering (MDC) table are not supported.


49       

         Clustering indexes are not supported for multidimensional
         clustering (MDC) or insert time clustering (ITC) tables.


50       

         A user-defined temporary table cannot be a multidimensional
         clustering (MDC) or insert time clustering (ITC) table.


51       

         Issuing DDL operations that affect expression-based indexes
         from a database partition that is not the catalog database
         partition is not supported.


52       

         The expression of a generated column cannot be modified or
         added to a column that was not generated using an expression if
         that column is a distribution key column or was used in the
         ORGANIZE BY clause, the PARTITION BY clause, or the DISTRIBUTE
         BY clause.


53       

         A column with a LONG VARCHAR, LONG VARGRAPHIC, LOB, or XML
         type, a distinct type on any of these types, or a structured
         type cannot be specified in the select-list of a scrollable
         cursor.


54       

         INPLACE table reorganization is not supported for the specified
         system catalog table.


55       

         Federated database system support and the concentrator feature
         cannot be active at the same time.


56       

         Online index reorganization in rebuild mode is not supported
         for spatial indexes in ALLOW WRITE mode.


57       

         Online index reorganization is supported on multi-dimensionally
         clustered (MDC) or insert time clustering (ITC) tables in ALLOW
         WRITE mode only when the CLEANUP option or RECLAIM EXTENTS
         option is specified.


58       

         For a Version 8 database: the XML data type can be used only as
         a transient data type and cannot be stored in the database nor
         returned to an application.


59       

         A function or method that contains SQL statements cannot be
         used in a partitioned database environment.


60       

         The ALTER TABLE ALTER COLUMN SET INLINE LENGTH statement is not
         allowed because there is an object of type VIEW, either typed
         or untyped, that depends on the typed table.


61       

         A text search function cannot be used in the expression for
         check constraints or generated columns.


62       

         The WITH CHECK OPTION clause cannot be used with views that
         reference text search functions directly or depend on other
         views referencing text search functions.


63       

         A column with a LOB type, distinct type on a LOB type, A column
         with a LONG VARCHAR, LONG VARGRAPHIC, DATALINK, LOB, XML type,
         distinct type on any of these types, or structured type cannot
         be specified in the select-list of an insensitive scrollable
         cursor.


64       

         Federated processing is not supported on this platform.


65       

         Altering the nickname local type from the current type to the
         specified type is not allowed.


66       

         The built-in transform group SYSSTRUCT is not supported.


67       

         Nicknames or views on nicknames cannot be specified as target
         in the MERGE statement.


68       

         In a partitioned database, the maximum number of distinct NEXT
         VALUE expressions supported in an SQL statement is 55.


69       

         Delete from view would cause a descendent table to be
         delete-connected via multiple paths to two or more tables that
         appear in the view definition. Either a check constraint or
         trigger defined on the descendent table needs to be fired
         according to the final result which is not guaranteed.


70       

         A column cannot be dropped, or have its length, data type,
         security, or nullability altered on a table which is a base
         table of a view enabled for query optimization.


71       

         CALL statement cannot be used in a trigger, a SQL function, a
         SQL method or a dynamic compound statement in a partitioned
         database environment.


72       

         A nullable column cannot be changed to become an identity
         column.


73       

         Backup images in a partitioned database environment are not
         allowed to include logs.


74       

         Updating a status field in the recovery history file by time
         stamp is not allowed.


75       

         Automatic statistics profiling is not supported on a multiple
         database partition system, on a system where SMP is enabled, or
         on a federated system.


83       

         For versions of DB2 database prior to V9.7, the statement
         cannot be processed because DATA CAPTURE CHANGES and COMPRESS
         YES are not compatible.


87       

         The following types of tables cannot be defined as partitioned
         tables: typed tables, staging tables, user temporary tables,
         and range clustered tables.


88       

         For DB2 database servers Version 9.7 GA and earlier, the REORG
         INDEXES or REORG TABLE commands are only supported on a
         partitioned table in ALLOW WRITE or ALLOW READ mode in the
         following situations.

          
         *  ALLOW WRITE and ALLOW READ are supported for REORG INDEXES
            when CLEANUP or ON DATA PARTITION are specified.
         *  ALLOW READ is supported for REORG TABLE when ON DATA
            PARTITION is specified and the INPLACE clause is not
            specified.


89       

         REORG INDEX is only supported for nonpartitioned indexes on
         partitioned tables.


90       

         For versions of DB2 database prior to Version 9.7, the
         PARTITIONED clause cannot be specified on the CREATE INDEX
         statement because partitioned indexes are not supported.


91       

         For versions of DB2 database prior to V9.7, the statement
         cannot be processed because DATA CAPTURE CHANGES is not
         supported for a table that has a compression dictionary.


92       

         Detaching from a table that is the parent of an enforced
         referential integrity constraint is not allowed.


93       

         Detaching a partition is not allowed for partitions that
         contain data that was appended using LOAD INSERT, and that have
         dependent materialized query tables or dependent staging tables
         that have not been incrementally refreshed with respect to the
         appended data.


95       

         A table with an attached partition whose integrity has not been
         verified yet cannot be altered to be a materialized query
         table.


97       

         Altering this data type is not supported for an element of the
         multinode distribution key, data distribution key, or MDC
         organizing dimension.


98       

         Altering the type of an identity column is not supported.


99       

         An alter table set data type changed the external UDF used by a
         check constraint.


101      

         The LOAD command is not supported for a table with type-1
         indexes in a large table space. Starting in Version 9.7, type-1
         indexes have been discontinued and have been replaced with
         type-2 indexes. You can convert indexes to type-2 indexes by
         using the REORG INDEXES ALL command with the CONVERT parameter.


102      

         A security policy cannot be added to a typed table.


103      

         On DB2 database servers Version 9.5 and earlier, online index
         reorganization in ALLOW WRITE mode is not supported for tables
         with XML columns.


104      

         In place table REORG is not allowed if an index on an XML
         column is defined on the table.


105      

         The REORG INDEX command is supported only for block indexes in
         CLEANUP or RECLAIM EXTENTS mode.


106      

         Attaching a partition to a materialized query table is not
         supported.


109      

         A text search function cannot be applied to the text index of a
         partitioned table if the text search function does not directly
         reference the partitioned table or is a member of a sub-select
         that contains an OUTER JOIN clause.


110      

         For SECLABEL, SECLABEL_BY_NAME, and SECLABEL_TO_CHAR, the
         security policy name parameter must be a string constant.


111      

         An audit policy cannot be associated with a typed table.


112      

         The health monitor does not support configuration of actions
         and notifications on non-root installations.


113      

         Use of a Compound SQL (compiled) statement in the body of a
         trigger is not supported in partitioned database environments.
         Prior to DB2 Version 9.7 Fix Pack 1, use of a Compound SQL
         (compiled) statement as an SQL function body is not supported
         in partitioned database environments.


114      

         Partitioned spatial indexes are not supported.


115      

         The function cannot be revalidated.


116      

         The same name was used for more than one named parameter marker
         in a compound SQL (compiled) statement that is dynamically
         prepared or executed.


119      

         If a field of a ROW variable has an ARRAY type, then array
         element values cannot be directly retrieved by specifying the
         corresponding index value for the element on the ROW variable
         field reference.

User response: 

The action corresponding to the reason code is as follows:

1        

         Correct the CREATE TABLE, ALTER TABLE or CREATE UNIQUE INDEX
         statement.


2        

         Do not attempt to update the distribution key columns for a
         multipartition table, or consider deleting and then inserting
         the row with the new values in the distribution columns.


3        

         Make the distribution key column not nullable, specify a
         different ON DELETE action, or change the distribution key of
         the table so that the foreign key does not include any columns
         of the distribution key.


4        

         Either specify DATA CAPTURE NONE or ensure that the table is
         placed in a table space in a single-partition database
         partition group that specifies the catalog partition.


5        

         Do not use the WITH CHECK OPTION clause, or remove the function
         or view from the view definition.


6        

         Transforms are automatic for user-defined distinct types. Use
         the CREATE TRANSFORM statement for user-defined structured
         types only.


7        

         Use a table space with a 4 KB page size for any table that
         includes long fields. If you are using DMS table spaces, you
         can place long fields in a table space with a 4 KB page size
         with other table or index data in table spaces with a different
         page size. When defining a LARGE TABLESPACE, use PAGESIZE 4K.


8        

         For servers prior to DB2 UDB Version 7.1, ensure that no column
         data types are structured types in the CREATE TABLE statement
         or ALTER TYPE ADD COLUMN statement. Ensure that no attribute
         data types are structured types in the CREATE TYPE statement or
         ALTER TYPE ADD ATTRIBUTE statement.


9        

         Do not define triggers on typed tables.


10       

         Either reduce the row size or number of columns in the table or
         specify two table spaces such that the long data is in a table
         space with a 4 KB page size and the base data is in a table
         space with an 8 KB page size.


11       

         When creating a typed table or typed view, specify a structured
         type that has at least one attribute defined.


12       

         For the type of a source key parameter, use only a user-defined
         structured type or a distinct type that is not sourced on a
         LOB, XML, LONG VARCHAR, or LONG VARGRAPHIC type.


13       

         In a CREATE TABLE or ALTER TABLE statement for a typed table,
         do not specify check constraints. In a CREATE VIEW statement
         for a typed view, do not specify the WITH CHECK OPTION clause.


14       

         Do not specify referential constraints involving typed tables
         in a CREATE TABLE or ALTER TABLE statement.


15       

         Do not specify a DEFAULT clause for a column with a reference
         data type in a CREATE TABLE or ALTER TABLE statement.


16       

         For servers prior to DB2 UDB Version 7.1, do not specify a
         structured type parameter or returns type when creating a
         user-defined function. Otherwise, do not specify a scoped
         reference type as a parameter or returns type. Do not specify a
         structured type as a return column of a table or row function.


17       

         Do not specify a typed table in the SET INTEGRITY statement.


18       

         Do not include specific column names when granting REFERENCES
         or UPDATE privileges on a typed table, typed view, or nickname.


19       

         Include a specific value when specifying the DEFAULT clause on
         a column of a typed table.


20       

         Drop the materialized query table, and re-create it with the
         desired attributes.


21       

         To drop or alter a column in a table that is a base table for a
         materialized query table, perform the following steps:

          
         1. Drop the dependent materialized query table.
         2. Drop the column of the base table, or alter the length, data
            type, nullability, or hidden attribute of this column.
         3. Re-create the materialized query table.


22       

         Issue the CREATE SUMMARY TABLE statement outside of the CREATE
         SCHEMA statement.


23       

         Either remove the REPLICATED specification or ensure that
         REFRESH DEFERRED is specified for the materialized query table
         definition.


24       

         Remove the reference to the materialized query table in the
         triggered-action in the BEFORE trigger.


25       

         Issue separate SET INTEGRITY IMMEDIATE CHECKED statements for
         each materialized query table.


26       

         Drop all replicated materialized query tables in the database
         partition group, and then issue the REDISTRIBUTE DATABASE
         PARTITION GROUP command again. Re-create the replicated
         materialized query tables.


27       

         Ensure that a subset of the columns defined for the
         materialized query table also is the set of columns that make
         up a unique index on the base table.


28       

         You can change a typed table or materialized query table name
         only by dropping the table and creating it again with the new
         name. Dropping the table might have implications on other
         objects that depend on the table, and the privileges on the
         table are lost.


29       

         Remove the FOR EXCEPTION clause from the SET INTEGRITY
         statement.


30       

         Issue the CREATE statement for the typed view or typed table
         outside of the CREATE SCHEMA statement.


31       

         Reduce the number of columns in the distribution key.


32       

         Either specify NO LINK CONTROL for the DATALINK column or place
         the table in a table space on a single-partitioned database
         partition group that specifies the catalog partition. If you
         are redistributing data to a multiple-partition database
         partition group, you must drop the table to continue with the
         redistribute.


33       

         Use one of the following approaches:

          
         *  Do not define a referential constraint with cascading effect
            (that is, with option ON DELETE CASCADE or ON DELETE SET
            NULL) on an underlying table of a materialized query table
            defined with REFRESH IMMEDIATE as the child.
         *  Do not define a REFRESH IMMEDIATE materialized query table
            whose underlying table is the child of a referential
            constraint with cascading effect (that is, with option ON
            DELETE CASCADE or ON DELETE SET NULL).


34       

         Remove the use of any unsupported object relational features.


35       

         Do not create or remove the GENERATED [ALWAYS | BY DEFAULT] AS
         IDENTITY ... attribute.


36       

         Drop the new node or nodes to go back to a single node
         configuration. If you require more nodes, drop the sequences or
         tables with identity columns before adding new nodes.


38       

         An index using an index extension cannot be created on a table
         in a multiple partition database partition group. A database
         partition group cannot become a multiple partition database
         partition group while an index using an index extension exists
         on a table in the database partition group. Either drop any
         such indexes and add the partition to the database partition
         group, in which case the indexes cannot be re-created, or leave
         the database partition group unchanged.


39       

         Remove the reference to a nickname or OLE DB table function, or
         remove the reference to the object that indirectly references
         one of these.


40       

         Remove the invocation of the IDENTITY_VAL_LOCAL function from
         the trigger definition or the SQL function definition.


41       

         Split the assignment into two separate statements. One
         statement must assign values only to SQL variables, and the
         other statement must assign values only to transition
         variables.


42       

         Drop the new node or nodes to return to a single node
         configuration. If you require more nodes, you must drop the
         triggers, functions, or methods containing control statements.


43       

         Reissue the RUNSTATS command and turn off the unsupported
         option.


44       

         You cannot use these EXPLAIN modes in SMP, MPP, and Data Joiner
         environments. If possible, run the query in serial mode.
         Otherwise, set the EXPLAIN mode to YES or EXPLAIN to provide
         the same information except for the actual cardinalities.


45       

         Do not specify the APPEND clause in an ALTER TABLE statement
         for multi-dimensionally clustered (MDC) or insert time
         clustering (ITC) tables.


46       

         Reissue the REORG command without specifying the INPLACE
         option.


47       

         Do not specify the EXTENSION clause in a CREATE INDEX statement
         for multidimensional clustering (MDC) or insert time clustering
         (ITC) tables.


48       

         Drop the multidimensional clustering (MDC) table and re-create
         it with the modified dimension specification.


49       

         Do not specify the CLUSTER clause in a CREATE INDEX statement
         for multidimensional clustering (MDC) or insert time clustering
         (ITC) tables.


50       

         Do not specify the ORGANIZE BY clause in a CREATE TABLE
         statement for declared global temporary tables.


51       

         Issue the CREATE INDEX statement from the catalog database
         partition.


52       

         Do not modify the expression or add it to an existing column.
         To change the composition of the PARTITIONING KEY clause, the
         ORGANIZE BY clause, the PARTITION BY clause, the DISTRIBUTE BY
         clause, or the generating expression of any of its members that
         are generated columns, drop and re-create the table and then
         repopulate it.


53       

         Modify the select-list of the scrollable cursor to not include
         a column with these types.


54       

         Reissue the REORG command without specifying the INPLACE
         option.


55       

         Turn off concentrator or federated database system support.
         Turn off the concentrator by setting the value of the database
         manager configuration parameter MAX_CONNECTIONS to be less than
         or equal to the value of the database manager configuration
         parameter MAX_COORDAGENTS. Turn off federated database system
         support by setting the FEDERATED parameter in the database
         manager configuration to NO.


56       

         Reissue the REORG INDEXES command, specifying ALLOW NONE or
         ALLOW READ.


57       

         Reissue the REORG INDEXES command, specifying ALLOW NONE or
         ALLOW READ.


58       

         For a Version 8 database: Input the XML data to one of the
         functions that accept XML input (ultimately XMLSERIALIZE) and
         store the output of the function in the database or return it
         to the application.


59       

         In a partitioned database environment, only use functions and
         methods defined with the NO SQL option.


60       

         Drop the views that depend on the typed table. Reissue the
         ALTER TABLE ALTER COLUMN SET INLINE LENGTH statement for the
         typed table, and re-create the views that you dropped.


61       

         Do not use the text search function in the expression for check
         constraints or generated columns. Use the LIKE function instead
         of CONTAINS, if possible.


62       

         Do not specify the WITH CHECK OPTION clause for this view.


63       

         Modify the select-list of the scrollable cursor to not include
         a column with these types.


64       

         Do not attempt federated processing on this platform.


65       

         Do not attempt to alter the nickname local type from the
         current type to the specified type.


66       

         Do not specify SYSSTRUCT as a transform group.


67       

         Do not specify a nickname or a view on nicknames as a target in
         the MERGE statement.


68       

         Reduce the number of distinct NEXT VALUE expressions in the
         statement, or change to a non-partitioned database.


69       

         Do not use DELETE FROM view-name.


70       

         Disable the views enabled for query optimization, drop or alter
         the column of the base table and then enable the views for
         query optimization.


71       

         In a partitioned database environment, do not use the CALL
         statement in a trigger, an SQL function, an SQL method, or a
         dynamic compound statement.


72       

         The column cannot become an identity column while it is
         nullable. To make this change, the table must be dropped and
         re-created with a not null column and then repopulated. Adding
         a new column as the identity column might also be considered.


73       

         Issue the BACKUP command without specifying the INCLUDE LOGS
         option.


74       

         Update the status field in the recovery history file by EID
         only.


75       

         Use one of the following approaches:

          

         Disable automatic statistics profiling for this database by
         setting the database configuration parameters AUTO_STATS_PROF
         and AUTO_PROF_UPD to OFF.

          

         Change the system to one that is on a single database
         partition, that does not have SMP enabled, and that is not
         federated.


83       

         If both DATA CAPTURE CHANGES and COMPRESS YES are being
         specified, you must only specify one of them. If one of DATA
         CAPTURE CHANGES or COMPRESS YES is being specified, you must
         not specify it as the other is already in effect on the table.


87       

         Create the table as a non-partitioned table.


88       

         Reissue the REORG INDEXES or REORG TABLE command using the
         default or ALLOW NO ACCESS mode. If REORG TABLE was specified
         with the INPLACE clause, remove the INPLACE clause. If REORG
         INDEXES was specified, consider using the REORG INDEX command
         for online reorganization of the nonpartitioned indexes on a
         partitioned table.


89       

         Use the REORG INDEXES ALL command to reorganize all indexes on
         the table or data partition.


90       

         Create a nonpartitioned index by using the default or by
         explicitly specifying NOT PARTITIONED on CREATE INDEX.


91       

         Do not specify DATA CAPTURE CHANGES for this table.


92       

         Drop the referential constraint by using the following
         statement:

          

         ALTER TABLE 'child-table'

          
         DROP CONSTRAINT ...

          

         Alternatively, alter the child table in the foreign key
         relationship so that the foreign key constraint is not
         enforced:

          

         ALTER TABLE 'child-table'

          
         ALTER FOREIGN KEY ... NOT ENFORCED

          

         Note: The failing detach statement was executed from the parent
         table in the foreign key constraint, while the constraint
         enforcement is executed on the child table. These two tables
         are distinct unless the foreign key constraint is
         self-referential. Next, resubmit the ALTER TABLE ... DETACH
         PARTITION statement. You can now alter the child table to
         enforce the foreign key constraint:

          

         ALTER TABLE 'child-table'

          
         ALTER FOREIGN KEY ... ENFORCED

          

         This will recheck the tables to ensure that the foreign key
         relationship is still being enforced.


93       

         Issue the SET INTEGRITY statement with the IMMEDIATE CHECKED
         option to maintain the dependent materialized query tables or
         dependent staging tables with respect to the data that was
         appended using LOAD INSERT.


95       

         Issue the SET INTEGRITY statement with the IMMEDIATE CHECKED or
         IMMEDIATE UNCHECKED option on the table to verify the integrity
         of the attached partition.


97       

         Do not alter the column data type.


98       

         Drop the identity attribute, alter the type, and then re-enable
         the identity attribute.


99       

         Drop the check constraint, and then re-issue the ALTER
         statement.


101      

         Use the REORG INDEXES command with the CONVERT parameter to
         convert existing indexes on the table to type-2 indexes before
         issuing the LOAD command.


102      

         Do not add a security policy to a typed table.


103      

         On DB2 database servers Version 9.5 and earlier, reissue the
         REORG INDEXES command, specifying ALLOW READ ACCESS or ALLOW NO
         ACCESS.


104      

         Remove the INPLACE option and reissue the REORG TABLE command.


105      

         Reissue the REORG INDEX command specifying CLEANUP, or RECLAIM
         EXTENT, or issue REORG INDEXES ALL to reorganize all indexes
         (including the block indexes) for the table.


106      

         Issue the ALTER TABLE statement to drop the materialized query
         attribute from the table before attempting to attach a
         partition to the table.


109      

         Modify the query to place the text search function in a
         sub-select that directly references the partitioned table that
         contains the text index and that does not contain an OUTER JOIN
         clause.


110      

         Provide a string constant for the security policy name
         parameter.


111      

         Do not associate an audit policy with a typed table.


112      

         Do not try to configure actions or notifications for the health
         monitor on non-root installations.


113      

         If possible, define the trigger using a compound SQL (inlined)
         statement, or remove the trigger definition. For DB2 Version
         9.7 prior to Fix Pack 1, define the function or trigger using a
         compound SQL (inlined) statement, or remove the function or
         trigger definition.


114      

         Create the spatial index as a nonpartitioned index by using the
         NOT PARTITIONED clause on the CREATE INDEX statement.


115      

         Record the privileges granted to the function in
         SYSCAT.ROUTINEAUTH, drop the function, recreate the function,
         and then grant the privileges to the function.


116      

         Give each named parameter marker in a compound SQL (compiled)
         statement that is dynamically prepared or executed a unique
         name.


119      

         Do not specify an index value when retrieving element values
         when the field of a ROW variable has an ARRAY type.

sqlcode: -270

sqlstate: 42997


   Related information:
   Installation requirements for DB2 database products
