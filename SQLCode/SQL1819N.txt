

SQL1819N  The DROP SERVER statement that you submitted could not be
      processed.

Explanation: 

The DROP SERVER statement is preceded in a unit of work by a SELECT
statement that references a nickname for a table or view within the data
source (or category of data sources) that the DROP SERVER statement
references.

User response: 

Let the unit of work finish; then resubmit the DROP SERVER statement.

 sqlcode: -1819

 sqlstate: 55006
