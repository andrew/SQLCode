

SQL0304N  A value cannot be assigned to a host variable because the
      value is not within the range of the host variable's data type.

Explanation: 

A FETCH, VALUES, SELECT, or assignment into a host variable list failed
because the host variable was not large enough to hold the retrieved
value.

The statement cannot be processed. No data was retrieved.

User response: 

Verify that table definitions are current and that the host variable has
the correct data type. For the ranges of SQL data types, refer to the
SQL Reference.

Federated system users: for the ranges of data types that are returned
from a data source, refer to the documentation for that data source.

 sqlcode: -304

 sqlstate: 22001, 22003
