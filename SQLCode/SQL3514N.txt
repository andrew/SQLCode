

SQL3514N  A utility system error occurred. Function code: "<function>".
      Reason code: "<reason-code>". Error code: "<error-code>".

Explanation: 

A system error occurred during database utility processing.

User response: 

Different actions are required depending on the value of "<function>".

The possible function codes are: 
*  1 - An error occurred while Load was sorting. 

   Try to restart the load. If the error persists, provide the function,
   reason code and error code to your technical service representative.

*  2 - An error occurred using the vendor sort utility. 

   Try the load again using the IBM Data Server Client/DB2 Server sort
   utility instead of the vendor sort. To do this, reset the Profile
   Registry value at the server to blank. You may have to restart the
   Database Manager in order to pick up the new Profile Registry value.
   If the error persists, provide the function, reason code and error
   code to your vendor sort technical service representative.
