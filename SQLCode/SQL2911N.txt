

SQL2911N  Binary field types can be specified only when the format is
      POSITIONAL.

Explanation: 

The INGEST command specified a binary field type, but the input file
format is not POSITIONAL. The command failed.

User response: 

One of the following:

*  If the input data is in delimited format, change the field type to
   one that specifies character data. For example, if the field type is
   INTEGER, change it to INTEGER EXTERNAL. If the field type is
   DB2SECURITYLABEL, change it to DB2SECURITYLABEL NAME or
   DB2SECURITYLABEL STRING.
*  If the input data is in positional format, change the INGEST command
   to specify FORMAT POSITIONAL. If needed, add the POSITION clause to
   each field definition.
