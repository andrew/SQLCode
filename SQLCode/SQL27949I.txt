

SQL27949I  The distribution data file specified at line "<linenum>" of
      the configuration file is ignored.

Explanation: 

The distribution file specified at line "<linenum>" of the configuration
file is ignored. The command line option will be used if it is
specified; otherwise, the first specification of distribution file in
the configuration file will be used.

User response: 

No action is required.
