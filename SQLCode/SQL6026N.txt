

SQL6026N  Database with catalog node "<node1>" cannot be restored to a
      database with catalog node "<node2>".

Explanation: 

In an environment that is not a DB2 pureScale environment, the catalog
node can exist on only one node and there is a discrepancy between the
backup image and the node being restored to. This can occur in the
following cases:

*  The backup image specified catalog node "<node1>" and the restore was
   attempted on an existing database whose catalog node is node
   "<node2>".
*  The restore was attempted to a new database and the catalog node has
   not been restored first. (Restore the catalog node first to create
   the database on all nodes).

User response: 

Verify that the correct backup image is being restored.

If you are restoring to an existing database and want to change the
catalog node to "<node2>", the existing database must be dropped first.

If you are restoring to a new database, restore the catalog node
"<node1>" first.
