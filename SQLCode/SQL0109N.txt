

SQL0109N  The statement or command was not processed because the
      following clause is not supported in the context where it is used:
      "<clause>".

Explanation: 

This message is returned when an SQL statement or command uses a clause
in a way that is not supported.

There are many different scenarios in which this message can be
returned. Some examples of scenarios in which this message can be
returned include the following:

*  A subquery, an INSERT statement, or a CREATE VIEW statement contains
   one of the following clauses: INTO, ORDER BY, or FOR UPDATE.
*  The RAISE_ERROR function was used as a select list item but the
   result of the RAISE_ERROR function was not cast to an appropriate
   data type.
*  A subselect isolation or lock request clause was specified in an XML
   context.
*  Federated environments only: A data source-specific restriction was
   violated in a pass-through session.

User response: 

Remove the clause from the statement or command and then resubmit the
statement or rerun the command.

Federated system users:
         

         For a pass-through session, perform the following
         troubleshooting steps:

          
         1. Determine what data source is causing the error.
         2. Examine the SQL dialect for that data source to determine
            which specific restriction has been violated.
         3. Adjust the failing statement as needed.
         4. Resubmit the statement or rerun the command.

sqlcode: -109

sqlstate: 42601


   Related information:
   Troubleshooting data source connection errors
   SQL statements
