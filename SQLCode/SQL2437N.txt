

SQL2437N  The data movement command failed because the utility was
      unable to resolve how implicitly hidden columns should be
      processed. No data was moved.

Explanation: 

You can move data into and out of DB2 databases, using utilities such as
LOAD, IMPORT, INGEST, and EXPORT.

There are several ways to specify how data movement utilities should
handle implicitly hidden columns:

*  Explicitly specify the list of columns to include in the data
   movement operation.
*  Specify the hidden column-related parameter in the data movement
   command.
*  Set the hidden column-related DB2 registry variable to indicate how
   all data movement utilities should handle hidden columns.

This message is returned when an attempt is made to move data into or
out of one or more tables that contain implicitly hidden columns, and
the data movement utility cannot determine whether the implicitly hidden
columns should be included in the data movement operation because none
of the methods described have been used to specify how hidden columns
should be handled.

User response: 

Run the utility again, specifying how implicitly hidden columns should
be handled using appropriate command parameters or DB2 registry
variables.


   Related information:
   Export utility overview
   Import overview
   Load overview
   INGEST command
   Hidden columns
