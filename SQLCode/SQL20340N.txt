

SQL20340N  The XML schema "<xmlschema-name>" includes at least one XML
      schema document in namespace "<namespace>" with component ID
      "<component-id>" that is not connected to the other XML schema
      documents in the same namespace using an include or redefine.

Explanation: 

The XML schema identified by identifier "<xmlschema-name>" includes
multiple XML schema documents in the namespace "<namespace>". At least
one of these XML schema documents is not connected to the other XML
schema documents in the same namespace using an include or redefine. One
such XML schema document is identified in the XML schema repository with
component identifier "<component-id>".

The statement cannot be processed.

User response: 

Correct the XML schema documents so that all XML schema documents within
a namespace are connected using an include or a redefine. The
"<component-id>" can be used to query SYSCAT.XSROBJECTCOMPONENTS for
further information about the specifically reference XML schema document
that is not connected within the namespace.

sqlcode: -20340

sqlstate: 22534
