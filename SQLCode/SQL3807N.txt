

SQL3807N  Instance or database "<name>" quiesce is pending.

Explanation: 

Another user has submitted the quiesce command and it has not yet
completed.

User response: 

Wait for the quiesce to complete.
