

SQL20436N  The data type specified for an array is not valid.

Explanation: 

The data type specified for an array in the CREATE TYPE statement, array
constructor, argument to UNNEST, argument to ARRAY_AGG, or target of
ARRAY_AGG is not valid. The following restrictions apply when specifying
a data type.

*  The following data types are not supported: 
   *  LONG VARCHAR
   *  LONG VARGRAPHIC
   *  REFERENCE
   *  XML
   *  BOOLEAN (prior to Version 9.7.5)
   *  user-defined data types other than row data type and array data
      type

*  The array index data type for an associative array must be INTEGER or
   VARCHAR.
*  An argument to UNNEST cannot be a nested array.
*  An argument to ARRAY_AGG and the target of ARRAY_AGG cannot be a
   nested array.
*  The target of an ARRAY_AGG cannot be a nested array.
*  ARRAY types or ROW types can be nested as elements in other ARRAY
   types, but there is a maximum nesting level which must not be
   exceeded.

User response: 

Ensure that the data type specified in a CREATE TYPE (array) statement,
an array constructor, the argument to UNNEST, the argument to ARRAY_AGG,
or as the target of ARRAY_AGG is supported.

sqlcode: -20436

sqlstate: 429C2


   Related information:
   ARRAY_AGG aggregate function
   CREATE TYPE (array) statement
   UNNEST table function
