

SQL3104N  The Export utility is beginning to export data to file
      "<name>".

Explanation: 

This is the normal beginning message.

User response: 

No action is required.
