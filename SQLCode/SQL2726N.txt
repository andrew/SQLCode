

SQL2726N  There is no partitioning key defined.

Explanation: 

At least one partitioning key must be defined.

User response: 

Specify one or more partitioning keys.

Altering the distribution key can only be done to a table whose table
space is associated with a single-partition database partition group.

You can add or drop distribution keys, using the ALTER TABLE statement.

You can also use the Design Advisor to migrate from a single-partition
to a multiple-partition database. Search the Information Center for a
topic called: "Using the Design Advisor to migrate from a
single-partition to a multiple-partition database".
