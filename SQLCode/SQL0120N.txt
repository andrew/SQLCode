

SQL0120N  Invalid use of an aggregate function or OLAP function.

Explanation: 

An aggregate function or OLAP function can only be used in the select
list of a fullselect, the having clause, or, with restrictions, in a
WHERE clause or GROUP BY clause.

A WHERE clause can contain an aggregate function or OLAP function only
if that clause appears within a subquery of a HAVING clause and the
argument of the function is a correlated reference to a group.

A GROUP BY clause can contain an aggregate function or OLAP function
only if the argument of the function is a correlated reference to a
column in a different subselect than the one containing the GROUP BY
clause.

An OLAP function cannot be used within the argument list of an XMLQUERY
or XMLEXISTS expression.

The statement cannot be processed.

User response: 

Change the statement so that the aggregate function or OLAP function is
not used or used only where it is supported.

 sqlcode: -120

 sqlstate: 42903
