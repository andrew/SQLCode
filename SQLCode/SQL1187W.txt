

SQL1187W  The database was created or upgraded successfully but an error
      occurred while creating the detailed deadlocks event monitor
      "<event-monitor-name>". The detailed deadlocks event monitor was
      not created.

Explanation: 

The CREATE DATABASE or UPGRADE DATABASE command could not create a
detailed deadlocks event monitor for the created or upgraded database.

User response: 

Create a detailed deadlocks event monitor if desired.
