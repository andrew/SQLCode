

SQL3400N  The method specified in METHOD is not valid for Non-Delimited
      ASCII files. It must be 'L' for locations.

Explanation: 

When loading from a non-delimited ASCII file, columns must be selected
by locations in the file.

The command cannot be processed.

User response: 

Resubmit the command with a valid set of locations for the columns in
the source file.
