

SQL0295N  The combined length for all container names for the table
      space is too long.

Explanation: 

The total space required to store the list of containers exceeds the
space allotted for this table space in the table space file.

User response: 

Try one or more of the following: 
*  Use symbolic links, mounted file systems, etc. to shorten the new
   container names.
*  Back up the table space and then use the database administration
   utility to reduce the number and/or name lengths of the containers.
   Restore the table space to the new containers.

 sqlcode: -295

 sqlstate: 54034
