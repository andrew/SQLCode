

SQL0975N  A new transaction could not be started because database or
      instance "<name>" is quiesced by user "<username>". Quiesce type:
      "<type>".

Explanation: 

Another user has quiesced the instance or database that you are
attempting to use, and no new transactions are allowed until the
instance or database is no longer in the quiesced state.

Quiesce type "<type>" refers to the instance or database already
quiesced and is a '1' for an instance and a '2' for a database.

User response: 

Contact the user who currently has the instance or database quiesced to
determine when DB2 will no longer be quiesced, and retry the request at
that time.

 sqlcode: -975

 sqlstate: 57046
