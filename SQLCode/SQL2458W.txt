

SQL2458W  The value of registry variable DB2_DATABASE_CF_MEMORY was
      changed after the first database activation. A database manager
      restart is required.

Explanation: 

Changing the setting of registry variable DB2_DATABASE_CF_MEMORY
requires a database manager restart for the new value to take effect.
Until the database manager is restarted, the databases are not aware of
the new value and will not have their CF memory configurations adjusted
according to the new value.

User response: 

Restart the database manager using the DB2STOP and DB2START commands.


   Related information:
   START DATABASE MANAGER command
   STOP DATABASE MANAGER command
   CF self-tuning memory parameter configuration
