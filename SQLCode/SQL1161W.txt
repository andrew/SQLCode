

SQL1161W  Reconcile processing failed. DataLink column(s) not defined on
      DB2 DataLinks Manager(s). Check the administration notification
      log for details.

Explanation: 

Metadata information about one of more DataLink columns of the table are
missing on DB2 DataLinks Managers. Reconcile processing has failed.
Table is placed in DataLink Reconcile Not Possible (DRNP) state.

User response: 

To take the table out of DataLink Reconcile Not Possible state, follow
the procedure mentioned under "Removing a table from the DataLink
Reconcile Not Possible state" in the Administration Guide.
