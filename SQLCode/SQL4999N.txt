

SQL4999N  A Precompiler Services or Run-time Services error occurred.

Explanation: 

A database manager error occurred that prevents Precompiler Services or
Run-Time Services from processing function calls.

No Precompiler Services or Run-Time Services function calls can be
processed.

User response: 

Record the message number (SQLCODE) and all error information from the
SQLCA if possible.

If trace was active, invoke the Independent Trace Facility at the
operating system command prompt. 
*  Environment: Outer Precompiler Using Precompiler Services API
*  Required information: 
   *  Problem description
   *  SQLCODE
   *  SQLCA contents if possible
   *  Trace file if possible.
