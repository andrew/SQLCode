

SQL0508N  The cursor specified in the UPDATE or DELETE statement is not
      positioned on a row.

Explanation: 

The program attempted to execute an UPDATE or DELETE WHERE CURRENT OF
cursor statement when the specified cursor was not positioned on an
object table row. The cursor must be positioned on the row to be updated
or deleted.

The cursor is no longer positioned on a row if the row is deleted. This
includes any use of cursors within a savepoint when a ROLLBACK TO
SAVEPOINT is performed.

Federated system users: the record in a remote data source has been
updated and/or deleted by another application (or a different cursor
within this application) and the record no longer exists.

The statement cannot be processed. No data is updated or deleted.

User response: 

Correct the logic of the application program to ensure that the cursor
is correctly positioned on the intended row of the object table before
the UPDATE or DELETE statement is executed. Note that the cursor is not
positioned on a row if FETCH returned message SQL0100W (SQLCODE = 100).

 sqlcode: -508

 sqlstate: 24504
