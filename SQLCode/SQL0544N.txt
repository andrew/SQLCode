

SQL0544N  The check constraint "<constraint-name>" cannot be added
      because the table contains a row that violates the constraint.

Explanation: 

At least one existing row in the table violates the check constraint
that is be added in the ALTER TABLE statement.

The statement cannot be processed.

User response: 

Examine the check constraint definition that was specified in the ALTER
TABLE statement and the data in the table to determine why there is a
violation of the constraint. Change either the check constraint or the
data so that the constraint is not violated.

 sqlcode: -544

 sqlstate: 23512
