

SQL3077N  The number of C records specified in the CCNT field in the T
      record "<value>" exceeds the maximum allowed "<maximum>".

Explanation: 

The value in the CCNT field of the T record is larger than the maximum
allowed for the indicated release.

The utility stops processing. No data is loaded.

User response: 

Examine the CCNT field in the T record.
