

SQL29015N  Error encountered during job cancellation. Reason code =
      "<reason-code>".

Explanation: 

An error was encountered while trying to cancel a job. The reason code
maps to an SQL or DB2 message.

User response: 

Examine the reason code, correct the error, and retry the action again.
