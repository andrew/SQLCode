

SQL16167N  XML document contained NDATA for a parameter entity.

Explanation: 

While parsing an XML document, the parser encountered NDATA for a
parameter entity. NDATA is not legal for parameter entities.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16167

sqlstate: 2200M
