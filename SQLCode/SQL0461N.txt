

SQL0461N  A value with data type "<source-data-type>" cannot be CAST to
      type "<target-data-type>".

Explanation: 

The statement contains a CAST with the first operand having a data type
of "<source-data-type>" to be cast to the data type
"<target-data-type>". This cast is not supported.

User response: 

Change the data type of either the source or target so that the cast is
supported. For predefined data types these are documented in the SQL
Reference. For a cast involving a user-defined distinct type, the cast
can be between the base data type and the user-defined distinct type or
from a data type that is promotable to the base data type to the
user-defined distinct type.

 sqlcode: -461

 sqlstate: 42846
