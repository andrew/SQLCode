

SQL1668N  The operation failed because the operation is not supported
      with this environment. Reason code: "<reason-code>".

Explanation: 

This message is returned when an attempt is made to perform an operation
that is not supported in a specific environment. The reason code
indicates why the operation failed:

1        

         The database includes one or more column-organized tables and
         the operation tried to enable or use functionality that is not
         supported with column-organized tables.


2        

         An attempt was made to create a column-organized table in a
         database that is configured to use rollforward recovery.


4        

         An attempt was made to create a column-organized table in an
         operating system environment that is not supported with
         column-organized tables.


5        

         An attempt was made to use column-organized tables in an
         environment in which intrapartition parallelism is disabled.


6        

         An attempt was made to use column-organized tables in a
         partitioned database environment.


7        

         An attempt was made to use column-organized tables in a DB2
         pureScale environment.


8        

         An attempt was made to create or access a column-organized
         table in an XA transaction.


9        

         This message is returned with reason code 9 when an attempt is
         made to use self-tuning for sort memory with column-organized
         table functionality:

          
         *  An attempt is made to set one or both of the following
            configuration parameters to "AUTOMATIC" when there are one
            or more column-organized tables in the database: SORTHEAP,
            SHEAPTHRES_SHR.
         *  An attempt is made to create a column-organized table while
            one or both of the following configuration parameters was
            set to "AUTOMATIC": SORTHEAP, SHEAPTHRES_SHR.


10       

         An attempt was made to create a replication-maintained
         materialized query table in a partitioned database environment
         or in a DB2 pureScale environment.

User response: 

Respond to this error according to the reason code:

1        

         To enable functionality that is not supported with
         column-organized tables, drop and recreate existing
         column-organized tables as row-organized tables.


2        

         Disable rollforward recovery and then re-issue the statement,
         or create the table as a row-organized table.


5        

         Enable intrapartition parallelism and then execute the
         operation again. Ensure that enough shared sort heap is
         available by setting the DB2_WORKLOAD registry variable to
         ANALYTICS or setting the sheapthres database manager
         configuration parameter to 0.


4, 6, and 7
         

         Create the table as a row-organized table.


8        

         Respond to reason code 8 in the following ways:

          
         *  Create or access column-organized tables in only non-XA
            transactions.
         *  In XA transactions, create or access only row-organized
            tables.


9        

         Respond to reason code 9 in the following ways:

          
         *  To use column-organized tables, or to create a
            column-organized table), disable self-tuning for sort memory
            by setting the SORTHEAP and SHEAPTHRES_SHR configuration
            parameters to appropriate, numerical values instead of
            "AUTOMATIC".
         *  To use self-tuning for sort memory, use row-organized tables
            instead of column-organized tables.


10       

         To create a materialized query table in a partitioned database
         environment or a DB2 pureScale environment, do not specify
         MAINTAINED BY REPLICATION when creating the materialized query
         table.

sqlcode: -1668

sqlstate: 56038


   Related information:
   Dropping a table
   Manageability enhancements
   Creating column-organized tables
   Enabling intrapartition parallelism for queries
   Rollforward recovery
   INSERT, UPDATE, and DELETE (including MERGE) statement restrictions
   for column-organized tables
