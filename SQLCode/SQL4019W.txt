

SQL4019W  Completed processing the preprocessed file
      "<preprocessed-file>".

Explanation: 

The precompiler has completed processing the preprocessed file.

User response: 

No action is required.
