//
//  MasterViewController.swift
//  SQLCode
//
//  Created by Andrew on 15/7/12.
//  Copyright (c) 2015年 Andrew. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController,UISearchBarDelegate {

    @IBOutlet weak var searchBar:UISearchBar!
    var objects = [AnyObject]()
    var showData = [AnyObject]()
    //var searchController: UISearchController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showData=self.objects
        
        // Do any additional setup after loading the view, typically from a nib.
        
        //self.navigationItem.leftBarButtonItem = self.editButtonItem()

        let settingButton=UIBarButtonItem(title: "设置", style: UIBarButtonItemStyle.Plain, target: self, action: "settings:")
        let searchButton = UIBarButtonItem(barButtonSystemItem: .Search, target: self, action: "searchButton:")
        self.navigationItem.leftBarButtonItem=settingButton
        self.navigationItem.rightBarButtonItem = searchButton
        
        readDir()
        
    }

    func applicationDocumentPath() -> String {
        let application = NSSearchPathForDirectoriesInDomains (.DocumentDirectory,.UserDomainMask, true )
        let documentPathString = application[0] 
        return documentPathString
    }
    
    func settings(sender:AnyObject){
        self.performSegueWithIdentifier("Settings", sender: self)
    }
    
    func readDir(){
        let mainBundle = NSBundle.mainBundle()
        let filelist:NSArray = mainBundle.pathsForResourcesOfType(".txt", inDirectory: "")
        for filePath in filelist {
            let path:NSString = (filePath as? NSString)!
            let pathURL = NSURL(fileURLWithPath: path as String).URLByDeletingPathExtension?.lastPathComponent
            self.insertCode(pathURL!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func insertCode(code: NSString) {
        showData.insert(code, atIndex: showData.count)
        objects.insert(code, atIndex: objects.count)
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }
    
    func searchButton(sender: AnyObject) {
        self.tableView.scrollsToTop=true
        self.searchBar.becomeFirstResponder()
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = showData[indexPath.row] as! NSString
            (segue.destinationViewController as! DetailViewController).detailItem = object
            }
        }
    }

    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        //println("开始搜索")
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        //println("开始录入")
        searchBar.showsCancelButton = true
        searchBar.showsScopeBar = true
        return true
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        //println("点击取消按钮")
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        self.showData=self.objects
        self.tableView.reloadData()
        
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.showData = self.objects
        }
        else {
            self.showData = []
            for ctrl in self.objects {
                if ctrl.lowercaseString.componentsSeparatedByString(searchText).count>1 {
                    self.showData.append(ctrl)
                }
            }
        }
        self.tableView.reloadData()
    }
    
    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showData.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) 

        let object = showData[indexPath.row] as! NSString
        cell.textLabel!.text = object.description
        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
}

