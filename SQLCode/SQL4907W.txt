

SQL4907W  Database "<name>" is recovered but one or more of the tables
      in the table space list included for the rollforward operation are
      placed in the Set Integrity Pending state.

Explanation: 

One or more of the tables involved in the point-in-time tablespace
recovery have referential constraints with tables outside of the table
space list used for recovery, or have dependent materialized query
tables or dependent staging tables outside of the table space list used
for recovery. All these tables are placed in the Set Integrity Pending
state. Rollforward operation has otherwise completed successfully.

User response: 

Check the state for the tables in the table spaces and take appropriate
actions if necessary.
