

SQL0391N  Invalid use of the row based function "<function-name>".

Explanation: 

The statement uses a row based function "<function_name>" that cannot be
used for one of the following reasons: 
*  The function is used in a GROUP BY or a HAVING clause but is not also
   included in the select list.
*  The function cannot be used in this context because of the recursive
   nature of the statement.
*  The function cannot be used in a check constraint.
*  The function cannot be used in a generated column.
*  The function cannot be used in a view definition where the WITH CHECK
   OPTION clause is specified or any view dependent on such a view
   having the WITH CHECK OPTION clause specified.
*  The function has an argument that does not resolve to a row of a base
   table. This would include the situation involving a result column of
   an outer join where NULL producing rows are possible.
*  The function cannot be used on rows from a replicated materialized
   query table.

The statement cannot be processed.

User response: 

Remove "<function-name>" from the context where it is not allowed.

 sqlcode: -391

 sqlstate: 42881
