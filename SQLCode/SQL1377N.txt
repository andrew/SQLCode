

SQL1377N  Creating or altering the sourced procedure is not supported at
      this data source.

Explanation: 

The sourced procedure cannot be created or altered at this data source.

User response: 

Submit the statement at a supported data source.

 sqlcode: -1377

 sqlstate: 560CL
