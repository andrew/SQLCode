

SQL1279W  Some indexes may not have been recreated.

Explanation: 

An error occurred during index recreation while performing a database
restart or following a table reorganization which may have prevented
some of the indexes from being recreated successfully. Details can be
found in administration notification log.

Database restart or Reorg table was successful.

User response: 

Examine the administration notification log to determine why the
index(es) could not be recreated and correct the problem. The invalid
indexes for a table will be recreated when the table is first accessed.
