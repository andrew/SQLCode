

SQL16034N  The QName "<qname>" is used as an atomic type in a sequence
      type, but is not defined in the in-scope schema type definitions
      as an atomic type. Error QName=err:XPST0051.

Explanation: 

The QName "<qname>" cannot be used as an atomic type because it is not
defined in the in-scope schema type definitions as an atomic type. Error
QName=err:XPST0051.

User response: 

Use a QName that is defined as an atomic type.

 sqlcode: -16034

 sqlstate: 10503
