

SQL22207N  The DB2 Administration server cannot execute the script at
      host "<hostname>". Reason code "<reason-code>".

Explanation: 

The DB2 Administration Server failed to execute the script for one of
the following reasons: 
1. The user specified an existing script and the script does not exist.
2. The script working directory is invalid.
3. The last line of the script failed to run because a statement
   termination character could not be found.
4. A system error occurred while attempting to execute the script.

User response: 

Depending on the reason for the failure, attempt one of the following: 
1. Verify that the script specified exists on host "<hostname>" at the
   path provided.
2. Verify that the working directory is valid on host "<hostname>".
3. Verify the script contents and resubmit the request.
4. Refer to the DB2 Administration Server's First Failure Data Capture
   Log for additional information.

If you continue to receive this error message after attempting the
suggested response, refer to the DB2 Administration Server's First
Failure Data Capture Log for additional information or contact IBM
Support.
