

SQL0845N  A PREVIOUS VALUE expression cannot be used before the NEXT
      VALUE expression generates a value in the current session for
      sequence "<sequence-name>".

Explanation: 

A PREVIOUS VALUE expression specified sequence "<sequence-name>", but a
value has not yet been generated for this sequence. A NEXT VALUE
expression must be issued in this session to generate a value for this
sequence before a PREVIOUS VALUE expression for the sequence can be
issued.

User response: 

Issue at least one NEXT VALUE expression for a sequence before issuing
any PREVIOUS VALUE expression for the same sequence in a session.

 sqlcode: -845

 sqlstate: 51035
