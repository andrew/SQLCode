

SQL1318N  The length of element "<name>" in the input structure for
      parameter 1 is not valid.

Explanation: 

The length values in the Database Connection Services directory entry
structure should not be less than zero nor greater than the maximum
length for the element it represents.

The function cannot be processed.

User response: 

If an element of the directory entry structure is specified, the
associated length value should represent the number of bytes in the
element. Otherwise, the length value should be zero. Ensure that all the
directory entry structure elements have the required specifications and
lengths for the command and resubmit the command.
