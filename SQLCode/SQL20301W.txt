

SQL20301W  The table space "<tablespace-name1>" does not have the same
      prefetch size as "<tablespace-name2>".

Explanation: 

All data table spaces used by a partitioned table should have the same
prefetch size for optimal query performance.

Query performance will usually be enhanced by assuring that all data
table spaces used by a partitioned table have the same prefetch size.
This allows the optimizer to more accurately estimate the cost of
alternative query plans and thus more effectively select the best plan.
Significantly different prefetch sizes reduce the optimizer's ability to
accurately estimate the cost of alternative query plans. The optimizer
picks the most frequently occurring prefetch size to cost alternative
query plans.

User response: 

The prefetch size for a table space can be found in SYSCAT.TABLESPACES.
To find all of the data table spaces used by a table 'table-name', and
their corresponding prefetch sizes, issue the query:

SELECT  
  SUBSTR(DATAPARTITIONNAME,1,15) 
      DATAPARTITIONNAME, 
  SUBSTR(TBSPACE,1,15) TBSPACE, 
  SYSCAT.DATAPARTITIONS.TBSPACEID	TBSPACEID,
  PREFETCHSIZE
    FROM 
  SYSCAT.TABLESPACES,
  SYSCAT.DATAPARTITIONS
    WHERE
  SYSCAT.TABLESPACES.TBSPACEID = 
    SYSCAT.DATAPARTITIONS.TBSPACEID
    AND TABNAME = 'table-name'

In order to match the prefetch size across all table spaces used by the
table, the following options are available, depending on the statement
issued that caused this warning:

*  Alter the table space to set the prefetch size to match other table
   spaces for the table.
*  If you issued a CREATE TABLE statement, drop the table and recreate
   the table with a set of compatible table spaces.
*  If you issued an ALTER TABLE statement with the ADD PARTITION clause,
   detach the newly added partition and resubmit the ALTER TABLE
   statement with a partition in a compatible table space.
*  If you issued an ALTER TABLE statement with the ATTACH clause, detach
   the newly attached partition and resubmit the ALTER TABLE statement
   with a table in a compatible table space.

sqlcode: +20301

sqlstate: 01674
