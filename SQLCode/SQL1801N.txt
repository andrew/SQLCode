

SQL1801N  Invalid request type.

Explanation: 

The specified request type is not supported for this command.

User response: 

Ensure that the request type is one of the following supported request
types: 
1. SQLE_CCA_REQUEST - CCA catalog node request for catalog and open scan
   command
2. SQLE_DAS_REQUEST - DAS catalog node request for catalog and open scan
   command
3. SQLE_CND_REQUEST - Open scan command for CCA and DAS catalog entries
