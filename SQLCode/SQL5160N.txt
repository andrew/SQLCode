

SQL5160N  Updates to "<parameter>" are currently disallowed. An
      operation is currently pending for this parameter. All
      applications must disconnect from the database and the database
      must be reactivated before new updates to this parameter can be
      applied.

Explanation: 

In a cluster environment, this database configuration parameter can have
only one pending operation. Upon database reactivation, cluster
resources are updated to ensure consistency between the cluster manager
and the database.

User response: 

To allow another update to the database configuration parameter, ensure
that all applications disconnect from the database and issue a DB2
CONNECT TO command. If the database was explicitly activated, deactivate
and reactivate it.
