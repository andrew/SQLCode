

SQL16265N  The XML document cannot be decomposed using XML schema
      "<xsrobject-name>" which is not enabled or is inoperative for
      decomposition.

Explanation: 

The XML schema identified by "<xsrobject-name>" is not in the correct
state to perform decomposition. The XML schema could be in one of the
following states: 
*  Not enabled for decomposition (possibly never enabled)
*  Disabled for decomposition
*  Inoperative for decomposition because of changes to the definitions
   of one or more tables specified in the annotations.

 Decomposition can only be performed using XML schemas that are enabled
for decomposition.

XML decomposition was not started.

User response: 

Ensure that the XML schema is enabled for decomposition before
attempting to use it for decomposition of an XML document. The
decomposition status of an XML schema can be checked by selecting the
DECOMPOSITION column of SYSCAT.XSROBJECTS for the XML schema identified
by "<xsrobject-name>".

sqlcode: -16265

sqlstate: 225D1
