

SQL1573N  The database cannot be activated or connected to in the
      current instance environment.

Explanation: 

This message is returned if

*  You are trying to activate or connect to a database that has not been
   verified for a DB2 pureScale environment, but are using a DB2
   pureScale environment.
*  You are trying to activate or connect to a database that has been
   verified for a DB2 pureScale environment, but are not using a DB2
   pureScale environment.

These two actions are not supported.

User response: 

If you are using a DB2 pureScale environment you can use the db2checkSD
utility to determine if the database can be used in that environment. If
the db2checkSD utility does not report any errors, activate or connect
to the database again.

sqlcode: -1573

sqlstate: 55001


   Related information:
   db2checkSD - Verify database is ready to upgrade to DB2 pureScale
   environment command
