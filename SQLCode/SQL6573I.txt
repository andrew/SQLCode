

SQL6573I  The remote execution of the splitter utility on partition
      "<node-number>" finished with remote execution code "<code>".

Explanation: 

The remote execution of the splitter utility on the specified partition
has completed.

User response: 

This is an informational message.
