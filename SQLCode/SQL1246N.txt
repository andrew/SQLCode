

SQL1246N  Connection settings cannot be changed while connections exist.

Explanation: 

One of the following occurred: 
*  An attempt was made to change the connection settings for an
   application using the SET CLIENT API. This was rejected because one
   or more connections exist.
*  The application contains both DB2 Call Level Interface API calls and
   calls to functions containing embedded SQL, and the connection
   management was not invoked using the CLI APIs.

User response: 

Possible actions: 
*  Ensure that the application disconnects from all servers before
   attempting to issue the SET CLIENT API (sqlesetc or sqlgsetc) or CLP
   command.
*  Ensure that all connection management requests are issued via the DB2
   Call Level Interface APIs if CLI is being used by the application.
