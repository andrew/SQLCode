

SQL0517N  The cursor "<name>" identifies a prepared statement that is
      not a SELECT or VALUES statement.

Explanation: 

The cursor "<name>" could not be used as specified because the prepared
statement named in the cursor declaration was not a SELECT or VALUES
statement.

The statement cannot be processed.

User response: 

Verify that the statement name is specified correctly in the PREPARE and
the DECLARE CURSOR for cursor "<name>" statements. Or correct the
program to ensure that only prepared SELECT or VALUES statements are
used in association with cursor declarations.

 sqlcode: -517

 sqlstate: 07005
