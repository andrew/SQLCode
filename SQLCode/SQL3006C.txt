

SQL3006C  An I/O error occurred while opening the message file.

Explanation: 

A system I/O error occurred while opening the message file. This error
can refer to a problem on either the client or the server.

The command cannot be processed.

User response: 

Resubmit the command with a valid message file name, including the
correct path.
