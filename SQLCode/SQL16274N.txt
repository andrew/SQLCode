

SQL16274N  An SQL error occurred while preparing to insert data for
      rowset "<rowsetname>". Information returned for the error includes
      SQLCODE "<sqlcode>", SQLSTATE "<sqlstate>", and message tokens
      "<token-list>".

Explanation: 

An SQL error occurred preparing to perform the operation on the table or
nickname associated with the specified rowset "<rowsetname>" based on
the annotated XML schema. Some possible causes are:

*  The syntax of a db2-xdb:expression or a db2-xdb:condition specified
   for the rowset is not a valid SQL expression or predicate.
*  A declared parameter for an SQL function or user-defined function
   invoked in an expression or predicate is incompatible with the type
   of the argument passed to the function.

The sqlcode, sqlstate and message token list (each token is separated by
the vertical bar character) are provided. The message tokens may be
truncated. See the corresponding message for the "<sqlcode>" for further
explanation of the error.

Decomposition of the XML document was not started.

User response: 

Check the message associated with the SQLCODE. Follow the action
suggested by that message. See the db2diag log file for complete
information about the error condition.

sqlcode: -16274

sqlstate: 225D2
