

SQL2518N  The RESTORE was not successful. An I/O error occurred while
      trying to restore the database configuration file.

Explanation: 

The database configuration file could not be restored due to an I/O
error.

The utility stops processing.

User response: 

Determine whether the I/O error can be corrected. Resubmit the command.
