

SQL1791N  The specified server definition, schema, or nickname does not
      exist.

Explanation: 

The procedure NNSTAT accepts a server definition, schema, and nickname
as input and one or more of these objects could not be found.

User response: 

Specify an existing server definition, schema, or nickname and resubmit
the statement.

sqlcode: -1791

sqlstate: 42704
