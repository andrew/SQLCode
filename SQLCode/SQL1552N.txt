

SQL1552N  The command failed because write operations for the database
      are suspended or are being suspended.

Explanation: 

You can suspend write operations for a database using the
db2SetWriteForDB API or the SET WRITE command with the SUSPEND clause.
There are some operations that cannot be performed on a database when
write operations for that database have been suspended, or while the
database manager is in the process of suspending write operations for
that database:

*  Backing up the database
*  Restoring the database
*  Restarting the database
*  Connecting to or activating the database
*  Updating or resetting database configuration files

This message is returned when an attempt is made to perform these kinds
of operations against a database that is in WRITE SUSPEND state, or that
is in the process of having write operations suspended.

User response: 

First, if write operations for the database are in the process of being
suspended, monitor the state of the database using the suspend_io
configuration parameter, and wait until the SET WRITE SUSPEND operation
completes before you continue.

Second, respond to this error according to the scenario in which the
message was returned:

Backing up or restoring the database:
         
         1. Resume write operations for the database by issuing the SET
            WRITE RESUME FOR DATABASE command.
         2. Perform the backup or restore operation again.


Restarting the database without the WRITE RESUME clause:
         

         Perform the restart again by performing one of the following
         actions:

          
         *  Issue the RESTART DATABASE command with the WRITE RESUME
            clause.
         *  Call the db2DatabaseRestart API specifying the
            DB2_RESUME_WRITE option

          

         In a multiple database partition environment, reissue the
         restart command or API call on each database partition.


Restarting the database with the WRITE RESUME clause in DB2 pureScale 
environments:
         
         *  If automatic restart is enabled, with the autorestart
            database configuration parameter set to ON, wait for several
            seconds and submit the restart again, specifying the WRITE
            RESUME clause or the DB2_RESUME_WRITE option.
         *  If automatic restart is disabled, with the autorestart
            database configuration parameter set to OFF, perform the
            following two steps: 
            1. Restart the database without specifying the WRITE RESUME
               clause or the DB2_RESUME_WRITE option.
            2. Restart the database again, specifying the WRITE RESUME
               clause or the DB2_RESUME_WRITE option.


Other scenarios (including connecting to the database, activating the 
database, or updating database configuration files):
         

         Restart the database by performing one of the following
         actions:

          
         *  Issue the RESTART command with the WRITE RESUME clause.
         *  Call the db2DatabaseRestart API specifying the
            DB2_RESUME_WRITE option.

          

         In DB2 pureScale environments, issuing the command or API on
         any one member will cause write operations to resume on all
         suspended members.


   Related information:
   suspend_io - Database I/O operations state configuration parameter
   db2SetWriteForDB API - Suspend or resume I/O writes for database
   SET WRITE command
   RESTART DATABASE command
   db2DatabaseRestart API - Restart database
