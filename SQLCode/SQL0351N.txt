

SQL0351N  An unsupported SQLTYPE was encountered in position
      "<position-number>" of the output SQLDA (select list).

Explanation: 

The element of the SQLDA at position "<position-number>" is for a data
type that either the application requestor or the application server
does not support. If the application is not using the SQLDA directly,
"<position-number>" could represent the position of an element in the
select list or a parameter of a CALL statement.

The statement cannot be processed.

User response: 

Change the statement to exclude the unsupported data type. For a select
statement, remove the names of any columns in the select-list with the
unsupported data type or use a cast in the query to cast the column to a
supported data type.

 sqlcode: -351

 sqlstate: 56084
