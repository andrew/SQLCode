

SQL20302W  More table spaces than required were specified in the IN or
      LONG IN clause. The extra table spaces are ignored.

Explanation: 

If the table being created is a non-partitioned table, then more than
one table space is specified in the IN or LONG IN clause. The first
table space specified is used to store the table data or long data. Only
one table space should be specified in the IN or LONG IN clause for a
non-partitioned table.

If the table being created is a partitioned table, then the number of
table spaces specified in the IN or LONG IN clause was more than the
number of partitions defined for the table. When adding partitions to a
partitioned table that was created with the long data in the same table
space as regular data, the LONG IN clause provided in the ADD PARTITION
clause is ignored.

The statement was processed successfully, but the extra table spaces
were ignored.

User response: 

None.

sqlcode: +20302

sqlstate: 01675
