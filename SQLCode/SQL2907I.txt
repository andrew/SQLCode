

SQL2907I  The following error occurred issuing the SQL "<sql-statement>"
      statement on table "<table-name>" using data from line
      "<line-number>" of TCP/IP port "<port-number>".

Explanation: 

This message provides the identification of the line and input TCP/IP
port where the error occurred for the following message listed.

The table is either the one specified on the SQL statement of the INGEST
command or the exception table. If the table is the one specified on the
SQL statement, and the SQL statement is INSERT or REPLACE, and the
INGEST command specified an exception table, the ingest utility will
attempt to insert the record into the exception table. Otherwise, the
ingest utility will discard the record.

User response: 

No action is required.
