

SQL2576N  Tablespace "<tablespace-name>" is being restored as part of an
      incremental RESTORE operation, but the RESTORE command did not
      specify the INCREMENTAL clause.

Explanation: 

To incrementally restore a tablespace, each RESTORE command must specify
the INCREMENTAL clause.

The utility stops processing.

User response: 

Reissue the RESTORE command and include the INCREMENTAL clause.
