

SQL20242N  The sample size specified in the TABLESAMPLE clause is not
      valid.

Explanation: 

The sample size specified in the TABLESAMPLE clause must be a positive
numeric value greater than zero and less than or equal to 100.

The statement was not processed.

User response: 

Change the sample size specified in the TABLESAMPLE clause to a valid
numeric value greater than zero and less than or equal to 100.

sqlcode: -20242

sqlstate: 2202H
