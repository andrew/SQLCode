

SQL5150N  The value specified for the configuration parameter
      "<parameter>" is less than the minimum allowable value of
      "<minimum-value>".

Explanation: 

The request is not completed because the value given for "<parameter>"
is too low. "<parameter>" may not be less than "<minimum value>"

User response: 

Ensure that the value specified for "<parameter>" is within the valid
range, then try the request again.
