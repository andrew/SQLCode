

SQL0787N  RESIGNAL statement is not within a handler.

Explanation: 

The RESIGNAL statement can only be used inside condition handlers.

User response: 

Remove the RESIGNAL statement or use a SIGNAL statement instead.

 sqlcode: -787

 sqlstate: 0K000
