

SQL2017N  Too many sessions are already started or OS/2 Start Session
      did not complete successfully.

Explanation: 

The BACKUP or RESTORE utility could not start the new session because: 
*  The maximum number of sessions is already started.
*  The OS/2 Start Session program returned an error.

The utility stops processing.

User response: 

Wait until some of the current sessions stop processing and resubmit the
command. Or, see the SQLERRD[0] field in the SQLCA for more information
and resubmit the command.
