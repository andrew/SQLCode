

SQL20119N  A ROW function must define at least two columns.

Explanation: 

A function that specifies ROW in the RETURNS clause must include a
column list with at least two columns.

User response: 

Either remove the ROW keyword from the RETURNS clause to make it a
scalar function or specify multiple columns in the column list of the
RETURNS clause.

 sqlcode: -20119

 sqlstate: 428F0
