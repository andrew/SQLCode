

SQL0212N  "<name>" is a duplicate table designator or is specified more
      than once in the REFERENCING clause of a trigger definition.

Explanation: 

The exposed table, view, alias, or correlation name specified by
"<name>" is identical to another exposed table, view, alias, or
correlation name in the same FROM clause.

If the statement is a CREATE TRIGGER, the REFERENCING clause may have
specified the same name as the subject table or may have the same name
for more than one of the OLD or NEW correlation names or the NEW_TABLE
or OLD_TABLE identifiers.

The statement cannot be processed.

User response: 

Rewrite the FROM clause of the SELECT statement. Associate correlation
names with table, view, or alias names so no exposed table, view, alias,
or correlation name is identical to any other exposed table, view,
alias, or correlation name in the FROM clause.

For a CREATE TRIGGER statement, change the names in the REFERENCING
clause so that there are no duplicates.

 sqlcode: -212

 sqlstate: 42712
