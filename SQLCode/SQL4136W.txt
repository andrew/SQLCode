

SQL4136W  Table or view "<schema-name>"."<name>" already exists.

Explanation: 

The table name or view name specified already exists in the catalog.

Processing continues.

User response: 

Correct the SQL statement.
