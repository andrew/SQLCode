

SQL22270N  The contact, or contact group, with name "<name>" cannot be
      added to the contact list.

Explanation: 

The contact, or contact group, already exists in the contact list.

User response: 

Create a new contact, or contact group, with a unique name.
