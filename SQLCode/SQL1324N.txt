

SQL1324N  Error converting data from nickname column
      "<schema>"."<name>"."<column>". Reason Code: "<reason-code>".
      Value: "<value>".

Explanation: 

A data conversion problem occurred while transferring data from or to
the remote source. Possible reasons: 

1        Numeric value was out of range

2        Numeric value syntax error

3        Base 64 decoding error

4        Hexbin decoding error

User response: 

Check the data type mapping between the remote type and the local type.
Also ensure that the remote system returns valid data.
