

SQL4123W  Only one table reference is allowed in the FROM clause for a
      grouped view.

Explanation: 

If the table identified by table name is a GROUPed view, then the FROM
clause must contain exactly one table reference.

Processing continues.

User response: 

Correct the SQL statement.
