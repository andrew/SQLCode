

SQL1844W  Data for column "<column-name>" were truncated between the
      remote data source and the federated server.

Explanation: 

Characters were truncated when data was transferred between a remote
data source and the federated server. Truncation can occur in many
situations. Some of these situations include an incorrect nickname
column definition (a column is too small for the remote data source
column data), or the presence of a conversion or type-cast function that
restricts the size of the data returned by the remote data source.

User response: 

To correct this problem, examine the statement for any type-cast or
conversion functions that might be restricting the size of data returned
from the remote data source. If the statement contains these functions,
recode the statement to allow larger data to be returned from the data
source. Then submit the statement again. If the statement does not
contain these functions, or if correcting the functions does not correct
the problem, examine the local column specification in the nickname in
the DB2 catalog. Using the ALTER NICKNAME statement or DROP NICKNAME and
CREATE NICKNAME statements, change the column specification to ensure
that the column size is larege enough to accomodate the data returned by
the remote data source.

sqlcode: +1844

sqlstate: 01004
