

SQL1101N  Remote database "<name>" on node "<node-name>" could not be
      accessed with the specified authorization id and password.

Explanation: 

A connection to the remote database "<name>" on node "<node-name>" was
requested, and the remote node does not accept the authorization ID and
password combination specified for this node (either in the remote
authorization tables or at runtime).

Federated system users: This error can also occur when any of the
following is true:

*  There is no user mapping and the remote authorization ID or remote
   password does not match the authorization ID and password that were
   specified when connecting to the DB2 federated database.
*  The user mapping does not specify the REMOTE_PASSWORD option, and no
   password was specified when connecting to the DB2 federated database.
*  The user mapping does not specify the REMOTE_PASSWORD option, and the
   remote password does not match the password specified when connecting
   to the DB2 federated database.
*  The user mapping does not specify the REMOTE_AUTHID option, and the
   remote authorization ID does not match the authorization ID specified
   when connecting to the DB2 federated database.
*  The remote authorization ID or remote password do not match those
   specified on the user mapping.

The request cannot be processed.

User response: 

Resubmit the request with a valid authorization ID and password
combination for the remote system.

sqlcode: -1101

 sqlstate: 08004
