

SQL1735N  The RESTORE command failed because encryption key label
      specified in the command does not match the label used to encrypt
      the backup image.

Explanation: 

To restore a database from a backup image, the RESTORE command must
specify an encryption key label that matches the label used when taking
the backup image.

User response: 

Change the RESTORE command to use the matching encryption key label. Run
the RESTORE command again.


   Related information:
   RESTORE DATABASE command
