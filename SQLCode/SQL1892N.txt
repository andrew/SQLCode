

SQL1892N  The address of the port number parameter is not valid.

Explanation: 

The application program used an address that is not valid for the port
number parameter. Either the address points to an unallocated buffer or
the character string in the buffer does not have a null terminator.

The command cannot be processed.

User response: 

Correct the application program so a correct address is used and the
input string is null terminated.
