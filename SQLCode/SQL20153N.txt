

SQL20153N  The split image of the database is in the suspended state.

Explanation: 

The database split image cannot be used while it is in the suspended
state.

User response: 

To resume I/O for this database split image, issue one of the following
db2inidb commands:

*  db2inidb <db-name> as mirror
*  db2inidb <db-name> as snapshot
*  db2inidb <db-name> as standby

In a DB2 pureScale environment, you can issue this command from any
member and need to issue the command only once.

In a partitioned database environment, you must execute the db2inidb
command on each database partition. You can run the command concurrently
on each database partition.

sqlcode: -20153

sqlstate: 55040
