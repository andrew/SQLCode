

SQL20505N  The WITH ORDINALITY clause is not valid with UNNEST of an
      associative array.

Explanation: 

The WITH ORDINALITY clause must not be specified when the argument of
the UNNEST table function is an associative array. An associative array
is not organized according to ordinal position.

User response: 

Remove the WITH ORDINALITY clause or change the argument of the UNNEST
function to an ordinary array. Try the statement again.

sqlcode: -20505

sqlstate: 428HT
