

SQL20427N  An error occurred during a text search administration
      procedure or command. The error message is
      "<text-search-error-msg>".

Explanation: 

A text search administration operation failed with an error message
"<text-search-error-msg>".

User response: 

Use the error message "<text-search-error-msg>" to determine the cause
of the error. If the message is truncated, you can see the db2diag log
file.

The first word in "<text-search-error-msg>" is an error identifier. If
the error identifier starts with 'CIE', use the db2ts command to obtain
more details, for example, db2ts help "<error-identifier>".

If the error identifier does not start with 'CIE', use the DB2 Text
Search documentation to obtain more details about
"<text-search-error-msg>".

sqlcode: -20427

sqlstate: 38H14
