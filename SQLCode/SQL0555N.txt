

SQL0555N  An authorization ID cannot revoke a privilege from itself.

Explanation: 

An authorization ID attempted to execute a REVOKE statement where the
authorization ID itself appears as one of the entries in the
authorization ID list from which privileges are to be revoked.

The statement cannot be processed.

User response: 

Remove the authorization ID from the list.

 sqlcode: -555

 sqlstate: 42502
