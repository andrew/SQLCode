

SQL1464W  Not all tasks were removed because some tasks were running.

Explanation: 

The SYSPROC.ADMIN_TASK_REMOVE procedure attempted to remove a set of
tasks, but not all were removed. The tasks that were not running were
removed by the procedure, but any tasks that were running when the
procedure attempted to remove them were not removed.

User response: 

Wait until the running tasks complete and then use the
SYSPROC.ADMIN_TASK_REMOVE procedure to remove the tasks. The
SYSTOOLS.ADMIN_TASK_STATUS view can be used to check the execution
status of tasks.

sqlcode: +1464

sqlstate: 0168S
