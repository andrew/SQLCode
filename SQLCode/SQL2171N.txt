

SQL2171N  The update of the recovery history file failed because the
      specified object part does not exist in the file.

Explanation: 

The entry specified to be updated in the recovery history file does not
exist in the file. The utility stops processing.

User response: 

Resubmit the command with a valid entry.
