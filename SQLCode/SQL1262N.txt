

SQL1262N  The point-in-time specified for rolling forward database
      "<name>" is not valid.

Explanation: 

The timestamp parameter specified for the point-in-time stopping value
is not valid. The timestamp must be entered in ISO format
(YYYY-MM-DD-hh.mm.ss.<ssssss> where YYYY represents year, MM represents
month, DD represents day, hh represents hours, mm represents minutes, ss
represents seconds, and ssssss represents optional microseconds).

The database is not rolled forward.

User response: 

Ensure that the timestamp is entered in the correct format.

When you issue a ROLLFORWARD DATABASE command, ensure that you are not
specifying a year greater than 2105.
