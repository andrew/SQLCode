

SQL16199N  XML document contains a type "<type-name>" which has
      complexContent and cannot be specified as the base in a
      simpleContent element.

Explanation: 

While parsing an XML document, the parser encountered a type
"<type-name>" which has complexContent specified as the base in a
simpleContennt element.

Parsing or validation did not complete.

User response: 

Correct the XML document to specify a different type as the base and try
the operation again.

sqlcode: -16199

sqlstate: 2200M
