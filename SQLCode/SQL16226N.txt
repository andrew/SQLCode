

SQL16226N  XML schema contains an invalid group specification for group
      "<group-name>". Reason code = "<reason-code>".

Explanation: 

While processing an XML schema the XML parser encountered an invalid
group specification. Possible reasons given by "<reason-code>" are: 
1. The group must contain (all | choice | sequence)
2. The group contains a reference to a group being redefined that must
   have minOccurs = maxOccurs = 1.
3. The attribute group specification does not match
   (annotation?.((attribute | attributeGroup)*, anyAttribute?))

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16226

sqlstate: 2200M
