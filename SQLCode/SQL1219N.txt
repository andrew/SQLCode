

SQL1219N  The request failed because private virtual memory could not be
      allocated.

Explanation: 

The instance was unable to allocate enough private virtual memory to
process the request. This may be a result of shared memory allocations
made in other (unrelated) processes.

User response: 

The problem may be corrected by: 
*  Stopping other applications running on the machine, especially those
   that use large amounts of shared memory.

 sqlcode: -1219

 sqlstate: 57011
