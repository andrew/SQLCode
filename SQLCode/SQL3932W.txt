

SQL3932W  The test synchronization session completed successfully. The
      satellite application version, however, is not set locally, or
      does not exist for this satellite's group at the satellite control
      server.

Explanation: 

The application version on the satellite is different from any that are
available for this satellite's group.

User response: 

Ensure that the application version on the satellite is set to the
correct value.
