

SQL2317W  SYSTEM SAMPLING was specified for RUNSTATS but is not
      supported for the statistical view specified. BERNOULLI SAMPLING
      was done instead.

Explanation: 

Page-level sampling, or SYSTEM SAMPLING, could not be performed on the
statistical view specified for RUNSTATS. For statistical views, SYSTEM
sampling can only be applied to a single base table referenced in the
view definition. If the view contains multiple tables, SYSTEM sampling
is possible if a single table among all the tables in the statistical
view can be identified as being joined with all primary keys or unique
index columns of the other tables used in the view.

The referential integrity constraints can be informational. If the view
meets the previously described criteria, page-level sampling will be
done on the child table.

Because the statistical view specified did not satisfy these conditions,
row-level sampling, or BERNOULLI SAMPLING, was done instead.

User response: 

Specify BERNOULLI SAMPLING for the statistical view to avoid having this
warning returned.

sqlcode: +2317

sqlstate: 0168V


   Related information:
   RUNSTATS command
