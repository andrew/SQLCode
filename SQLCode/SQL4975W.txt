

SQL4975W  Roll-forward operation was canceled successfully. The database
      or selected table spaces have to be restored on members or nodes
      "<node-list>".

Explanation: 

A roll-forward operation was canceled before it successfully completed,
and the database or selected table spaces are left in in an inconsistent
state. The database or selected table spaces are in restore pending
state on the listed members or nodes.

If ",..." is displayed at the end of the member or node list, see the
administration notification log for the complete list of members or
nodes.

Note: The member or node numbers provide useful information only in DB2
pureScale environments and partitioned database environments. Otherwise,
the information should be ignored.

User response: 

Restore the database or selected table spaces on the listed members or
nodes. The table spaces that are in restore pending state can be
identified on the specified members or nodes by the MON_GET_TABLESPACE
table function. In environments other than DB2 pureScale environments,
you can also use the db2dart utility.
