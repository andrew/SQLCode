

SQL5162N  The db2dsdriver.cfg configuration file contains the parameter
      "<parameter1>", which has the same value as the parameter
      "<parameter2>", but these parameters cannot have the same value.

Explanation: 

The db2dsdriver.cfg configuration file contains database information,
and is used by the following drivers and clients:

*  IBM Data Server Driver for ODBC and CLI
*  IBM Data Server Driver Package
*  For DB2 Version 9.7: for CLI and open source applications, the IBM
   Data Server Client and IBM Data Server Runtime Client

The information in the db2dsdriver.cfg file is similar to the
information that is in the system database directory on an IBM Data
Server Client or IBM Data Server Runtime Client.

The client driver configuration file cannot contain the same value for
parameters. If true is specified for one parameter, false must be
specified for the other.

User response: 

1. Change the value of one of the parameters in the db2dsdriver.cfg file
   so that the two parameters do not have the same value.
2. Stop the application process and start it again for the new
   db2dsdriver.cfg file settings to take effect.


   Related information:
   IBM data server driver configuration file
