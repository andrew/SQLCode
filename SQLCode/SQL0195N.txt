

SQL0195N  Last column of "<table-name>" cannot be dropped.

Explanation: 

An attempt was made to drop one or more columns using an ALTER TABLE
statement. The columns cannot be dropped from table "<table-name>"
because at least one of the existing columns must be preserved when
altering a table.

User response: 

Ensure table "<table-name>" will have at least one column once the ALTER
statement is complete. Either remove the DROP of one of the columns and
try the request again, or, if all of the columns should be removed, drop
the table and create it again.

 sqlcode: -195

 sqlstate: 42814
