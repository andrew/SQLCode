

SQL2450I  The db2convert command successfully completed the INIT, COPY,
      and REPLAY phases of the conversion process for all row-organized
      tables that satisfy the specified matching criteria. If the
      database is recoverable, a backup can be taken at this point.

Explanation: 

You can convert row-organized tables into column-organized tables by
using the db2convert command.

You can control which tables to convert by specifying matching criteria
with db2convert parameters:

*  All tables in the database
*  Tables created by a specific user (-u parameter)
*  Tables in a specified schema (-z parameter)
*  One specified table (-t parameter)

You can perform the conversion operation in one step. However, you can
also choose to perform the conversion operation in two steps:

1. Perform only the INIT, COPY, and REPLAY phases of the conversion by
   calling the db2convert command specifying the -stopBeforeSwap
   parameter.
2. Complete the conversion operation by calling the db2convert command
   specifying the -continue parameter.

Performing the operation in two steps allows you to back up the database
before completing the conversion.

This message is returned when the db2convert utility has successfully
completed the first step of the two-step conversion process for all
tables that match the specified criteria.

User response: 

To complete the conversion of the tables to column-organized tables,
call the db2convert command specifying the same table criteria and the
-continue parameter.


   Related information:
   db2convert - Convert row-organized tables into column-organized
   tables
