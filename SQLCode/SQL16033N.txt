

SQL16033N  The target data type "<type-name>" of a cast or castable
      expression is not an atomic data type defined for the in-scope XML
      schema types or is a data type that cannot be used in a cast or
      castable expression. Error QName=err:XPST0080.

Explanation: 

The cast or castable expression specifies a target data type
"<type-name>" that cannot be used. The predefined XML schema types
xs:NOTATION, xs:anySimpleType, and xdt:anyAtomicType cannot be used as
the target type of a cast or castable expression. If "<type-name>" is
not one of these restricted types, then either the data type is not
defined for the in-scope XML schema types or the data type is not an
atomic type.

The XQuery expression cannot be processed.

User response: 

Perform one of the following actions:

*  If the target data type is xs:NOTATION, xs:anySimpleType, or
   xdt:anyAtomicType, specify a different target data type or remove the
   cast or castable expression.
*  If the target data type is not defined by the in-scope XML types,
   specify a data type that is in-scope or change the XML schema to
   include the data type.
*  If the target data type is not atomic, specify a different target
   data type or remove the cast or castable expression.

 sqlcode: -16033

 sqlstate: 10507
