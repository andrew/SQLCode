

SQL4911N  The host variable data type is not valid.

Explanation: 

The data type of the host variable is not valid.

The function cannot be completed.

User response: 

Correct the data type of the host variable and call the function again.
