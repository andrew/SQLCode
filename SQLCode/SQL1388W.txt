

SQL1388W  An error occurred while trying to access a resource or part of
      a resource requested. Partial information was still returned.
      Details are available in the administration notification log with
      message "<message-number>".

Explanation: 

An error occurred when trying to access one or more sources of
information requested. The information which could be collected is valid
and was returned but some records may be missing.

User response: 

The query results are not complete. Refer to the related entries in the
administration notification log for more information then correct and
resubmit the command. If the problem persists, contact IBM support.
