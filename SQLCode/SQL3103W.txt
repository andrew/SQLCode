

SQL3103W  The number of columns in the METHOD parameter is less than the
      number of columns in the Action String (e.g. "REPLACE into ...")
      parameter.

Explanation: 

The number of columns taken from the input file or table is less than
the number to be put in the output table or file.

Only data for the columns indicated in the input table or file will be
processed. Data in the excess output columns is not processed.

User response: 

Review the data in the output table or file.
