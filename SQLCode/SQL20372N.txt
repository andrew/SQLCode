

SQL20372N  The trusted context "<context-name>" specified authorization
      ID "<authorization-name>" which is already specified for another
      trusted context.

Explanation: 

A CREATE TRUSTED CONTEXT or ALTER TRUSTED CONTEXT statement for
"<context-name>" specified SYSTEM AUTHID "<authorization-name>", but
this authorization ID is already defined to use a different trusted
context. A system authorization ID that is defined as the SYSTEM AUTHID
for a trusted context cannot be associated with any other trusted
context as the SYSTEM AUTHID.

Use the following query to determine which trusted context is already
using the authorization ID:

SELECT CONTEXTNAME FROM SYSCAT.CONTEXTS 
WHERE SYSTEMAUTHID = <authorization-name>

The statement could not be processed.

User response: 

Change the authorization ID to be the system authorization ID for the
trusted context and reissue the CREATE or ALTER statement.

sqlcode: -20372

sqlstate: 428GL
