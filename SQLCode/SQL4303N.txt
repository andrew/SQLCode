

SQL4303N  Java stored procedure or user-defined function "<name>",
      specific name "<spec-name>" could not be identified from external
      name "<string>".

Explanation: 

The CREATE PROCEDURE or CREATE FUNCTION statement that declared this
stored procedure or user-defined function had a badly formatted EXTERNAL
NAME clause. The external name must be formatted as follows:
"package.subpackage.class!method".

User response: 

Submit a corrected CREATE PROCEDURE or CREATE FUNCTION statement.

 sqlcode: -4303

 sqlstate: 42724
