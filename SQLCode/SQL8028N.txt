

SQL8028N  The "<feature>" feature is being used without a "<license>"
      license. DB2 has detected that this feature is being used without
      the appropriate entitlements. Ensure that you have purchased
      appropriate entitlements from your IBM representative or
      authorized dealer and have updated your license using the db2licm
      command.

User response: 




   Related information:
   DB2 license files
   db2licm - License management tool command
