

SQL3131W  The field containing "<text>" in row "<row-number>" and column
      "<column-number>" was truncated into a TIMESTAMP field because the
      data is longer than the database column.

Explanation: 

The timestamp value in the specified field is longer than the length of
the string representation of a timestamp.

The timestamp value is truncated to fit into the table.

User response: 

Compare the value in the output table with the input file. If necessary,
correct the input file and resubmit the command or edit the data in the
table.
