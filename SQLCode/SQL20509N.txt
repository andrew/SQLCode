

SQL20509N  The module alias "<alias-name>" cannot be used as the target
      module of the DDL statement.

Explanation: 

The ALTER MODULE statement, COMMENT statement, and DROP statement cannot
specify module alias "<alias-name>" as the target module in order to
alter, comment on, or drop the module to which the alias refers.

User response: 

Specify the module name to which the alias "<alias-name>" refers and
submit the SQL statement again.

sqlcode: -20509

sqlstate: 560CT
