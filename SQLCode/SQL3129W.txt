

SQL3129W  A date, time, or timestamp field was padded with blanks. Row
      number: "<row-number>". Column number: "<column-number>". Text in
      the field: "<text>".

Explanation: 

The field data in the input file was shorter than the database column.

The data on the right is padded with blanks.

User response: 

Compare the value in the output table with the input file. If necessary,
correct the input file and resubmit the command or edit the data in the
table.
