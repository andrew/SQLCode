

SQL1729N  The command failed because the label "<label>" does not exist
      in the keystore.

Explanation: 

When you administer and maintain encryption master key labels you
provide the label name as a parameter to the CREATE DATABASE, BACKUP
DATABASE, and RESTORE DATABASE commands. You can also administer the
labels with the ADMIN_ROTATE_MASTER_KEY function. This error is returned
when you provide a label that is not found in the keystore.

User response: 

Provide a valid label and retry the command or function.


   Related information:
   BACKUP DATABASE command
