

SQL1817N  The CREATE SERVER statement does not identify the
      "<type-or-version>" of data source that you want defined to the
      federated database.

Explanation: 

When a CREATE SERVER statement references the wrapper that you
specified, it must also identify the "<type-or-version>" of data source
that is to be defined to the federated database.

User response: 

In the CREATE SERVER statement, code the "<type-or-version>" option so
that it designates the "<type-or-version>" of data source being defined.
Then run the CREATE SERVER statement again.

 sqlcode: -1817

 sqlstate: 428EU
