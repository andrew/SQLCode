

SQL5091N  The entry in the database configuration file for the size of
      one log file extension (logext) is not in the valid range.

Explanation: 

The value for the size of one log file extension must be between 4 and
256.

The requested change is not made.

User response: 

Resubmit the command with a valid value for the size of one log file
extension.
