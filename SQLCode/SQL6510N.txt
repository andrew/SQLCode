

SQL6510N  The program failed to create the temporary directory at local
      non-NFS space of partition "<partition-num>".

Explanation: 

The program needs a temporary working directory at local non-NFS space
of all partitioning and loading partitions.

User response: 

Please ensure your working space is clean.
