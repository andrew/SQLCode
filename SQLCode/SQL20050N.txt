

SQL20050N  The search target or search argument "<parameter-name>" does
      not match a name in the function being created.

Explanation: 

Each search target in an index exploitation rule has to match some
parameter name of the function that is being created. Each search
argument in an index exploitation rule must match either an expression
name in the EXPRESSION AS clause or a parameter name of the function
being created. Parameter names must be specified in the parameter list
for the function.

The statement cannot be processed.

User response: 

Specify only valid names of the function in the search target or search
argument.

 sqlcode: -20050

 sqlstate: 428E8
