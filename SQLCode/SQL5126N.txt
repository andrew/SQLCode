

SQL5126N  The UPDATE DATABASE MANAGER CONFIGURATION command failed
      because the specified database manager configuration parameter is
      not supported by the current instance node type. Database manager
      configuration parameter: "<parameter-name>". Instance node type:
      "<instance-node-type>".

Explanation: 

Different DB2 database products are associated with different DB2
database manager instance node types. The current instance node type is
indicated by the runtime token "<instance-node-type>":

1        

         Database server with local and remote clients


2        

         Client


3        

         Database server with local clients


4        

         Partitioned database Server with local and remote clients

You can configure DB2 database functionality by setting database manager
configuration parameters. Each instance node type supports only the
database manager configuration parameters for the DB2 database products
that are associated with that instance node type.

This message is returned when an attempt is made to update a database
manager configuration parameter that is not supported by the current
instance node type.

User response: 

1. List the database manager configuration parameters that are supported
   by the current instance node type by using the GET DATABASE MANAGER
   CONFIGURATION command.
2. Perform one of the following actions: 
   *  Achieve the desired configuration by updating one or more database
      manager configuration parameters that are supported with the
      current instance node type.
   *  If the functionality associated with the failed configuration
      attempt is supported by a different instance node type, update or
      upgrade the instance node type to a type that supports the
      functionality and then reissue the UPDATE DATABASE MANAGER
      CONFIGURATION command.

sqlcode: -5126

sqlstate: 5U001


   Related information:
   Upgrading DB2 Version 10.1 or DB2 Version 9.7 instances
   Updating an instance to a higher level within a release using the
   db2iupdt command
   Modifying instances
   UPDATE DATABASE MANAGER CONFIGURATION command
   GET DATABASE MANAGER CONFIGURATION command
   nodetype - Instance node type configuration parameter
