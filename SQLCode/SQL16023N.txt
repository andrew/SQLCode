

SQL16023N  The XQuery prolog cannot contain multiple declarations for
      the same namespace prefix "<ns-prefix>". Error QName=err:XQST0033.

Explanation: 

The prolog contains multiple declarations for the namespace prefix
"<ns-prefix>". Although a namespace declaration in a prolog can override
a prefix that has been predeclared in the static context, the same
namespace prefix cannot be declared more than once in the prolog.

The XQuery expression cannot be processed.

User response: 

Remove any extra declarations for the prefix from the prolog, or change
the prefix that is assigned to the extra declarations. Verify that the
prefixes used in the query reference the correct namespaces.

 sqlcode: -16023

 sqlstate: 10503
