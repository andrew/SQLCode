

SQL0954C  Not enough storage is available in the application heap to
      process the statement.

Explanation: 

All available memory for the application has been used.

The statement cannot be processed.

User response: 

Terminate the application on receipt of this message. If the applheapsz
database configuration parameter is set to AUTOMATIC, you will need to
increase either the APPL_MEMORY database configuration setting, or the
INSTANCE_MEMORY database manager configuration setting. Otherwise,
increase the applheapsz database configuration parameter to allow a
larger application heap.

When updating configuration parameters, it is recommended to change them
by 10% of the current size at a time until the error condition is
resolved. To change applheapsz, enter a command similar to the
following, which sets applheapsz to size 4000 for database sample: 

db2 UPDATE DB CFG FOR sample USING APPLHEAPSZ 4000

If all associated configuation parameters are set to either AUTOMATIC or
COMPUTED, then the memory demands of the instance exceed the amount of
memory configured on the machine. Possible solutions include reducing
the database workload, enabling the connection concentrator feature, or
adding additional memory to the machine.

 sqlcode: -954

 sqlstate: 57011
