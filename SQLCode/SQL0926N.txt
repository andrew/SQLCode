

SQL0926N  SQL ROLLBACK invalid for application execution environment.

Explanation: 

ROLLBACK is disallowed in the following cases: 
1. In a Distributed Transaction Processing environment such as CICS, a
   static SQL ROLLBACK statement was attempted, but a rollback statement
   specific to the environment is required. For example, in a CICS
   environment this would be the CICS SYNCPOINT ROLLBACK command.
2. A DB2 application precompiled or set to use CONNECT 2 has issued a
   dynamic SQL ROLLBACK statement, whereas only static SQL ROLLBACKs are
   allowed.
3. When issued from a stored procedure, SQL ROLLBACK is also restricted
   if the calling program is executing in a distributed unit of work
   (CONNECT type 2) or Distributed Transaction Processing environment.

User response: 


1. Remove the statement issuing the ROLLBACK and replace it with a
   statement which does the valid equivalent for the environment.
2. In the case of a connect type 2, use only static COMMIT.
3. In the case of a stored procedure, remove it entirely.

 sqlcode: -926

 sqlstate: 2D521
