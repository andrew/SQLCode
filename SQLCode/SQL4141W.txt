

SQL4141W  An error has occurred when attempting to produce message
      "<message-number>" in module "<module-name>".

Explanation: 

The FLAGGER has attempted to produce an undefined message.

Processing continues.

User response: 

Record this message number (SQLCODE), module name and error code in the
message. Contact your technical service representative with the
information.
