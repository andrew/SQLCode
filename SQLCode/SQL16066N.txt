

SQL16066N  The argument passed to the aggregate function
      "<function-name>" is not valid. Error QName=err:FORG0006.

Explanation: 

The argument that was passed to the aggregate function "<function-name>"
is not valid because the argument does not meet any of the conditions
that are required for arguments to the function "<function-name>".

User response: 

Try one of the following actions:

*  If the function is fn:avg, verify that the following conditions are
   met: If the input sequence contains duration values, the values must
   be either all xdt:yearMonthDuration values or all xdt:dayTimeDuration
   values. If the input sequence contains numeric values, the values
   must all be promotable to a single common type that is one of the
   four numeric types, xdt:yearMonthDuration or xdt:dayTimeDuration or
   one if its subtypes.
*  If the function is fn:max or fn:min, verify that the following
   conditions are met: All items in the input sequence must be numeric
   or derived from a single base type for which the gt operator (for
   fn:max) or lt operator (for fn:min) is defined. If the input sequence
   contains numeric values, the values must all be promotable to a
   single common type and the values in the sequence must have a total
   order. If the input sequence contains duration values, the values
   must be either all xdt:yearMonthDuration values or all
   xdt:dayTimeDuration values.
*  If the function is fn:sum, verify that the following conditions are
   met: All items in the input sequence must be numeric or derived from
   a single base type. The type must support addition. If the input
   sequence contains numeric values, the values must all be promotable
   to a single common type. If the input sequence contains duration
   values, the values must be either all xdt:yearMonthDuration values or
   all xdt:dayTimeDuration values.

 sqlcode: -16066

 sqlstate: 10608
