

SQL2711N  Invalid column delimiter (CDELIMITER) at line "<line>" of the
      configuration file.

Explanation: 

The column delimiter (CDELIMITER) specified in the configuration file is
not valid.

User response: 

Make sure the column delimiter (CDELIMITER) is a single byte character.
