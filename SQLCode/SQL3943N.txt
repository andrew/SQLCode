

SQL3943N  Synchronization session identifier exceeds the maximum length
      of "<length>" characters.

Explanation: 

The specified synchronization session identifier is longer than the
allowed length of "<length>" characters.

User response: 

Ensure that the identifier is no more than nnn characters.
