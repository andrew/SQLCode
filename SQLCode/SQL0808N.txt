

SQL0808N  The CONNECT statement semantics are not consistent with those
      of other existing connections.

Explanation: 

The CONNECT statement originates from a source file which was
precompiled having different connection options (SQLRULES, CONNECT type,
SYNCPOINT, or RELEASE type) than that of a source file for which a
connection exists.

User response: 

Ensure that all source files are precompiled using the same CONNECT
options, or if this is not possible, call the SET CLIENT api to set the
desired options for the application process before issuing the first
CONNECT statement.

 sqlcode: -808

 sqlstate: 08001
