

SQL2200N  The qualifier for the table or index name is too long.

Explanation: 

The authid must be 1 to 128 bytes in length.

The utility stops processing.

User response: 

Resubmit the command with the correct qualifier.
