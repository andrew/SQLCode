

SQL1153N  The utility ID "<utility-ID>" does not exist.

Explanation: 

The specified utility ID could not be found. Either an invalid ID has
been specified or the utility has already completed.

User response: 

Verify that the utility exists and resubmit the command. To determine if
the utility has completed, review the database manager snapshot data.
