

SQL1305N  An internal DCE error occurred.

Explanation: 

DB2 processing failed due to an internal DCE error.

User response: 

Make sure DCE is started. If the problem persists, contact a service
representative for assistance.

 sqlcode: -1305

 sqlstate: 58004
