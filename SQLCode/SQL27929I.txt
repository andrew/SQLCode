

SQL27929I  Partitioning number returned from hash function: "<hexvalue>"
      (hex) "<decvalue>" (decimal).

Explanation: 

This informational message reports the result of the hashing function in
both hex and decimal format.

User response: 

No action is required.
