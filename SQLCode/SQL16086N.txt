

SQL16086N  The replacement sequence of a replace expression contains
      invalid nodes for the specified target node. Error
      QName=err:"<error-name>".

Explanation: 

A node in the replacement sequence cannot be used to replace the target
node. The reason is listed based on the error Qname:

err:XUDY0010
         The value of keywords are not specified and the target node is
         not an attribute node. The replacement sequence must contain
         only element, text, comment, or processing instruction nodes
         but at least one item in the sequence is an attribute node.

err:XUDY0011
         The value of keywords are not specified and the target node is
         an attribute node. The replacement sequence must contain only
         attribute nodes but at least one item in the sequence is not an
         attribute node.

The XQuery expression cannot be processed.

User response: 

Correct the source expression in the with clause of the replace
expression to ensure that the replacement sequence contains only valid
nodes for the target node.

sqlcode: -16086

sqlstate: 10706
