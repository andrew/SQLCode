

SQL0557N  The specified combination of privileges cannot be granted or
      revoked.

Explanation: 

One of the following occurred: 
*  The GRANT or REVOKE statement contains a combination of privileges
   that are of different classes. The privileges must all be of one
   class. Examples are DATABASE, PLAN, or TABLE.
*  The GRANT statement attempted to grant a privilege for a view that is
   not allowed. ALTER, INDEX and REFERENCES cannot be granted for a
   view.

The statement cannot be processed.

User response: 

Correct and resubmit the statement.

 sqlcode: -557

 sqlstate: 42852
