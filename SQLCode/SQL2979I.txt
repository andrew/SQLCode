

SQL2979I  The ingest utility is starting at "<timestamp>".

Explanation: 

The ingest utility is starting at the indicated timestamp. The utility
will also issue a message indicating the job ID.

User response: 

No user response is required.


   Related information:
   Restarting a failed ingest operation
