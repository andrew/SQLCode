

SQL0799W  A SET statement for special register "<special-register-name>"
      was ignored because either the special register does not exist on
      the database server or an invalid value was specified for the
      special register.

Explanation: 

A special register is a storage area that is defined by the database
manager for an application process. Special registers are used to store
information that can be referenced in SQL statements.

Some special registers can be updated using the SET variable statement.

A DB2 client or driver can be configured to set special registers. When
a DB2 client or driver is configured to set special registers, the
client or driver sends a SET variable statement to the database server
at the same time as an application SQL statement.

This message is returned when an attempt is made to update a special
register, but either the special register does not exist on the database
server or an invalid value was specified for the special register.

When a DB2 client or driver sends a SET variable statement to the
database server at the same time as an application SQL statement, the
database server processes the application SQL statement regardless or
whether setting the special register succeeds or fails.

User response: 

If the DB2 client or driver is configured to set special registers,
validate the special register settings that are specified in the client
configuration file db2dsdriver.cfg.

sqlcode: +799

sqlstate: 01527


   Related information:
   Special registers
   SET variable statement
   IBM data server driver configuration file structure
