

SQL0669N  A system required index cannot be dropped explicitly.

Explanation: 

The DROP INDEX statement attempted to drop an index required to: 
*  enforce the primary key constraint on a table
*  enforce a unique constraint on a table
*  enforce the uniqueness of the object identifier (OID) column of a
   typed table hierarchy
*  maintain a replicated materialized query table
*  maintain an XML column in the table.

A system required index cannot be dropped using the DROP INDEX
statement.

The statement cannot be processed. The specified index is not dropped.

User response: 

If you do not want to keep the primary or unique constraint, use the
DROP PRIMARY KEY clause or the DROP CONSTRAINT clause of the ALTER TABLE
statement to remove the primary key or unique constraint. If the index
was created only for enforcing the primary or unique key, then the index
will be dropped. If not, the DROP INDEX statement could then be
processed.

The index for an OID column can only be dropped by dropping the table.

The index required to maintain a replicated materialized query table can
only be dropped by first dropping the replicated materialized query
table.

The system-required indexes associated with one or more XML columns in a
table cannot be dropped explicitly. Such indexes are maintained by the
database manager to support the XML column in the table. The index
specified in the DROP INDEX statement cannot be dropped without dropping
the table.

 sqlcode: -669

 sqlstate: 42917
