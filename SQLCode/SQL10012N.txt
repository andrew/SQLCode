

SQL10012N  An unexpected operating system error was received while
      loading the specified library "<name>".

Explanation: 

An unexpected error occurred when trying to load the library module
specified in the program name field.

User response: 

Resubmit the current command. If the error continues, stop the database
manager and start it again. If the error continues, reinstall the
database manager.

If reinstallation does not correct the error, record the message number
(SQLCODE) and all information in the SQLCA if possible.

If trace was active, invoke the Independent Trace Facility at the
operating system command prompt. Then contact IBM as specified in this
guide.

 sqlcode: -10012

 sqlstate: 42724
