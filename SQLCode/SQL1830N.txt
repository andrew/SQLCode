

SQL1830N  RETURNS clause must be specified prior to a predicate
      specification using the EXPRESSION AS clause.

Explanation: 

The RETURNS clause is not specified before the PREDICATE clause that
includes the EXPRESSION AS clause. The RETURNS clause may have been
included after the predicate specification or may be missing.

The statement cannot be processed.

User response: 

Specify the CREATE FUNCTION statement with the RESULTS clause prior to
the PREDICATE clause.

 sqlcode: -1830

 sqlstate: 42627
