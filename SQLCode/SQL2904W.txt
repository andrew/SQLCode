

SQL2904W  The field value at line number "<line-number>" and byte
      position "<byte-position>" was truncated because the data is
      longer than the field length.

Explanation: 

The specified field has a value that is longer than the field's length.
The value has been truncated. For example, if the field is defined as
CHAR(3) but the value is "ABCDEF", the value is truncated to "ABC".

User response: 

If the truncation is acceptable, no further action is required. To avoid
this message, specify a longer field length on the INGEST command or
edit the input data so that the length of the field value is less than
or equal to the length of the field.
