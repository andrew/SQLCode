

SQL9306N  One or more of the field names are too long. The maximum
      length is "<max-length>".

Explanation: 

The total length of a field name includes the specified prefix and/or a
column suffix, which can either be its name or its number. This total
length must not exceed the maximum length.

The command cannot be processed.

User response: 

Ensure that all the field names do not exceed the maximum length.
