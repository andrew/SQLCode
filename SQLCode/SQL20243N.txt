

SQL20243N  The view "<view-name>" is the target in the MERGE statement,
      but is missing the INSTEAD OF trigger for the "<operation>"
      operation.

Explanation: 

The view "<view-name>" is a direct or indirect target in the MERGE
statement and has an INSTEAD OF trigger defined for it, but does not
have INSTEAD OF triggers defined for all operations. The trigger for the
"<operation>" operation is not present.

User response: 

Create INSTEAD OF triggers for the UPDATE, DELETE and INSERT operations
on view "<view-name>", or drop all INSTEAD OF triggers for the view.

sqlcode: -20243

sqlstate: 428FZ
