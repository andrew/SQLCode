

SQL2545W  Warning! The backup image on the TSM server is currently
      stored on mountable media. The time required to make it available
      is unknown.

Explanation: 

The backup image is not immediately accessible by the TSM server. The
restore process can continue and make the request to the server to
retrieve the data. The time required is unknown.

User response: 

Return to the utility with the callerac parameter indicating processing
to continue or end.
