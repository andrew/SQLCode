

SQL3517N  An unexpected record was read from the input source.

Explanation: 

The utility has encountered a record that is in a format that is not
valid. The source may have been corrupted when copied from the original
source.

Processing terminates.

User response: 

Copy the record from the original source in binary and restart the LOAD
or Import.
