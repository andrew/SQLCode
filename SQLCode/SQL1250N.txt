

SQL1250N  A database partition is not added because XML features have
      been used in the instance.

Explanation: 

An instance of the database manager has already had at least one
database created where an XML object, such as a table with a column of
data type XML or a XML schema repository object, was created. Even if
the XML objects no longer exist, the database manager instance is still
considered to have used XML. Once the database manager instance is known
to have used XML in this way, the instance is no longer permitted to
have more than a single database partition. Any attempt to add a
database partition will return this error.

User response: 

If any of the databases that are part of the instance are using columns
of data type XML or XML schema repository objects, then a different
instance will be required to establish an instance with multiple
database partitions.

If the databases in the instance are not actually intended for storage
of XML data, the instance must be cleansed of all references to XML
data. Indicators still exist in the instance and the databases, even
when there are no longer columns of data type XML and XML schema
repository objects. There are two approaches to clearing the instance
XML indicator and database level XML indicators.

1. Export non-XML data from the existing databases in the instance.
   Create a new instance with new databases. Import the data into the
   database in the new instance.
2. Use a password protected db2pdcfg option to change the XML indicators
   in the database configuration for each database within the instance
   and for the database manager configuration of the instance. Before
   this is done, it is extremely important that no database in the
   instance includes any columns of type XML and that the XML schema
   respository of each database is empty. Contact IBM Service to use
   this option.
