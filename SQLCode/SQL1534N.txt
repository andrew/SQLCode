

SQL1534N  The call to db2dsdcfgfill failed because invalid command
      options were specified.

Explanation: 

You can use the db2dsdcfgfill command to create and populate a
db2dsdriver.cfg configuration file based on the contents of the local
database directory, node directory, and DCS directory.

This message is returned when an invalid parameter or parameter value is
specified with the db2dsdcfgfill command.

User response: 

Run db2dsdcfgfill again, specifying valid command options.


   Related information:
   db2dsdcfgfill - Create configuration file db2dsdriver.cfg
   IBM data server driver configuration file
