

SQL3252N  The Load METHOD "<method>" option is incompatible with the
      specified file format.

Explanation: 

The Load utility was invoked with a METHOD option which is incompatible
with the file format specified.

User response: 

Review the documentation for restrictions and incompatibilities and
reissue the Load command using a different METHOD option or file format
to accommodate this restriction.
