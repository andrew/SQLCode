

SQL0107N  The name "<name>" is too long. The maximum length is
      "<length>".

Explanation: 

The name returned as "<name>" is too long. The maximum length in bytes
permitted for names of that type is indicated by "<length>". This does
not include any escape characters, if present.

Federated system users: If in a pass-through session, a data
source-specific limit might have been exceeded.

The statement cannot be processed.

Note: Where character data conversions are performed for applications
and databases running under different code pages, this error can be
returned because the result of the conversion exceeds the length limit.

User response: 

Choose a shorter name or correct the spelling of the object name.

Federated system users: For a pass-through session, determine what data
source is causing the error. Examine the SQL dialect for that data
source to determine which specific limit has been exceeded, and adjust
the failing statement as needed.

sqlcode: -107

sqlstate: 42622, 10901
