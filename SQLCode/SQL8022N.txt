

SQL8022N  Database partitioning feature is being used without database
      partitioning license. DB2 has detected that the database
      partitioning feature is being used but the database partitioning
      feature license has not been installed. Ensure that you have
      purchased database partitioning feature entitlements from your IBM
      representative or authorized dealer and have updated your license
      using the db2licm command.

User response: 




   Related information:
   DB2 license files
   db2licm - License management tool command
