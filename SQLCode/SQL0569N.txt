

SQL0569N  Authorization ID "<authorization-name>" does not uniquely
      identify a user, a group or a role in the system.

Explanation: 

The authorization ID specified by the GRANT or REVOKE statement does not
uniquely identify a user, a role, or a group in the security namespace.
The reference to "<authorization-name>" is ambiguous. Note that when
using DCE security, the USER, GROUP or ROLE keyword is always required.

User response: 

Change the statement to explicitly specify the USER, GROUP or ROLE
keyword to uniquely identify the specified authorization id.

 sqlcode: -569

 sqlstate: 56092
