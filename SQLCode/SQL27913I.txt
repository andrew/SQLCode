

SQL27913I  Input partitioning map was successfully read.

Explanation: 

This informational message indicates that the input partition map file
was successfully read.

User response: 

No action is required.
