

SQL4950N  Compound SQL statements containing user-defined SQLDAs are not
      supported in this environment.

Explanation: 

Compound SQL statements containing user-defined SQLDAs are not supported
in a 16-bit application.

User response: 

Move the statement out of the compound SQL block or replace the
statement with one that uses host variables instead of an SQLDA.
