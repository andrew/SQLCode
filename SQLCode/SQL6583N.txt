

SQL6583N  The partitioning key definition is incompatible with
      partitioned database load mode "<load-mode>".

Explanation: 

An identity column was specified as part of the partitioning key
definition, but the load mode specified was not PARTITION_AND_LOAD and
the identityoverride modifier was not specified.

User response: 

Either change the load mode to PARTITION_AND_LOAD, specify the
identityoverride modifier, or remove the identity column from the
partitioning key definition.
