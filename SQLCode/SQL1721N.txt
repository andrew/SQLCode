

SQL1721N  Starting the DB2 database manager failed because of a problem
      with a configuration file that is needed by RDMA.

Explanation: 

In DB2 pureScale environments, you can specify that communication
between the cluster caching facility (CF) and DB2 members uses Remote
Direct Memory Access (RDMA) by setting the database manager
configuration parameter CF_TRANSPORT_METHOD to "RDMA".

This message is returned when the CF_TRANSPORT_METHOD configuration
parameter is set to "RDMA" and there is a problem with configuration
files related to RDMA functionality.

Examples of problems that could cause this message to be returned:

*  The direct access transport (DAT) configuration file, dat.conf, is
   missing or contains invalid entries
*  Files are missing from the OpenFabrics Enterprise Distribution (OFED)
   package

User response: 

Respond to this error in one of the following ways:

*  If using RDMA is not needed, unset the CF_TRANSPORT_METHOD database
   manager configuration parameter.
*  To use RDMA, verify files associated with DAT configuration, such as
   the dat.conf file, and file associated with the OFED software.


   Related information:
   cf_transport_method - Network transport method configuration
   parameter
   Installation prerequisites for DB2 pureScale Feature (Linux)
