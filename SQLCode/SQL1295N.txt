

SQL1295N  The routing information object name being used for global
      directory access is not specified or not valid.

Explanation: 

In order to use global directory services to access a remote database
with a database protocol that is not native to this client, the name of
a routing information object must be specified either in the
route_obj_name database manager configuration parameter, or in the
DB2ROUTE environment variable. You either did not specify it, or the
name you specified is not valid.

Note that this message may be returned from an intermediate node
involved in your connection. For example, if you are trying to connect
to a DRDA server via a DB2 Connect gateway and your client workstation
does not use global directory services, this message may be returned
from the DB2 Connect gateway.

User response: 

Consult with your database administrator for the correct object name to
use, specify it and try again.

 sqlcode: -1295

 sqlstate: 08001
