

SQL0986N  A file error occurred while processing a user table. The table
      is not usable.

Explanation: 

The data in the table is no longer valid.

The system cannot process any statements using the table.

User response: 

Restore the database from a backup version if the database is
inconsistent.

If installing the sample database, drop it and install the sample
database again.

 sqlcode: -986

 sqlstate: 58004
