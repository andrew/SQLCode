

SQL6522N  The program found a path name for the input data files in the
      load command.

Explanation: 

Path name for input data files in the load command is not allowed. There
is a separate parameter (data_path) for this purpose.

User response: 

Please correct the configuration file.
