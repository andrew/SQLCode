

SQL4124W  A reference to column "<column>" derived from a column
      function is invalid in a WHERE clause.

Explanation: 

A VALUE EXPRESSION directly contained in the SEARCH CONDITION of a WHERE
clause must not include a reference to a column derived from a column
function.

Processing continues.

User response: 

Correct the SQL statement.
