

SQL6567N  The "<option-name>" option appears multiple times in the
      AutoLoader configuration file.

Explanation: 

An option parameter was specified multiple times inside the AutoLoader
configuration file.

User response: 

Correct the configuration file so that each option appears at most only
once.
