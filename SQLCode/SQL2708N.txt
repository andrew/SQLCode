

SQL2708N  Failed to open the output data file "<out-data-file>".

Explanation: 

The utility cannot open the output data file "<out-data-file>" for
writing.

User response: 

Please ensure the output data file is writable.
