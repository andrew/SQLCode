//
//  DetailViewController.swift
//  SQLCode
//
//  Created by Andrew on 15/7/12.
//  Copyright (c) 2015年 Andrew. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!


    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail: AnyObject = self.detailItem {
            let code:String=detail.description
            if let label = self.textView {
                let fileManager = NSFileManager.defaultManager()
                let mainBundle = NSBundle.mainBundle()
                let codepath = mainBundle.bundlePath.stringByAppendingString("/"+code+".txt")
                if let readData = fileManager.contentsAtPath(codepath){
                    let content = NSString(data: readData, encoding: NSUTF8StringEncoding)?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
                    label.text=content
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

