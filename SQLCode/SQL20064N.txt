

SQL20064N  Transform group "<group-name>" does not support any data type
      specified as a parameter or returned data type.

Explanation: 

The transform group "<group-name>" specified in the TRANSFORM GROUP
clause is not defined for any data type that is included in the
parameter list or the RETURNS clause of a function or method.

The statement cannot be processed.

User response: 

Remove the transform group from the function or method definition.

 sqlcode: -20064

 sqlstate: 428EN
