

SQL6013N  Host filename "<host-filename>" is either too long or does not
      begin with an alphabetic character.

Explanation: 

The host filename does not begin with an alphabetic character; or, if
the host is a VM System, the host filename, filetype, or filemode are
too long.

The command cannot be processed.

User response: 

Retry the command with a correct host filename syntax.
