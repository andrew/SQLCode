

SQL2540W  Restore is successful, however a warning "<warn>" was
      encountered during Database Restore while processing in No
      Interrupt mode.

Explanation: 

The Database Restore utility was invoked in No Interrupt mode, i.e.
WITHOUT PROMPTING. During the processing, one or more warnings were
encountered but not returned at the time they were encountered. The
Restore has completed successfully and the warning messages found are
shown at the completion in this message.

User response: 

Ensure that the action that caused this warning to be generated has not
resulted in a condition not wanted.
