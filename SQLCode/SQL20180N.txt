

SQL20180N  column "<column-name>" in table "<table-name>" cannot be
      altered as specified

Explanation: 

Column "<column-name>" cannot be altered for one of the following
reasons:

*  ADD COLUMN cannot be specified on ALTER TABLE if the table is defined
   as a history table.
*  ALTER COLUMN cannot be specified on ALTER TABLE if the table is
   defined as a history table.
*  ALTER COLUMN with SET GENERATED cannot be specified on ALTER TABLE
   for a column of a history table.
*  ALTER COLUMN with SET GENERATED AS cannot be specified on ALTER TABLE
   for a column that already is a generated column or has a defined
   default.
*  DROP COLUMN cannot be specified on ALTER TABLE if the table is
   defined as a history table.
*  DROP COLUMN with DROP NOT NULL cannot be specified on ALTER TABLE for
   a column defined as part of a BUSINESS_TIME period.
*  DROP COLUMN with DROP GENERATED cannot be specified on ALTER TABLE
   for a column defined as row-begin or row-end on a system-period
   temporal table.

The statement cannot be processed.

User response: 

Change the name of the column to a column that can be altered or
dropped, and recreate the table with the necessary attributes.

sqlcode: -20180

sqlstate: 428FR
