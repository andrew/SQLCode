

SQL16160N  XML document contains an anonymous simple type in element
      "<element-name>" with a name attribute.

Explanation: 

While parsing an XML document, the parser encountered an anonymous
simple type in an element named "<element-name>", yet that element had a
name attribute. The combination of anonymous type and name attribute is
not permitted.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16160

sqlstate: 2200M
