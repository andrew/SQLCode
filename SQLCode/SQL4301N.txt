

SQL4301N  Java or .NET interpreter startup or communication failed,
      reason code "<reason-code>".

Explanation: 

An error occurred while attempting to start or communicate with a Java
interpreter. The reason codes are: 

1        Java environment variables or Java database configuration
         parameters are invalid.

2        A Java Native Interface call to the Java interpreter failed.

3        The "db2java.zip" file may be corrupt or missing.

4        The Java interpreter has terminated itself and cannot be
         restarted.

5        Unable to load a dependent .NET library.

6        A call to the .NET interpreter failed.

User response: 

For Java, ensure that the Java database configuration parameters
(jdk_path and java_heap_sz) are correctly set. Ensure that a supported
Java runtime environment is installed. Ensure that internal DB2 classes
(COM.ibm.db2) are not overridden by user classes.

For .NET, ensure that the DB2 instance is configured correctly to run a
.NET procedure or function (mscoree.dll must be present in the system
PATH). Ensure that db2clr.dll is present in the sqllib/bin directory,
and that IBM.Data.DB2 is installed in the global assembly cache.

 sqlcode: -4301

 sqlstate: 58004
