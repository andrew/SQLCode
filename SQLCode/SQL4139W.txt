

SQL4139W  You must specify a VIEW COLUMN LIST for
      "<schema-name>"."<table>".

Explanation: 

If any two columns in the table specified by the QUERY SPECIFICATION
have the same column name, or if any column of that table is an unnamed
column, then a VIEW COLUMN LIST must be specified.

Processing continues.

User response: 

Correct the SQL statement.
