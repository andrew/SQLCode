

SQL6030N  START or STOP DATABASE MANAGER failed. Reason code
      "<reason-code>".

Explanation: 

The reason code indicates the error. The statement cannot be processed.

1        

         Cannot access the sqllib directory of the instance.


2        

         The full path name added to the profile filename is too long.


3        

         Cannot open the profile file.


4        

         This message is returned with reason code 4 for the following
         reasons:

          
         *  The specified value for DBPARTITIONNUM is not defined in the
            db2nodes.cfg file in the sqllib directory.
         *  The specified value for DBPARTITIONNUM or MEMBER is outside
            of the valid range. The valid range is: 0 - 999.
         *  The specified value for the db2iupdt command parameter -mid
            is outside of the valid range. The valid range is: 0 - 127.


5        

         The nodenum parameter must be specified when a command option
         is specified.


6        

         The port parameter value is not valid.


7        

         The new couple hostname/port is not unique.


8        

         The FORCE option cannot be specified when the QUIESCE option is
         specified.


9        

         The hostname and port parameters must be specified when using
         the ADD DBPARTITIONNUM option.


10       

         Cannot update the db2nodes.cfg file in the sqllib directory for
         the ADD DBPARTITIONNUM or RESTART option.


11       

         The hostname parameter value is not valid.


12       

         The pointer to the sqledbstrtopt or sqledbstopopt structure is
         not valid.


13       

         No port value is defined for your DB2 instance id
         (/etc/services file on UNIX-based systems).


14       

         The port value is not in the valid port range defined for your
         DB2 instance id (/etc/services file on UNIX-based systems).


15       

         A hostname value has no corresponding port 0 defined in the
         db2nodes.cfg file in the sqllib directory.


16       

         The value specified for the command or option parameter is not
         valid.


17       

         The DROP option cannot be specified when the NODENUM option is
         not specified.


18       

         The value specified for the callerac parameter is not valid.


19       

         Unable to create the UNIX socket directory /tmp/db2_<ver>_<rel>
         /$DB2INSTANCE.


20       

         This message is returned with reason code 20 for the following
         reasons:

          
         *  The number specified with the ADD DBPARTITIONNUM option
            already exists in the db2nodes.cfg file or has been already
            been added since the last stop database manager command was
            issued.
         *  The member number specified with the db2iupdt command -mid
            parameter already exists in the db2nodes.cfg file


21       

         The tablespace type specified with the ADD DBPARTITIONNUM
         option is not valid.


22       

         The tablespace specified with the ADD DBPARTITIONNUM option is
         out of range.


23       

         The computer name parameter must be specified for the ADD
         DBPARTITIONNUM option.


24       

         The user name parameter must be specified for the ADD
         DBPARTITIONNUM option.


25       

         The computer name is not valid.


26       

         The user name is not valid.


27       

         The password is not valid.


28       

         The password is expired.


29       

         The user account specified is either disabled, expired, or
         restricted.


31       

         The cluster interconnect netname parameter is not valid.


32       

         A DB2 database manager call to the cluster manager failed.


33       

         The identifier does not match the type that is defined in the
         db2nodes.cfg file in the sqllib directory.


34       

         The value specified for the QUIESCE option is not valid.


35       

         The member parameter must be specified when the QUIESCE option
         is used.


36       

         You can recover from a failed ADD or DROP operation in the DB2
         pureScale environment using the db2iupdt command with the
         -fixtopology parameter. This reason code is returned when the
         db2iupdt command is called with the -fixtopology parameter but
         the cluster caching facility does not detect any failed ADD or
         DROP operations. Because there are no topology fixes required,
         when the db2iupdt utility tried to stop the database, the stop
         command failed.


37       

         The transport type of the new member or CF specified does not
         match the transport type used by existing members and CFs, as
         defined in the netname field in the db2nodes.cfg file.


38       

         Starting a DB2 member failed because of uDAPL configuration
         problems or uDAPL runtime errors.


39       

         The setting of CF_NUM_CONNS is not valid for the number of
         members in the cluster. This can happen if CF_NUM_CONNS is set
         to a fixed value that is too high.


40       

         Add member cannot be issued from a host that is not in the DB2
         pureScale cluster.


41       

         An online add member operation cannot be performed on a host
         machine where a CF is currently running or where a CF is in the
         process of restarting.

User response: 

The action corresponding to the reason code is:

1        

         Ensure that the $DB2INSTANCE userid has the required
         permissions to access the sqllib directory of the instance.


2        

         Change the profile name to a shorter name so that the total
         length of the fully qualified path added to the profile name
         length is smaller than the SQL_PROFILE_SZ defined in the file
         sqlenv.h.


3        

         Ensure that the profile file exists.


4        
         *  Call the command again, specifying a value for
            DBPARTITIONNUM or MEMBER that is in the valid range of 0 -
            999. To specify an existing database partition, specify a
            value for DBPARTITIONNUM that corresponds to a database
            partition number that is defined in db2nodes.cfg.
         *  For the db2iupdt command, specify a value for the -mid
            parameter in the valid range of 0 - 127.


5        

         Resubmit the command with the nodenum parameter specified.


6        

         Ensure that the port value is between 0 and 999. If a value is
         not specified, the port value defaults to 0.


7        

         Ensure that the new couple hostname/port is not already defined
         in the db2nodes.cfg file in the sqllib directory.


8        

         Do not specify the FORCE option when you specify the QUIESCE
         option.


9        

         Ensure that the hostname and the port values are specified when
         you specify the ADD DBPARTITIONNUM option.


10       

         Ensure that the $DB2INSTANCE username has write access to the
         sqllib directory of the instance, that there is sufficient disk
         space, and the file exists.


11       

         Ensure that the specified hostname is defined on the system.


12       

         Ensure that the pointer is not NULL and points to the
         sqledbstrtopt for the sqlepstr() API, or to the sqledbstopopt
         structure for the sqlepstp() API.


13       

         Ensure that the services file (/etc/services on UNIX-based
         systems) contains an entry for your DB2 instance id.


14       

         Ensure that you only use port values that are specified in the
         services file (/etc/services file on UNIX-based systems) for
         your instance.


15       

         Ensure that all the hostname values have a port 0 defined in
         the db2nodes.cfg file in the sqllib directory including the
         restart option parameters.


16       

         Ensure that the value specified for the option parameter is
         within the valid range.


17       

         Specify the ADD DBPARTITIONNUM option when you specify the DROP
         option.


18       

         Ensure that the value specified for the callerac parameter is
         within the valid range.


19       

         Check the permission of the /tmp filesystem to make sure all
         intermediate directories of /tmp/db2_<ver>_<rel>/ $DB2INSTANCE
         can be created.


20       

         Ensure that the correct database partition or member number is
         being specified. You must stop the database manager to update
         the db2nodes.cfg file with nodes that have been added to the
         system since the previous stop database manager command.


21       

         Ensure that the value specified for the tablespace type is
         within the valid range.


22       

         Ensure that the tablespace value specified is defined in the
         db2nodes.cfg and is between 0 and 999.


23       

         Specify the computer name of the system on which the new member
         is created using the COMPUTER option.


24       

         Specify a valid domain account user name and password for the
         new member using the USER and PASSWORD options.


25       

         Resubmit the command with a valid computer name.


26       

         Resubmit the command with a valid user name.


27       

         Resubmit the command with a valid password.


28       

         Change/update the account password and resubmit the command.


29       

         Resubmit the command with a valid user account.


31       

         Ensure that the length of the cluster interconnect netname is
         not longer than SQL_HOSTNAME_SZ.


32       

         Correct the problem that caused the cluster manager call to
         fail and resubmit the command:

          
         *  Review the db2diag log file for error messages from the
            cluster manager.
         *  Respond to the cluster manager error messages in the db2diag
            log file to correct the underlying problem that prevented
            the cluster manager from removing the path from its
            configuration.
         *  Resubmit the START or STOP DATABASE MANAGER command.


33       

         Ensure that the identifier matches the type defined in the
         db2nodes.cfg file in the sqllib directory when issuing this
         command with either the MEMBER or CF option.


34       

         Ensure that the value specified for the QUIESCE option is
         within the valid range.


35       

         Resubmit the command with the member parameter specified.


36       

         No action is necessary in order to fix the topology. To view
         the current topology of the DB2 pureScale instance, perform the
         following steps:

          
         1. Query the topology of the instance using the following
            command: 
            db2instance -list

         2. If db2iupdt -fixtopology was issued after an add or drop
            operation, start the instance using the db2start command.


37       

         Resubmit the command, specifying a different netname. Ensure
         that the transport type for the netname specified, as defined
         in the db2nodes.cfg file, matches the transport type used by
         existing members and CFs.


38       

         Respond to reason code 38 by performing the following
         troubleshooting steps:

          
         1. Identify the uDAPL problems that are preventing the start
            operation from succeeding, using available diagnostic
            information in DB2 database logs (such as the db2diag
            diagnostic log files) and system logs.
         2. Troubleshoot and resolve the source of the uDAPL errors.
         3. Perform the start operation again.


39       

         Ensure the value of CF_NUM_CONNS is valid for the number of
         members in the cluster and resubmit the command. If
         CF_NUM_CONNS is set to a fixed value, it might need to be
         reduced in order for all members to be able to connect to the
         CF.


40       

         Run the add member command on a host that is part of the DB2
         pureScale cluster.


41       

         Respond to reason code 41 in one of the following ways:

          
         *  To perform an online add member operation, stop the CF on
            the target host machine before performing the add member
            operation.
         *  If all CFs are on the target host machine, stop the DB2
            instance and then add the member to that host machine.


   Related information:
   db2iupdt - Update instances command
   db2instance - Query state of DB2 instance command
   START DATABASE MANAGER command
   STOP DATABASE MANAGER command
   Creating node configuration files
   Configuring the network settings of hosts in a DB2 pureScale
   environment on an InfiniBand network (AIX)
   Configuring the network settings of hosts for a DB2 pureScale
   environment on an InfiniBand network (Linux)
   Identifying uDAPL over InfiniBand communication errors
