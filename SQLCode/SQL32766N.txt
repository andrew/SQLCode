

SQL32766N  Error message information from non-DB2 product:
      "<token-list>".

Explanation: 

A software product that interacts with DB2, but is not part of DB2, is
returning information about an error situation in "<token-list>".

User response: 

Use the information in "<token-list>" along with any documentation
available for the product that is returning the information to determine
how to handle this error situation.

sqlcode: -32766

sqlstate: (any SQLSTATE might be returned)
