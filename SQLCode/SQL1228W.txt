

SQL1228W  DROP DATABASE has completed but the database alias name or
      database name "<name>" could not be found on "<num>" nodes.

Explanation: 

The drop database command has completed successfully, however, there are
some nodes where the database alias or database name was not found. It
is possible that DROP DATABASE AT NODE was already performed on these
nodes.

User response: 

This is a warning message only. No response is necessary.
