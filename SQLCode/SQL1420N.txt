

SQL1420N  Too many concatenation operators.

Explanation: 

The database manager has reached an internal limit while evaluating an
expression, of long or large object string result type, that contains
concatenation operators.

User response: 

Reduce the number of concatenations in the expression and try again.

 sqlcode: -1420

 sqlstate: 54001
