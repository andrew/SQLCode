

SQL1275N  The stop time passed to the rollforward utility must be
      greater than or equal to timestamp "<timestamp>", because database
      "<name>" on nodes "<node-list>" contains information later than
      the specified time.

Explanation: 

The stop time passed to the rollforward utility must be greater than or
equal to the time the backup ended on the specified nodes.

If ",..." is displayed at the end of the node list, see the
administration notification log for the complete list of nodes.

(Note: If you are using a partitioned database server, the node number
or numbers indicate which nodes the error occurred on. Otherwise, it is
not pertinent and should be ignored.)

User response: 

Do one of the following:

*  Resubmit the command with a stop time greater than or equal to
   "<timestamp>".
*  Restore an earlier backup on the specified nodes, then issue the
   ROLLFORWARD DATABASE command again.


   Related information:
   ROLLFORWARD DATABASE command
