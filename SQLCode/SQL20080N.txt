

SQL20080N  Method specification for "<method-name>" cannot be dropped
      because a method body exists.

Explanation: 

The method specification "<method-name>" still has an existing method
body that must be dropped before the method specification can be
dropped.

The statement cannot be processed.

User response: 

Use the DROP METHOD statement with the same method specification to drop
the method body and then issue the ALTER TYPE statement again to drop
the method specification.

 sqlcode: -20080

 sqlstate: 428ER
