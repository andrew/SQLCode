

SQL3341N  Invalid table space name provided with the USE option. Reason
      code = "<reason-code>".

Explanation: 

Only system temporary table spaces can be used for rebuilding the
indexes in a table space other than the index table space. The page size
of the system temporary table space must match the page size of the
index table space.

User response: 

Resubmit the command with a table space name referring to a system
temporary table space with the correct page size. The "<reason-code>"
describes the failure as follows: 
1. Table space name in USE clause not found.
2. Table space must be a system temporary table space.
3. Page size in system temporary table space must match page size in
   index table space.
