

SQL4903N  The length of parameter "<n>" of function "<name>" is not
      valid.

Explanation: 

The length of the specified parameter in the specified function is not
valid.

The function cannot be completed.

User response: 

Correct the specified parameter and call the function again.

sqlcode: -4903

sqlstate: 42611


   Related information:
   SQL and XML limits
