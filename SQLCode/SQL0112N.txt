

SQL0112N  The operand of the column function "<name>" includes a column
      function, a scalar fullselect, or a subquery.

Explanation: 

The operand of a column function cannot include: 
*  a column function
*  a scalar fullselect
*  a subquery
*  an XMLQUERY or XMLEXISTS expression except as an operand of an XMLAGG
   column function.

In a SELECT list, the operand of an arithmetic operator cannot be a
column function that includes the DISTINCT keyword.

The statement cannot be processed.

User response: 

Correct the use of the column function to eliminate the invalid
expression and try again.

 sqlcode: -112

 sqlstate: 42607
