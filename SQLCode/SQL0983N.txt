

SQL0983N  The transaction log does not belong to the current database.

Explanation: 

The signature stored in the log file does not match the database
dependent signature. This error usually occurs when the user specified
that the log file be stored in a directory different from where the
database is stored. File redirection can be involved.

The statement cannot be processed.

User response: 

Resubmit the command with the proper access to the log file.

 sqlcode: -983

 sqlstate: 57036
