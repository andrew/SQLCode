

SQL0494W  The number of result sets is greater than the number of
      locators.

Explanation: 

The number of result set locators specified on the ASSOCIATE LOCATORS
statement is less than the number of result sets returned by the stored
procedure. The first "n" result set locator values are returned, where
"n" is the number of result set locator variables specified on the SQL
statement.

The SQL statement is successful. The SQLWARN3 field is set to 'Z'.

User response: 

Increase the number of result set locator variables specified on the SQL
statement.

sqlcode: +494

sqlstate: 01614
