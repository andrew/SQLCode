

SQL3503W  The utility has loaded "<number>" rows which equals the total
      count specified by the user.

Explanation: 

The number of rows loaded has equaled the total number of rows that the
user specified at invocation of the utility.

The utility has successfully completed.

User response: 

No response required.
