

SQL27942I  There are "<number-of-warnings>" warning message(s) and
      "<number-of-rejected-records>" rejected records.

Explanation: 

This informational message reports there are "<number-of-warnings>"
warning message(s) and "<number-of-rejected-records>" rejected record(s)
encountered during the operation.

User response: 

No action is required.
