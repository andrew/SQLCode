

SQL30021N  Execution failed because of a Distributed Protocol Error that
      will affect the successful execution of subsequent commands and
      SQL statements: Manager "<manager>" at Level "<level>" not
      supported.

Explanation: 

A system error occurred that prevented successful connection of the
application to the remote database. This message (SQLCODE) is produced
for SQL CONNECT statement. "<manager>" and "<level>" are numeric values
that identify the incompatibility between the client and the server.

The command cannot be processed.

User response: 

Record the message number, the "<manager>", and "<level>" values. Record
all error information from the SQLCA, if possible. Attempt to connect to
the remote database again.

If the problem persists, invoke the Independent Trace Facility at the
operating system command prompt. Then, contact your service
representative with the following information: 
*  Problem description
*  SQLCODE and reason code
*  SQLCA contents if possible
*  Trace file if possible.

 sqlcode: -30021

 sqlstate: 58010
