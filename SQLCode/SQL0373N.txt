

SQL0373N  A DEFAULT clause cannot be specified for column or SQL
      variable "<name>"

Explanation: 

A DEFAULT clause was specified when defining or changing column or SQL
variable "<name>". The data type for the column definition or SQL
variable declaration does not support DEFAULT. A CREATE or ALTER TABLE
statement cannot use the DEFAULT clause when defining the following:

*  an identity column
*  a ROWID column
*  an XML column
*  a row change timestamp column
*  a security label column
*  a row-begin column
*  a row-end column
*  a transaction-start-ID column

The statement cannot be processed.

User response: 

Remove the DEFAULT clause and resubmit the statement.

sqlcode: -373

sqlstate: 42623
