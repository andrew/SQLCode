

SQL1473N  Cannot commit the transaction because the time difference
      between the system time on the local node and the virtual
      timestamps of node(s) "<node-list>" is greater than the
      max_time_diff database manager configuration parameter. The
      transaction is rolled back.

Explanation: 

The system time difference for the machines in the configuration (listed
in the db2nodes.cfg file) is greater than the max_time_diff database
manager configuration parameter.

If ",..." is displayed at the end of the node list, see the syslog file
for the complete list of nodes.

User response: 

Synchronize the system times on all machines, and ensure that the
max_time_diff parameter is configured to allow for normal communication
delays among the database machines.

 sqlcode: -1473

 sqlstate: 40504
