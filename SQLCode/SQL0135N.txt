

SQL0135N  The input for a long string column in an INSERT statement or
      UPDATE statement must be from a host variable or be the keyword
      NULL.

Explanation: 

The UPDATE or INSERT is using constants, column names, or subqueries
where it should be using NULL or a host variable.

A long string column is either a LONG VARCHAR, LONG VARGRAPHIC,
VARCHAR(n) where n is greater than 254 but less than or equal to 32767,
or VARGRAPHIC(n) where n is greater than 127 but less than or equal to
16383.

User response: 

Refer to the DB2 for VM Application Programming manual for information
on the use of long strings. Correct the statement. Try again.

 sqlcode: -135

 sqlstate: 56033
