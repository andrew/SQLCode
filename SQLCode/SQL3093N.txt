

SQL3093N  The input file is not a valid WSF file.

Explanation: 

The first record in the worksheet format (WSF) file was not a
beginning-of-file (BOF) record, or the version of the WSF file is not
supported.

The IMPORT utility stops processing. No data is imported.

User response: 

Verify that the file is a valid WSF file and that the name was entered
correctly.
