

SQL4173W  You must declare cursor "<cursor>" before using it.

Explanation: 

The cursor specified has not been declared in a DECLARE CURSOR
statement.

Processing continues.

User response: 

Correct the SQL statement.
