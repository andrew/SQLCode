

SQL0553N  An object cannot be created with the schema name
      "<schema-name>".

Explanation: 

The reason the schema name "<schema-name>" is invalid depends on the
type of object that is being created.

*  Table, view, index and package objects cannot be created with the
   schema name SYSCAT, SYSFUN, SYSPUBLIC, SYSSTAT, SYSIBM, or SYSIBMADM.
   It is strongly advised that schema names should not start with SYS
   since additional schemas starting with these letters may be reserved
   for exclusive use of DB2 products in the future.
*  All other types of objects (for example: user defined functions,
   distinct types, triggers, schemas, aliases, usage lists) cannot be
   created with any schema name that starts with the letters SYS.

The statement cannot be processed.

User response: 

Use a valid schema name or remove the explicit schema name and rerun the
statement.

sqlcode: -553

sqlstate: 42939
