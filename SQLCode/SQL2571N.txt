

SQL2571N  An automatic restore is unable to proceed. Reason code:
      "<reason-code>".

Explanation: 

An error was encountered during the automatic restore process. This
error occurred during an incremental restore or during a rebuild of a
database from table space images or from a subset of table spaces in a
database image. The utility was unable to complete as intended. The
utility stops processing.

In the case of an incremental restore, this error is returned after the
initial definitions have been restored and the processing of the
required incremental restore set cannot be completed successfully.

In the case of a rebuild, this error is returned after the initial
target image has been restored and the processing of the remaining
required restore set cannot be completed successfully.

The error is a result of one of the following reason codes:

1        The backup image corresponding to the specified timestamp could
         not be found in the database history.

2        An error occurred trying to determine which table spaces to
         restore.

3        A required backup image could not be found in the database
         history.

4        The wrong rebuild type was specified for the incremental
         rebuild intended.

5        Cannot automatically restore only temporary table spaces.

User response: 

If this is an incremental restore or an incremental rebuild, issue a
RESTORE INCREMENTAL ABORT command to clean up any resources that may
have been created during processing. Perform a manual incremental
restore to restore the database from this backup image.

If this is a non-incremental rebuild then complete the rebuild, if
necessary, by issuing table space restores against the remaining images
required to rebuild the database as intended. Please see the DB2
diagnostics log for additional information.
