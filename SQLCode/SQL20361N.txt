

SQL20361N  The switch user request using authorization ID
      "<authorization-name>" within trusted context "<context-name>"
      failed with reason code "<reason-code>".

Explanation: 

The switch user request within the trusted context "<context-name>"
failed. The trusted connection is in an unconnected state.

User response: 

Use the authorization ID "<authorization-name>" and the following
explanations for reason code "<reason-code>" to determine what action to
take. 

1        The authorization ID is not an allowed user of the trusted
         context. Provide an authorization ID that is an allowed user of
         the trusted context as described in the trusted context
         definition.

2        The switch user request did not include the authentication
         token. Provide the authentication token for the authorization
         ID.

3        The trusted context object is either disabled, dropped, or its
         system authorization ID altered. Only the authorization ID that
         established the trusted connection is allowed. Provide this
         authorization ID.

sqlcode: -20361

sqlstate: 42517
