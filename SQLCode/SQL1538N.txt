

SQL1538N  The following keyword is not supported in the current
      environment: "<keyword>".

Explanation: 

The keyword you specified is not supported in a DB2 pureScale
environment.

User response: 

Rerun the command or execute the SQL statement again specifying only
supported keywords.

sqlcode: -1538

sqlstate: 56038
