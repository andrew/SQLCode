

SQL1494W  Activate database is successful, however, there is already a
      connection to the database.

Explanation: 

There is already database connection on one or more nodes.

User response: 

No action required.
