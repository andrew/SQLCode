

SQL4010N  Illegal nesting of Compound SQL statements.

Explanation: 

This error is returned when a BEGIN COMPOUND clause is detected as a
sub-statement of a Compound SQL statement.

User response: 

Resubmit the precompilation without the nested BEGIN COMPOUND.
