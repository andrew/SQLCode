

SQL16053N  The codepoint "<codepoint>" is not valid in a character
      string. Error QName=err:FOCH0001.

Explanation: 

A value that was passed to the fn:codepoints-to-string function in an
XQuery expression contains a "<codepoint>" that is not a legal XML
character.

The XQuery expression cannot be processed.

User response: 

Modify the argument to pass in valid codepoints only, or remove the
expression.

 sqlcode: -16053

 sqlstate: 10603
