

SQL1111N  The program name "<name>" specified was invalid.

Explanation: 

The DLL (dynamic link library) module or program name syntax is not
correct.

The command cannot be processed.

User response: 

Ensure that the DLL or program name is specified correctly.

 sqlcode: -1111

 sqlstate: 42724
