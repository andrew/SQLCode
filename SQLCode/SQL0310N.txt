

SQL0310N  SQL statement contains too many host variables.

Explanation: 

The maximum number of host variables was exceeded in the statement.

The statement cannot be processed.

User response: 

Ensure the statement has fewer host variables or is less complex.
