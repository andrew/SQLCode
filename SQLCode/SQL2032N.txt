

SQL2032N  The "<parameter>" parameter is not valid.

Explanation: 

The parameter is incorrectly specified. Either the value is out of range
or is incorrect.

User response: 

Resubmit the command with a correct value for the parameter.

 sqlcode: -2032

 sqlstate: 22531
