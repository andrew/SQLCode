

SQL20020N  Operation "<operation-type>" is not valid for typed tables.

Explanation: 

The operation identified by "<operation-type>" cannot be performed on a
typed table.

The statement cannot be processed.

User response: 

Remove the ADD COLUMN clause, ADD PERIOD clause, or SET DATATYPE clause
from the ALTER statement. Columns can only be added by re-defining the
table with a structured type that includes the new column as an
attribute. Similarly, the data type of a column can only be changed by
re-defining the table with a type that includes the column with a
different data type.

sqlcode: -20020

sqlstate: 428DH
