

SQL16119N  XML document contains an entity reference to "<entity-name>"
      that is not terminated.

Explanation: 

While parsing an XML document the parser encountered an entity reference
to "<entity-name>" without the expected termination character.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16119

sqlstate: 2200M
