

SQL3169N  The FORCEIN option may be used to make the PC/IXF column
      "<name>" acceptable for loading into database column "<name>".

Explanation: 

This is for information only about the optional use of the FORCEIN
option.

User response: 

No action required.
