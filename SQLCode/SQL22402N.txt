

SQL22402N  No activity monitor reports can be found.

Explanation: 

No activity monitor reports can be found because either the report ID or
the report type specified is invalid.

User response: 

Specify a valid report ID or a valid report type, then try the request
again.

sqlcode: -22402

sqlstate: 5U003
