

SQL0341N  A cyclic reference exists between the common table expressions
      "<name1>" and "<name2>".

Explanation: 

The common table expression "<name1>" refers to "<name2>" in a FROM
clause within its fullselect and "<name2>" refers to "<name1>" in a FROM
clause within its fullselects. Such forms of cyclic references are not
allowed.

The statement cannot be processed.

User response: 

Remove the cyclic reference from one of the common table expressions.

 sqlcode: -341

 sqlstate: 42835
