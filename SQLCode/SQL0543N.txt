

SQL0543N  A row in a parent table cannot be deleted because the check
      constraint "<constraint-name>" restricts the deletion.

Explanation: 

The delete operation cannot be executed because the target table is a
parent table and is connected with a referential constraint to a
dependent table with a delete rule of SET NULL. However, a check
constraint defined on the dependent table restricts the column from
containing a null value.

The statement cannot be processed.

User response: 

Examine the foreign key and its delete rule in the dependent table and
the conflicting check constraint. Change either the delete rule or the
check constraint so that they do not conflict with each other.

 sqlcode: -543

 sqlstate: 23511
