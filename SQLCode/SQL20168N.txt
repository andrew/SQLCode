

SQL20168N  The ALTER BUFFERPOOL statement is currently in progress.

Explanation: 

A buffer pool cannot be dropped or altered if an ALTER operation is
already in progress.

User response: 

Wait until the ALTER operation has completed. Use the snapshot monitor
to check the progress of the ongoing ALTER operation.

 sqlcode: -20168

 sqlstate: 55051
