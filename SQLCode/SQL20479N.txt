

SQL20479N  The ALTER or RENAME statement failed on the table
      "<table-name>" because the table is part of row or column access
      control definitions. Reason code "<reason-code>".

Explanation: 

The table "<table-name>" in an ALTER or RENAME statement cannot be
altered as specified for one of the following reasons:

1        

         The table is referenced in one or more column mask or row
         permission definitions.


2        

         A column in the table is referenced in one or more column mask
         or row permission definitions.

The statement cannot be processed.

User response: 

*  Drop the permission or mask and recreate them after the alter or
   rename operation is completed.
*  Consider temporarily protecting the table on which the permission or
   mask was defined at the row level to ensure there is no window in
   which the table remains without row and column access control
   protection.

sqlcode: -20479

sqlstate: 42917
