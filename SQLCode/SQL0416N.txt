

SQL0416N  You cannot specify a result column longer than 254 bytes in
      the SELECT or VALUES statements connected by a set operator other
      than UNION ALL.

Explanation: 

One of the SELECT or VALUES statements connected by a set operator
specifies a result column that is longer than 254 bytes. VARCHAR or
VARGRAPHIC result columns longer than 254 bytes can be used only with
the UNION ALL set operator.

The statement cannot be processed.

User response: 

Either use the UNION ALL operator instead of UNION, or remove the result
columns longer than 254 bytes from the SELECT or VALUES statements.

 sqlcode: -416

 sqlstate: 42907
