

SQL0959C  Not enough storage is available in the communication heap of
      the server to process the statement.

Explanation: 

All available memory in the server communication heap has been used.

The command or statement cannot be processed.

User response: 

Terminate the application on receipt of this message. Increase the size
of the communication heap ( comheapsz) parameter in the server
workstation database manager configuration file.

NOTE: This message is applicable only for releases of DB2 prior
toVersion 2 .

 sqlcode: -959

 sqlstate: 57011
