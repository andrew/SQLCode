

SQL7001N  Unknown command "<command>" was requested.

Explanation: 

The command submitted to REXX could not be recognized.

The command cannot be processed.

User response: 

Verify that the command is a valid SQL statement and rerun the
procedure. Note that all commands must be in uppercase.
