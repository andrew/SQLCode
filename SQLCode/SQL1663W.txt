

SQL1663W  Log archive compression is not fully enabled for
      "<log-archive-method>".

Explanation: 

Log archive compression is not fully enabled for "<log-archive-method>"
until "<log-archive-method>" is set to DISK, TSM, or VENDOR.

User response: 

You can change "<log-archive-method>" to DISK, TSM, or VENDOR with the
UPDATE DATABASE CONFIGURATION command.


   Related information:
   UPDATE DATABASE CONFIGURATION command
   logarchmeth2 - Secondary log archive method configuration parameter
   logarchmeth1 - Primary log archive method configuration parameter
