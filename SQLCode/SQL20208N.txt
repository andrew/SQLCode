

SQL20208N  The table "<table-name>" was not created. Reason code:
      "<reason-code>".

Explanation: 

The table cannot be created because it violates a restriction as
indicated by the following reason code:

1        

         The table used to define a staging table is not a materialized
         query table with the REFRESH DEFERRED option.


2        

         The table used to define the staging table already has a
         staging table associated with it.


3        

         A materialized query table that references nicknames cannot be
         created if the CREATE TABLE statement is issued from a
         non-catalog database partition.


4        

         A materialized query table that references a protected table, a
         view that depends on a protected table, or a nickname on which
         caching is not allowed cannot be created.


5        

         A security policy cannot be added to a materialized query table
         or to a staging table.


6        

         A materialized query table cannot have more than one
         DB2SECURITYLABEL column, and that column should not be wrapped
         in any function.


7        

         A materialized query table that references protected tables
         with more than one DB2SECURITYLABEL column cannot be created.


8        

         If a referred base table has a DB2SECURITYLABEL column, the
         column must appear in the FULLSELECT of the query.


9        

         When a table, tableA, is altered to become a materialized query
         table, if a column in the fullselect of the query is of type
         DB2SECURITYLABEL, the corresponding column of tableA must also
         be of type DB2SECURITYLABEL.


10       

         A staging table cannot be created for a replication-maintained
         materialized query table.

User response: 

The action corresponding to the reason code is:

1        

         Specify a materialized query table with the REFRESHED DEFERRED
         option to define the staging table.


2        

         Specify a materialized query table that is not associated with
         a staging table.


3        

         Issue the CREATE TABLE statement from the catalog database
         partition.


4        

         Correct the fullselect specified in the CREATE TABLE statement
         so that it does not reference a nickname on which caching is
         not allowed.


5        

         Remove the SECURITY POLICY clause from the CREATE TABLE
         statement


6        

         Reference a protected table that has at most one
         DB2SECURITYLABEL column and that column should not be wrapped
         in a function.


7        

         The materialized query table should have at most one
         DB2SECURITYLABEL column.


8        

         Select the same DB2SECURITYLABEL column for the materialized
         query table as referred to in the base table.


9        

         To alter a table, tableA, to become a materialized query table,
         define the fullselect so that any DB2SECURITYLABEL column in
         the fullselect corresponds to a column in tableA that is of
         type DB2SECURITYLABEL.


10       

         No action is required. A staging table cannot be created on a
         replication-maintained materialized query table.

sqlcode: -20208

sqlstate: 428FG


   Related information:
   Creating tables for staging data
   Materialized query table restrictions
