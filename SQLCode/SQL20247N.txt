

SQL20247N  Table "<table-name>" cannot be partitioned and contain a
      column with data type DATALINK.

Explanation: 

An attempt was made to create or alter table "<table-name>" in such a
way that would create a partitioned table that contained a column with
data type DATALINK. A table cannot be both partitioned and contain such
a column.

User response: 

Create or alter the table to be either partitioned or to include a
column of data type DATALINK, but not both.

sqlcode: -20247

sqlstate: 429BH
