

SQL6500W  RESTARTCOUNT in the load command may cause some problems.

Explanation: 

Since multiple loading processes for the same table are completely
independent, it is almost impossible to have an identical restartcount
for those multiple loading processes.

User response: 

Please make sure you have the correct load command.
