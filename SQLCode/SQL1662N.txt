

SQL1662N  Log archive compression failed while archiving or retrieving
      log file "<log-file>" for "<log-archive-method>" for database
      "<database>" on member "<member-number>". Reason code:
      "<reason-code>".

Explanation: 

While archive log file compression is enabled, an error occurred when
archiving or retrieving an archived log file.

User response: 

Check the db2diag.log file for more details.

Contact IBM Support.


   Related information:
   Archived log file compression
