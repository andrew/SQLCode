

SQL4953N  The call_type parameter of function "<name>" is not valid.

Explanation: 

The call_type parameter of the specified function in the application is
not valid.

The function cannot be completed.

User response: 

Correct the call_type parameter in the application program.
