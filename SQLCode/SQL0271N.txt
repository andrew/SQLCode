

SQL0271N  Index file for table with fid "<fid>" is either missing, or is
      not valid.

Explanation: 

The index file for the table with fid "<fid>" is required during
processing. The file is either missing, or it is not valid.

The statement cannot be processed, and the application is still
connected to the database. This condition does not affect other
statements that do not use the index on this table.

User response: 

Ensure that all users are disconnected from the database, then issue the
RESTART DATABASE command on all nodes. Then try the request again.

The index (or indexes) is re-created when the database is restarted.

 sqlcode: -271

 sqlstate: 58004
