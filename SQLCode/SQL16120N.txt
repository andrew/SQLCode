

SQL16120N  Document Type Definition (DTD) contains a content model
      specification for element "<element-name>" that was not
      terminated.

Explanation: 

While processing a DTD, the XML parser encountered a content model that
was not terminated for an element named "<element-name>".

Parsing or validation did not complete.

User response: 

Correct the DTD and try the operation again.

sqlcode: -16120

sqlstate: 2200M
