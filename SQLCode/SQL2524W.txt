

SQL2524W  Warning! Restoring to an existing database which appears to be
      the same but the alias "<dbase>" of the existing database does not
      match the alias "<dbase>" of the backup image. The target database
      will be overwritten by the backup version.

Explanation: 

The database seeds of the target database and the database image are the
same, indicating these are the same databases, the database names are
the same, but the database aliases are not the same. The target database
will be overwritten by the backup version.

User response: 

Return to the utility with the callerac parameter indicating processing
to continue or end.
