

SQL6073N  An add or drop operation of a database partition or DB2 member
      failed. SQLCODE = "<sqlcode>". Database name = "<database_name>".

Explanation: 

The operation failed with the specified SQLCODE.

User response: 

Take any required corrective action, then try the request again.


   Related information:
   Topology changes (add or delete members)
