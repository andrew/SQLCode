

SQL20504N  The statement failed because the target object of the
      anchored data type is unsupported or is being used in an
      unsupported context.

Explanation: 

An anchored data type is a data type that is defined to be the same as
that of another object. If the underlying object data type changes, the
anchored data type also changes. An anchored data type can reference a
variety of objects, but certain objects cannot be referenced.

This message is returned when an attempt is made to reference an
unsupported target in an anchored data type, or to reference a target in
an anchored data type in an unsupported context.

User response: 

Remove any anchored data type references that are unsupported or that
are used in unsupported contexts and then reissue the statement.

sqlcode: -20504

sqlstate: 428HS


   Related information:
   Anchored data type
   Restrictions on the anchored data type
