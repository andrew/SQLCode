

SQL1241N  An invalid value was specified for the "<tbs-name>" table
      space definition when creating a database. Attribute is
      "<string>".

Explanation: 

The value for a table space attribute was out of range. See the DB2
Information Center (http://publib.boulder.ibm.com/infocenter/db2luw/v9)
for the format of the sqletsdesc structure used for the create database
api. The identified attribute is the field name of this structure.

User response: 

 Correct the create database request.
