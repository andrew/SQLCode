

SQL3966N  Synchronization session failed. Reason code "<reason-code>".

Explanation: 

The synchronization session failed to complete for one of the following
reasons: 
*  (01) Missing authentication information.
*  (02) Some scripts required for synchronization are missing.
*  (03) System files are corrupted or missing.
*  (04) A system error prevented scripts from executing.

User response: 

Try the request again. If failure persists, contact the help desk or
your system administrator.
