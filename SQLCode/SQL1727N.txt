

SQL1727N  The statement was not precompiled because the following host
      variable array or structure array contains an unsupported data
      type: "<variable_name>"

Explanation: 

The COMPATIBILITY_MODE ORA precompile option allows the use of host
variable arrays, structure arrays, and indicator arrays in an embedded
SQL statement. A host variable array or structure array declared for an
INSERT, UPDATE, or DELETE statement must contain supported data types.

User response: 

1. Remove the unsupported data types in the declared host variable
   arrays, or structure arrays.
2. Precompile the modified embedded SQL application.


   Related information:
   C-array host and indicator variables
