

SQL0318N  An END DECLARE SECTION was found without a previous BEGIN
      DECLARE SECTION.

Explanation: 

An END DECLARE SECTION statement was found, but there was no previous
BEGIN DECLARE SECTION.

The statement cannot be processed.

User response: 

Enter a BEGIN DECLARE SECTION before an END DECLARE SECTION.
