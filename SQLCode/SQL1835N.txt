

SQL1835N  Extended Search object "<object_name>" of type "<object_type>"
      could not be found on the remote Extended Search Server
      "<es_host_name>".

Explanation: 

Extended Search object "<object_name>" of type "<object_type>" could not
be found on the remote Extended Search Server "<es_host_name>".

User response: 

Verify that the object name is defined on this server and is of type
"<object_type>".
