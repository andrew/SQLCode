

SQL2048N  An error occurred while accessing object "<object>". Reason
      code: "<reason-code>".

Explanation: 

An error occurred while accessing an object during the processing of a
database utility. The following is a list of reason codes:

1        

         An invalid object type is encountered.


2        

         A lock object operation failed. The lock wait may have reached
         the lock timeout limit specified in the database configuration.


3        

         An unlock object operation failed during the processing of a
         database utility.


4        

         Access to an object failed.


5        

         An object in the database is corrupted.


6        

         This message is returned with reason code 6 when a table space
         cannot be accessed for one of multiple reasons, including the
         following reasons:

          
         *  The state of the table space does not support the type of
            access attempt. Some examples of database states that can
            prevent some types of database access include: quiesced,
            offline, and backup in progress.
         *  One or more of the table space containers are not available.

          

         For example, this message can be returned with reason code 6
         when an attempt is made to back up a table space that is
         already in the processes of being backed up.


7        

         A delete object operation failed.


8        

         Trying to load/quiesce into a table that is not defined on this
         partition.


9        

         The BACKUP utility encountered an end-of-file in an unexpected
         place while processing an object. This does not necessarily
         mean that the data is corrupt, but the BACKUP utility is not
         able to process the data in its current state.

The utility stops processing.

User response: 

1        

         Ensure that "<object>" is of valid type.


2        

         Increase the locktimeout database configuration parameter.


3        

         Check that there are not any issues while locking "<object>",
         then retry the operation.


4, 7     

         Check that "<object>" exists and is accessible. Ensure that you
         have correct privileges/permissions to access it.


5        

         If object is db2rhist.asc, make sure that the db2rhist file is
         accessible by the instance owner. If permissions are set
         correctly, the db2rhist file may be corrupt. Move the existing
         file from its existing location, or delete it. DB2 will create
         a new db2rhist file the next time it needs to access it. Note:
         deleting the db2rhist file will cause historical information in
         the db2rhist file to be lost.

          

         If "<object>" is any other database control file, you may need
         to recover the database.


6        

         Determine the current state of the table space using the
         MON_GET_TABLESPACE table function and then perform the
         following troubleshooting steps:

          
         *  If the table space is offline, attempt to determine the
            underlying problem and correct it. For example: 
            *  If the file system is not mounted, mount the file system,
               and then bring the table space online.
            *  If table space files have been deleted, perform a restore
               operation.

         *  If the table space is quiesced, make the table space
            available using the QUIESCE TABLESPACES FOR TABLE command
            with either the RESET clause or the EXCLUSIVE clause. Note
            that the userid holding the quiesce might be needed to
            perform the QUIESCE RESET or QUIESCE EXCLUSIVE operation.
         *  If the table space is in the process of being backed up,
            wait for the backup operation to complete.


8        

         Ensure that you specify an appropriate table.


9        

         Perform a REORG operation on the specified table, and resubmit
         the BACKUP command.


   Related information:
   Backup overview
