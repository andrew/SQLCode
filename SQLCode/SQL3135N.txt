

SQL3135N  The number of columns in the METHOD parameter is greater than
      the number of columns in the target table.

Explanation: 

The number of data columns in the METHOD parameter must be less than or
equal to the number of data columns in the actual table.

User response: 

Specify the correct number of input columns in the METHOD parameter and
resubmit the command.
