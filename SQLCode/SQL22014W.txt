

SQL22014W  The health monitor returned no health related data.

Explanation: 

There is no health data for this instance or the health monitor is off.

User response: 

Verify that the health monitor is running on this instance.
