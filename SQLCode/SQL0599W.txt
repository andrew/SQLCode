

SQL0599W  Comparison functions are not created for a distinct type based
      on a long string data type.

Explanation: 

Comparison functions are not created for a distinct type based on a long
string data type (BLOB, CLOB, DBCLOB, LONG VARCHAR, or LONG VARGRAPHIC)
since the corresponding functions are not available for these built-in
data types.

This is a warning situation. The statement is processed successfully.

User response: 

No action is required.

 sqlcode: +599

 sqlstate: 01596
