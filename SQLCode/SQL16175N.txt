

SQL16175N  XML document contains a schema where the root element
      "<element-name>" could not be resolved.

Explanation: 

While parsing an XML document, the parser encountered a problem
resolving the root element of a schema document.

Parsing or validation did not complete.

User response: 

Correct the XML document and try the operation again.

sqlcode: -16175

sqlstate: 2200M
