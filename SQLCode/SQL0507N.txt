

SQL0507N  The cursor specified in the UPDATE or DELETE statement is not
      open.

Explanation: 

The program attempted to execute an UPDATE or DELETE WHERE CURRENT OF
cursor statement when the specified cursor was not open.

The statement cannot be processed. No update or delete was performed.

User response: 

Check for a previous message (SQLCODE) that may have closed the cursor.
Note that after the cursor is closed, any fetches or close cursor
statements receive SQLCODE -501 and any updates or deletes receive
SQLCODE -507. Correct the logic of the application program to ensure
that the specified cursor is open at the time the UPDATE or DELETE
statement is executed.

 sqlcode: -507

 sqlstate: 24501
