

SQL1592N  The INCREMENTAL option is not valid with reason code
      "<reason-code>" since the table "<table-name>" cannot be
      incrementally processed.

Explanation: 

The cause is based on the reason code "<reason-code>":

32       

         The table is not a REFRESH IMMEDIATE materialized query table,
         nor a REFRESH DEFERRED materialized query table with a
         supporting staging table, nor a PROPAGATE IMMEDIATE staging
         table.


33       

         A LOAD REPLACE or LOAD INSERT has occurred to the table if it
         is a materialized query table or staging table.


34       

         A LOAD REPLACE has occurred to the table after the last
         integrity check.


35       

         One of the following:

          
         *  The materialized query or staging table was newly created.
            Full processing is required for the first time the table is
            checked for integrity after it has been created.
         *  New constraint has been added to the table itself or its
            parents (or its underlying table if it is a materialized
            query table or staging table) while in the Set Integrity
            Pending state.
         *  If it is a materialized query table or staging table, a LOAD
            REPLACE has occurred to any underlying table of the table
            after the last refresh.
         *  If it is a materialized query table, at least one underlying
            table was forced to full access (using the FULL ACCESS
            option) before the materialized query table was refreshed.
         *  If it is a staging table, at least one underlying table was
            forced to full access (USING the FULL ACCESS option) before
            the staging table was propagated.
         *  If it is a deferred materialized query table and its
            corresponding staging table is in incomplete state.
         *  Some of its parents (or underlying tables for materialized
            query tables or staging tables) have been non-incrementally
            checked for integrity.
         *  The table was in the Set Integrity Pending state before
            database upgrade. Full processing is required for the first
            time the table is checked for integrity after database
            upgrade.
         *  The table was placed in the Set Integrity Pending state
            during a point in time roll-forward operation.

User response: 

Do not specify the INCREMENTAL option. The system will check the entire
table for constraint violations (or if it is a materialized query table,
recompute the materialized query table definition query).

sqlcode: -1592

 sqlstate: 55019
