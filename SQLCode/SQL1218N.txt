

SQL1218N  There are no pages currently available in bufferpool
      "<buffpool-num>".

Explanation: 

All of the pages in the bufferpool are currently being used. A request
to use another page failed.

The statement cannot be processed.

User response: 

The bufferpool is not large enough to provide pages to all database
processes or threads at this time. The bufferpool is too small or there
are too many active processes or threads.

The statement may be successful if executed again. If this error occurs
frequently, some or all of the following actions may prevent further
failures: 
1. increase the bufferpool size
2. decrease the maximum number of database agents and/or connections
3. decrease the maximum degree of parallelism
4. decrease the prefetch size for table spaces that are in this
   bufferpool
5. move some table spaces into other bufferpools.

 sqlcode: -1218

 sqlstate: 57011
