

SQL3327N  A system error occurred (reason code1 = "<reason-code-1>" and
      reason code2 = "<reason-code-2>").

Explanation: 

A system error occurred during processing.

The utility stops processing.

User response: 

Record all error information from the SQLCA, if possible. Retain the
message file. Terminate all applications using the database. Reboot the
system. Restart the database. Try the command again.

If sufficient memory resources exist and the problem continues, invoke
the Independent Trace Facility at the operating system command prompt.
