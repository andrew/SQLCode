

SQL6540N  File type "<file-type>" specified in the load command is not
      valid.

Explanation: 

Valid file types are ASC (positional ASCII) or DEL (delimited ASCII).

User response: 

Correct the load command in the configuration file.
