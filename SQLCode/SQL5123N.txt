

SQL5123N  Database "<name>" cannot be configured because an I/O error
      occurred while accessing the log control file.

Explanation: 

There are two log control files:

*  Primary log control file SQLOGCTL1.LFH
*  Secondary log control file SQLOGCTL2.LFH 

   The secondary log control file is a mirror copy of the primary log
   control file for use in the event that there is a problem with the
   primary log control file.

This error was returned because the DB2 database manager could access
neither the primary log control file nor the secondary log control file
for the specified database.

If the database manager can access neither of the log control files, you
cannot use the database.

The requested change was not made.

User response: 

Restore the database from a backup copy or recreate the database.
