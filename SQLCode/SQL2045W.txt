

SQL2045W  Warning! Error "<error>" occurred while writing to media
      "<media>".

Explanation: 

A database utility process encountered error "<error>" returned by the
operating system while writing to the media "<media>". The utility
returns so that the user may attempt to fix the problem or cancel the
operation.

The utility waits for a response to continue.

User response: 

Consult the troubleshooting documentation for your operating system and
correct the "<error>" condition. Return to the utility with the correct
caller action parameter to indicate if processing should continue or
terminate.
