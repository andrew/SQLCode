

SQL16059N  No statically known namespace exists for the prefix in the
      value "<value>" cast to xs:QName. Error QName=err:FONS0003.

Explanation: 

An XQuery expression that casts to xs:QName specifies a "<value>" that
uses a prefix, but the prefix cannot be mapped to a URI because there is
no statically known namespace for the specified prefix.

The XQuery expression cannot be processed.

User response: 

In the cast expression, specify a prefix in the value that exists as a
statically known namespace. If the prefix is correct, ensure that there
is a namespace declaration for the specified prefix.

 sqlcode: -16059

 sqlstate: 10607
