

SQL0104N  An unexpected token "<token>" was found following "<text>".
      Expected tokens may include: "<token-list>".

Explanation: 

A syntax error in the SQL statement or the input command string for the
SYSPROC.ADMIN_CMD procedure was detected at the specified token
following the text "<text>". The "<text>" field indicates the 20
characters of the SQL statement or the input command string for the
SYSPROC.ADMIN_CMD procedure that preceded the token that is not valid.

As an aid, a partial list of valid tokens is provided in the SQLERRM
field of the SQLCA as "<token-list>". This list assumes the statement is
correct to that point.

This message can be returned when text is passed to the command line
processor (CLP) in command mode and the text contains special characters
that are interpreted by the operating system shell, such as single or
double quotes, which are not identified with an escape character.

The statement cannot be processed.

User response: 

Respond to this error in one of the following ways:

*  Examine and correct the statement in the area of the specified token.
*  If you are using the CLP in command mode and there are any special
   characters, such as quotes, in the command, use an escape character,
   such as the backslash character, to cause the operating system shell
   to not take any special action for those special characters. You
   could also issue the statement using CLP in interactive mode or batch
   mode to avoid any processing of special characters by the operating
   system shell.

sqlcode: -104

sqlstate: 42601


   Related information:
   Command line processor features
