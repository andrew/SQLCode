

SQL3254N  The utility is beginning to load data from the Table
      "<schema>"."<tablename>" in database "<database>".

Explanation: 

This is an informational message indicating that a load from an SQL
statement fetching the contents of a table on a cataloged database has
begun.

User response: 

No action is required.
