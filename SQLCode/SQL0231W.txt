

SQL0231W  Current position of cursor "<cursor-name>" is not valid for
      FETCH of the current row.

Explanation: 

A FETCH CURRENT or FETCH RELATIVE 0 statement was issued for scrollable
cursor "<cursor-name>". The operation is not valid, because the cursor
is not positioned on a row of the result table. A FETCH of the current
row is not allowed following a FETCH BEFORE or FETCH AFTER statement, or
following a FETCH statement that resulted in SQLCODE +100.

The statement cannot be processed. The cursor position is unchanged.

User response: 

Ensure that the cursor is positioned on a row of the result table before
attempting to fetch the current row.

 sqlcode: +231

 sqlstate: 02000
