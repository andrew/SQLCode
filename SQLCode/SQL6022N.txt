

SQL6022N  System database directory is not shared by all nodes.

Explanation: 

All nodes should be accessing one physical copy of the system database
directory.

The command cannot be processed.

User response: 

Ensure that all nodes are accessing the system database directory that
resides in the sqllib directory, then try the request again.
