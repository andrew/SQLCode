

SQL0265N  A duplicate member number or database partition number was
      detected in the list of database partition numbers.

Explanation: 

For the CREATE DATABASE PARTITION GROUP statement, a database partition
number can only appear once in the ON DBPARTITIONNUMS clause.

For the CREATE TABLESPACE and ALTER TABLESPACE statement, a database
partition number can appear only once and in only one ON DBPARTITIONNUMS
clause.

For the ALTER DATABASE PARTITION GROUP statement or REDISTRIBUTE
DATABASE PARTITION GROUP command, one of the following occurred:

*  The database partition number appeared more than once in the ADD
   DBPARTITIONNUMS or the DROP DBPARTITIONNUMS clause.
*  The database partition number appeared in both the ADD
   DBPARTITIONNUMS and the DROP DBPARTITIONNUMS clause.
*  The database partition number to be added is already a member of the
   database partition group.

The statement cannot be processed.

For a call WLM_ALTER_MEMBER_SUBSET statement, the member to be added is
already a member of the member subset.

User response: 

Ensure that the database partition names or database partition numbers
in the ON DBPARTITIONNUMS, ADD DBPARTITIONNUMS, or DROP DBPARTITIONNUMS
clause are unique. For the CREATE TABLESPACE and ALTER TABLESPACE
statements, ensure that a database partition number appears in no more
than one ON DBPARTITIONNUMS clause.

In addition, for the ALTER DATABASE PARTITION GROUP statement or
REDISTRIBUTE DATABASE PARTITION GROUP command:

*  Do not specify a database partition number in both the ADD
   DBPARTITIONNUMS and the DROP DBPARTITIONNUMS clause.
*  Remove the database partition number from the ADD DBPARTITIONNUMS
   clause if the database partition number is already defined in the
   database partition group.

For the call WLM_ALTER_MEMBER_SUBSET statement, remove the member number
from the member list.

sqlcode: -265

sqlstate: 42728


   Related information:
   Member subsets overview
