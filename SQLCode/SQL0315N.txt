

SQL0315N  The host variable is incorrectly declared.

Explanation: 

The host variable is not declared correctly for one of the following
reasons: 
*  The type specified is not one that is supported.
*  The length specification is 0, negative or too large.
*  An incorrect syntax is specified.

The variable remains undefined.

User response: 

Ensure that you correctly specify only the declarations the database
manager supports.
