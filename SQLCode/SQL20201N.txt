

SQL20201N  The install, replace or remove of "<jar-id>" failed as the
      jar name is invalid.

Explanation: 

The jar name specified on the install, replace or remove jar procedure
was invalid. For example, the jar id may be of the improper format, may
not exist to be replaced or removed, or can not be installed as it
already exists.

User response: 

Ensure the jar id is of the correct format. If the jar id exists, it may
need to be removed before it can be installed. For the remove or replace
procedures, ensure the jar id exists.

 sqlcode: -20201

 sqlstate: 46002
