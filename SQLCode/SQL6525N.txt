

SQL6525N  The program can not read the input data file "<file-name>".

Explanation: 

Either the input data file was not found, or it is not readable.

User response: 

Please check the existence and permissions of the input data file.
