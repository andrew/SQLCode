

SQL0902C  A system error occurred. Subsequent SQL statements cannot be
      processed. IBM software support reason code: "<reason-code>".

Explanation: 

This message is returned when the database manager encounters a critical
error, such as a severe operating system error, or an error accessing
storage media, which might prevent a database from continuing to be
usable. There are different scenarios in which the database manager
might encounter an operating system error, or a severe media error. Here
is one example of a scenario in which this message can be returned:

*  This message can be returned when a database manager resource
   requires a semaphore, but there are not enough semaphores to satisfy
   the request.

The runtime token, "<reason-code>", might sometimes be empty, and is
intended to assist IBM software support personnel only.

User response: 

Respond to this message by performing the following troubleshooting
steps:

1. Review diagnostic information in db2diag log files to identify errors
   that occurred before this message was returned.
2. If there are messages in the db2diag log files indicating that too
   many semaphores have been requested, or that there are not enough
   semaphores to serve the database manager requests, increase the
   number of semaphores using operating system parameters.

If the error continues after performing the troubleshooting steps
described, contact IBM software support for assistance:

1. Collect diagnostic information using trace facilities such as the DB2
   trace and the Independent Trace Facility.
2. Collect the following diagnostic information: 
   *  Problem description
   *  SQLCODE
   *  Reason "<reason>"
   *  SQLCA contents if possible
   *  Trace files, if possible

3. Contact IBM software support.

Federated system users: isolate the problem to the data source failing
the request and take the necessary diagnostic steps for that data
source. The problem determination procedures for data sources vary, so
refer to the applicable data source manuals.

sqlcode: -902

sqlstate: 58005


   Related information:
   Troubleshooting data source connection errors
   Modifying kernel parameters for DB2 Connect (HP-UX)
   Modifying kernel parameters for DB2 Connect (Linux)
   Modifying kernel parameters for DB2 Connect (Solaris)
   Troubleshooting multi-threaded embedded SQL applications
   DB2 diagnostic (db2diag) log files
   Analyzing db2diag log files using db2diag tool
   Interpretation of diagnostic log file entries
   Contacting IBM Software Support
   Exchanging information with IBM
