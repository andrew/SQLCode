

SQL2021N  The correct diskette is not in the drive.

Explanation: 

The diskette to be used for Backup Database or for Restore Database is
not in the drive or is not valid.

The command cannot be processed.

User response: 

Verify that the correct diskette is in the drive or insert a new
diskette.
