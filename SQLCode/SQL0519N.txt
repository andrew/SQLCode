

SQL0519N  The PREPARE statement identifies the SELECT or VALUES
      statement of the open cursor "<name>".

Explanation: 

The application program attempted to prepare the SELECT or VALUES
statement for the specified cursor when that cursor is already open.

The statement cannot be prepared. The cursor was not affected.

User response: 

Correct the application program so it does not attempt to prepare the
SELECT or VALUES statement for a cursor that is open.

 sqlcode: -519

 sqlstate: 24506
