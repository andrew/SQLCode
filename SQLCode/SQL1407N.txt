

SQL1407N  The "<option-name>" option is incompatible with "<feature>".

Explanation: 

The utility does not support the "<option_name>" option with
"<feature>".

User response: 

Resubmit the command using compatible options.
