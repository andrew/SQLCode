

SQL1046N  The authorization ID is not valid.

Explanation: 

The authorization specified at logon is not valid for either the data
source or the database manager. One of the following occurred:

*  The authorization contains more than 30 characters for Windows
   platforms or 8 characters for other platforms.
*  The authorization contains characters not valid for an authorization.
   Valid characters are A through Z, a through z, 0 through 9, #, @ and
   $.
*  The authorization is PUBLIC or public.
*  The authorization begins with SYS, sys, IBM, ibm, SQL or sql.
*  The authorization violates some data source-specific naming
   convention.

The command cannot be processed.

User response: 

Log on with a valid authorization ID.

Federated system users: if necessary isolate the problem to the data
source rejecting the request and use an authorization ID valid for that
data source.

sqlcode: -1046

sqlstate: 28000


   Related information:
   Troubleshooting data source connection errors
