

SQL6048N  A communication error occurred during START or STOP DATABASE
      MANAGER processing.

Explanation: 

A TCP/IP communication error occurred while the START or STOP DATABASE
MANAGER command was trying to establish connection with all the nodes
defined in the sqllib/db2nodes.cfg file including the new node you
attempted to add.

This message can also be returned when a password has expired.

User response: 

Do the following:

*  Ensure that the node has the proper authorization defined in the
   .rhosts or the host.equiv files.
*  Ensure that the application is not using more than (500 + (1995 - 2 *
   total_number_of_nodes)) file descriptors at the same time.
*  Ensure all the Enterprise Server Edition environment variables are
   defined in the profile file.
*  Ensure the profile file is written in the Korn Shell script format.
*  Ensure that all the host names defined in the db2nodes.cfg file in
   the sqllib directory are defined on the network and are running.
*  Ensure that the DB2FCMCOMM registry variable is set correctly.
*  Review the db2diag log files for more information.
