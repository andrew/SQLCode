

SQL16215N  XML document contains an included schema "<include-uri>" that
      has a different target namespace "<targetns-uri>".

Explanation: 

While parsing an XML document, the parser encountered a namespace
mismatch. The XML schema that was included with URI "<include-uri>" has
a different target namespace URI "<targetns-uri>".

Parsing or validation did not complete.

User response: 

Correct the mismatch in namespaces for the XML document and try the
operation again.

sqlcode: -16215

sqlstate: 2200M
