

SQL3149N  "<number-1>" rows were processed from the input file.
      "<number-2>" rows were successfully inserted into the table.
      "<number-3>" rows were rejected.

Explanation: 

This summary message tells how many rows of data were read from the
input file, how many rows were successfully inserted into the database
table, and how many rows were rejected. If using the INSERT_UPDATE
option, the number of rows updated is the number of rows processed minus
the number inserted and rejected.

User response: 

None, because this is a summary message. The detail messages may suggest
corrective action.
