

SQL0298N  Bad container path.

Explanation: 

The container path violates one of the following requirements:

*  Container paths must be valid fully-qualified absolute paths or valid
   relative paths. The latter are interpreted relative to the database
   directory.
*  For EXTEND, REDUCE, RESIZE and DROP operations, the specified
   container path must exist.
*  The path must be read/write accessible to the instance id (check file
   permissions on UNIX-based systems).
*  Containers must be of the type specified in the command (directory,
   file or device).
*  Containers (directories) in system managed table spaces must be empty
   when designated as containers and must not be nested underneath other
   containers.
*  The containers for one database must not be located underneath the
   directory of another database, and they may not be underneath any
   directory that appears to be for another database. This rules out any
   directory of the form SQLnnnnn, where 'n' is any digit.
*  The container must be within the file size limit for the operating
   system.
*  Containers (files) for dropped database managed table spaces can only
   be reused as containers (directories) for system managed table spaces
   after all agents terminate and vice versa.
*  During a redirected restore, an SMS container was specified for a DMS
   table space or a DMS container was specified for an SMS table space.
*  The specified type of the container for an EXTEND, REDUCE, RESIZE, or
   DROP operation does not match the type of the container (FILE or
   DEVICE) that was specified when the container was created.

This message will also be returned if any other unexpected error
occurred which prevents DB2 from accessing the container.

If you are using a cluster manager, this error can be returned if the
DB2 database manager failed to add the database container path to the
cluster manager configuration. If the cluster manager cannot access this
path, the cluster manager will not be able to successfully manage a
failover involving this path. Error messages from the cluster manager
will be recorded in the db2diag log file.

User response: 

Specify another container location or change the container to make it
acceptable to DB2 (such as changing file permissions) and try again.

If you are using a cluster manager, correct the problem and resubmit the
command:

1. Review the db2diag log file for error messages from the cluster
   manager.
2. Respond to the cluster manager error messages in the db2diag log file
   to correct the underlying problem that prevented the DB2 database
   manager from adding the path to the cluster manager configuration.
3. Resubmit the command.

sqlcode: -298

sqlstate: 428B2
