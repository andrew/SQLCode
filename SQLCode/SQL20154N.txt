

SQL20154N  The requested insert or update operation into view
      "<view-name>" is not allowed because no target table can be
      determined for a row. Reason code = "<reason-code>".

Explanation: 

The specified view contains a UNION ALL query. The "<reason-code>"
indicates that a given row either:

1. does not satisfy the check constraint of any underlying base table,
   or
2. satisfies all the check constraints for more than one underlying base
   table.

Federated system users: Some other data source specific limitation may
be preventing the row from being inserted.

User response: 

Ensure that the check constraints used by the underlying base tables to
partition their rowsets cover the set of rows to be inserted. Also,
ensure that for a view defined with UNION ALL for its fullselect that
WITH ROW MOVEMENT is also specified if updates are to move rows from one
underlying table to another. For example, given the check constraints
(T1.c1 in (1,2)) on T1, and (T2.c1 in (2,3)) on T2, and view V1 as a
union of T1 and T2,

1. the row c1 = 4 does not satisfy the check constraints of either
   underlying base table and
2. the row c1 = 2 satisfies the check constraints of both underlying
   base tables.

Federated system users: If the reason is unknown, isolate the problem to
the data source failing the request and examine the object definition
and the update restrictions for that data source.

sqlcode: -20154

sqlstate: 23513


   Related information:
   Troubleshooting data source connection errors
