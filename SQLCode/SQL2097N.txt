

SQL2097N  The activity cannot be mapped to the service subclass you
      specified because at least one of the input parameters to the
      routine WLM_REMAP_ACTIVITY is not valid. Reason code =
      "<reason-code>".

Explanation: 

The routine WLM_REMAP_ACTIVITY failed because one or more of the input
parameters is not valid. The reason codes are as follows:

1        

         The activity can be remapped only to a service subclass under
         the service superclass of the activity. Specify the service
         superclass name of the activity in the service_superclass_name
         parameter or set it to null. Setting the
         service_superclass_name parameter to null defaults the input
         parameter to the current service superclass name of the
         activity.


2        

         A valid service subclass under the service superclass of the
         activity must be specified in the service_subclass_name
         parameter.


3        

         Specify Y in the log_evmon_record parameter to log an event
         monitor record to the THRESHOLD VIOLATIONS event monitor when
         the activity is remapped on a partition. Specify N in the
         log_evmon_record parameter to prevent logging an event monitor
         record to the THRESHOLD VIOLATION event monitor when the
         activity is remapped on a partition.

User response: 

Ensure that the condition in the reason code is satisfied and reinvoke
the WLM_REMAP_ACTIVITY routine.

sqlcode: -2097

sqlstate: 5U046
