

SQL4977N  Dropped table export directory "<directory>" is not valid.

Explanation: 

The export directory path specified on the ROLLFORWARD command is not
valid. The export directory path must be a directory in a file system.
This directory must be accessible by the instance owner id.

User response: 

Resubmit the command with a valid export directory path.
