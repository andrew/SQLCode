

SQL1138W  The unique index "<name>" was migrated to support deferred
      uniqueness checking. A new index was not created.

Explanation: 

A CREATE INDEX operation was attempted on an existing index. Since the
index had not yet been migrated to support deferred uniqueness checking,
this migration was performed.

The migrated format of the unique index will allow multiple row updates
to check the uniqueness of the columns of the index at the end of the
update statement instead of when each row is updated.

User response: 

No action is required.

 sqlcode: +1138

 sqlstate: 01550
