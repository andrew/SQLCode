

SQL2445N  The db2convert command failed because no tables satisfy the
      matching criteria specified.

Explanation: 

You can convert row-organized tables into column-organized tables by
using the db2convert command.

You can control which tables to convert by specifying matching criteria
with db2convert parameters:

*  All tables in the database
*  Tables created by a specific user (-u parameter)
*  Tables in a specified schema (-z parameter)
*  One specified table (-t parameter)

For example, the following command converts all row-organized tables in
database db1 that satisfy the matching criteria "owned by user1 and in
schemaA":

db2convert -d db1 -u user1 -z schemaA

This message is returned when no tables satisfy the matching criteria
that were specified with the db2convert command.

User response: 

To convert row-organized tables into column-organized tables, call the
db2convert command specifying criteria that match one or more tables in
the database.


   Related information:
   db2convert - Convert row-organized tables into column-organized
   tables
