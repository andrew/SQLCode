

SQL0540N  The definition of table "<table-name>" is incomplete because
      it lacks a primary index or a required unique index.

Explanation: 

The table named was defined with a PRIMARY KEY clause or a UNIQUE
clause. Its definition is incomplete, and it cannot be used until a
unique index is defined for the primary key (the primary index) and for
each set of columns in any UNIQUE clause (the required unique indexes).
An attempt was made to use the table in a FOREIGN KEY clause or in an
SQL manipulative statement.

The statement cannot be executed.

User response: 

Define a primary index or a required unique index on the table before
referencing it.

 sqlcode: -540

 sqlstate: 57001
