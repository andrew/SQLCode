

SQL16259N  Invalid many-to-many mappings detected in XML schema document
      "<uri1>" near line "<lineno1>" and in XML schema document "<uri2>"
      near line "<lineno2>".

Explanation: 

The two identified annotations specify an invalid many-to-many
relationship between elements that map to the same rowset. If two
element declarations have a sequence model group as their lowest common
ancestor then only one of the paths from the element declaration up to
that model group can have maxOccurs>1 on one or more elements
declarations or modelgroups in that path. The XML schema documents can
be determined by matching "<uri1>" and "<uri2>" to the SCHEMALOCATION
column of catalog view SYSCAT.XSROBJECTCOMPONENTS.

The XML schema is not enabled for decomposition.

User response: 

Correct the annotations such that there are no many-to-many mappings.
Consult annotated XML schema documentation for rules on mapping elements
and attributes.

sqlcode: -16259

sqlstate: 225DE
