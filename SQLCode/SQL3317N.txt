

SQL3317N  The string pointed to by the filetmod parameter contains
      conflicting information.

Explanation: 

The filetmod string defines the generation and product family for the
output file. More than one generation or product family was defined in
the string.

The utility stops processing. The output file is not created.

User response: 

Change the filetmod string to define only one generation and product
family. Resubmit the command.
