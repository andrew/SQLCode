

SQL0973N  Not enough storage is available in the "<heap-name>" heap or
      stack to process the statement.

Explanation: 

All available memory for this heap has been used.

The statement cannot be processed.

User response: 

Terminate the application on receipt of this message (SQLCODE). Modify
the "<heap-name>" configuration parameter to increase the heap or stack
size.

If the "<heap-name>" configuration parameter is set to AUTOMATIC, you
will need to increase the APPL_MEMORY database configuration setting,
the DATABASE_MEMORY database configuration settings, or the
INSTANCE_MEMORY database manager configuration setting. Otherwise,
modify the "<heap-name>" configuration parameter value to increase the
heap size.

The package cache and catalog cache have soft limits. For these you will
need to increase the DATABASE_MEMORY database configuration setting or
the INSTANCE_MEMORY database manager configuration setting.

When modifying the DATABASE_MEMORY database configuration parameter,
setting the parameter to AUTOMATIC will tell the database manager to
manage database memory automatically.

For the application shared heap size, you will need to increase either
the APPL_MEMORY database configuration setting, or the INSTANCE_MEMORY
database manager configuration setting.

When updating configuration parameters, it is recommended to change them
by 10% of the current size at a time until the error condition is
resolved.

For example, if "<heap-name>" is UTIL_HEAP_SZ and the database name is
TORDB1, to update this database configuration parameter to 10000, issue
the following command:

  db2 update db cfg
  for TORDB1
  using UTIL_HEAP_SZ 10000

To view a list of the database configuration parameters, use the GET
DATABASE CONFIGURATION command.

To update a database manager configuration parameter, say MON_HEAP_SZ,
to a new size of 100, issue the following command:

  db2 update dbm cfg
  using MON_HEAP_SZ 100

To view a list of the database manager configuration parameters, use the
GET DATABASE MANAGER CONFIGURATION command.

If all associated configuation parameters are set to either AUTOMATIC or
COMPUTED, then the memory demands of the instance exceed the amount of
memory configured on the machine. Possible solutions include reducing
the database workload, enabling the connection concentrator feature, or
adding additional memory to the machine.

sqlcode: -973

sqlstate: 57011


   Related information:
   database_memory - Database shared memory size configuration parameter
   instance_memory - Instance memory configuration parameter
