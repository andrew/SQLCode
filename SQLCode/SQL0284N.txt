

SQL0284N  Table creation failed because the table space
      "<tablespace-name>" that was specified in the statement after the
      clause "<clause>" is not a supported type of table space for that
      clause. Table space type: "<tablespace-type>".

Explanation: 

This message is returned when an attempt is made to create a table with
properties that are not supported with the type of table space in which
the table is being created.

This message can be returned when the following types of SQL statements
are executed:

*  CREATE TABLE
*  CREATE GLOBAL TEMPORARY TABLE
*  DECLARE GLOBAL TEMPORARY TABLE

Examples of the kinds of incompatibilities that can cause this message
to be returned include the following situations:

*  An attempt was made to create a regular table in a table space that
   is not a REGULAR or LARGE table space.
*  An attempt was made to create or declare a temporary table in a table
   space that is not a USER TEMPORARY table space.
*  An attempt was made to create an insert time clustering (ITC) table
   in a table space that is not a managed by database table space.
*  An attempt was made to create a column-organized table in a table
   space that is not defined as MANAGED BY AUTOMATIC STORAGE and does
   not have an associated storage group.

User response: 

Correct the statement to specify a table space with the correct type for
the "<clause>" clause.

sqlcode: -284

sqlstate: 42838


   Related information:
   DECLARE GLOBAL TEMPORARY TABLE statement
   CREATE GLOBAL TEMPORARY TABLE statement
   CREATE TABLE statement
