

SQL2501C  The database was restored but the data in the restored
      database was unusable.

Explanation: 

The RESTORE utility either could not read data from the restored
database or only a portion of the database was restored. Both cases
indicate that the restored database is not usable.

The database is unusable and the RESTORE utility stops processing.

User response: 

Resubmit the RESTORE command.
