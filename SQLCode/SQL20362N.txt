

SQL20362N  Attribute "<attribute-name>" with value "<value>" cannot be
      dropped or altered because it is not part of the definition of
      trusted context "<context-name>".

Explanation: 

Attribute "<attribute-name>" was specified for a trusted context, but
the trusted context is not defined with an attribute with this name. The
statement could not be processed.

User response: 

Remove the name of the unsupported attribute and re-issue the statement.

sqlcode: -20362

sqlstate: 4274C
