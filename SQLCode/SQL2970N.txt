

SQL2970N  Database "<db-name>" uses node "<node-name>", but the utility
      cannot find the node in the node directory.

Explanation: 

The database directory contains an entry for the specified database and
the entry specifies a node that does not exist.

User response: 

Define the node or modify the entry in the database directory to specify
an existing node.
