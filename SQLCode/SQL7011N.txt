

SQL7011N  Required parameter "<parameter>" not given.

Explanation: 

The parameter "<parameter>" is required by REXX command syntax but was
not specified.

The command cannot be processed.

User response: 

Specify the required parameter value, and run the procedure again.
