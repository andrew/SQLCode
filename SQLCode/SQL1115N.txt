

SQL1115N  The number of sqlvars in the output SQLDA was changed from
      "<count-1>" to "<count-2>".

Explanation: 

The remote procedure changed the sqld field in the output SQLDA; sqld is
the number of used sqlvars in the SQLDA.

The stored procedure does not return any data.

User response: 

Correct the remote stored procedure so the sqld field in the output
SQLDA is not changed.

 sqlcode: -1115

 sqlstate: 39502
