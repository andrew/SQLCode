

SQL1276N  Database "<name>" cannot be brought out of rollforward pending
      state until roll-forward has passed a point in time greater than
      or equal to "<timestamp>", because node "<node-number>" contains
      information later than the specified time.

Explanation: 

A request was made to bring the database or subset of table spaces out
of rollforward pending state by specifying caller action
SQLUM_ROLLFWD_STOP, SQLUM_STOP, SQLUM_ROLLFWD_COMPLETE, or
SQLUM_COMPLETE. However, the database or at least one of the table
spaces to be rolled forward was backed up online. The request cannot be
granted until the database or all table spaces have been rolled forward
to the end of the online backup timestamp on the specified node.

This error can also occur if not all the log files are provided to
perform the requested recovery.

(Note : if you are using a partitioned database server, the node number
indicates which node the error occurred on. Otherwise, it is not
pertinent and should be ignored.)

User response: 

If the stoptime specified on the ROLLFORWARD command is smaller then
"<timestamp>", resubmit the command with a stoptime greater than or
equal to "<timestamp>".

Verify that all the log files were provided. the ROLLFORWARD QUERY
STATUS command shows which log file is to be processed next. Some of the
reasons for missing log files include:

*  the log path has changed. The file can be found in the old log path.
*  DB2 is unable to find the log file from the archive location
   reflected by the current LOGARCHMETH1 or LOGARCHMETH2 database
   configuration parameters.

If the missing log file is found, copy it to the log path and resubmit
the command.
