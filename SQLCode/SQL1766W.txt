

SQL1766W  The command completed successfully. However, LOGINDEXBUILD was
      not enabled before HADR was started.

Explanation: 

If the database configuration parameter LOGINDEXBUILD is not set to ON
before HADR is started, any index creation, recreate, or reorganization
on the current or future primary database server may not be recovered on
the current or future secondary database server using HADR.

User response: 

To enable full logging, set the database configuration parameter
LOGINDEXBUILD to ON.
