

SQL2957N  The ingest operation failed to restart because the ingest
      utility could not find the restart log table. Restart log table
      name: "<table-name>".

Explanation: 

You can stream data from files and pipes into DB2 database tables by
using the ingest utility. If the ingest utility fails before completing,
you can restart the ingest operation from the last commit point. To make
an ingest operation restartable, a restart log table must be created
before the ingest operation is initiated.

This message is returned when an attempt is made to restart the ingest
utility but the ingest utility could not find the restart log table.

User response: 

*  To run the ingest operation as not restartable, reissue the command
   specifying RESTART OFF.
*  To perform a restartable ingest operation, create the restart log
   table and then run the ingest utility again


   Related information:
   db2Ingest API- Ingest data from an input file or pipe into a DB2
   table.
   INGEST command
   Creating the restart table
   Restarting a failed ingest operation
