

SQL2073N  DATALINK processing failed because of internal problems at the
      database server or DB2 Data Links Manager.

Explanation: 

An unexpected error occurred while processing DATALINK values.

User response: 

Resubmit the command. If the problem still exists, resubmit the command
after shutdown and restart of DB2 and the DB2 Data Links Managers.

The Restore utility can avoid DATALINK processing by specifying WITHOUT
DATALINK.
