

SQL20113N  Null cannot be returned from method "<method-id>" defined
      with SELF AS RESULT.

Explanation: 

The method with method identifier "<method-id>" is defined with SELF AS
RESULT. The invocation of the method used a non-null instance of a
structured type so the method cannot return a null instance.

User response: 

Change the method implementation to ensure that a null value is not
returned as the return value for the method. One possibility is to set
all the attributes of the returned structured type to null value. To
determine the name of the method that failed, use the following query: 

SELECT FUNCSCHEMA, FUNCNAME,
       SPECIFICNAME
  FROM SYSCAT.FUNCTIONS
  WHERE FUNCID = method-id

 sqlcode: -20113

 sqlstate: 22004
