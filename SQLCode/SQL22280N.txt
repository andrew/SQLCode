

SQL22280N  This action cannot be performed because the scheduler is
      quiesced.

Explanation: 

No action can be performed until the scheduler is activated again.

User response: 

Activate the scheduler.
