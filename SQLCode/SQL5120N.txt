

SQL5120N  Old and new log parameters cannot both be modified at the same
      time.

Explanation: 

The user is attempting to modify both the previous log parameters and
the new parameters. The application should only support the parameters
of the current release.

The request is denied.

User response: 

Modify only the parameters of the current release and retry the command.
