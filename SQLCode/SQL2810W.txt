

SQL2810W  Node: "<node>" changed in instance: "<instance>" {Host:
      "<host-name>" Machine: "<machine-name>" Port: "<port-num>"}

Explanation: 

The DB2NCHG processing has completed successfully.

User response: 

No further action is required.
