

SQL6569I  The AutoLoader is now issuing all split requests.

Explanation: 

The AutoLoader is now issuing the split operation on each of the target
split partitions.

User response: 

This is an informational message.
